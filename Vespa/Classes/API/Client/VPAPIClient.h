//
//  VPAPIClient.h
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VPAccountAPI.h"
#import "VPActivityAPI.h"
#import "VPClientAPI.h"
#import "VPCommentAPI.h"
#import "VPGroupAPI.h"
#import "VPMeApi.h"
#import "VPMediaAPI.h"
#import "VPPostAPI.h"
#import "VPThreadAPI.h"

@class VPGeo;

/**
 VPAPIClient is a wrapper and aggreate for all kind of apis.
 **/
@interface VPAPIClient : NSObject <VPAccountAPI, VPActivityAPI, VPClientAPI, VPCommentAPI, VPGroupAPI, VPMeAPI, VPMediaAPI, VPPostAPI, VPThreadAPI>

+ (instancetype)sharedInstance;

- (void)initSession: (NSString *)token;
- (void)setGeo: (VPGeo *)geo;

@end

