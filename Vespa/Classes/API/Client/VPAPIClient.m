//
//  VPAPIClient.m
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPAPIClient.h"

#import "VPAccount.h"
#import "VPAccountRestAPI.h"
#import "VPActivityRestAPI.h"
#import "VPClientRestAPI.h"
#import "VPCommentRestAPI.h"
#import "VPConstants.h"
#import "VPGroupRestAPI.h"
#import "VPHTTPClientManager.h"
#import "VPMediaRestAPI.h"
#import "VPMeRestAPI.h"
#import "VPPostRestAPI.h"
#import "VPThreadRestAPI.h"


@interface VPAPIClient ()
@property (nonatomic, strong, readonly) id<VPAccountAPI> accountAPI;
@property (nonatomic, strong, readonly) id<VPActivityAPI> activityAPI;
@property (nonatomic, strong, readonly) id<VPClientAPI> clientAPI;
@property (nonatomic, strong, readonly) id<VPCommentAPI> commentAPI;
@property (nonatomic, strong, readonly) id<VPGroupAPI> groupAPI;
@property (nonatomic, strong, readonly) id<VPMeAPI> meAPI;
@property (nonatomic, strong, readonly) id<VPMediaAPI> mediaAPI;
@property (nonatomic, strong, readonly) id<VPPostAPI> postAPI;
@property (nonatomic, strong, readonly) id<VPThreadAPI> threadAPI;
@end

@implementation VPAPIClient

#pragma mark - Setup initialize
+ (instancetype)sharedInstance
{
    static VPAPIClient *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}

- (void)initSession: (NSString *)token
{
    if (token) {
        VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
        [httpClient setSession:token];
    }
}

- (void)setGeo: (VPGeo *)geo
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setGeo:geo];
}

- (void)setup
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    VPAccount *me = (VPAccount *)[defaults customObjectForKey:kVPUDMe];
    [self initSession:me.token];
    
    //in case we are not able to get geo, we will use pre-stored one
    VPGeo *geo = (VPGeo *)[defaults customObjectForKey:kVPUDLocation];
    [self setGeo:geo];
    
    _accountAPI = [VPAccountRestAPI sharedInstance];
    _activityAPI = [VPActivityRestAPI sharedInstance];
    _clientAPI = [VPClientRestAPI sharedInstance];
    _commentAPI = [VPCommentRestAPI sharedInstance];
    _groupAPI = [VPGroupRestAPI sharedInstance];
    _meAPI = [VPMeRestAPI sharedInstance];
    _mediaAPI = [VPMediaRestAPI sharedInstance];
    _postAPI = [VPPostRestAPI sharedInstance];
    _threadAPI = [VPThreadRestAPI sharedInstance];
}


#pragma mark - Account API
- (NSOperation *)signupWithDeviceId:(NSString *)deviceId
                        deviceModel:(NSString *)deviceModel
                            success:(void (^)(VPAccount *))success
                            failure:(void (^)(NSError *))failure
{
    return [_accountAPI signupWithDeviceId:deviceId
                               deviceModel:deviceModel
                                   success:success
                                   failure:failure];
}


#pragma mark - Activity API
- (NSOperation *)fetchActivitiesWithParam: (NSDictionary *)params
                                  success: (void (^)(VPActivityList *activityList))success
                                  failure: (void (^)(NSError *error))failure
{
    return [_activityAPI fetchActivitiesWithParam:params success:success failure:failure];
}

- (NSOperation *)fetchActivitiesWithUrl: (NSString *)urlStr
                                success: (void (^)(VPActivityList *activityList))success
                                failure: (void (^)(NSError *error))failure
{
    return [_activityAPI fetchActivitiesWithUrl:urlStr success:success failure:failure];
}

- (NSOperation *)ackActivityWithId:(NSNumber *)activityId success:(void (^)(void))success failure:(void (^)(NSError *))failure
{
    return [_activityAPI ackActivityWithId:activityId success:success failure:failure];
}


#pragma mark - Client API
- (NSOperation *)drawCardWithId:(NSString *)deckId
                      isRequest:(BOOL)isRequest
                        success:(void (^)(NSString *deckId, NSArray *deck))success
                        failure:(void (^)(NSError *error))failure
{
    return [_clientAPI drawCardWithId:deckId isRequest:isRequest success:success failure:failure];
}

- (NSOperation *)getInitDataWithSuccess:(void (^)(NSDictionary *initData))success
                                failure:(void (^)(NSError *))failure
{
    return [_clientAPI getInitDataWithSuccess:success
                                      failure:failure];
}

- (NSOperation *)reportFeedback:(NSString *)feedback
                        success:(void (^)(void))success
                        failure:(void (^)(NSError *error))failure
{
    return [_clientAPI reportFeedback:feedback success:success failure:failure];
}

- (NSOperation *) reportInviteWithType:(NSString *)type
                               success:(void (^)(void))success
                               failure:(void (^)(NSError *error))failure
{
    return [_clientAPI reportInviteWithType:type success:success failure:failure];
}

- (NSOperation *)reportLat:(NSNumber *)lat lng:(NSNumber *)lng isFirst:(BOOL)isFirst success:(void (^)(void))success failure:(void (^)(NSError *))failure
{
    return [_clientAPI reportLat:lat lng:lng isFirst:isFirst success:success failure:failure];
}

- (NSOperation *) reportNotificationToken:(NSString *)token
                                  success:(void (^)(void))success
                                  failure:(void (^)(NSError *))failure
{
    return [_clientAPI reportNotificationToken:token success:success failure:failure];
}

- (NSOperation *) reportShareWithPostId:(NSNumber *)postId
                                   type:(NSString *)type
                                success:(void (^)(void))success
                                failure:(void (^)(NSError *error))failure
{
    return [_clientAPI reportShareWithPostId:postId type:type success:success failure:failure];
}

- (NSOperation *)reverseGeo:(CLLocation *)location
                    success:(void (^)(VPGeo *geo))success
                    failure:(void (^)(NSError *error))failure
{
    return [_clientAPI reverseGeo:location success:success failure:failure];
}


#pragma mark - Comment API
- (NSOperation *)createCommentWithType: (NSString *)type
                                postId: (NSNumber *)postId
                       parentCommentId: (NSNumber *)parentCommentId
                                  body: (NSString *)body
                                   geo: (VPGeo *)geo
               photoPickerOptionItemId: (NSNumber *)itemId
                                 photo: (NSData *)photoData
                             photoSize: (NSInteger)photoSize
                           photoHeight: (NSInteger)photoHeight
                            photoWidth: (NSInteger)photoWidth
                           photoFormat: (NSString *)photoFormat
                               success: (void (^)(VPComment *comment))success
                               failure: (void (^)(NSError *error))failure
{
    return [_commentAPI createCommentWithType:type
                                       postId:postId
                                parentCommentId:parentCommentId
                                           body:body
                                            geo:geo
                      photoPickerOptionItemId:itemId
                                          photo:photoData
                                      photoSize:photoSize
                                    photoHeight:photoHeight
                                     photoWidth:photoWidth
                                    photoFormat:photoFormat
                                        success:success
                                        failure:failure];
}

- (NSOperation *)fetchCommentsWithParam: (NSDictionary *)params
                                success: (void (^)(VPCommentList *commentList))success
                                failure: (void (^)(NSError *error))failure
{
    return [_commentAPI fetchCommentsWithParam:params
                                    success:success
                                    failure:failure];
}

- (NSOperation *)fetchCommentsWithUrl: (NSString *)urlStr
                              success: (void (^)(VPCommentList *commentList))success
                              failure: (void (^)(NSError *error))failure
{
    return [_commentAPI fetchCommentsWithUrl:urlStr
                                  success:success
                                  failure:failure];
}

- (NSOperation *)fetchCommentPhotoPickerOptionItemsWithUrl: (NSString *)urlStr
                                                  optionId: (NSNumber *)optionId
                                                   success: (void (^)(VPCommentPhotoPickerOptionItemList *itemList))success
                                                   failure: (void (^)(NSError *error))failure
{
    return [_commentAPI fetchCommentPhotoPickerOptionItemsWithUrl:urlStr optionId:optionId success:success failure:failure];
}

- (NSOperation *)fetchCommentPhotoPickerOptionsWithParam: (NSDictionary *)params
                                                 success: (void (^)(NSArray *options))success
                                                 failure: (void (^)(NSError *error))failure
{
    return [_commentAPI fetchCommentPhotoPickerOptionsWithParam:params success:success failure:failure];
}

- (NSOperation *)voteCommentWithId:(NSNumber *)commentId down:(BOOL)down up:(BOOL)up undown:(BOOL)undown unup:(BOOL)unup success:(void (^)(void))success failure:(void (^)(NSError *))failure
{
    return [_commentAPI voteCommentWithId:commentId down:down up:up undown:undown unup:unup success:success failure:failure];
}


#pragma mark - Group API
- (NSOperation *)fetchGroupsWithParam:(NSDictionary *)param success:(void (^)(VPGroupList *))success failure:(void (^)(NSError *))failure
{
    return [_groupAPI fetchGroupsWithParam:param success:success failure:failure];
}

- (NSOperation *)fetchGroupsWithUrl:(NSString *)urlStr success:(void (^)(VPGroupList *))success failure:(void (^)(NSError *))failure
{
    return [_groupAPI fetchGroupsWithUrl:urlStr success:success failure:failure];
}

- (NSOperation *)fetchGroupWithId:(NSNumber *)groupId success:(void (^)(VPGroup *))success failure:(void (^)(NSError *))failure
{
    return [_groupAPI fetchGroupWithId:groupId success:success failure:failure];
}


#pragma mark - Me API
- (NSOperation *)fetchMeWithParam: (NSDictionary *)params
                          success: (void (^)(VPAccount *me))success
                          failure: (void (^)(NSError *error))failure
{
    return [_meAPI fetchMeWithParam:params
                            success:success
                            failure:failure];
}

- (NSOperation *)fetchMyPostsWithType: (VPMyPostType)type
                                param: (NSDictionary *)params
                              success: (void (^)(VPPostList *postList))success
                              failure: (void (^)(NSError *error))failure
{
    return [_meAPI fetchMyPostsWithType:type
                                  param:params
                                success:success
                                failure:failure];
}

- (NSOperation *)fetchMyPostsWithUrl: (NSString *)url
                             success: (void (^)(VPPostList *postList))success
                             failure: (void (^)(NSError *error))failure
{
    return [_meAPI fetchMyPostsWithUrl:url
                               success:success
                               failure:failure];
}

- (NSOperation *)getMyStatusWithSuccess: (void (^)(VPStatus *status))success
                                failure: (void (^)(NSError *error))failure
{
    return [_meAPI getMyStatusWithSuccess:success failure:failure];
}

- (NSOperation *)updateProfileWithAvatar: (NSString *)avatarUrl
                                   phone: (NSString *)phoneNum
                              screenName: (NSString *)screenName
                                 success: (void (^)(void))success
                                 failure: (void (^)(NSError *error))failure
{
    return [_meAPI updateProfileWithAvatar:avatarUrl
                                     phone:phoneNum
                                screenName:screenName
                                   success:success
                                   failure:failure];
}


#pragma mark - Media API
- (NSOperation *)uploadMediasWithArray: (NSArray *)medias
                               success: (void (^)(NSArray *medias))success
                               failure: (void (^)(NSError *error))failure
{
    return [_mediaAPI uploadMediasWithArray:medias
                                        success:success
                                        failure:failure];
}

- (NSOperation *)uploadMediaWithKey: (NSString *)key
                               data: (NSData *)data
                           mimeType: (NSString *)type
                            success: (void (^)(NSDictionary *media))success
                            failure: (void (^)(NSError *error))failure
{
    return [_mediaAPI uploadMediaWithKey:key
                                        data:data
                                    mimeType:type
                                     success:success
                                     failure:failure];
}


#pragma mark - Post API
- (NSOperation *)createFlagWithPostId: (NSNumber *)postId
                            complaint: (NSString *)complaint
                              success: (void (^)(void))success
                              failure: (void (^)(NSError *error))failure
{
    return [_postAPI createFlagWithPostId:postId complaint:complaint success:success failure:failure];
}

- (NSOperation *)createPostWithType: (NSString *)type
                               body: (NSString *)body
                                geo: (VPGeo *)geo
                            groupId: (NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                              audio: (NSData *)audioData
                      audioDuration: (NSInteger)audioDurationInMS
                          audioSize: (NSUInteger)audioSize
                        audioFormat: (NSString *)audioFormat
                              photo: (NSData *)photoData
                          photoSize: (NSUInteger)photoSize
                        photoHeight: (NSInteger)photoHeight
                         photoWidth: (NSInteger)photoWidth
                        photoFormat: (NSString *)photoFormat
                      photoUploaded: (BOOL)photoUploaded
                               text: (NSString *)textData
                textBackgroundColor: (NSString *)textBackgroundColorHex
                          textColor: (NSString *)textColorHex
                         textFormat: (NSString *)textFormat
                               link: (NSString *)linkData
                          linkTitle: (NSString *)linkTitle
                       linkEmbedUrl: (NSString *)linkEmbedUrl
                    linkEmbedHeight: (NSInteger)linkEmbedHeight
                     linkEmbedWidth: (NSInteger)linkEmbedWidth
                    linkEmbedFormat: (NSString *)linkEmbedFormat
                              video: (NSData *)videoData
                          videoSize: (NSUInteger)videoSize
                        videoFormat: (NSString *)videoFormat
                            success: (void (^)(VPPost *))success
                            failure: (void (^)(NSError *error))failure;
{
    return [_postAPI createPostWithType:type
                                   body:body
                                    geo:geo
                                groupId:groupId
                       createThreadType:createThreadType
                               threadId:threadId
                                  audio:audioData
                          audioDuration:audioDurationInMS
                              audioSize:audioSize
                            audioFormat:audioFormat
                                  photo:photoData
                              photoSize:photoSize
                            photoHeight:photoHeight
                             photoWidth:photoWidth
                            photoFormat:photoFormat
                          photoUploaded:photoUploaded
                                   text:textData
                    textBackgroundColor:textBackgroundColorHex
                              textColor:textColorHex
                             textFormat:textFormat
                                   link:linkData
                              linkTitle:linkTitle
                           linkEmbedUrl:linkEmbedUrl
                        linkEmbedHeight:linkEmbedHeight
                         linkEmbedWidth:linkEmbedWidth
                        linkEmbedFormat:linkEmbedFormat
                                  video:videoData
                              videoSize:videoSize
                            videoFormat:videoFormat
                                success:success
                                failure:failure];
}

- (NSOperation *)deletePostWithId: (NSNumber *)postId
                          success: (void(^)(void))success
                          failure: (void(^)(NSError *error))failure
{
    return [_postAPI deletePostWithId:postId
                              success:success
                              failure:failure];
}

- (NSOperation *)fetchAnnouncementPostsWithParam:(NSDictionary *)param success:(void (^)(VPPostList *))success failure:(void (^)(NSError *))failure
{
    return [_postAPI fetchAnnouncementPostsWithParam:param success:success failure:failure];
}

- (NSOperation *)fetchFeaturedPostsWithParam:(NSDictionary *)param success:(void (^)(VPPostList *))success failure:(void (^)(NSError *))failure
{
    return [_postAPI fetchFeaturedPostsWithParam:param success:success failure:failure];
}

- (NSOperation *)fetchPostWithId: (NSNumber *)postId
                         success: (void (^)(VPPost *post))success
                         failure: (void (^)(NSError *error))failure
{
    return [_postAPI fetchPostWithId:postId
                             success:success
                             failure:failure];
}

- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             success:(void (^)(VPPostList *postList))success
                             failure:(void (^)(NSError *errpr))failure
{
    return [_postAPI fetchPostsWithParam:params success:success failure:failure];
}

- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             groupId:(NSNumber *)groupId
                             success:(void (^)(VPPostList *))success
                             failure:(void (^)(NSError *))failure
{
    return [_postAPI fetchPostsWithParam:params groupId:groupId success:success failure:failure];
}

- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             hashtag:(NSString *)hashtag
                             success:(void (^)(VPPostList *))success
                             failure:(void (^)(NSError *))failure
{
    return [_postAPI fetchPostsWithParam:params hashtag:hashtag success:success failure:failure];
}

- (NSOperation *)fetchPostsWithUrl: (NSString *)url
                           success: (void (^)(VPPostList *postList))success
                           failure: (void (^)(NSError *error))failure
{
    return [_postAPI fetchPostsWithUrl:url success:success failure:failure];
}

- (NSOperation *)likePostWithId: (NSNumber *)postId
                         status: (BOOL)status
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure
{
    return [_postAPI likePostWithId:postId
                             status:status
                            success:success
                            failure:failure];
}

- (NSOperation *)starPostWithId:(NSNumber *)postId
                            num:(NSUInteger)num
                        success:(void (^)(void))success
                        failure:(void (^)(NSError *))failure
{
    return [_postAPI starPostWithId:postId num:num success:success failure:failure];
}

- (NSOperation *)votePostWithId: (NSNumber *)postId
                           down: (BOOL)down
                             up: (BOOL)up
                         undown: (BOOL)undown
                           unup: (BOOL)unup
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure;
{
    return [_postAPI votePostWithId:postId down:down up:up undown:undown unup:unup success:success failure:failure];
}


#pragma mark - Thread API
- (NSOperation *)fetchRootPostWithId:(NSNumber *)threadId success:(void (^)(VPPost *))success failure:(void (^)(NSError *))failure
{
    return [_threadAPI fetchRootPostWithId:threadId success:success failure:failure];
}

- (NSOperation *)fetchThreadWithId:(NSNumber *)threadId success:(void (^)(VPPostList *))success failure:(void (^)(NSError *))failure
{
    return [_threadAPI fetchThreadWithId:threadId success:success failure:failure];
}

- (NSOperation *)performThreadActionWithId:(NSNumber *)threadId actionKey:(NSString *)actionKey actionVal:(NSString *)actionVal success:(void (^)(void))success failure:(void (^)(NSError *))failure
{
    return [_threadAPI performThreadActionWithId:threadId actionKey:actionKey actionVal:actionVal success:success failure:failure];
}

@end
