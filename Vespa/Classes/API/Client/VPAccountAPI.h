//
//  VPUserAPI.h
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@class VPAccount;


@protocol VPAccountAPI <NSObject>

- (NSOperation *)signupWithDeviceId: (NSString *)deviceId
                        deviceModel: (NSString *)deviceModel
                            success: (void (^)(VPAccount *account))success
                            failure: (void (^)(NSError *error))failure;

@end
