//
//  VPUserRestAPI.h
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VPAccountAPI.h"

@interface VPAccountRestAPI : NSObject <VPAccountAPI>

+ (instancetype)sharedInstance;

@end
