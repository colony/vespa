//
//  VPUserRestAPI.m
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPAccountRestAPI.h"

#import "NSString+Util.h"
#import "VPAccount.h"
#import "VPError.h"
#import "VPHTTPClientManager.h"
#import "VPRestClientManager.h"

static NSString *kBindPath = @"accounts/bind";
static NSString *kSignupPath = @"accounts/signup";
static NSString *kLoginPath =  @"accounts/login";

@implementation VPAccountRestAPI

+ (instancetype)sharedInstance
{
    static VPAccountRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}


#pragma mark - setup
- (void)setup
{
    RKObjectManager *manager = [VPRestClientManager sharedClient];
    
    //object mapping
    RKMapping *accountMapping = [VPAccount getModelMapping];
    RKMapping *accountInverseMapping = [VPAccount getModelInverseMapping];

    //signup
    RKRequestDescriptor *signupRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:accountInverseMapping
                                                                                         objectClass:[VPAccount class]
                                                                                          rootKeyPath:nil
                                                                                                method:RKRequestMethodPOST];
    RKResponseDescriptor *signupResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:accountMapping
                                                                                                  method:RKRequestMethodPOST
                                                                                             pathPattern:kSignupPath
                                                                                                 keyPath:nil
                                                                                             statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addRequestDescriptor:signupRequestDescriptor];
    [manager addResponseDescriptor:signupResponseDescriptor];
    
    //login
    RKResponseDescriptor *loginResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:accountMapping
                                                                                                 method:RKRequestMethodPOST
                                                                                            pathPattern:kLoginPath
                                                                                                keyPath:nil
                                                                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:loginResponseDescriptor];
    
}


#pragma mark - API implementation
- (NSOperation *)signupWithDeviceId: (NSString *)deviceId
                        deviceModel: (NSString *)deviceModel
                            success: (void (^)(VPAccount *account))success
                            failure: (void (^)(NSError *error))failure
{
//    if ([NSString isEmptyString:deviceId]) {
//        VPLog(@"No idfa found");
//        NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
//        SAFE_BLOCK_RUN(failure, error);
//        return nil;
//    }
    
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (![NSString isEmptyString:deviceId])
        [param setValue:deviceId forKey:@"idfa"];
    if (![NSString isEmptyString:deviceModel])
        [param setValue:deviceModel forKey:@"device_model"];
    
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kBindPath
                                                      parameters:param];
    AFHTTPRequestOperation *operation = [[AFJSONRequestOperation alloc]initWithRequest: request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        VPLog(@"%@", responseObject);
        VPAccount *newAccount = [[VPAccount alloc]init];
        newAccount.userId = responseObject[@"id"];
        newAccount.token = responseObject[@"token"];
        SAFE_BLOCK_RUN(success, newAccount);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        VPLog(@"%@", [error localizedDescription])
        SAFE_BLOCK_RUN(failure, error);
    }];
    
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

@end
