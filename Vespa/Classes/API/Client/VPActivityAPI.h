//
//  VPActivityAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@class VPPaginator;
@class VPActivityList;

@protocol VPActivityAPI <NSObject>

- (NSOperation *)fetchActivitiesWithParam: (NSDictionary *)params
                                  success: (void (^)(VPActivityList *activityList))success
                                  failure: (void (^)(NSError *error))failure;

- (NSOperation *)fetchActivitiesWithUrl: (NSString *)urlStr
                                success: (void (^)(VPActivityList *activityList))success
                                failure: (void (^)(NSError *error))failure;

- (NSOperation *)ackActivityWithId: (NSNumber *)activityId
                           success: (void (^)(void))success
                           failure: (void (^)(NSError *error))failure;

@end