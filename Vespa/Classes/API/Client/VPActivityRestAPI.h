//
//  VPActivityRestAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VPActivityAPI.h"

@interface VPActivityRestAPI : NSObject<VPActivityAPI>

+ (instancetype)sharedInstance;

@end
