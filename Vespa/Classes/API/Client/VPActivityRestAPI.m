//
//  VPActivityRestAPI.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPActivityRestAPI.h"

#import "VPActivity.h"
#import "VPError.h"
#import "VPHttpClientManager.h"
#import "VPPaginator.h"
#import "VPRestClientManager.h"
#import "VPUtil.h"


static NSString * const kGetActivitiesPath = @"activities";
static NSString * const kAckActivityPath = @"activities/:id/ack";


@implementation VPActivityRestAPI

+ (instancetype)sharedInstance
{
    static VPActivityRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}


#pragma mark - setup
- (void)setup
{
    RKObjectManager *manager = [VPRestClientManager sharedClient];
    
    //object mapping
    RKMapping *actListMapping = [VPActivityList getModelMapping];
    
    //fetch posts
    RKResponseDescriptor *fetchActivitiesResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:actListMapping
                                                                                                           method:RKRequestMethodGET
                                                                                                      pathPattern:kGetActivitiesPath
                                                                                                          keyPath:nil
                                                                                                      statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:fetchActivitiesResponseDescriptor];
}


#pragma mark - API implementation

- (NSOperation *)fetchActivitiesWithParam: (NSDictionary *)params
                                  success: (void (^)(VPActivityList *activityList))success
                                  failure: (void (^)(NSError *error))failure;
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kGetActivitiesPath
                                                      parameters:params];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPActivityList *actList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, actList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled)
                                                                                        return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchActivitiesWithUrl: (NSString *)urlStr
                                success: (void (^)(VPActivityList *activityList))success
                                failure: (void (^)(NSError *error))failure;
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [VPUtil requestWithURL:urlStr];
    if (!request) {
        NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
        SAFE_BLOCK_RUN(failure, error);
        return nil;
    }
    
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPActivityList *actList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, actList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled)
                                                                                        return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)ackActivityWithId: (NSNumber *)actId
                           success: (void (^)(void))success
                           failure: (void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:[kAckActivityPath stringByReplacingOccurrencesOfString:@":id" withString:[actId stringValue]]
                                                      parameters:nil];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                            SAFE_BLOCK_RUN(success);
                                                                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            SAFE_BLOCK_RUN(failure, error);
                                                                                        }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

@end
