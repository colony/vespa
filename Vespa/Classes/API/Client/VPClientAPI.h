//
//  VPClientAPI.h
//  Vespa
//
//  API endpoint managing client operation i.e. report device token, app usage, client configuration
//
//  Created by Jiayu Zhang on 11/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@class VPGeo;

@protocol VPClientAPI <NSObject>

- (NSOperation *)drawCardWithId:(NSString *)deckId
                      isRequest:(BOOL)isRequest
                        success:(void (^)(NSString *deckId, NSArray *deck))success
                        failure:(void (^)(NSError *error))failure;

- (NSOperation *)getInitDataWithSuccess:(void (^)(NSDictionary *initData))success
                                 failure:(void (^)(NSError *))failure;

- (NSOperation *)reportFeedback:(NSString *)feedback
                        success:(void (^)(void))success
                        failure:(void (^)(NSError *error))failure;

/**
 * isFirst - indicate if this is first time report the latlng, hint server to update the signup geo info for user
 */
- (NSOperation *)reportLat:(NSNumber *)lat
                       lng:(NSNumber *)lng
                   isFirst:(BOOL)isFirst
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *error))failure;

- (NSOperation *)reportInviteWithType:(NSString *)type
                               success:(void (^)(void))success
                               failure:(void (^)(NSError *error))failure;

- (NSOperation *)reportNotificationToken:(NSString *)token
                                  success:(void (^)(void))success
                                  failure:(void (^)(NSError *error))failure;

- (NSOperation *)reportShareWithPostId:(NSNumber *)postId
                                   type:(NSString *)type
                                success:(void (^)(void))success
                                failure:(void (^)(NSError *error))failure;

- (NSOperation *)reverseGeo:(CLLocation *)location
                    success:(void (^)(VPGeo *geo))success
                    failure:(void (^)(NSError *error))failure;

@end
