//
//  VPClientRestAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 11/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VPClientAPI.h"

@interface VPClientRestAPI : NSObject<VPClientAPI>

+ (instancetype)sharedInstance;

@end
