//
//  VPClientRestAPI.m
//  Vespa
//
//  Created by Jiayu Zhang on 11/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VPClientRestAPI.h"
#import "VPError.h"
#import "VPGeo.h"
#import "VPHTTPClientManager.h"
#import "VPRestClientManager.h"

static NSString *const kDrawCard = @"client/card";
static NSString *const kPathOfClientConfig = @"client/config";
static NSString *const kPathOfInitData = @"client/init";
static NSString *const kReportFeedbackPath = @"client/report/feedback";
static NSString *const kReportGeoPath = @"client/report/geo";
static NSString *const kReportInvitePath = @"client/report/invite";
static NSString *const kReportNotiPath = @"client/report/notification";
static NSString *const kReportSharePath = @"client/report/share";
static NSString *const kReverseGeoPath = @"client/util/reversegeo";

@implementation VPClientRestAPI

+ (instancetype)sharedInstance
{
    static VPClientRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}

- (void)setup
{
    RKObjectManager *manager = [VPRestClientManager sharedClient];
    
    //object mapping
    RKMapping *geoMapping = [VPGeo getModelMapping];
    
    //reverse geo
    RKResponseDescriptor *reverseGeoResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:geoMapping
                                                                                                      method:RKRequestMethodGET
                                                                                                 pathPattern:kReverseGeoPath
                                                                                                     keyPath:nil
                                                                                                 statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:reverseGeoResponseDescriptor];
}

//- (NSOperation *) getClientConfigWithSuccess:(void (^)(NSDictionary *))success
//                                     failure:(void (^)(NSError *))failure
//{
//    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
//                                                            path:kPathOfClientConfig
//                                                      parameters:nil];
//    
//    AFHTTPRequestOperation *operation = [[AFJSONRequestOperation alloc]initWithRequest: request];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id response) {
//        VPLog(@"%@", response);
//        if (operation.response.statusCode == 200 && [response isKindOfClass:[NSDictionary class]]) {
//            SAFE_BLOCK_RUN(success, (NSDictionary *)response);
//        } else {
//            NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
//            VPLog(@"%@", [error localizedDescription]);
//            SAFE_BLOCK_RUN(failure, error);
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        VPLog(@"%@", error);
//        if (operation.isCancelled)
//            return;
//        
//        SAFE_BLOCK_RUN(failure, error);
//    }];
//    [httpClient enqueueHTTPRequestOperation:operation];
//    return operation;
//}

- (NSOperation *)drawCardWithId:(NSString *)deckId
                      isRequest:(BOOL)isRequest
                        success:(void (^)(NSString *deckId, NSArray *deck))success
                        failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (deckId)
        param[@"deck_id"] = deckId;
    param[@"is_request"] = @(isRequest);
    
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                            path:kDrawCard
                                                      parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                            if (response.statusCode == 200) {
                                                                                                NSDictionary *d = (NSDictionary *)JSON;
                                                                                                SAFE_BLOCK_RUN(success, d[@"deck_id"], d[@"deck"]);
                                                                                            } else {
                                                                                                NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
                                                                                                SAFE_BLOCK_RUN(failure, error);
                                                                                            }
                                                                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            SAFE_BLOCK_RUN(failure, error);
                                                                                        }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *) getInitDataWithSuccess:(void (^)(NSDictionary *))success
                                 failure:(void (^)(NSError *))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                            path:kPathOfInitData
                                                      parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFJSONRequestOperation alloc]initWithRequest: request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id response) {
        VPLog(@"%@", response);
        if (operation.response.statusCode == 200 && [response isKindOfClass:[NSDictionary class]]) {
            SAFE_BLOCK_RUN(success, (NSDictionary *)response);
        } else {
            NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
            SAFE_BLOCK_RUN(failure, error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        VPLog(@"%@", error)
        if (operation.isCancelled)
            return;
        
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *)reportFeedback:(NSString *)feedback
                        success:(void (^)(void))success
                        failure:(void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kReportFeedbackPath
                                                      parameters:@{@"feedback":feedback}];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        SAFE_BLOCK_RUN(success);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *) reportInviteWithType:(NSString *)type
                               success:(void (^)(void))success
                               failure:(void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kReportInvitePath
                                                      parameters:@{@"type":type}];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        SAFE_BLOCK_RUN(success);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *)reportLat:(NSNumber *)lat
                       lng:(NSNumber *)lng
                   isFirst:(BOOL)isFirst
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kReportGeoPath
                                                      parameters:@{@"lat":lat, @"lng":lng, @"is_first":@(isFirst)}];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        SAFE_BLOCK_RUN(success);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *) reportNotificationToken:(NSString *)token
                                  success:(void (^)(void))success
                                  failure:(void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kReportNotiPath
                                                      parameters:@{@"token":token}];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        SAFE_BLOCK_RUN(success);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *) reportShareWithPostId:(NSNumber *)postId
                                   type:(NSString *)type
                                success:(void (^)(void))success
                                failure:(void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kReportSharePath
                                                      parameters:@{@"type":type, @"post_id":postId}];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        SAFE_BLOCK_RUN(success);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *)reverseGeo:(CLLocation *)location
                    success:(void (^)(VPGeo *geo))success
                    failure:(void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kReverseGeoPath
                                                      parameters:@{@"lat":@(location.coordinate.latitude),
                                                                   @"lon":@(location.coordinate.longitude)}];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPGeo *geo = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, geo);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

@end
