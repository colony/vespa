//
//  VPCommentAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/20/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@class VPComment;
@class VPCommentList;
@class VPCommentPhotoPickerOptionItemList;
@class VPGeo;
@class VPPaginator;

@protocol VPCommentAPI <NSObject>

- (NSOperation *)createCommentWithType: (NSString *)type
                                postId: (NSNumber *)postId
                         parentCommentId: (NSNumber *)parentCommentId
                                    body: (NSString *)body
                                     geo: (VPGeo *)geo
                 photoPickerOptionItemId: (NSNumber *)itemId
                                   photo: (NSData *)photoData
                               photoSize: (NSInteger)photoSize
                             photoHeight: (NSInteger)height
                              photoWidth: (NSInteger)width
                             photoFormat: (NSString *)format
                                 success: (void (^)(VPComment *comment))success
                                 failure: (void (^)(NSError *error))failure;

/**
 There are two types of parameters. 
 One is used as some sort of hint for underlying implementation, will only exist within client scope
 Another one is used as http parameter that will be sent over to server
 
 post_id - ID of the post whose comments to be fetched (http)
 */
- (NSOperation *)fetchCommentsWithParam: (NSDictionary *)params
                                success: (void (^)(VPCommentList *commentList))success
                                failure: (void (^)(NSError *error))failure;

- (NSOperation *)fetchCommentsWithUrl: (NSString *)urlStr
                              success: (void (^)(VPCommentList *commentList))success
                              failure: (void (^)(NSError *error))failure;

- (NSOperation *)fetchCommentPhotoPickerOptionItemsWithUrl: (NSString *)urlStr
                                                  optionId: (NSNumber *)optionId
                                            success: (void (^)(VPCommentPhotoPickerOptionItemList *itemList))success
                                            failure: (void (^)(NSError *error))failure;

- (NSOperation *)fetchCommentPhotoPickerOptionsWithParam: (NSDictionary *)params
                                                 success: (void (^)(NSArray *options))success
                                                 failure: (void (^)(NSError *error))failure;

- (NSOperation *)voteCommentWithId:(NSNumber *)commentId
                              down: (BOOL)down
                                up: (BOOL)up
                            undown: (BOOL)undown
                              unup: (BOOL)unup
                           success: (void (^)(void))success
                           failure: (void (^)(NSError *error))failure;

@end
