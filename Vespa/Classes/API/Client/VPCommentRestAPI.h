//
//  VPCommentRestAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/20/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentAPI.h"

@interface VPCommentRestAPI : NSObject<VPCommentAPI>

+ (instancetype)sharedInstance;

@end
