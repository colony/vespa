//
//  VPCommentRestAPI.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/20/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentRestAPI.h"

#import "NSData+ImageContentType.h"
#import "VPComment.h"
#import "VPCommentPhotoPickerOption.h"
#import "VPError.h"
#import "VPGeo.h"
#import "VPHTTPClientManager.h"
#import "VPPaginator.h"
#import "VPRestClientManager.h"
#import "VPUtil.h"

static NSString *kCreateCommentPath = @"comments";
static NSString *kGetCommentsPath = @"comments";
static NSString *kGetCommentPhotoPickerOptionListPath = @"comments/photopicker";
static NSString *kGetCommentPhotoPickerOptionPath = @"comments/photopicker/:id";
static NSString *kVoteCommentPath = @"comments/:id/vote";


@implementation VPCommentRestAPI

+ (instancetype)sharedInstance
{
    static VPCommentRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}

#pragma mark - setup
- (void)setup
{
    RKObjectManager *manager = [VPRestClientManager sharedClient];
    
    //object mapping
    RKMapping *commentMapping = [VPComment getModelMapping];
    RKMapping *commentListMapping = [VPCommentList getModelMapping];
    RKMapping *commentPhotoPickerOptionMapping = [VPCommentPhotoPickerOption getModelMapping];
    RKMapping *commentPhotoPickerOptionItemListMapping = [VPCommentPhotoPickerOptionItemList getModelMapping];
    
    //create comment
    RKResponseDescriptor *createCommentResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:commentMapping
                                                                                                         method:RKRequestMethodPOST
                                                                                                    pathPattern:kCreateCommentPath
                                                                                                        keyPath:nil
                                                                                                    statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:createCommentResponseDescriptor];
    
    //fetch comments
    RKResponseDescriptor *fetchCommentsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:commentListMapping
                                                                                                         method:RKRequestMethodGET
                                                                                                    pathPattern:kGetCommentsPath
                                                                                                        keyPath:nil
                                                                                                    statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:fetchCommentsResponseDescriptor];
    
    RKResponseDescriptor *fetchCommentPhotoPickerOptionListResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:commentPhotoPickerOptionMapping
                                                                                                                             method:RKRequestMethodGET
                                                                                                                        pathPattern:kGetCommentPhotoPickerOptionListPath
                                                                                                                            keyPath:nil
                                                                                                                        statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:fetchCommentPhotoPickerOptionListResponseDescriptor];
    
    RKResponseDescriptor *fetchCommentPhotoPickerOptionItemListResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:commentPhotoPickerOptionItemListMapping
                                                                                                                             method:RKRequestMethodGET
                                                                                                                        pathPattern:kGetCommentPhotoPickerOptionPath
                                                                                                                            keyPath:nil
                                                                                                                        statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:fetchCommentPhotoPickerOptionItemListResponseDescriptor];
}


#pragma mark - API implementation
- (NSOperation *)createCommentWithType: (NSString *)type
                                postId: (NSNumber *)postId
                       parentCommentId: (NSNumber *)parentCommentId
                                  body: (NSString *)body
                                   geo: (VPGeo *)geo
               photoPickerOptionItemId: (NSNumber *)itemId
                                 photo: (NSData *)photoData
                             photoSize: (NSInteger)photoSize
                           photoHeight: (NSInteger)photoHeight
                            photoWidth: (NSInteger)photoWidth
                           photoFormat: (NSString *)photoFormat
                               success: (void (^)(VPComment *comment))success
                               failure: (void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                         path:kCreateCommentPath
                                                                   parameters:nil
                                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                        [formData appendPartWithFormData:[body dataUsingEncoding:NSUTF8StringEncoding] name:@"body"];
                                                        [formData appendPartWithFormData:[type dataUsingEncoding:NSUTF8StringEncoding] name:@"type"];
                                                        [formData appendPartWithFormData:[[postId stringValue] dataUsingEncoding:NSUTF8StringEncoding] name:@"post_id"];
                                                        if (parentCommentId) {
                                                            [formData appendPartWithFormData:[[parentCommentId stringValue] dataUsingEncoding:NSUTF8StringEncoding] name:@"parent_id"];
                                                        }
                                                        if (geo) {
                                                            [formData appendPartWithFormData:[[geo toHttpParamValue] dataUsingEncoding:NSUTF8StringEncoding] name:@"geo"];
                                                        }
                                                        if (itemId) {
                                                            [formData appendPartWithFormData:[[itemId stringValue] dataUsingEncoding:NSUTF8StringEncoding] name:@"option_item_id"];
                                                        }
                                                        if (photoData) {
                                                            [formData appendPartWithFileData:photoData name:@"photo_data" fileName:@"photo_data" mimeType:[photoData imageContentType]];
                                                            [self appendForm:formData data:photoFormat name:@"photo_format"];
                                                            [self appendForm:formData data:[@(photoHeight) stringValue] name:@"photo_height"];
                                                            [self appendForm:formData data:[@(photoWidth) stringValue] name:@"photo_width"];
                                                            [self appendForm:formData data:[@(photoSize) stringValue] name:@"photo_size"];
                                                        }
                                                    }];
    
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPComment *comment = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, comment);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    VPLog(@"%@", error);
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}


- (NSOperation *)fetchCommentsWithParam: (NSDictionary *)params
                                success: (void (^)(VPCommentList *commentList))success
                                failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kGetCommentsPath
                                                      parameters:params];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPCommentList *cmtList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, cmtList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled)
                                                                                        return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchCommentsWithUrl: (NSString *)urlStr
                              success: (void (^)(VPCommentList *commentList))success
                              failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [VPUtil requestWithURL:urlStr];
    if (!request) {
        NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
        SAFE_BLOCK_RUN(failure, error);
        return nil;
    }
    
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPCommentList *cmtList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, cmtList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled)
                                                                                        return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
    
}


- (NSOperation *)fetchCommentPhotoPickerOptionItemsWithUrl: (NSString *)urlStr
                                                  optionId: (NSNumber *)optionId
                                                   success: (void (^)(VPCommentPhotoPickerOptionItemList *itemList))success
                                                   failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request;
    if (urlStr) {
        request = [VPUtil requestWithURL:urlStr];
    } else {
        request = [restClient requestWithObject:nil
                                         method:RKRequestMethodGET
                                           path:[kGetCommentPhotoPickerOptionPath stringByReplacingOccurrencesOfString:@":id" withString:[optionId stringValue]]
                                     parameters:nil];
    }
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPCommentPhotoPickerOptionItemList *itemList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, itemList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled)
                                                                                        return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchCommentPhotoPickerOptionsWithParam: (NSDictionary *)params
                                                 success: (void (^)(NSArray *options))success
                                                 failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kGetCommentPhotoPickerOptionListPath
                                                      parameters:params];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    NSArray *optionList = mappingResult.array;
                                                                                    SAFE_BLOCK_RUN(success, optionList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled)
                                                                                        return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)voteCommentWithId:(NSNumber *)commentId
                              down: (BOOL)down
                                up: (BOOL)up
                            undown: (BOOL)undown
                              unup: (BOOL)unup
                           success: (void (^)(void))success
                           failure: (void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:[kVoteCommentPath stringByReplacingOccurrencesOfString:@":id" withString:[commentId stringValue]]
                                                      parameters:@{@"down":@(down),@"up":@(up),@"undown":@(undown),@"unup":@(unup)}];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                            SAFE_BLOCK_RUN(success);
                                                                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            SAFE_BLOCK_RUN(failure, error);
                                                                                        }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

#pragma mark - private
- (void)appendForm:(id<AFMultipartFormData>)form data:(NSString *)data name:(NSString *)name
{
    if (data) {
        [form appendPartWithFormData:[data dataUsingEncoding:NSUTF8StringEncoding] name:name];
    }
}

@end
