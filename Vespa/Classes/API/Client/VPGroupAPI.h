//
//  VPGroupAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 7/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@class VPGroup;
@class VPGroupList;

@protocol VPGroupAPI <NSObject>

- (NSOperation *)fetchGroupWithId:(NSNumber *)groupId
                          success:(void (^)(VPGroup *group))success
                          failure:(void (^)(NSError *error))failure;

- (NSOperation *)fetchGroupsWithParam:(NSDictionary *)param
                              success:(void (^)(VPGroupList *groupList))success
                              failure:(void (^)(NSError *error))failure;

- (NSOperation *)fetchGroupsWithUrl:(NSString *)urlStr
                            success:(void (^)(VPGroupList *groupList))success
                            failure:(void (^)(NSError *error))failure;

/**
 * Retrieve a simple group list, each item in result array is VPGroup but only
 * contains groupId and name. The REST is mainly used to retrieve a complete list
 * of groups to present for things like a pick list
 */
//- (NSOperation *)fetchSimpleGroupsSuccess:(void (^)(NSArray *simpleGroups))success
//                                  failure:(void (^)(NSError *error))failure;

@end
