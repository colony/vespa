//
//  VPGroupRestAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 7/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroupAPI.h"

@interface VPGroupRestAPI : NSObject<VPGroupAPI>

+ (instancetype)sharedInstance;

@end
