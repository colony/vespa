//
//  VPGroupRestAPI.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroupRestAPI.h"

#import "VPError.h"
#import "VPGroup.h"
#import "VPHTTPClientManager.h"
#import "VPRestClientManager.h"
#import "VPUtil.h"


static NSString *kGetGroupPath = @"groups/:id";
static NSString *kGetGroupsPath = @"groups";
static NSString *kGetSimpleGroups = @"groups/simple";


@implementation VPGroupRestAPI

+ (instancetype)sharedInstance
{
    static VPGroupRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}


#pragma mark - setup
- (void)setup
{
    RKObjectManager *manager = [VPRestClientManager sharedClient];
    
    //object mapping
    RKMapping *groupMapping = [VPGroup getModelMapping];
    RKMapping *groupListMapping = [VPGroupList getModelMapping];
    
    //fetch groups
    RKResponseDescriptor *getGroupsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:groupListMapping
                                                                                                     method:RKRequestMethodGET
                                                                                                pathPattern:kGetGroupsPath
                                                                                                    keyPath:nil
                                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:getGroupsResponseDescriptor];
    
    //fetch a single group
    RKResponseDescriptor *getAGroupResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:groupMapping
                                                                                                     method:RKRequestMethodGET
                                                                                                pathPattern:kGetGroupPath
                                                                                                    keyPath:nil
                                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:getAGroupResponseDescriptor];
}


#pragma mark - API implementation
- (NSOperation *)fetchGroupsWithParam:(NSDictionary *)param success:(void (^)(VPGroupList *))success failure:(void (^)(NSError *))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kGetGroupsPath
                                                      parameters:param];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPGroupList *groupList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, groupList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchGroupsWithUrl:(NSString *)urlStr success:(void (^)(VPGroupList *))success failure:(void (^)(NSError *))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [VPUtil requestWithURL:urlStr];
    if (!request) {
        NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
        SAFE_BLOCK_RUN(failure, error);
        return nil;
    }
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPGroupList *groupList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, groupList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchGroupWithId:(NSNumber *)groupId success:(void (^)(VPGroup *))success failure:(void (^)(NSError *))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:[kGetGroupPath stringByReplacingOccurrencesOfString:@":id" withString:[groupId stringValue]]
                                                      parameters:nil];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPGroup *group = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, group);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled)
                                                                                        return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchSimpleGroupsSuccess:(void (^)(NSArray *simpleGroups))success
                                  failure:(void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                            path:kGetSimpleGroups
                                                      parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFJSONRequestOperation alloc]initWithRequest: request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id response) {
        if (operation.response.statusCode == 200 && [response isKindOfClass:[NSArray class]]) {
            NSMutableArray *result = [NSMutableArray new];
            for (id item in response) {
                NSDictionary *itemDict = (NSDictionary *)item;
                VPGroup *group = [VPGroup new];
                group.groupId = itemDict[@"id"];
                group.name = itemDict[@"name"];
                [result addObject:group];
            }
            SAFE_BLOCK_RUN(success, result);
        } else {
            NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
            SAFE_BLOCK_RUN(failure, error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.isCancelled)
            return;
        
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

@end
