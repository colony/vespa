//
//  VPMeApi.h
//  Vespa
//
//  Created by Jiayu Zhang on 11/14/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


typedef NS_ENUM(NSInteger, VPMyPostType) {
    kVPMyAllPosts = 0,
    kVPMyLikePosts,
    kVPMyStingPosts
};


@class VPAccount;
@class VPPaginator;
@class VPPostList;
@class VPStatus;

@protocol VPMeAPI <NSObject>

/*
 Fetch my user info
 */
- (NSOperation *)fetchMeWithParam: (NSDictionary *)params
                          success: (void (^)(VPAccount *me))success
                          failure: (void (^)(NSError *error))failure;

/*
 Fetch my posts with specified type and parameters. Usually this fetch first page of results
 */
- (NSOperation *)fetchMyPostsWithType: (VPMyPostType)type
                                param: (NSDictionary *)params
                              success: (void (^)(VPPostList *postList))success
                              failure: (void (^)(NSError *error))failure;

/*
 Fetch my posts with specified URL. Usually this fetch sibiling page of results by using the URL from the paginator.
 */
- (NSOperation *)fetchMyPostsWithUrl: (NSString *)url
                           success: (void (^)(VPPostList *postList))success
                           failure: (void (^)(NSError *error))failure;

- (NSOperation *)getMyStatusWithSuccess: (void (^)(VPStatus *status))success
                                failure: (void (^)(NSError *error))failure;

- (NSOperation *)updateProfileWithAvatar: (NSString *)avatarUrl
                                   phone: (NSString *)phoneNum
                              screenName: (NSString *)screenName
                                 success: (void (^)(void))success
                                 failure: (void (^)(NSError *error))failure;
@end
