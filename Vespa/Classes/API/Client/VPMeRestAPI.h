//
//  VPMeRestApi.h
//  Vespa
//
//  Created by Jiayu Zhang on 11/14/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPMeApi.h"

@interface VPMeRestAPI : NSObject<VPMeAPI>

+ (instancetype)sharedInstance;

@end
