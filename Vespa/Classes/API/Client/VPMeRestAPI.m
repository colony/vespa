//
//  VPMeRestApi.m
//  Vespa
//
//  Created by Jiayu Zhang on 11/14/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPMeRestAPI.h"

#import "NSString+Util.h"
#import "VPAccount.h"
#import "VPError.h"
#import "VPHTTPClientManager.h"
#import "VPPaginator.h"
#import "VPPost.h"
#import "VPRestClientManager.h"
#import "VPStatus.h"
#import "VPUtil.h"

static NSString *kMyProfilePath = @"users/me";
static NSString *kMyAllPostsPath = @"me/posts/create";
static NSString *kMyLikePostsPath = @"me/posts/like";
static NSString *kMyStingPostsPath = @"me/posts/sting";
static NSString *kMyStatusPath = @"me/status";

@implementation VPMeRestAPI

+ (instancetype)sharedInstance
{
    static VPMeRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}


#pragma mark - setup
- (void)setup
{
    RKObjectManager *manager = [VPRestClientManager sharedClient];
    
    //object mapping
    RKMapping *postListMapping = [VPPostList getModelMapping];
//    RKMapping *accountMapping = [VPAccount getModelMapping];
    RKMapping *statusMapping = [VPStatus getModelMapping];
    
    //fetch me
//    RKResponseDescriptor *fetchMeResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:accountMapping
//                                                                                                    method:RKRequestMethodGET
//                                                                                                pathPattern:kMyProfilePath
//                                                                                                    keyPath:nil
//                                                                                                 statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
//    [manager addResponseDescriptor:fetchMeResponseDescriptor];
    
    //fetch my posts
        RKResponseDescriptor *fetchMyPostsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postListMapping
                                                                                                          method:RKRequestMethodGET
                                                                                                     pathPattern:kMyAllPostsPath
                                                                                                         keyPath:nil
                                                                                                     statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
        [manager addResponseDescriptor:fetchMyPostsResponseDescriptor];
    
    //fetch my status
    RKResponseDescriptor *fetchMyStatusResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:statusMapping
                                                                                                         method:RKRequestMethodGET
                                                                                                    pathPattern:kMyStatusPath
                                                                                                        keyPath:nil
                                                                                                    statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:fetchMyStatusResponseDescriptor];
}


#pragma mark - API implementation
- (NSOperation *)fetchMeWithParam: (NSDictionary *)params
                          success: (void (^)(VPAccount *me))success
                          failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kMyProfilePath
                                                      parameters:params];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPAccount *me = mappingResult.firstObject;
                                                                                    VPLog(@"%@", mappingResult);
                                                                                    SAFE_BLOCK_RUN(success, me);
                                                                                    
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    VPLog(@"%@", error);
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchMyPostsWithType: (VPMyPostType)type
                                param: (NSDictionary *)params
                              success: (void (^)(VPPostList *postList))success
                              failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:[self typeToPath:type]
                                                      parameters:params];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchMyPostsWithUrl: (NSString *)urlStr
                           success: (void (^)(VPPostList *postList))success
                           failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [VPUtil requestWithURL:urlStr];
    if (!request) {
        NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
        SAFE_BLOCK_RUN(failure, error);
        return nil;
    }
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)getMyStatusWithSuccess: (void (^)(VPStatus *status))success
                                failure: (void (^)(NSError *error))failure;
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kMyStatusPath
                                                      parameters:nil];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPStatus *status = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, status);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    VPLog(@"%@", [error localizedDescription]);
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)updateProfileWithAvatar: (NSString *)avatarUrl
                                   phone: (NSString *)phoneNum
                              screenName: (NSString *)screenName
                                 success: (void (^)(void))success
                                 failure: (void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (![NSString isEmptyString:avatarUrl]){
        param[@"avatar"] = avatarUrl;
    }
    if (![NSString isEmptyString:phoneNum]){
        param[@"phone"] = phoneNum;
    }
    if (![NSString isEmptyString:screenName]){
        param[@"screen_name"] = screenName;
    }
    
    //TODO: what if there's no thing to update? return NSError with custom code?
    
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kMyProfilePath
                                                      parameters:param];
    
    AFHTTPRequestOperation *operation = [[AFJSONRequestOperation alloc]initWithRequest: request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        VPLog(@"%@", responseObject);
        if ([responseObject isKindOfClass:[NSDictionary class]]){
            NSDictionary *r = (NSDictionary *)responseObject;
            if ([r[@"result"] isEqualToString:@"ok"]){
                SAFE_BLOCK_RUN(success);
            } else {
                NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
                SAFE_BLOCK_RUN(failure, error);
            }
        } else {
            //TODO: different code for different thing?
            NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
            SAFE_BLOCK_RUN(failure, error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        VPLog(@"%@", error);
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}


#pragma mark - private methods
- (NSString *)typeToPath: (VPMyPostType)type
{
    switch (type) {
        case kVPMyAllPosts:
            return kMyAllPostsPath;
            
        case kVPMyLikePosts:
            return kMyLikePostsPath;
            
        case kVPMyStingPosts:
            return kMyStingPostsPath;
            
        default:
            return nil;
    }
}
@end
