//
//  VPMediaAPI.h
//  Vespa
//
//  Created by Jay on 11/8/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol VPMediaAPI <NSObject>

// meidas is array of dictionary {key: keyname, data: data, type: mime type}
- (NSOperation *)uploadMediasWithArray: (NSArray *)medias
                               success: (void (^)(NSArray *medias))success
                               failure: (void (^)(NSError *error))failure;

- (NSOperation *)uploadMediaWithKey: (NSString *)key
                               data: (NSData *)data
                            mimeType: (NSString *)type
                            success: (void (^)(NSDictionary *media))success
                            failure: (void (^)(NSError *error))failure;

@end
