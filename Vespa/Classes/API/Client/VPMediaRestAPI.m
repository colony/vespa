//
//  VPMediaRestAPI.m
//  Vespa
//
//  Created by Jay on 11/8/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPMediaRestAPI.h"

#import "NSData+ImageContentType.h"
#import "NSString+Util.h"
#import "VPError.h"
#import "VPHTTPClientManager.h"

static NSString *kUploadMediaPath = @"medias";

@implementation VPMediaRestAPI

+ (instancetype)sharedInstance
{
    static VPMediaRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - API implementation
- (NSOperation *)uploadMediasWithArray: (NSArray *)medias
                               success: (void (^)(NSArray *medias))success
                               failure: (void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                         path:kUploadMediaPath
                                                                   parameters:nil
                                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                        for (NSDictionary *media in medias) {
                                                            NSString *key = media[@"key"];
                                                            NSData *data = media[@"data"];
                                                            NSString *type = media[@"type"];
                                                            type = [NSString isEmptyString:type] ? [data imageContentType] : type;
                                                            if (![NSString isEmptyString:key] && data.length > 0 && ![NSString isEmptyString:type]) {
                                                                [formData appendPartWithFileData:data name:key fileName:key mimeType:type];
                                                            } else {
                                                                VPLog(@"error");
                                                            }
                                                        }
                                                    }];
    AFHTTPRequestOperation *operation = [[AFJSONRequestOperation alloc]initWithRequest: request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        VPLog(@"%@", responseObject);
        NSArray *medias = nil;
        if ([responseObject isKindOfClass:[NSArray class]]) {
            medias = responseObject;
        } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
            medias = @[responseObject];
        }
        if (medias) {
            SAFE_BLOCK_RUN(success, medias);
        } else {
            NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
            SAFE_BLOCK_RUN(failure, error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        VPLog(@"%@", error)
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    return operation;
}

- (NSOperation *)uploadMediaWithKey: (NSString *)key
                               data: (NSData *)data
                           mimeType: (NSString *)type
                            success: (void (^)(NSDictionary *media))success
                            failure: (void (^)(NSError *error))failure;
{
    if ([NSString isEmptyString:key] || data.length == 0) {
        NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
        SAFE_BLOCK_RUN(failure, error);
        return nil;
    }
    
    NSMutableDictionary *mediaDictionary = [[NSMutableDictionary alloc]initWithDictionary:@{@"key": key,
                                                                                           @"data": data}];
    if (![NSString isEmptyString:type]) {
        mediaDictionary[@"type"] = type;
    }
    NSArray *mediaArray = @[mediaDictionary];
    return [self uploadMediasWithArray:mediaArray
                               success:^(NSArray *medias) {
                                   success(medias.firstObject);
                               }
                               failure:failure];
    
}
@end
