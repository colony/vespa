//
//  VPPostAPI.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@class VPGeo;
@class VPPaginator;
@class VPPost;
@class VPPostList;

@protocol VPPostAPI <NSObject>

- (NSOperation *)createFlagWithPostId: (NSNumber *)postId
                            complaint: (NSString *)complaint
                              success: (void (^)(void))success
                              failure: (void (^)(NSError *error))failure;

/**
 * Create an audio or photo or text post.
 * When audio/photo, the body param stores caption
 * When text, leave body to nil, use text param instead
 */
- (NSOperation *)createPostWithType: (NSString *)type
                               body: (NSString *)body
                                geo: (VPGeo *)geo
                            groupId: (NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                              audio: (NSData *)audioData
                      audioDuration: (NSInteger)audioDurationInMS
                          audioSize: (NSUInteger)audioSize
                        audioFormat: (NSString *)audioFormat
                              photo: (NSData *)photoData
                          photoSize: (NSUInteger)photoSize
                        photoHeight: (NSInteger)photoHeight
                         photoWidth: (NSInteger)photoWidth
                        photoFormat: (NSString *)photoFormat
                      photoUploaded: (BOOL)photoUploaded
                               text: (NSString *)textData
                textBackgroundColor: (NSString *)textBackgroundColorHex
                          textColor: (NSString *)textColorHex
                         textFormat: (NSString *)textFormat
                               link: (NSString *)linkData
                          linkTitle: (NSString *)linkTitle
                       linkEmbedUrl: (NSString *)linkEmbedUrl
                    linkEmbedHeight: (NSInteger)linkEmbedHeight
                     linkEmbedWidth: (NSInteger)linkEmbedWidth
                    linkEmbedFormat: (NSString *)linkEmbedFormat
                              video: (NSData *)videoData
                          videoSize: (NSUInteger)videoSize
                        videoFormat: (NSString *)videoFormat
                            success: (void (^)(VPPost *))success
                            failure: (void (^)(NSError *error))failure;

- (NSOperation *)deletePostWithId: (NSNumber *)postId
                          success: (void(^)(void))success
                          failure: (void(^)(NSError *error))failure;

- (NSOperation *)fetchAnnouncementPostsWithParam:(NSDictionary *)param
                                         success:(void (^)(VPPostList *))success
                                         failure:(void (^)(NSError *))failure;

- (NSOperation *)fetchFeaturedPostsWithParam:(NSDictionary *)param
                                     success:(void (^)(VPPostList *))success
                                     failure:(void (^)(NSError *))failure;

- (NSOperation *)fetchPostWithId: (NSNumber *)postId
                         success: (void (^)(VPPost *post))success
                         failure: (void (^)(NSError *error))failure;

/**
 Parameters:
    params (inject as http query params) -
        pagination i.e. _a,_b,_e,_k,_n,_o
 */
- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             success:(void (^)(VPPostList *postList))success
                             failure:(void (^)(NSError *error))failure;

- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             groupId:(NSNumber *)groupId
                             success:(void (^)(VPPostList *))success
                             failure:(void (^)(NSError *))failure;

- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             hashtag:(NSString *)hashtag
                             success:(void (^)(VPPostList *))success
                             failure:(void (^)(NSError *))failure;

/**
 Fetch posts (feed) with specified URL. Usually this fetch sibiling page of results by using the URL from the paginator.
 */
- (NSOperation *)fetchPostsWithUrl: (NSString *)urlStr
                           success: (void (^)(VPPostList *postList))success
                           failure: (void (^)(NSError *error))failure;


- (NSOperation *)likePostWithId: (NSNumber *)postId
                         status: (BOOL)status
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure;

- (NSOperation *)starPostWithId: (NSNumber *)postId
                            num: (NSUInteger)num
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure;

- (NSOperation *)votePostWithId: (NSNumber *)postId
                           down: (BOOL)down
                             up: (BOOL)up
                         undown: (BOOL)undown
                           unup: (BOOL)unup
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure;
@end
