//
//  VPPostRestAPI.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VPPostAPI.h"

@interface VPPostRestAPI : NSObject<VPPostAPI>

+ (instancetype)sharedInstance;

@end
