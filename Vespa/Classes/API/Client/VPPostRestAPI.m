//
//  VPPostRestAPI.m
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPPostRestAPI.h"

#import "NSData+ImageContentType.h"
#import "VPError.h"
#import "VPHTTPClientManager.h"
#import "VPGeo.h"
#import "VPPaginator.h"
#import "VPPost.h"
#import "VPRestClientManager.h"
#import "VPUtil.h"


static NSString *kCreatePostPath = @"posts";
static NSString *kDeletePostPath = @"posts/:id";
static NSString *kFlagPostPath = @"posts/:id/flag";
static NSString *kGetPostPath = @"posts/:id";
static NSString *kLikePostPath = @"posts/:id/like";
static NSString *kStarPostPath = @"posts/:id/star";
static NSString *kVotePostPath = @"posts/:id/vote";

static NSString *kGetAnnouncementFeedPath = @"feed/announcement";
static NSString *kGetFeaturedFeedPath = @"feed/featured";
static NSString *kGetPostFeedPath = @"feed";
static NSString *kGetPostFeedByGroupIdPath = @"feed/group/:id";
static NSString *kGetPostFeedByHashtagPath = @"feed/hashtag/:tag";


@implementation VPPostRestAPI

+ (instancetype)sharedInstance
{
    static VPPostRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}


#pragma mark - setup
- (void)setup
{
    RKObjectManager *manager = [VPRestClientManager sharedClient];
    
    //object mapping
    RKMapping *postMapping = [VPPost getModelMapping];
    RKMapping *postListMapping = [VPPostList getModelMapping];

    //create post
    RKResponseDescriptor *createPostResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postMapping
                                                                                                      method:RKRequestMethodPOST
                                                                                                 pathPattern:kCreatePostPath
                                                                                                     keyPath:nil
                                                                                                 statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:createPostResponseDescriptor];
    
    //fetch posts/feed by different criteria
    RKResponseDescriptor *getAnnouncementFeedResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postListMapping
                                                                                                               method:RKRequestMethodGET
                                                                                                          pathPattern:kGetAnnouncementFeedPath
                                                                                                              keyPath:nil
                                                                                                          statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    RKResponseDescriptor *getFeaturedFeedResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postListMapping
                                                                                                           method:RKRequestMethodGET
                                                                                                      pathPattern:kGetFeaturedFeedPath
                                                                                                          keyPath:nil
                                                                                                      statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    RKResponseDescriptor *getFeedResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postListMapping
                                                                                                   method:RKRequestMethodGET
                                                                                              pathPattern:kGetPostFeedPath
                                                                                                  keyPath:nil
                                                                                              statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    RKResponseDescriptor *getFeedByHashtagResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postListMapping
                                                                                                            method:RKRequestMethodGET
                                                                                                       pathPattern:kGetPostFeedByHashtagPath
                                                                                                           keyPath:nil
                                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    RKResponseDescriptor *getFeedByGroupIdResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postListMapping
                                                                                                            method:RKRequestMethodGET
                                                                                                       pathPattern:kGetPostFeedByGroupIdPath
                                                                                                           keyPath:nil
                                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptorsFromArray:@[getAnnouncementFeedResponseDescriptor,
                                               getFeaturedFeedResponseDescriptor,
                                               getFeedResponseDescriptor,
                                               getFeedByHashtagResponseDescriptor,
                                               getFeedByGroupIdResponseDescriptor]];
    
    //fetch a single post
    RKResponseDescriptor *fetchAPostResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postMapping
                                                                                                      method:RKRequestMethodGET
                                                                                                 pathPattern:@"posts/:id"
                                                                                                     keyPath:nil
                                                                                                 statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptor:fetchAPostResponseDescriptor];
}


#pragma mark - API implementation
- (NSOperation *)createFlagWithPostId: (NSNumber *)postId
                            complaint: (NSString *)complaint
                              success: (void (^)(void))success
                              failure: (void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:[kFlagPostPath stringByReplacingOccurrencesOfString:@":id" withString:[postId stringValue]]
                                                      parameters:@{@"complaint_message":complaint}];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest: request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        SAFE_BLOCK_RUN(success);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        VPLog(@"%@", error)
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *)createPostWithType: (NSString *)type
                               body: (NSString *)body
                                geo: (VPGeo *)geo
                            groupId: (NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                              audio: (NSData *)audioData
                      audioDuration: (NSInteger)audioDurationInMS
                          audioSize: (NSUInteger)audioSize
                        audioFormat: (NSString *)audioFormat
                              photo: (NSData *)photoData
                          photoSize: (NSUInteger)photoSize
                        photoHeight: (NSInteger)photoHeight
                         photoWidth: (NSInteger)photoWidth
                        photoFormat: (NSString *)photoFormat
                      photoUploaded: (BOOL)photoUploaded
                               text: (NSString *)textData
                textBackgroundColor: (NSString *)textBackgroundColorHex
                          textColor: (NSString *)textColorHex
                         textFormat: (NSString *)textFormat
                               link: (NSString *)linkData
                          linkTitle: (NSString *)linkTitle
                       linkEmbedUrl: (NSString *)linkEmbedUrl
                    linkEmbedHeight: (NSInteger)linkEmbedHeight
                     linkEmbedWidth: (NSInteger)linkEmbedWidth
                    linkEmbedFormat: (NSString *)linkEmbedFormat
                              video: (NSData *)videoData
                          videoSize: (NSUInteger)videoSize
                        videoFormat: (NSString *)videoFormat
                            success: (void (^)(VPPost *))success
                            failure: (void (^)(NSError *error))failure;
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                         path:kCreatePostPath
                                                                   parameters:nil
                                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                        if (geo) {
                                                            [formData appendPartWithFormData:[[geo toHttpParamValue] dataUsingEncoding:NSUTF8StringEncoding] name:@"geo"];
                                                        }
                                                        [self appendForm:formData data:body name:@"body"];
                                                        [self appendForm:formData data:type name:@"type"];
                                                        [self appendForm:formData data:[groupId stringValue] name:@"group_id"];
                                                        [self appendForm:formData data:createThreadType name:@"create_thread_type"];
                                                        [self appendForm:formData data:[threadId stringValue] name:@"thread_id"];
                                                        
                                                        if (audioData) {
                                                            // i.e. audio/wav, audio/m4a
                                                            NSString *mimeType =  [@"audio/" stringByAppendingString:[audioFormat lowercaseString]];
                                                            [formData appendPartWithFileData:audioData name:@"audio_data" fileName:@"audio_data" mimeType:mimeType];
                                                            [formData appendPartWithFormData:[audioFormat dataUsingEncoding:NSUTF8StringEncoding] name:@"audio_format"];
                                                            [formData appendPartWithFormData:[[@(audioDurationInMS) stringValue] dataUsingEncoding:NSUTF8StringEncoding] name:@"audio_duration"];
                                                            [formData appendPartWithFormData:[[@(audioSize) stringValue] dataUsingEncoding:NSUTF8StringEncoding] name:@"audio_size"];
                                                        }
                                                        
                                                        if (linkData) {
                                                            [self appendForm:formData data:linkData name:@"link_data"];
                                                            [self appendForm:formData data:linkTitle name:@"link_title"];
                                                            
                                                            // link embed item might be nil
                                                            [self appendForm:formData data:linkEmbedUrl name:@"link_embed_url"];
                                                            [self appendForm:formData data:[@(linkEmbedHeight) stringValue] name:@"link_embed_height"];
                                                            [self appendForm:formData data:[@(linkEmbedWidth) stringValue] name:@"link_embed_width"];
                                                            [self appendForm:formData data:linkEmbedFormat name:@"link_embed_format"];
                                                        }
                                                        
                                                        if (photoData) {
                                                            [formData appendPartWithFileData:photoData name:@"photo_data" fileName:@"photo_data" mimeType:[photoData imageContentType]];
                                                            [self appendForm:formData data:photoFormat name:@"photo_format"];
                                                            [self appendForm:formData data:[@(photoHeight) stringValue] name:@"photo_height"];
                                                            [self appendForm:formData data:[@(photoWidth) stringValue] name:@"photo_width"];
                                                            [self appendForm:formData data:[@(photoSize) stringValue] name:@"photo_size"];
                                                            [self appendForm:formData data:[@(photoUploaded) stringValue] name:@"photo_uploaded"];
                                                        }

                                                        if (textData) {
                                                            [formData appendPartWithFormData:[textData dataUsingEncoding:NSUTF8StringEncoding] name:@"text_data"];
                                                            [formData appendPartWithFormData:[textBackgroundColorHex dataUsingEncoding:NSUTF8StringEncoding] name:@"text_background_color"];
                                                            [formData appendPartWithFormData:[textColorHex dataUsingEncoding:NSUTF8StringEncoding] name:@"text_color"];
                                                            [formData appendPartWithFormData:[textFormat dataUsingEncoding:NSUTF8StringEncoding] name:@"text_format"];
                                                        }
                                                        
                                                        if (videoData) {
                                                            // i.e. video/mp4
                                                            NSString *mimeType =  [@"video/" stringByAppendingString:[videoFormat lowercaseString]];
                                                            [formData appendPartWithFileData:videoData name:@"video_data" fileName:@"video_data" mimeType:mimeType];
                                                            [self appendForm:formData data:videoFormat name:@"video_format"];
                                                            [self appendForm:formData data:[@(videoSize) stringValue] name:@"video_size"];
                                                        }
                                                    }];
    
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPost *post = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, post);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    VPLog(@"Fail to create post %@", [error userInfo]);
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)deletePostWithId: (NSNumber *)postId
                          success: (void(^)(void))success
                          failure: (void(^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"DELETE"
                                                            path:[kDeletePostPath stringByReplacingOccurrencesOfString:@":id" withString:[postId stringValue]]
                                                      parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest: request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // responseObject is NSData
        VPLog(@"Response: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        SAFE_BLOCK_RUN(success);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        VPLog(@"%@", error)
        SAFE_BLOCK_RUN(failure, error);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchAnnouncementPostsWithParam:(NSDictionary *)param
                                         success:(void (^)(VPPostList *postList))success
                                         failure:(void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kGetAnnouncementFeedPath
                                                      parameters:param];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled) return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchFeaturedPostsWithParam:(NSDictionary *)param
                                     success:(void (^)(VPPostList *))success
                                     failure:(void (^)(NSError *))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kGetFeaturedFeedPath
                                                      parameters:param];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled) return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchPostWithId: (NSNumber *)postId
                         success: (void (^)(VPPost *post))success
                         failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:[kGetPostPath stringByReplacingOccurrencesOfString:@":id" withString:[postId stringValue]]
                                                      parameters:nil];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPost *post = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, post);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled)
                                                                                        return;

                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             success:(void (^)(VPPostList *postList))success
                             failure:(void (^)(NSError *))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:kGetPostFeedPath
                                                      parameters:params];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled) return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             groupId:(NSNumber *)groupId
                             success:(void (^)(VPPostList *))success
                             failure:(void (^)(NSError *))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:[kGetPostFeedByGroupIdPath stringByReplacingOccurrencesOfString:@":id" withString:[groupId stringValue]]
                                                      parameters:params];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled) return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchPostsWithParam:(NSDictionary *)params
                             hashtag:(NSString *)hashtag
                             success:(void (^)(VPPostList *))success
                             failure:(void (^)(NSError *))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:[kGetPostFeedByHashtagPath stringByReplacingOccurrencesOfString:@":tag" withString:hashtag]
                                                      parameters:params];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled) return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchPostsWithUrl: (NSString *)urlStr
                           success: (void (^)(VPPostList *postList))success
                           failure: (void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [VPUtil requestWithURL:urlStr];
    if (!request) {
        NSError *error = [NSError errorWithDomain:VPDataErrorDomain code:0 userInfo:nil];
        SAFE_BLOCK_RUN(failure, error);
        return nil;
    }
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled) return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)likePostWithId: (NSNumber *)postId
                         status: (BOOL)status
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:[kLikePostPath stringByReplacingOccurrencesOfString:@":id" withString:[postId stringValue]]
                                                      parameters:@{@"status":(status ? @"1" : @"0")}];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        VPLog(@"%@", JSON);
        SAFE_BLOCK_RUN(success);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        SAFE_BLOCK_RUN(failure, error);
    }];
    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest: request];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        // responseObject is NSData
//        VPLog(@"Response: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
//        SAFE_BLOCK_RUN(success);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        VPLog(@"%@", error)
//        SAFE_BLOCK_RUN(failure, error);
//    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *)starPostWithId: (NSNumber *)postId
                            num: (NSUInteger)num
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:[kStarPostPath stringByReplacingOccurrencesOfString:@":id" withString:[postId stringValue]]
                                                      parameters:@{@"num":@(num)}];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                            SAFE_BLOCK_RUN(success);
                                                                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            SAFE_BLOCK_RUN(failure, error);
                                                                                        }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (NSOperation *)votePostWithId: (NSNumber *)postId
                           down: (BOOL)down
                             up: (BOOL)up
                         undown: (BOOL)undown
                           unup: (BOOL)unup
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure;
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];

    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:[kVotePostPath stringByReplacingOccurrencesOfString:@":id" withString:[postId stringValue]]
                                                      parameters:@{@"down":@(down),@"up":@(up),@"undown":@(undown),@"unup":@(unup)}];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                            SAFE_BLOCK_RUN(success);
                                                                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            SAFE_BLOCK_RUN(failure, error);
                                                                                        }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}


#pragma mark - private
- (void)appendForm:(id<AFMultipartFormData>)form data:(NSString *)data name:(NSString *)name
{
    if (data) {
        [form appendPartWithFormData:[data dataUsingEncoding:NSUTF8StringEncoding] name:name];
    }
}


@end
