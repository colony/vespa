//
//  VPThreadAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@class VPPost;
@class VPPostList;

@protocol VPThreadAPI <NSObject>

- (NSOperation *)fetchRootPostWithId:(NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure;

- (NSOperation *)fetchThreadWithId:(NSNumber *)threadId
                           success:(void (^)(VPPostList *postList))success
                           failure:(void (^)(NSError *error))failure;

- (NSOperation *)performThreadActionWithId:(NSNumber *)threadId
                                 actionKey:(NSString *)actionKey
                                 actionVal:(NSString *)actionVal
                                   success:(void (^)(void))success
                                   failure:(void (^)(NSError *error))failure;

@end
