//
//  VPThreadRestAPI.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPThreadAPI.h"

@interface VPThreadRestAPI : NSObject<VPThreadAPI>

+ (instancetype)sharedInstance;

@end
