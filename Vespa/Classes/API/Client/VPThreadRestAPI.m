//
//  VPThreadRestAPI.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPThreadRestAPI.h"

#import "VPHTTPClientManager.h"
#import "VPPost.h"
#import "VPRestClientManager.h"


static NSString * const kGetRootPostPath = @"threads/:id/root";
static NSString * const kGetThreadPath = @"threads/:id";
static NSString * const kPerformActionPath = @"threads/:id/action";


@implementation VPThreadRestAPI

+ (instancetype)sharedInstance
{
    static VPThreadRestAPI *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}


#pragma mark - setup
- (void)setup
{
    RKObjectManager *manager = [VPRestClientManager sharedClient];
    
    // object mapping
    RKMapping *postMapping = [VPPost getModelMapping];
    RKMapping *postListMapping = [VPPostList getModelMapping];
    
    // descriptors
    RKResponseDescriptor *getThreadResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postListMapping
                                                                                                             method:RKRequestMethodGET
                                                                                                        pathPattern:kGetThreadPath
                                                                                                            keyPath:nil
                                                                                                        statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    RKResponseDescriptor *getRootPostResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:postMapping
                                                                                                       method:RKRequestMethodGET
                                                                                                  pathPattern:kGetRootPostPath
                                                                                                      keyPath:nil
                                                                                                  statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [manager addResponseDescriptorsFromArray:@[getThreadResponseDescriptor, getRootPostResponseDescriptor]];
}


#pragma mark - API implementation
- (NSOperation *)fetchRootPostWithId:(NSNumber *)threadId success:(void (^)(VPPost *post))success failure:(void (^)(NSError *error))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:[kGetRootPostPath stringByReplacingOccurrencesOfString:@":id" withString:[threadId stringValue]]
                                                      parameters:nil];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPost *post = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, post);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled) return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)fetchThreadWithId:(NSNumber *)threadId success:(void (^)(VPPostList *))success failure:(void (^)(NSError *))failure
{
    VPRestClientManager *restClient = [VPRestClientManager sharedClient];
    NSMutableURLRequest *request = [restClient requestWithObject:nil
                                                          method:RKRequestMethodGET
                                                            path:[kGetThreadPath stringByReplacingOccurrencesOfString:@":id" withString:[threadId stringValue]]
                                                      parameters:nil];
    RKObjectRequestOperation *operation = [restClient objectRequestOperationWithRequest:request
                                                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                                                    VPPostList *postList = mappingResult.firstObject;
                                                                                    SAFE_BLOCK_RUN(success, postList);
                                                                                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                                    if (operation.isCancelled) return;
                                                                                    SAFE_BLOCK_RUN(failure, error);
                                                                                }];
    [restClient enqueueObjectRequestOperation:operation];
    return operation;
}

- (NSOperation *)performThreadActionWithId:(NSNumber *)threadId
                                 actionKey:(NSString *)actionKey
                                 actionVal:(NSString *)actionVal
                                   success:(void (^)(void))success
                                   failure:(void (^)(NSError *error))failure
{
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:[kPerformActionPath stringByReplacingOccurrencesOfString:@":id" withString:[threadId stringValue]]
                                                      parameters:@{@"action_key":actionKey,@"action_val":actionVal}];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                            SAFE_BLOCK_RUN(success);
                                                                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            SAFE_BLOCK_RUN(failure, error);
                                                                                        }];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

@end
