//
//  VPHTTPClientListener.m
//  Vespa
//
//  Created by Jay on 10/26/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPHTTPClientListener.h"
#import <AFNetworking/AFNetworking.h>
#import "Reachability.h"
#import "UIAlertView+Blocks.h"


@interface VPHTTPClientListener ()

@property (strong, nonatomic, readonly) Reachability *internetReachable;
@property (assign, nonatomic) NetworkStatus currentNetworkStatus;
@property (assign, nonatomic) NSTimeInterval lastHostNotReachableNotificationTime; // seconds since epoch

@end


@implementation VPHTTPClientListener

+ (instancetype)sharedListener
{
    static VPHTTPClientListener *sharedListener = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedListener = [[self alloc] init];
    });
    return sharedListener;
}

- (id)init
{
    if (self = [super init]) {
        _internetReachable = [Reachability reachabilityForInternetConnection];
        _currentNetworkStatus = -1; // not init yet
        _lastHostNotReachableNotificationTime = 0;
    }
    return self;
}

- (void)startListen
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self];
    [center addObserver:self
               selector:@selector(handleHTTPOperationFinish:)
                   name:AFNetworkingOperationDidFinishNotification
                 object:nil];
    [center addObserver:self
               selector:@selector(handleReachabilityChanged:)
                   name:kReachabilityChangedNotification
                 object:nil];
    [_internetReachable startNotifier];
}

- (void)stopListen
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_internetReachable stopNotifier];
}


#pragma mark - notification handling
- (void)handleHTTPOperationFinish:(NSNotification *)notification
{
    AFHTTPRequestOperation *operation = (AFHTTPRequestOperation *)[notification object];
    
    if (![operation isKindOfClass:[AFHTTPRequestOperation class]]) {
        return;
    }
    
    VPLog(@"%@ Status Code: %ld", operation.request.URL, (long)operation.response.statusCode);
    NSInteger statusCode = [operation.response statusCode];
    if (statusCode == 503) {
        VPLog(@"HTTP Operation Error Code 503");
        [self postOrSuppressNotReachableNotification:kHostNotReachableNotification userInfo:@{@"serverMaintenance":@(YES)}];
        return;
    }
    else if (statusCode == 500) {
        VPLog(@"HTTP Operation Error Code 500");
        [VPReports reportError:kVPErrorServerInternalError message:operation.responseString error:operation.error];
    }
    else if (statusCode == 499) {
        VPLog(@"HTTP Operation Error Code 499: %@", operation.responseString);
        [UIAlertView showWithTitle:nil message:operation.responseString cancelButtonTitle:@"DISMISS" otherButtonTitles:nil tapBlock:nil];
    }
    
    if (operation.error)
    {
        switch (operation.error.code) {
            case kCFURLErrorTimedOut:
            {
                VPLog(@"HTTP Operation Timed Out");
                [VPReports reportError:kVPErrorHttpTimeout message:[NSString stringWithFormat:@"%@ timeout", operation.request.URL.absoluteString] error:operation.error];
                break;
            }
            case kCFURLErrorCannotConnectToHost:
            case kCFURLErrorNotConnectedToInternet:
            {
                VPLog(@"Cannot connect to our server");
                [self postOrSuppressNotReachableNotification:kHostNotReachableNotification userInfo:nil];
                break;
            }
            default:
                break;
        }
    }
}

- (void)handleReachabilityChanged:(NSNotification *)noti
{
    NetworkStatus internetStatus = [self.internetReachable currentReachabilityStatus];
    
    if (internetStatus == NotReachable)
        VPLog(@"The internet is down");
    
    if (self.currentNetworkStatus >= 0 && self.currentNetworkStatus != internetStatus) {
        if (internetStatus == NotReachable) {
            [self postOrSuppressNotReachableNotification:kInternetNotReachableNotification userInfo:nil];
        }
    }
    self.currentNetworkStatus = internetStatus;
}


#pragma mark - private method
- (void)postOrSuppressNotReachableNotification:(NSString *)notificationName userInfo:(NSDictionary *)userInfo
{
    BOOL b = NO;
    @synchronized(self) {
        NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
        // Suppress the noti if we did "5 sec" ago
        if (now - self.lastHostNotReachableNotificationTime > 5){
            self.lastHostNotReachableNotificationTime = now;
            b = YES;
        }
    }
    
    if (b) {
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:userInfo];
    }
}

@end
