//
//  VPHTTPClient.h
//  Vespa
//
//  Created by Jay on 10/26/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@class VPGeo;

@interface VPHTTPClientManager : AFHTTPClient

+ (instancetype)sharedClient;
+ (instancetype)sharedSecureClient;

- (void)setGeo: (VPGeo *)geo;
- (void)setSession: (NSString *)session;

@end
