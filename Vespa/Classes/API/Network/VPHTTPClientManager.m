//
//  VPHTTPClient.m
//  Vespa
//
//  Created by Jay on 10/26/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPHTTPClientManager.h"
#import "VPHTTPClientListener.h"
#import "VPConstants.h"
#import "VPGeo.h"

@implementation VPHTTPClientManager

+ (instancetype)sharedClient
{
    static VPHTTPClientManager *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *url = [NSString stringWithFormat:@"%@%@", kVPAPIHost, kVPAPIPath];
        sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:url]];
    });
    return sharedClient;
}

+ (instancetype)sharedSecureClient
{
    static VPHTTPClientManager *sharedSecureClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *url = [NSString stringWithFormat:@"%@%@", kVPAPISecureAPIHost, kVPAPIPath];
        sharedSecureClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:url]];
    });
    return sharedSecureClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    //Customize initialization
    NSLocale* locale = [NSLocale currentLocale];
    NSString *localeIdentifier = [locale objectForKey:NSLocaleIdentifier];
    [self setDefaultHeader:kVPLocaleHeaderKey value:localeIdentifier];
    [self setDefaultHeader:kVPClientHeaderKey value:kVPClientHeaderVal];
    
    [[VPHTTPClientListener sharedListener] startListen];
    
    return self;
}

- (void)dealloc
{
    [[VPHTTPClientListener sharedListener] stopListen];
}

- (void)setGeo:(VPGeo *)geo
{
    [self setDefaultHeader:kVPGeoHeaderKey value:[NSString stringWithFormat:@"%.7f,%.7f", [geo.latitude doubleValue], [geo.longitude doubleValue]]];
}

- (void)setSession: (NSString *)session
{
    [self setDefaultHeader:@"Authorization" value:[NSString stringWithFormat:@"Bearer %@", session]];
}
@end
