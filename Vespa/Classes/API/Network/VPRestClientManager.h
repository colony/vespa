//
//  VPRestClientManager.h
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <RestKit/RestKit.h>

@interface VPRestClientManager : RKObjectManager

+ (instancetype)sharedClient;
+ (instancetype)sharedSecureClient;

@end
