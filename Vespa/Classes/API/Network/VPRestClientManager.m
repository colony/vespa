//
//  VPRestClientManager.m
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


#import "VPRestClientManager.h"

#import "VPHTTPClientManager.h"
#import "VPPaginator.h"

@implementation VPRestClientManager

+ (instancetype)sharedClient
{
    static VPRestClientManager *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
        sharedClient = [[self alloc] initWithHTTPClient:httpClient];
        [sharedClient setup];
    });
    return sharedClient;
}

+ (instancetype)sharedSecureClient
{
    static VPRestClientManager *sharedSecureClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        VPHTTPClientManager *httpSecureClient = [VPHTTPClientManager sharedSecureClient];
        sharedSecureClient = [[self alloc] initWithHTTPClient:httpSecureClient];
        [sharedSecureClient setup];
    });
    return sharedSecureClient;
}

- (void)setup
{
//    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
    
//    // pagination
//    RKMapping *paginatorMapping = [VPPaginator getModelMapping];
//    RKResponseDescriptor *paginatorResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:paginatorMapping
//                                                                                                     method:RKRequestMethodGET
//                                                                                                pathPattern:nil
//                                                                                                    keyPath:@"pagination"
//                                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
//    [self addResponseDescriptor:paginatorResponseDescriptor];
    // location transformer
//    RKCLLocationValueTransformer *locationValueTransformer = [RKCLLocationValueTransformer locationValueTransformerWithLatitudeKey:@"lat" longitudeKey:@"lon"];
//    [[RKValueTransformer defaultValueTransformer] addValueTransformer:locationValueTransformer];
    
//    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/plain"];
}

@end

