//
//  NSData+ImageContentType.h
//  Vespa
//
//  Created by Jay on 11/8/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (ImageContentType)

- (NSString *)imageContentType;

@end
