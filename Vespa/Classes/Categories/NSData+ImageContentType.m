//
//  NSData+ImageContentType.m
//  Vespa
//
//  Created by Jay on 11/8/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "NSData+ImageContentType.h"

@implementation NSData (ImageContentType)

- (NSString *)imageContentType
{
    if (self.length == 0)
        return nil;
    
    uint8_t c;
    [self getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}
@end
