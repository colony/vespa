//
//  NSString+Util.h
//  Vespa
//
//  Created by Jay on 11/5/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@interface NSString (Util)

+ (BOOL)isBlankString:(NSString *)str;

+ (BOOL)isEmptyString:(NSString *)str;

/**
 * Determine if two specified strings are identical, except that, if both
 * str are nil or empty, returns false
 */
+ (BOOL)isEqualButNotEmpty:(NSString *)s1 and:(NSString *)s2 caseSensitive:(BOOL)sensitive;

+ (NSString *)randomAlphanumericStringWithLength:(NSInteger)length;

- (id)toJSON;

@end
