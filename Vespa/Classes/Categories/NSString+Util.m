//
//  NSString+Util.m
//  Vespa
//
//  Created by Jay on 11/5/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "NSString+Util.h"

@implementation NSString (Util)

+ (BOOL)isBlankString:(NSString *)str
{
    return [self isEmptyString:str] || [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0;
}

+ (BOOL)isEmptyString: (NSString *)str
{
    return (str == nil) || [str length] == 0;
}

+ (BOOL)isEqualButNotEmpty:(NSString *)s1 and:(NSString *)s2 caseSensitive:(BOOL)sensitive
{
    if ([self isEmptyString:s1] && [self isEmptyString:s2])
        return NO;
    if (sensitive)
        return [s1 isEqualToString:s2];
    else
        return [[s1 lowercaseString] isEqualToString:[s2 lowercaseString]];
}

+ (NSString *)randomAlphanumericStringWithLength:(NSInteger)length
{
    static NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomStr = [NSMutableString stringWithCapacity:length];
    for (int i = 0; i < length; i++) {
        [randomStr appendFormat:@"%C", [letters characterAtIndex:arc4random_uniform([letters length])]];
    }
    return [NSString stringWithString:randomStr]; // Returns a copy of immutable one
}

- (id)toJSON
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}

@end
