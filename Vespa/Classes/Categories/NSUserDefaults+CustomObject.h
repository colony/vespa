//
//  NSUserDefaults+CustomObject.h
//  Vespa
//
//  Created by Jay on 2/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface NSUserDefaults (CustomObject)

- (id)customObjectForKey:(NSString *)defaultName;
- (void)setCustomObject:(id<NSCoding>)value forKey:(NSString *)defaultName;

- (CATransform3D)CATransform3DForKey:(NSString *)defaultName;
- (void)setCATransform3D:(CATransform3D)transform forKey:(NSString *)defaultName;

- (CLLocationCoordinate2D)CLLocationCoordinate2DForKey:(NSString *)defaultName;
- (void)setCLLocationCoordinate2D:(CLLocationCoordinate2D)coord forKey:(NSString *)defaultName;

@end
