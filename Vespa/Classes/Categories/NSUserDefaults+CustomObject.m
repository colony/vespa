//
//  NSUserDefaults+CustomObject.m
//  Vespa
//
//  Created by Jay on 2/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "NSUserDefaults+CustomObject.h"

@implementation NSUserDefaults (CustomObject)

- (id)customObjectForKey:(NSString *)defaultName
{
    NSData *encodedObject = [self objectForKey:defaultName];
    if (encodedObject) {
        @try {
            return [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        } @catch (NSException *exception) {
            return nil;
        }
    }
    return nil;
}

- (void)setCustomObject:(id<NSCoding>)value forKey:(NSString *)defaultName
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:value];
    [self setObject:encodedObject forKey:defaultName];
}

- (CLLocationCoordinate2D)CLLocationCoordinate2DForKey:(NSString *)defaultName
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:defaultName];
    CLLocationCoordinate2D coord;
    [data getBytes:&coord length:sizeof(coord)];
    return coord;
}

- (void)setCLLocationCoordinate2D:(CLLocationCoordinate2D)coord forKey:(NSString *)defaultName
{
    NSData *data = [NSData dataWithBytes:&coord length:sizeof(coord)];
    [self setObject:data forKey:defaultName];
}

- (CATransform3D)CATransform3DForKey:(NSString *)defaultName
{
    NSString *serializedStr = [self objectForKey:defaultName];
    if (serializedStr) {
        if ([serializedStr isEqualToString:@"CATransform3DIdentity"]) {
            return CATransform3DIdentity;
        }
        
        NSArray *fields = [serializedStr componentsSeparatedByString:@" "];
        CATransform3D trans;
        trans.m11 = [fields[0] floatValue];
        trans.m12 = [fields[1] floatValue];
        trans.m13 = [fields[2] floatValue];
        trans.m14 = [fields[3] floatValue];
        trans.m21 = [fields[4] floatValue];
        trans.m22 = [fields[5] floatValue];
        trans.m23 = [fields[6] floatValue];
        trans.m24 = [fields[7] floatValue];
        trans.m31 = [fields[8] floatValue];
        trans.m32 = [fields[9] floatValue];
        trans.m33 = [fields[10] floatValue];
        trans.m34 = [fields[11] floatValue];
        trans.m41 = [fields[12] floatValue];
        trans.m42 = [fields[13] floatValue];
        trans.m43 = [fields[14] floatValue];
        trans.m44 = [fields[15] floatValue];
        return trans;
    }
    return CATransform3DIdentity;
}

- (void)setCATransform3D:(CATransform3D)transform forKey:(NSString *)defaultName
{
    NSString *serializedStr = CATransform3DIsIdentity(transform)
    ?   @"CATransform3DIdentity"
    : [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@",
       [self prettyFloat:transform.m11],
       [self prettyFloat:transform.m12],
       [self prettyFloat:transform.m13],
       [self prettyFloat:transform.m14],
       [self prettyFloat:transform.m21],
       [self prettyFloat:transform.m22],
       [self prettyFloat:transform.m23],
       [self prettyFloat:transform.m24],
       [self prettyFloat:transform.m31],
       [self prettyFloat:transform.m32],
       [self prettyFloat:transform.m33],
       [self prettyFloat:transform.m34],
       [self prettyFloat:transform.m41],
       [self prettyFloat:transform.m42],
       [self prettyFloat:transform.m43],
       [self prettyFloat:transform.m44]
       ];
    [self setObject:serializedStr forKey:defaultName];
}

- (NSString*)prettyFloat:(CGFloat)f
{
    if (f == 0) {
        return @"0";
    } else if (f == 1) {
        return @"1";
    } else {
        return [NSString stringWithFormat:@"%.3f", f];
    }
}

@end
