//
//  UIButton+Addition.h
//  Vespa
//
//  Created by Jiayu Zhang on 5/17/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@interface UIButton (Addition)

@property (nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;

@end
