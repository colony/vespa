//
//  UIImageView+Addition.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/25/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@interface UIImageView (Util)

- (UIColor *)getPixelColorAtPoint:(CGPoint)point;

/* The actual displayed image size by applying UIImageView's contentMode */
- (CGSize)displayedImageSize;

@end


@interface UIImageView (Geometry)

- (CGPoint)convertPointFromImage:(CGPoint)imagePoint;
- (CGRect)convertRectFromImage:(CGRect)imageRect;

- (CGPoint)convertPointFromView:(CGPoint)viewPoint;
- (CGRect)convertRectFromView:(CGRect)viewRect;

@end


//https://github.com/gurmundi7/UIImageView-AnimationCompletionBlock
//http://spin.atomicobject.com/2014/08/27/animate-images-uiimageview-completion-handler/
@interface UIImageView (AnimationCompletion)

- (void)startAnimatingWithCompletionBlock:(void (^)(BOOL success))block;

@end
