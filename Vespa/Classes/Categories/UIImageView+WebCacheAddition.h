//
//  UIImageView+WebCacheAddition.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/2/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "SDWebImageManager.h"


typedef void(^VPWebImageCompletionBlock)(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL, BOOL isLowQuality);

@interface UIImageView (WebCacheAddition)


/**
 * A utility method extend functionality of SDWebImage's WebCache category, if only one url (either low or high) is
 * provided, it will fall back to sd_setImageWithURL:, otherwise, it will behave differently as following:
 * 1) Internally, it will try to fetch both low and high quality image from the specified urls, if the completedBlock
 *    is triggered first for high-quality image, the completedBlock for low-quality image will be cancelled
 * 2) Options SDWebImageDelayPlaceholder, SDWebImageDelayPlaceholder are ignored
 * 3) Caller is responsible of setting the image via completedBlock, the impl won't
 */
- (void)vp_setImageWithLowURL:(NSURL *)lowUrl
                   lowOptions:(SDWebImageOptions)lowOptions
                      highURL:(NSURL *)highUrl
                  highOptions:(SDWebImageOptions)highOptions
             placeholderImage:(UIImage *)placeholder
                    completed:(VPWebImageCompletionBlock)completedBlock;

- (void)vp_cancelCurrentLHImageLoad;

@end
