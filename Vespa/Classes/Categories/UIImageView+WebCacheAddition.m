//
//  UIImageView+WebCacheAddition.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/2/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "UIImageView+WebCacheAddition.h"
#import "UIImageView+WebCache.h"
#import "UIView+WebCacheOperation.h"

// LH - Low & High quality
#define VP_IMAGE_OPERATION_KEY @"UIImageViewLHImageLoad"

@implementation UIImageView (WebCacheAddition)

- (void)vp_setImageWithLowURL:(NSURL *)lowUrl
                   lowOptions:(SDWebImageOptions)lowOptions
                      highURL:(NSURL *)highUrl
                  highOptions:(SDWebImageOptions)highOptions
             placeholderImage:(UIImage *)placeholder
                    completed:(VPWebImageCompletionBlock)completedBlock
{
    // If only one URL provided, fall back to SDWebImage+WebCache impl
    if (lowUrl && !highUrl) {
        [self sd_setImageWithURL:lowUrl placeholderImage:placeholder options:lowOptions completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            completedBlock(image, error, cacheType, imageURL, YES);
        }];
        return;
    } else if (highUrl && !lowUrl) {
        [self sd_setImageWithURL:highUrl placeholderImage:placeholder options:highOptions completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            completedBlock(image, error, cacheType, imageURL, NO);
        }];
        return;
    }
    
    [self vp_cancelCurrentLHImageLoad];
    
    // Ignore option SDWebImageDelayPlaceholder
    if (placeholder) {
        dispatch_main_async_safe(^{
            self.image = placeholder;
        });
    }
    
    __block BOOL highFinished = NO;
    __weak __typeof(self)wself = self;
    NSMutableArray *operationsArray = [[NSMutableArray alloc] initWithCapacity:2];
    
    id <SDWebImageOperation> operationHigh = [SDWebImageManager.sharedManager downloadImageWithURL:highUrl options:highOptions progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (!wself) return;
        dispatch_main_sync_safe(^{
            if (!wself) return;
            if (image && finished)
                highFinished = YES;
            if (image && completedBlock) {
                completedBlock(image, error, cacheType, lowUrl, NO);
            }
        });
    }];
    
    id <SDWebImageOperation> operationLow = [SDWebImageManager.sharedManager downloadImageWithURL:lowUrl options:lowOptions progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (!wself) return;
        dispatch_main_sync_safe(^{
            if (!wself) return;
            if (highFinished)
                return;
            if (image && completedBlock) {
                completedBlock(image, error, cacheType, lowUrl, YES);
            }
        });
    }];
    
    [operationsArray addObject:operationHigh];
    [operationsArray addObject:operationLow];
    [self sd_setImageLoadOperation:operationsArray forKey:VP_IMAGE_OPERATION_KEY];
}

- (void)vp_cancelCurrentLHImageLoad
{
    [self sd_cancelImageLoadOperationWithKey:VP_IMAGE_OPERATION_KEY];
}

@end
