//
//  UINavigationItem+Util.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/8/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (Util)

/**
   Make sure you understand the concept before use
   http://situee.blogspot.com/2014/10/ios-set-navigation-bar-back-button-title.html
 */
- (void)setCustomBackTitle:(NSString*)title;

- (void)setCustomTitle:(NSString*)title;
- (void)setCustomTitle:(NSString*)title textColor:(UIColor*)color font:(UIFont*)font;

- (void)setCustomLeftWithTitle:(NSString*)title textColor:(UIColor*)color font:(UIFont*)font target:(id)target action:(SEL)action;
- (void)setCustomRightWithTitle:(NSString*)title textColor:(UIColor*)color font:(UIFont*)font target:(id)target action:(SEL)action;

- (void)setCustomLeftWithView:(UIView*)view;
- (void)setCustomRightWithView:(UIView*)view;

/**
 http://stackoverflow.com/questions/19233591/custom-uibarbuttonitem-alignment-off-with-ios7/19317105#19317105
 http://stackoverflow.com/questions/18861201/uibarbuttonitem-with-custom-view-not-properly-aligned-on-ios-7-when-used-as-left
 There's a lot of problem regarding with alignment of left/right navbar with custom UI.
 Instead of doing it in a generic way, here we provide the function to tweak the alignment for convenience
 The specified space will be added to left side of leftnavitem, right side of rightnavitem.
 Negative value tweak outwards (push to sides of screen), positive value tweak inwards
 */
- (void)tweakLeftWithSpace:(CGFloat)space;
- (void)tweakRightWithSpace:(CGFloat)space;

@end
