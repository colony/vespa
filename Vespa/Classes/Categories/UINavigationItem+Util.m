//
//  UINavigationItem+Util.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/8/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "UINavigationItem+Util.h"
#import "VPUI.h"
#import "UIView+Frame.h"

#define DEFAULT_TEXT_COLOR [UIColor whiteColor]
#define DEFAULT_TITLE_FONT [VPUI fontOpenSans:18 thickness:2]
#define DEFAULT_FONT [VPUI fontOpenSans:14 thickness:2]

@implementation UINavigationItem (Util)

- (void)setCustomBackTitle:(NSString*)title
{
    self.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)setCustomTitle:(NSString*)title
{
    [self setCustomTitle:title textColor:DEFAULT_TEXT_COLOR font:DEFAULT_TITLE_FONT];
}

- (void)setCustomTitle:(NSString*)title textColor:(UIColor*)color font:(UIFont*)font
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.font = font ?: DEFAULT_TITLE_FONT;
    label.textColor = color ?: DEFAULT_TEXT_COLOR;
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    [label sizeToFit];
    
    self.titleView = label;
}

- (void)setCustomLeftWithTitle:(NSString*)title textColor:(UIColor*)color font:(UIFont*)font target:(id)target action:(SEL)action
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
    [item setTitleTextAttributes:@{
                                   NSForegroundColorAttributeName:color ? color : DEFAULT_TEXT_COLOR,
                                   NSFontAttributeName:font ? font : DEFAULT_FONT
                                   }
                        forState:UIControlStateNormal];
    self.leftBarButtonItem = item;
}

- (void)setCustomRightWithTitle:(NSString*)title textColor:(UIColor*)color font:(UIFont*)font target:(id)target action:(SEL)action
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
    [item setTitleTextAttributes:@{
                                   NSForegroundColorAttributeName:color ? color : DEFAULT_TEXT_COLOR,
                                   NSFontAttributeName:font ? font : DEFAULT_FONT
                                   }
                        forState:UIControlStateNormal];
    self.rightBarButtonItem = item;
}

- (void)setCustomLeftWithView:(UIView*)view
{
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.leftBarButtonItem = item;
}

- (void)setCustomRightWithView:(UIView*)view
{
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.rightBarButtonItem = item;
}

- (void)tweakLeftWithSpace:(CGFloat)space
{
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spaceItem.width = space;
    NSMutableArray *items = [NSMutableArray new];
    [items addObject:spaceItem];
    [items addObjectsFromArray:self.leftBarButtonItems];
    self.leftBarButtonItems = items;
}

- (void)tweakRightWithSpace:(CGFloat)space
{
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spaceItem.width = space;
    NSMutableArray *items = [NSMutableArray new];
    [items addObject:spaceItem];
    [items addObjectsFromArray:self.rightBarButtonItems];
    self.rightBarButtonItems = items;
}

@end
