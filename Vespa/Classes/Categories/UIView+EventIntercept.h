//
//  UIView+EventIntercept.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/28/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@interface UIView (EventIntercept)

/** 
 * YES if stop sending event to super view or next responder, NO otherwise.
 * This feature is particularly useful when we want to block certain events
 * from happening. e.g. we don't want to navigate to detail view page if user
 * clicks on cells in feed page
 */
@property (assign, nonatomic) BOOL stopEventPropagation;

@end
