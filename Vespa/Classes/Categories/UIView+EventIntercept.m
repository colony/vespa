//
//  UIView+EventIntercept.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/28/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "UIView+EventIntercept.h"
#import <objc/runtime.h>

@implementation UIView (EventIntercept)

- (void)setStopEventPropagation:(BOOL)enabled
{
    objc_setAssociatedObject(self, @selector(stopEventPropagation), @(enabled), OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)stopEventPropagation
{
    return [objc_getAssociatedObject(self, @selector(stopEventPropagation)) boolValue];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.stopEventPropagation)
        return;
    [super touchesBegan:touches withEvent:event];
}

@end
