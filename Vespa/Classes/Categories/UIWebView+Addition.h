//
//  UIWebView+Addition.h
//  Vespa
//
//  Created by Jiayu Zhang on 10/1/15.
//  Copyright © 2015 Colony. All rights reserved.
//


@interface UIWebView (Addition)

- (void)cleanup;

@end
