//
//  VPAccount.h
//  Vespa
//
//  Created by Jay on 11/4/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPUser.h"

//typedef NSString AccountType;
//static NSString *const kPasswordAccountType = @"PASSWORD";
//static NSString *const kFacebookAccountType = @"FACEBOOK";

@interface VPAccount : VPBaseModel<VPRKMappingProtocol,NSCopying>

//the secret id paired with data for authentication (such as email or fb id)
//@property (nonatomic, copy) NSString *accountId;

//the secret data to prove user's identity (such as password or fb accesstoken)
//@property (nonatomic, copy) NSString *accountData;

//type of authentication, the pair of data and id has different meaning per type (such as PASSWORD,FACEBOOK)
//@property (nonatomic, copy) NSString *accountType;

//user's session token
@property (nonatomic, copy) NSString *token;

@property (nonatomic, copy) NSNumber *userId;

@end
