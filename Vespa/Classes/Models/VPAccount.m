//
//  VPAccount.m
//  Vespa
//
//  Created by Jay on 11/4/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPAccount.h"

@implementation VPAccount

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"id": @"userId",
                                                  @"token": @"token"
                                                  }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPAccount *copy = [[VPAccount allocWithZone:zone] init];
    [copy setUserId:self.userId];
    [copy setToken:self.token];
    return copy;
}
@end
