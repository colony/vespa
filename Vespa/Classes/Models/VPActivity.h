//
//  VPActivity.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"

@class VPPaginator;

@interface VPActivity : VPBaseModel<VPRKMappingProtocol, NSCopying>

@property (copy, nonatomic) NSString *body;
@property (copy, nonatomic) NSDate *createTime;
@property (copy, nonatomic) NSNumber *activityId;
@property (assign, nonatomic) BOOL read;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSNumber *actorId;
@property (copy, nonatomic) NSNumber *commentId;
@property (copy, nonatomic) NSNumber *postId;
@property (copy, nonatomic) NSNumber *threadId;

@end


@interface VPActivityList : VPBaseModel<VPRKMappingProtocol, NSCopying>

@property (strong, nonatomic) VPPaginator *paginator;
@property (strong, nonatomic) NSArray *activities;
@property (copy, nonatomic) NSNumber *size;

@end
