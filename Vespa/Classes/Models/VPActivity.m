//
//  VPActivity.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPActivity.h"
#import "VPPaginator.h"


@implementation VPActivity

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"body"            : @"body",
                                                  @"create_time"     : @"createTime",
                                                  @"id"              : @"activityId",
                                                  @"read"            : @"read",
                                                  @"type"            : @"type",
                                                  @"actor_id"        : @"actorId",
                                                  @"comment_id"      : @"commentId",
                                                  @"post_id"         : @"postId",
                                                  @"thread_id"       : @"threadId"
                                                  }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPActivity *copy = [[VPActivity allocWithZone:zone] init];
    copy.read = self.read;
    copy.body = self.body;
    copy.createTime = self.createTime;
    copy.activityId = self.activityId;
    copy.type = self.type;
    copy.actorId = self.actorId;
    copy.commentId = self.commentId;
    copy.postId = self.postId;
    copy.threadId = self.threadId;
    return copy;
}

@end


@implementation VPActivityList

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{@"size": @"size"}];
    
    RKMapping *pageMapping = [VPPaginator getModelMapping];
    RKMapping *activityMapping = [VPActivity getModelMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"activities" withMapping:activityMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pagination" toKeyPath:@"paginator" withMapping:pageMapping]];
    
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPActivityList *copy = [[VPActivityList allocWithZone:zone] init];
    [copy setPaginator:[self.paginator copyWithZone:zone]];
    [copy setActivities:[[NSArray alloc] initWithArray:self.activities copyItems:YES]];
    [copy setSize:self.size];
    return copy;
}

@end
