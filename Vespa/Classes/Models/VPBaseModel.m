//
//  VPBaseModel.m
//  Vespa
//
//  Created by Jay on 10/26/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import <objc/runtime.h>


@implementation VPBaseModel

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        //super class's properties
        unsigned int superPropertyCounter = 0;
        objc_property_t *superProperties = class_copyPropertyList([self superclass], &superPropertyCounter);
        for (unsigned int i = 0; i < superPropertyCounter; i++) {
            objc_property_t property = superProperties[i];
            const char *propertyName = property_getName(property);
            NSString *key = [NSString stringWithUTF8String:propertyName];
            // need to ignore these property
            if ([key isEqualToString:@"hash"]||
                [key isEqualToString:@"superclass"]||
                [key isEqualToString:@"description"] ||
                [key isEqualToString:@"debugDescription"]) {
                continue;
            }
            //            VPLog(@"%@, %@",key,[aDecoder decodeObjectForKey:key])
            [self setValue:[aDecoder decodeObjectForKey:key] forKey:key];
        }
        free(superProperties);
        
        //self class's properties
        unsigned int propertyCounter = 0;
        objc_property_t *properties = class_copyPropertyList([self class], &propertyCounter);
        for (unsigned int i = 0; i < propertyCounter; i++) {
            objc_property_t property = properties[i];
            const char *propertyName = property_getName(property);
            NSString *key = [NSString stringWithUTF8String:propertyName];
            // need to ignore these property
            if ([key isEqualToString:@"hash"]||
                [key isEqualToString:@"superclass"]||
                [key isEqualToString:@"description"] ||
                [key isEqualToString:@"debugDescription"]) {
                continue;
            }
            //            VPLog(@"%@, %@",key,[aDecoder decodeObjectForKey:key])
            [self setValue:[aDecoder decodeObjectForKey:key] forKey:key];
        }
        free(properties);
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    //super class's properties
    unsigned int superPropertyCounter = 0;
    objc_property_t *superProperties = class_copyPropertyList([self superclass], &superPropertyCounter);
    for (unsigned int i = 0; i < superPropertyCounter; i++) {
        objc_property_t property = superProperties[i];
        const char *propertyName = property_getName(property);
        NSString *key = [NSString stringWithUTF8String:propertyName];
        // need to ignore these property
        if ([key isEqualToString:@"hash"]||
            [key isEqualToString:@"superclass"]||
            [key isEqualToString:@"description"] ||
            [key isEqualToString:@"debugDescription"]) {
            continue;
        }
        [aCoder encodeObject:[self valueForKey:key] forKey:key];
    }
    free(superProperties);
    
    //self class's properties
    unsigned int propertyCounter = 0;
    objc_property_t *properties = class_copyPropertyList([self class], &propertyCounter);
    for (unsigned int i = 0; i < propertyCounter; i++) {
        objc_property_t property = properties[i];
        const char *propertyName = property_getName(property);
        NSString *key = [NSString stringWithUTF8String:propertyName];
        // need to ignore these property
        if ([key isEqualToString:@"hash"]||
            [key isEqualToString:@"superclass"]||
            [key isEqualToString:@"description"] ||
            [key isEqualToString:@"debugDescription"]) {
            continue;
        }
        [aCoder encodeObject:[self valueForKey:key] forKey:key];
    }
    free(properties);
}

@end
