//
//  VPBasePersistModel.h
//  Vespa
//
//  Created by Jay on 10/26/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VPRKMappingProtocol.h"

@interface VPBasePersistModel : NSManagedObject

@end
