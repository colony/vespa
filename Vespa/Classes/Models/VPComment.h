//
//  VPComment.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"

@class VPGeo;
@class VPMedia;
@class VPPaginator;
@class VPUser;

@interface VPComment : VPBaseModel<VPRKMappingProtocol,NSCopying>

@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSNumber *commentId;
@property (nonatomic, copy) NSDate *createTime;
@property (nonatomic, strong) NSArray *hashtags;
@property (nonatomic, copy) VPGeo *geo;
@property (nonatomic, assign) BOOL isBadReviewed;
@property (nonatomic, copy) NSNumber *numOfDownvotes;
@property (nonatomic, copy) NSNumber *numOfUpvotes;
@property (nonatomic, copy) VPUser *owner;
@property (nonatomic, copy) VPMedia *ownerIcon;
@property (nonatomic, copy) VPComment *parentComment;
@property (nonatomic, copy) VPMedia *photo;
@property (nonatomic, copy) NSNumber *postId;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *voteStatus;

- (BOOL)isDownvote;
- (BOOL)isUpvote;
- (BOOL)isNovote;

- (BOOL)isPhotoType;

/**
 * Return the body properly displayed when the comment in parent area in UI
 * For example, we will display [image] or [gif] if the comment comes with photo media
 */
- (NSString *)properBodyForParentComment;

@end

@interface VPCommentList : VPBaseModel<VPRKMappingProtocol, NSCopying>

@property (strong, nonatomic) VPPaginator *paginator;
@property (strong, nonatomic) NSArray *comments;
@property (copy, nonatomic) NSNumber *size;

@end