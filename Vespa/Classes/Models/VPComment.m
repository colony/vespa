//
//  VPComment.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPComment.h"

#import "VPGeo.h"
#import "VPMedia.h"
#import "VPPaginator.h"
#import "VPUser.h"


@implementation VPComment

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    NSDictionary *attrMappingDict = @{
                                      @"body"           : @"body",
                                      @"create_time"    : @"createTime",
                                      @"hashtags"       : @"hashtags",
                                      @"id"             : @"commentId",
                                      @"is_bad_reviewed":@"isBadReviewed",
                                      @"num_downvotes"  : @"numOfDownvotes",
                                      @"num_upvotes"    : @"numOfUpvotes",
                                      @"post_id"        : @"postId",
                                      @"type"           : @"type",
                                      @"vote_status"    : @"voteStatus"
                                      };
    [mapping addAttributeMappingsFromDictionary:attrMappingDict];
    
    RKMapping *userMapping = [VPUser getModelMapping];
    RKMapping *geoMapping = [VPGeo getModelMapping];
    RKMapping *mediaMapping = [VPMedia getModelMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"owner"
                                                                            toKeyPath:@"owner"
                                                                          withMapping:userMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"geo"
                                                                            toKeyPath:@"geo"
                                                                          withMapping:geoMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"owner_icon"
                                                                            toKeyPath:@"ownerIcon"
                                                                          withMapping:mediaMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"photo"
                                                                            toKeyPath:@"photo"
                                                                          withMapping:mediaMapping]];

    // Create a recursive mapping to own class
    RKObjectMapping *innerMapping = [mapping copy];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"parent"
                                                                            toKeyPath:@"parentComment"
                                                                          withMapping:innerMapping]];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPComment *copy = [[VPComment allocWithZone:zone] init];
    [copy setBody:self.body];
    [copy setCommentId:self.commentId];
    [copy setCreateTime:self.createTime];
    [copy setHashtags:[[NSArray alloc] initWithArray:self.hashtags copyItems:YES]];
    [copy setGeo:[self.geo copyWithZone:zone]];
    [copy setIsBadReviewed:self.isBadReviewed];
    [copy setNumOfDownvotes:self.numOfDownvotes];
    [copy setNumOfUpvotes:self.numOfUpvotes];
    [copy setOwner:[self.owner copyWithZone:zone]];
    [copy setOwnerIcon:[self.ownerIcon copyWithZone:zone]];
    [copy setParentComment:[self.parentComment copyWithZone:zone]];
    [copy setPhoto:[self.photo copyWithZone:zone]];
    [copy setPostId:self.postId];
    [copy setType:self.type];
    return copy;
}

- (BOOL)isDownvote
{
    return [self.voteStatus isEqualToString:@"DOWNVOTE"];
}

- (BOOL)isUpvote
{
    return [self.voteStatus isEqualToString:@"UPVOTE"];
}

- (BOOL)isNovote
{
    return [self.voteStatus isEqualToString:@"NOVOTE"];
}

- (BOOL)isPhotoType
{
    return [self.type isEqualToString:@"PHOTO"];
}

- (NSString *)properBodyForParentComment
{
    if (!self.type || [self.type isEqualToString:@"SIMPLE"])
        return self.body;
    
    NSMutableString *mutableBody = [NSMutableString stringWithString:self.body];
    [mutableBody appendString:@" [image]"];
    return mutableBody;
}

@end


@implementation VPCommentList

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{@"size": @"size"}];
    
    RKMapping *pageMapping = [VPPaginator getModelMapping];
    RKMapping *commentMapping = [VPComment getModelMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"comments" withMapping:commentMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pagination" toKeyPath:@"paginator" withMapping:pageMapping]];
    
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPCommentList *copy = [[VPCommentList allocWithZone:zone] init];
    [copy setPaginator:[self.paginator copyWithZone:zone]];
    [copy setComments:[[NSArray alloc] initWithArray:self.comments copyItems:YES]];
    [copy setSize:self.size];
    return copy;
}

@end
