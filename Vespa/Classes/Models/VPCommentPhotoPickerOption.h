//
//  VPCommentPhotoPickerOption.h
//  Vespa
//
//  Created by Jiayu Zhang on 8/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"

@class VPMedia;
@class VPPaginator;

@interface VPCommentPhotoPickerOption : VPBaseModel<VPRKMappingProtocol>

@property (nonatomic, copy) NSString *coverImageUrl;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *optionId;

@end


@interface VPCommentPhotoPickerOptionItem : VPBaseModel<VPRKMappingProtocol>

@property (nonatomic, copy) NSNumber *itemId;
@property (nonatomic) VPMedia *media;

@end


@interface VPCommentPhotoPickerOptionItemList : VPBaseModel<VPRKMappingProtocol>

@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) VPPaginator *paginator;

@end

