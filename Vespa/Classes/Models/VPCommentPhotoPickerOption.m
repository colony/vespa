//
//  VPCommentPhotoPickerOption.m
//  Vespa
//
//  Created by Jiayu Zhang on 8/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentPhotoPickerOption.h"

#import "VPMedia.h"
#import "VPPaginator.h"


@implementation VPCommentPhotoPickerOption

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"cover_image_url":@"coverImageUrl",
                                                  @"id"             :@"optionId",
                                                  @"name"           :@"name"
                                                  }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return [((RKObjectMapping *)[self getModelMapping]) inverseMapping];
}

@end


@implementation VPCommentPhotoPickerOptionItem

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"id"             :@"itemId"
                                                  }];
    
    RKMapping *mediaMapping = [VPMedia getModelMapping];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"media"
                                                                           toKeyPath:@"media"
                                                                         withMapping:mediaMapping]];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return [((RKObjectMapping *)[self getModelMapping]) inverseMapping];
}

@end


@implementation VPCommentPhotoPickerOptionItemList

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    RKMapping *pageMapping = [VPPaginator getModelMapping];
    RKMapping *itemMapping = [VPCommentPhotoPickerOptionItem getModelMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"items" withMapping:itemMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pagination" toKeyPath:@"paginator" withMapping:pageMapping]];

    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return [((RKObjectMapping *)[self getModelMapping]) inverseMapping];
}

@end
