//
//  VPGeo.h
//  Vespa
//
//  Created by Jay on 1/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "VPBaseModel.h"

@interface VPGeo : VPBaseModel<VPRKMappingProtocol, NSCopying, NSCoding>

@property (nonatomic, copy) NSNumber *latitude;
@property (nonatomic, copy) NSNumber *longitude;
@property (nonatomic, copy, readwrite) CLLocation *location;

/** A human readable place name (i.e. blahblah university, restaurant, etc) */
@property (nonatomic, copy) NSString *placeName;
@property (nonatomic, copy) NSString *sublocality; //neighborhood, borough

@property (nonatomic, copy) NSString *locality; //city
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *countryCode;


/** Serialize the obj to string to pass over the network */
- (NSString *)toHttpParamValue;


/** Determine is united states */
- (BOOL)isUS;

/** Checker see if fields are empty or nil */
- (BOOL)hasSublocality;
- (BOOL)hasLocality;
- (BOOL)hasState;
- (BOOL)hasCountry;
- (BOOL)hasCountryCode;
- (BOOL)hasPlaceName;

/** See if the geo contains sufficient data for display */
- (BOOL)hasEnoughInfoToDisplay;

@end
