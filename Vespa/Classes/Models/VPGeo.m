//
//  VPGeo.m
//  Vespa
//
//  Created by Jay on 1/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGeo.h"
#import "NSString+Util.h"


static NSInteger const _kVersion = 3;

@interface VPGeo ()
{
    // Since we custom getter and setter, we need declare the ivar here
    CLLocation *_location;
}

@end


@implementation VPGeo

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"name"        : @"placeName",
                                                  @"neighborhood": @"sublocality",
                                                  @"city"        : @"locality",
                                                  @"state"       : @"state",
                                                  @"country"     : @"country",
                                                  @"country_code": @"countryCode",
                                                  @"lat"         : @"latitude",
                                                  @"lon"         : @"longitude"
                                                  }];
    
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (CLLocation *)location
{
    if (!_location) {
        if (_latitude && _longitude) {
            CLLocationDegrees lat = [self.latitude doubleValue];
            CLLocationDegrees lon = [self.longitude doubleValue];
            _location = [[CLLocation alloc]initWithLatitude:lat longitude:lon];
        }
    }
    return _location;
}

- (void)setLocation:(CLLocation *)location
{
    _location = location;
    _latitude = @(_location.coordinate.latitude);
    _longitude = @(_location.coordinate.longitude);
}

- (NSString *)toHttpParamValue
{
    if (![NSString isEmptyString:self.country]) {
        // If we have at least country info, inject all extra geo info into the string
        return [NSString stringWithFormat:@"%td|%@|%@|%@|%@|%@|%@|%@|%@",
                _kVersion,
                [NSString stringWithFormat:@"%.7f", [self.latitude doubleValue]],
                [NSString stringWithFormat:@"%.7f", [self.longitude doubleValue]],
                self.placeName?:@"",
                self.sublocality?:@"",
                self.locality?:@"",
                self.state?:@"",
                self.country,
                self.countryCode];
    } else {
        return [NSString stringWithFormat:@"%td|%@|%@", _kVersion, self.latitude, self.longitude];
    }
}

- (BOOL)isUS
{
    return [self.countryCode isEqualToString:@"US"];
}


#pragma mark - delegate
- (id)copyWithZone:(NSZone *)zone
{
    VPGeo *copy = [[VPGeo allocWithZone:zone] init];
    copy.latitude = self.latitude;
    copy.longitude = self.longitude;
    copy.placeName = self.placeName;
    copy.sublocality = self.sublocality;
    copy.locality = self.locality;
    copy.state = self.state;
    copy.country = self.country;
    copy.countryCode = self.countryCode;
    return copy;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.latitude = [aDecoder decodeObjectForKey:@"latitude"];
        self.longitude = [aDecoder decodeObjectForKey:@"longitude"];
        self.placeName = [aDecoder decodeObjectForKey:@"placeName"];
        self.sublocality = [aDecoder decodeObjectForKey:@"sublocality"];
        self.locality = [aDecoder decodeObjectForKey:@"locality"];
        self.state = [aDecoder decodeObjectForKey:@"state"];
        self.country = [aDecoder decodeObjectForKey:@"country"];
        self.countryCode = [aDecoder decodeObjectForKey:@"countryCode"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.latitude forKey:@"latitude"];
    [aCoder encodeObject:self.longitude forKey:@"longitude"];
    [aCoder encodeObject:self.placeName forKey:@"placeName"];
    [aCoder encodeObject:self.sublocality forKey:@"sublocality"];
    [aCoder encodeObject:self.locality forKey:@"locality"];
    [aCoder encodeObject:self.state forKey:@"state"];
    [aCoder encodeObject:self.country forKey:@"country"];
    [aCoder encodeObject:self.countryCode forKey:@"countryCode"];
}

- (BOOL)hasSublocality
{
    return ![NSString isEmptyString:self.sublocality];
}

- (BOOL)hasLocality
{
    return ![NSString isEmptyString:self.locality];
}

- (BOOL)hasState
{
    return ![NSString isEmptyString:self.state];
}

- (BOOL)hasCountry
{
    return ![NSString isEmptyString:self.country];
}

- (BOOL)hasCountryCode
{
    return ![NSString isEmptyString:self.countryCode];
}

- (BOOL)hasPlaceName
{
    return ![NSString isEmptyString:self.placeName];
}

- (BOOL)hasEnoughInfoToDisplay
{
    // At least, we need display the full country name if data is inperfect
    return [self hasCountry] || [self hasPlaceName];
}

@end
