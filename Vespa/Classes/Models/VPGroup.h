//
//  VPGroup.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"

@class VPPaginator;

@interface VPGroup : VPBaseModel<VPRKMappingProtocol,NSCopying>

@property (nonatomic, copy) NSString *coverImageUrl;
@property (nonatomic, copy) NSNumber *groupId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *numOfDownVotes;
@property (nonatomic, copy) NSNumber *numOfUpVotes;
@property (nonatomic, copy) NSString *ruleImageUrl;
@property (nonatomic, copy) NSString *ruleText;

//
// RULE related properties
//
@property (nonatomic) NSArray *allowedPostTypes;
@property (nonatomic, copy) NSString *threadType;
@property (nonatomic, assign) BOOL isReversed;

/** 
 * Populate the allowedPostTypes array from an int which is computed by turn on the bit whose
 * position corresponds to a PostType. Note, we strictly follow the server's logic for the code
 * computation
 */
- (void)setAllowedPostTypesFromCode:(NSInteger)code;

@end

@interface VPGroupList : VPBaseModel<VPRKMappingProtocol, NSCopying>

@property (strong, nonatomic) VPPaginator *paginator;
@property (strong, nonatomic) NSArray *groups;
@property (copy, nonatomic) NSNumber *size;

@end
