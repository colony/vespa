//
//  VPGroup.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroup.h"
#import "VPPaginator.h"
#import "VPUtil.h"


@implementation VPGroup

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"cover_image_url"            : @"coverImageUrl",
                                                  @"id"                         : @"groupId",
                                                  @"name"                       : @"name",
                                                  @"num_downvotes"              : @"numOfDownVotes",
                                                  @"num_upvotes"                : @"numOfUpVotes",
                                                  @"rule_description_image_url" : @"ruleImageUrl",
                                                  @"rule_description_text"      : @"ruleText",
                                                  @"allowed_post_types"         : @"allowedPostTypes",
                                                  @"thread_type"                : @"threadType",
                                                  @"is_reversed"                : @"isReversed"
                                                  }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return [((RKObjectMapping *)[self getModelMapping]) inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPGroup *copy = [[VPGroup allocWithZone:zone] init];
    [copy setCoverImageUrl:self.coverImageUrl];
    [copy setGroupId:self.groupId];
    [copy setName:self.name];
    [copy setNumOfDownVotes:self.numOfDownVotes];
    [copy setNumOfUpVotes:self.numOfUpVotes];
    [copy setRuleImageUrl:self.ruleImageUrl];
    [copy setRuleText:self.ruleText];
    [copy setAllowedPostTypes:[[NSArray alloc] initWithArray:self.allowedPostTypes copyItems:YES]];
    [copy setIsReversed:self.isReversed];
    return copy;
}

- (void)setAllowedPostTypesFromCode:(NSInteger)code
{
    NSMutableArray *ary = [NSMutableArray new];
    
    // AUDIO,CARD,LINK,PHOTO,TEXT,VIDEO
    NSArray *supportedPostTypes = [VPUtil supportedPostTypes];
    for (int i = 0; i < supportedPostTypes.count; i++) {
        if (code & (1 << i)) {
            [ary addObject:supportedPostTypes[i]];
        }
    }
    self.allowedPostTypes = [NSArray arrayWithArray:ary];
}

@end


@implementation VPGroupList

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"size": @"size"
                                                  }];
    
    RKMapping *pageMapping = [VPPaginator getModelMapping];
    RKMapping *groupMapping = [VPGroup getModelMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"groups" withMapping:groupMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pagination" toKeyPath:@"paginator" withMapping:pageMapping]];
    
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPGroupList *copy = [[VPGroupList allocWithZone:zone] init];
    [copy setGroups:[[NSArray alloc] initWithArray:self.groups copyItems:YES]];
    [copy setPaginator:[self.paginator copyWithZone:zone]];
    [copy setSize:self.size];
    return copy;
}

@end

