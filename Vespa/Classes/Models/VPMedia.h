//
//  VPMedia.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"

@interface VPMedia : VPBaseModel<VPRKMappingProtocol,NSCopying>

/*
 AUDIO, AVATAR, ICON, PHOTO, TEXT
 */
@property (nonatomic, copy) NSString *type;

/*
 AUDIO type
 */
@property (nonatomic, assign) NSInteger audioDuration; // ms
@property (nonatomic, copy) NSString *audioFormat;
@property (nonatomic, copy) NSString *audioUrl;

/*
 AVATAR type
 */
//@property (nonatomic, copy) NSString *avatar;
//@property (nonatomic, copy) NSString *avatarThumbnail;


/*
 ICON type
 NOTE, it depends on how other codes use the properties here, general guidance is that systemId has preference over iconUrl, 
 if systemId can't be resolved, iconUrl will be lookedup
 */
@property (nonatomic, copy) NSString *iconColorHex;
@property (nonatomic, copy) NSString *iconSystemId;
@property (nonatomic, copy) NSString *iconUrl;


/*
 PHOTO type
 */
@property (nonatomic, copy) NSString *photoFormat;
@property (nonatomic, assign) NSInteger photoHeight;
@property (nonatomic, assign) NSInteger photoWidth;
@property (nonatomic, copy) NSString *photoUrl;
@property (nonatomic, assign) BOOL photoUploaded;
@property (nonatomic, copy) NSString *photoThumbnailUrl;
@property (nonatomic, assign) NSInteger photoThumbnailHeight;
@property (nonatomic, assign) NSInteger photoThumbnailWidth;


/*
 TEXT type
 */
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *textBackgroundColor;
@property (nonatomic, copy) NSString *textColor;
@property (nonatomic, copy) NSString *textFormat;

/*
 LINK type
 */
@property (nonatomic, copy) NSString *linkUrl;
@property (nonatomic, copy) NSString *linkHost;
@property (nonatomic, copy) NSString *linkTitle;
@property (nonatomic, copy) NSString *linkDescription;
@property (nonatomic, copy) NSString *linkEmbedItemFormat;
@property (nonatomic, assign) NSInteger linkEmbedItemHeight;
@property (nonatomic, assign) NSInteger linkEmbedItemWidth;
@property (nonatomic, copy) NSString *linkEmbedItemUrl;

/*
 VIDEO type
 */
@property (nonatomic, copy) NSString *videoFormat;
@property (nonatomic, copy) NSString *videoUrl;



- (BOOL)isGif;

@end
