//
//  VPMedia.m
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPMedia.h"

@implementation VPMedia

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"audio_format":         @"audioFormat",
                                                  @"audio_duration":       @"audioDuration",
                                                  @"audio_url":            @"audioUrl",
                                                  
                                                  @"icon_color_hex":       @"iconColorHex",
                                                  @"icon_system_id":       @"iconSystemId",
                                                  @"icon_url":             @"iconUrl",
                                                  
                                                  @"photo_url":            @"photoUrl",
                                                  @"photo_h":              @"photoHeight",
                                                  @"photo_w":              @"photoWidth",
                                                  @"photo_format":         @"photoFormat",
                                                  @"photo_uploaded":       @"photoUploaded",
                                                  @"photo_thumbnail_url":  @"photoThumbnailUrl",
                                                  @"photo_thumbnail_h":    @"photoThumbnailHeight",
                                                  @"photo_thumbnail_w":    @"photoThumbnailWidth",
                                                  
                                                  @"text":                  @"text",
                                                  @"text_background_color": @"textBackgroundColor",
                                                  @"text_color":            @"textColor",
                                                  @"text_format":           @"textFormat",
                                                  
                                                  @"link_url":              @"linkUrl",
                                                  @"link_host":             @"linkHost",
                                                  @"link_title":            @"linkTitle",
                                                  @"link_description":      @"linkDescription",
                                                  @"link_embed_item_url":   @"linkEmbedItemUrl",
                                                  @"link_embed_item_h":     @"linkEmbedItemHeight",
                                                  @"link_embed_item_w":     @"linkEmbedItemWidth",
                                                  @"link_embed_item_format":@"linkEmbedItemFormat",
                                                  
                                                  @"video_url":             @"videoUrl",
                                                  @"video_format":          @"videoFormat",
                                                  
                                                  @"type":                  @"type"
                                                  }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPMedia *copy = [[VPMedia allocWithZone:zone] init];
    [copy setType:self.type];
    if ([self.type isEqualToString:@"PHOTO"]) {
        [copy setPhotoUrl:self.photoUrl];
        [copy setPhotoFormat:self.photoFormat];
        [copy setPhotoHeight:self.photoHeight];
        [copy setPhotoWidth:self.photoWidth];
        [copy setPhotoThumbnailUrl:self.photoThumbnailUrl];
        [copy setPhotoThumbnailHeight:self.photoThumbnailHeight];
        [copy setPhotoThumbnailWidth:self.photoThumbnailWidth];
        [copy setPhotoUploaded:self.photoUploaded];
    }
    else if ([self.type isEqualToString:@"ICON"]) {
        [copy setIconColorHex:self.iconColorHex];
        [copy setIconSystemId:self.iconSystemId];
        [copy setIconUrl:self.iconUrl];
    }
    else if ([self.type isEqualToString:@"AUDIO"]) {
        [copy setAudioDuration:self.audioDuration];
        [copy setAudioFormat:self.audioFormat];
        [copy setAudioUrl:self.audioUrl];
    }
    else if ([self.type isEqualToString:@"TEXT"]) {
        [copy setText:self.text];
        [copy setTextBackgroundColor:self.textBackgroundColor];
        [copy setTextColor:self.textColor];
        [copy setTextFormat:self.textFormat];
    }
    else if ([self.type isEqualToString:@"VIDEO"]) {
        [copy setVideoFormat:self.videoFormat];
        [copy setVideoUrl:self.videoUrl];
    }
    return copy;
}

- (BOOL)isGif
{
    return [self.type isEqualToString:@"PHOTO"] && [self.photoFormat isEqualToString:@"GIF"];
}

@end
