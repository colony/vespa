//
//  VPPaginator.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"

@interface VPPaginator : VPBaseModel<VPRKMappingProtocol, NSCopying>

@property (nonatomic, copy) NSString *nextPageUrl;
@property (nonatomic, copy) NSString *previousPageUrl;

@end
