//
//  VPPaginator.m
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPPaginator.h"

@implementation VPPaginator

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                               @"next_url": @"nextPageUrl",
                                               @"prev_url": @"previousPageUrl"
                                               }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPPaginator *copy = [[VPPaginator allocWithZone:zone] init];
    [copy setNextPageUrl:self.nextPageUrl];
    [copy setPreviousPageUrl:self.previousPageUrl];
    return copy;
}

@end

