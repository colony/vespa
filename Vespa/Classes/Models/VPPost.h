//
//  VPPost.h
//  Vespa
//
//  Created by Jiayu Zhang on 11/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"

@class VPUser;
@class VPMedia;
@class VPGeo;
@class VPPaginator;
@class VPThread;


@interface VPPost : VPBaseModel<VPRKMappingProtocol,NSCopying>

@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSString *clazz;
@property (nonatomic) NSDate *createTime;
@property (nonatomic, copy) NSString *featuredTitle;
@property (nonatomic) VPGeo *geo;
@property (nonatomic, copy) NSNumber *groupId;
@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, strong) NSArray *hashtags;
@property (nonatomic, assign) BOOL isAnnouncement;
@property (nonatomic, assign) BOOL isBadReviewed;
@property (nonatomic, assign) BOOL isBlocked;
@property (nonatomic, copy) NSNumber *numOfComments;
@property (nonatomic, copy) NSNumber *numOfDownvotes;
@property (nonatomic, copy) NSNumber *numOfUpvotes;
@property (nonatomic) VPUser *owner;
@property (nonatomic, copy) NSNumber *postId;
@property (nonatomic, copy) NSNumber *threadId;
@property (nonatomic, copy) NSString *threadAnnotation;
@property (nonatomic, copy) NSString *type;
@property (nonatomic) NSDate *updateTime;
@property (nonatomic, copy) NSString *voteStatus;

// Media properties are populated corresponding to postType
@property (nonatomic) VPMedia *audioMedia;
@property (nonatomic) VPMedia *audioBackgroundMedia;
@property (nonatomic) VPMedia *linkMedia;
@property (nonatomic) VPMedia *photoMedia;
@property (nonatomic) VPMedia *textMedia;
@property (nonatomic) VPMedia *videoMedia;
@property (nonatomic) VPMedia *videoThumbnailMedia;

- (BOOL)hasLinkMedia;
- (BOOL)isAudioType;
- (BOOL)isCardType;
- (BOOL)isLinkType;
- (BOOL)isPhotoType;
- (BOOL)isTextType;
- (BOOL)isVideoType;

- (BOOL)isPublicClazz;
- (BOOL)isThreadClazz;

- (BOOL)isThreadRoot;

- (BOOL)isDownvote;
- (BOOL)isUpvote;
- (BOOL)isNovote;

/** 
 * Convert postType to mediaType that could be recognized by [VPUtil presentMediaViewController:mediaType:]
 * Return AUDIO,CARD,LINK,PHOTO,TEXT,VIDEO for convenience 
 */
- (NSString *)mediaType;

@end


@interface VPPostList : VPBaseModel<VPRKMappingProtocol, NSCopying>

// Only set if the postlist is by the hashtag
@property (copy, nonatomic) NSString *hashtag;
@property (strong, nonatomic) VPPaginator *paginator;
@property (strong, nonatomic) NSArray *posts;
@property (copy, nonatomic) NSNumber *size;

// Only set if the postList is a thread
@property (nonatomic) VPThread *thread;

@end
