//
//  VPPost.m
//  Vespa
//
//  Created by Jiayu Zhang on 11/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPPost.h"

#import "NSString+Util.h"
#import "VPGeo.h"
#import "VPMedia.h"
#import "VPPaginator.h"
#import "VPThread.h"
#import "VPUser.h"

@implementation VPPost

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"body"           : @"body",
                                                  @"clazz"          : @"clazz",
                                                  @"create_time"    : @"createTime",
                                                  @"featured_title" : @"featuredTitle",
                                                  @"group_id"       : @"groupId",
                                                  @"group_name"     : @"groupName",
                                                  @"hashtags"       : @"hashtags",
                                                  @"is_announcement": @"isAnnouncement",
                                                  @"is_bad_reviewed": @"isBadReviewed",
                                                  @"is_blocked"     : @"isBlocked",
                                                  @"num_comments"   : @"numOfComments",
                                                  @"num_downvotes"  : @"numOfDownvotes",
                                                  @"num_upvotes"    : @"numOfUpvotes",
                                                  @"id"             : @"postId",
                                                  @"thread_id"      : @"threadId",
                                                  @"thread_annotation":@"threadAnnotation",
                                                  @"type"           : @"type",
                                                  @"update_time"    : @"updateTime",
                                                  @"vote_status"    : @"voteStatus"
                                                  }];

    RKMapping *userMapping = [VPUser getModelMapping];
    RKMapping *mediaMapping = [VPMedia getModelMapping];
    RKMapping *geoMapping = [VPGeo getModelMapping];

    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"audio"
                                                                            toKeyPath:@"audioMedia"
                                                                          withMapping:mediaMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"audio_background"
                                                                            toKeyPath:@"audioBackgroundMedia"
                                                                          withMapping:mediaMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"link"
                                                                            toKeyPath:@"linkMedia"
                                                                          withMapping:mediaMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"photo"
                                                                            toKeyPath:@"photoMedia"
                                                                          withMapping:mediaMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"text"
                                                                            toKeyPath:@"textMedia"
                                                                          withMapping:mediaMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"video"
                                                                            toKeyPath:@"videoMedia"
                                                                          withMapping:mediaMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"video_thumbnail"
                                                                            toKeyPath:@"videoThumbnailMedia"
                                                                          withMapping:mediaMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"owner"
                                                                            toKeyPath:@"owner"
                                                                          withMapping:userMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"geo"
                                                                            toKeyPath:@"geo"
                                                                          withMapping:geoMapping]];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPPost *copy = [[VPPost allocWithZone:zone] init];
    [copy setBody:self.body];
    [copy setClazz:self.clazz];
    [copy setCreateTime:self.createTime];
    [copy setHashtags:[[NSArray alloc] initWithArray:self.hashtags copyItems:YES]];
    [copy setIsAnnouncement:self.isAnnouncement];
    [copy setIsBadReviewed:self.isBadReviewed];
    [copy setIsBlocked:self.isBlocked];
    [copy setGeo:[self.geo copyWithZone:zone]];
    [copy setNumOfComments:self.numOfComments];
    [copy setNumOfDownvotes:self.numOfDownvotes];
    [copy setNumOfUpvotes:self.numOfUpvotes];
    [copy setAudioMedia:[self.audioMedia copyWithZone:zone]];
    [copy setAudioBackgroundMedia:[self.audioBackgroundMedia copyWithZone:zone]];
    [copy setLinkMedia:[self.linkMedia copyWithZone:zone]];
    [copy setPhotoMedia:[self.photoMedia copyWithZone:zone]];
    [copy setTextMedia:[self.textMedia copyWithZone:zone]];
    [copy setVideoMedia:[self.videoMedia copyWithZone:zone]];
    [copy setVideoThumbnailMedia:[self.videoThumbnailMedia copyWithZone:zone]];
    [copy setOwner:[self.owner copyWithZone:zone]];
    [copy setPostId:self.postId];
    [copy setThreadAnnotation:self.threadAnnotation];
    [copy setThreadId:self.threadId];
    [copy setType:self.type];
    [copy setUpdateTime:self.updateTime];
    [copy setVoteStatus:self.voteStatus];
    return copy;
}

- (BOOL)isAudioType
{
    return [self.type isEqualToString:@"AUDIO"];
}

- (BOOL)isCardType
{
    return [self.type isEqualToString:@"CARD"];
}

- (BOOL)isLinkType
{
    return [self.type isEqualToString:@"LINK"];
}

- (BOOL)isPhotoType
{
    return [self.type isEqualToString:@"PHOTO"] || [self.type isEqualToString:@"SIMPLE"];
}

- (BOOL)isTextType
{
    return [self.type isEqualToString:@"TEXT"];
}

- (BOOL)isVideoType
{
    return [self.type isEqualToString:@"VIDEO"];
}

- (BOOL)isPublicClazz
{
    return [self.clazz isEqualToString:@"PUBLIC"];
}

- (BOOL)isThreadClazz
{
    return [self.clazz isEqualToString:@"THREAD"];
}

- (BOOL)isThreadRoot
{
    return [self isPublicClazz] && (self.threadId != nil);
}

- (BOOL)isDownvote
{
    return [self.voteStatus isEqualToString:@"DOWNVOTE"];
}

- (BOOL)isUpvote
{
    return [self.voteStatus isEqualToString:@"UPVOTE"];
}

- (BOOL)isNovote
{
    return [self.voteStatus isEqualToString:@"NOVOTE"];
}

- (BOOL)hasLinkMedia
{
    return [self isLinkType] && ![NSString isEmptyString:self.linkMedia.linkEmbedItemUrl];
}

- (NSString *)mediaType
{
    if ([self isPhotoType])
        return @"PHOTO";
    return self.type;
}

@end


@implementation VPPostList

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"hashtag"         : @"hashtag",
                                                  @"size"            : @"size"
                                                  }];
    
    RKMapping *pageMapping = [VPPaginator getModelMapping];
    RKMapping *postMapping = [VPPost getModelMapping];
    RKMapping *postThreadMapping = [VPThread getModelMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"posts" withMapping:postMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pagination" toKeyPath:@"paginator" withMapping:pageMapping]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"thread" toKeyPath:@"thread" withMapping:postThreadMapping]];
    
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPPostList *copy = [[VPPostList allocWithZone:zone] init];
    [copy setHashtag:self.hashtag];
    [copy setPaginator:[self.paginator copyWithZone:zone]];
    [copy setPosts:[[NSArray alloc] initWithArray:self.posts copyItems:YES]];
    [copy setSize:self.size];
    return copy;
}

@end
