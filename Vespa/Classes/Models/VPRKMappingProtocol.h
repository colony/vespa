//
//  VPRKMappingProtocol.h
//  Vespa
//
//  Created by Jay on 10/26/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@protocol VPRKMappingProtocol <NSObject>

+ (RKMapping *)getModelMapping;
+ (RKMapping *)getModelInverseMapping;

@end
