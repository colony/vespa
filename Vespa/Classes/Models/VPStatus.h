//
//  VPStatus.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"


typedef NS_ENUM(NSInteger, VPStatusUpdate)
{
    kVPStatusUpdate = 0,
    kVPStatusPointsUpdate,
    kVPStatusPointsIncr,
    kVPStatusUnreadNotificationsUpdate
};


@interface VPStatus : VPBaseModel<VPRKMappingProtocol>

@property (nonatomic, copy) NSNumber *numUnreadActivities;
@property (nonatomic, copy) NSNumber *numUnreadAnnouncements;
@property (nonatomic, copy) NSNumber *points;

/** 
 * The impl uses Key-Value Coding, make sure the specified propertyKey conforms to KVC
 * Returns milliseconds (long) since epoch
 */
- (long long)syncTimeOf:(NSString *)key;
- (void)setValue:(id)value forKey:(NSString *)key syncTime:(long long)syncTime;

- (NSInteger)computeApplicationBadgeNumber;

@end
