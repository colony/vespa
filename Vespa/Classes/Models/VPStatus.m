//
//  VPStatus.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPStatus.h"

@interface VPStatus ()

@property (assign, nonatomic) long long numUnreadActivitiesSyncTime;
@property (assign, nonatomic) long long numUnreadAnnouncementsSyncTime;
@property (assign, nonatomic) long long pointsSyncTime;

@end


@implementation VPStatus

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"num_unread_activities"   : @"numUnreadActivities",
                                                  @"num_unread_announcements": @"numUnreadAnnouncements",
                                                  @"points"                  : @"points"
                                                  }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (long long)syncTimeOf:(NSString *)key
{
    NSNumber *val = [self valueForKey:[key stringByAppendingString:@"SyncTime"]];
    return [val longLongValue];
}

- (void)setValue:(id)value forKey:(NSString *)key syncTime:(long long)syncTime
{
    [self setValue:value forKey:key];
    [self setValue:@(syncTime) forKey:[key stringByAppendingString:@"SyncTime"]];
}

- (NSInteger)computeApplicationBadgeNumber
{
    return [self.numUnreadActivities integerValue] + [self.numUnreadAnnouncements integerValue];
}

//- (id)copyWithZone:(NSZone *)zone
//{
//    VPStatus *copy = [[VPStatus allocWithZone:zone] init];
//    [copy setNumUnreadNotifications:self.numUnreadNotifications];
//    [copy setPoints:self.points];
//    [copy setLastSyncTime:[[NSMutableDictionary alloc] initWithDictionary:self.lastSyncTime]];
//    return copy;
//}

@end
