//
//  VPThread.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"


@interface VPThread : VPBaseModel<VPRKMappingProtocol,NSCopying>

@property (nonatomic, copy) NSString *actionTerm;
@property (nonatomic) NSArray *allowedPostTypes;
@property (nonatomic, copy) NSString *data;
@property (nonatomic, copy) NSNumber *threadId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *type;

/**
 * Auxiliary thread data are returned as a serialized string, internally, we deserialized it into a
 * NSDictionary, the caller should query the correct key for each type, details consult server doc
 * Available keys:
 *   DECK_ID
 *   OWNER_ID
 *   WIN_POST_ID
 */
- (NSString *)getThreadData:(NSString *)key;

- (void)setThreadData:(NSString *)key value:(NSString *)value;

@end