//
//  VPThread.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPThread.h"


@implementation VPThread
{
    NSMutableDictionary *_dataDict;
}

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"action_term"        : @"actionTerm",
                                                  @"allowed_post_types" : @"allowedPostTypes",
                                                  @"data"               : @"data",
                                                  @"id"                 : @"threadId",
                                                  @"title"              : @"title",
                                                  @"type"               : @"type"
                                                  }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPThread *copy = [[VPThread allocWithZone:zone] init];
    [copy setActionTerm:self.actionTerm];
    [copy setAllowedPostTypes:[[NSArray alloc] initWithArray:self.allowedPostTypes copyItems:YES]];
    [copy setData:self.data];
    [copy setThreadId:self.threadId];
    [copy setTitle:self.title];
    [copy setType:self.type];
    return copy;
}

- (NSString *)getThreadData:(NSString *)key
{
    if (!self.data)
        return nil;
    
    if (!_dataDict) {
        _dataDict = [NSMutableDictionary new];
        // Deserialize the thread data based on type
        if ([self.type isEqualToString:@"CARD_GAME"]) {
            // 1|DECK_ID|OWNER_ID|WIN_POST_ID
            NSArray *tokens = [self.data componentsSeparatedByString:@"|"];
            [_dataDict setValue:tokens[1] forKey:@"DECK_ID"];
            [_dataDict setValue:tokens[2] forKey:@"OWNER_ID"];
            [_dataDict setValue:tokens[3] forKey:@"WIN_POST_ID"];
        }
    }
    return [_dataDict objectForKey:key];
}

- (void)setThreadData:(NSString *)key value:(NSString *)value
{
    if (!_dataDict) {
        _dataDict = [NSMutableDictionary new];
    }
    [_dataDict setObject:value forKey:key];
}

@end