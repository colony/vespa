//
//  VPUser.h
//  Vespa
//
//  Created by Jay on 11/4/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseModel.h"
#import "VPRKMappingProtocol.h"

@interface VPUser : VPBaseModel<VPRKMappingProtocol,NSCopying>

//@property (nonatomic, copy) NSString *avatar;
//@property (nonatomic, copy) NSString *avatarThumbnail;
@property (nonatomic, copy) NSDate *createTime;
//@property (nonatomic, copy) NSNumber *numOfAllPosts;
//@property (nonatomic, copy) NSNumber *numOfLikePosts;
//@property (nonatomic, copy) NSNumber *numOfStingPosts;
//@property (nonatomic, copy) NSString *screenName;
//@property (nonatomic, copy) NSDate *updateTime;
@property (nonatomic, copy) NSNumber *userId;

@end

