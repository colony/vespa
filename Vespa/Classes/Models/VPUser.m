//
//  VPUser.m
//  Vespa
//
//  Created by Jay on 11/4/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPUser.h"

@implementation VPUser

+ (RKMapping *)getModelMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{
//                                                  @"avatar": @"avatar",
//                                                  @"avatar_thumbnail": @"avatarThumbnail",
//                                                  @"create_time": @"createTime",
                                                  @"id": @"userId"
//                                                  @"num_likes": @"numOfLikePosts",
//                                                  @"num_posts": @"numOfAllPosts",
//                                                  @"num_stings": @"numOfStingPosts",
//                                                  @"screen_name": @"screenName",
//                                                  @"update_time": @"updateTime"
                                                  }];
    return mapping;
}

+ (RKMapping *)getModelInverseMapping
{
    return  [((RKObjectMapping *)[self getModelMapping])inverseMapping];
}

- (id)copyWithZone:(NSZone *)zone
{
    VPUser *copy = [[VPUser allocWithZone:zone] init];
    [copy setCreateTime:self.createTime];
    [copy setUserId:self.userId];
    return copy;
}

@end
