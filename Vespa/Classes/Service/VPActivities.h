//
//  VPActivities.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@class VPActivityList;

@interface VPActivities : NSObject

+ (NSOperation *)fetchActivitiesWithUrl: (NSString *)url
                                  param: (NSDictionary *)params
                                success: (void (^)(VPActivityList *actList))success
                                failure: (void (^)(NSError *error))failure;

+ (NSOperation *)ackActivityWithId: (NSNumber *)activityId;

@end
