//
//  VPActivities.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPActivities.h"

#import "VPAPIClient.h"

@implementation VPActivities

+ (NSOperation *)fetchActivitiesWithUrl: (NSString *)url
                                  param: (NSDictionary *)params
                                success: (void (^)(VPActivityList *actList))success
                                failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    if (url) {
        return [client fetchActivitiesWithUrl:url success:success failure:failure];
    } else {
        return [client fetchActivitiesWithParam:params success:success failure:failure];
    }
}

+ (NSOperation *)ackActivityWithId: (NSNumber *)activityId
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client ackActivityWithId:activityId success:nil failure:^(NSError *error) {
        VPLog(@"Fail to ack activity: %@", [error localizedDescription]);
    }];
}

@end
