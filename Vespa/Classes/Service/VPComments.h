//
//  VPComments.h
//  Vespa
//
//  Created by Jiayu Zhang on 7/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@class VPComment;
@class VPCommentList;

@interface VPComments : NSObject

+ (NSOperation *)createCommentWithPostId: (NSNumber *)postId
                         parentCommentId: (NSNumber *)parentCommentId
                                    body: (NSString *)body
                 photoPickerOptionItemId: (NSNumber *)itemId
                                   image: (UIImage *)img
                                 success: (void (^)(VPComment *comment))success
                                 failure: (void (^)(NSError *error))failure;

+ (NSOperation *)fetchCommentsWithPostId: (NSNumber *)postId
                                     url: (NSString *)url
                                   param: (NSDictionary *)params
                                 success: (void (^)(VPCommentList *commentList))success
                                 failure: (void (^)(NSError *error))failure;

+ (NSOperation *)voteCommentWithId:(NSNumber *)commentId
                            status:(VPVoteStatus)status
                           success:(void (^)(void))success
                           failure:(void (^)(NSError *error))failure;

@end
