//
//  VPComments.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPComments.h"

#import "VPAPIClient.h"
#import "VPComment.h"
#import "VPConfigManager.h"
#import "VPLocationManager.h"


@implementation VPComments

+ (NSOperation *)createCommentWithPostId: (NSNumber *)postId
                         parentCommentId: (NSNumber *)parentCommentId
                                    body: (NSString *)body
                 photoPickerOptionItemId: (NSNumber *)itemId
                                   image: (UIImage *)img
                                 success: (void (^)(VPComment *comment))success
                                 failure: (void (^)(NSError *error))failure
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:postId forKey:@"postId"];
    if (img)
        [param setObject:@(YES) forKey:@"hasImg"];
    if (itemId)
        [param setObject:itemId forKey:@"optionItemId"];
    [VPReports reportEvent:kVPEventCommentPost withParam:param mode:kVPEventModeOnce];
    
    NSData *imgData = nil;
    if (img) {
        float q = [[VPConfigManager sharedInstance] getFloat:kVPConfigJPEGCompressionQuality];
        imgData = UIImageJPEGRepresentation(img, q);
    }
    
    NSString *type = (imgData == nil && itemId == nil) ? @"SIMPLE" : @"PHOTO";
    
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client createCommentWithType:type
                                  postId:postId
                         parentCommentId:parentCommentId
                                    body:body
                                     geo:[[VPLocationManager sharedInstance] currentGeo]
                 photoPickerOptionItemId:itemId
                                   photo:imgData
                               photoSize:(imgData != nil ? [imgData length] : 0)
                             photoHeight:(img != nil ? (img.size.height * img.scale) : 0)
                              photoWidth:(img != nil ? (img.size.width * img.scale) : 0)
                             photoFormat:(imgData != nil ? @"JPG" : nil)
                                 success:success
                                 failure:failure];
}

+ (NSOperation *)fetchCommentsWithPostId: (NSNumber *)postId
                                     url: (NSString *)url
                                   param: (NSDictionary *)params
                                success : (void (^)(VPCommentList *commentList))success
                                 failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    if (!url) {
        NSMutableDictionary *mparams;
        if (!params) {
            mparams = [[NSMutableDictionary alloc]initWithObjectsAndKeys:postId, @"post_id", nil];
        } else {
            mparams = [[NSMutableDictionary alloc]initWithDictionary:params];
            [mparams setValue:postId forKey:@"post_id"];
        }
        return [client fetchCommentsWithParam:mparams success:success failure:failure];
    } else {
        return [client fetchCommentsWithUrl:url success:success failure:failure];
    }
}

+ (NSOperation *)voteCommentWithId:(NSNumber *)commentId
                            status:(VPVoteStatus)status
                           success:(void (^)(void))success
                           failure:(void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    switch (status) {
        case kVPVoteStatusDown:
            return [client voteCommentWithId:commentId down:YES up:NO undown:NO unup:NO success:success failure:failure];
            
        case kVPVoteStatusUndown:
            return [client voteCommentWithId:commentId down:NO up:NO undown:YES unup:NO success:success failure:failure];
            
        case kVPVoteStatusUp:
            return [client voteCommentWithId:commentId down:NO up:YES undown:NO unup:NO success:success failure:failure];
            
        case kVPVoteStatusUnup:
            return [client voteCommentWithId:commentId down:NO up:NO undown:NO unup:YES success:success failure:failure];
            
        case kVPVoteStatusDownSwitch:
            return [client voteCommentWithId:commentId down:YES up:NO undown:NO unup:YES success:success failure:failure];
            
        case kVPVoteStatusUpSwitch:
            return [client voteCommentWithId:commentId down:NO up:YES undown:YES unup:NO success:success failure:failure];
    }
}

@end
