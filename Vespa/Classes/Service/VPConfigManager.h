//
//  VPConfigManager.h
//  Vespa
//
//  Created by Jiayu Zhang on 11/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//



@interface VPConfigManager : NSObject

+ (instancetype)sharedInstance;

- (id)get: (NSString *)key;

- (BOOL)getBool: (NSString *)key;

- (float)getFloat: (NSString *)key;

- (NSInteger)getInteger: (NSString *)key;

- (NSString *)getString: (NSString *)key;

/**
 * Upon initialization, the manager load configuration from UserDefaults by key 'ClientConfig'.
 * VPInitDataManager will fetch and sync the up-to-date configuration from remote server, after
 * it parses the response, it will import the fresh config to the manager
 */
- (void)importConfig:(NSDictionary *)config;

// Since the underlying config json could be very complex, it is okay to encapsulate
// some pieces into object and present it outside, i.e.
// - (SomeObject *)getSomeObject;

@end
