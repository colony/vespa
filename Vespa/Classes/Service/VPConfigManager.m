//
//  VPConfigManager.m
//  Vespa
//
//  Created by Jiayu Zhang on 11/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


#import "VPConfigManager.h"


@interface VPConfigManager ()

@property (strong, nonatomic) NSDictionary *data;

@end


@implementation VPConfigManager

+ (instancetype)sharedInstance
{
    static VPConfigManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)get: (NSString *)key
{
    return [_data objectForKey:key];
}

- (BOOL)getBool: (NSString *)key
{
    return [[_data objectForKey:key] boolValue];
}

- (float)getFloat: (NSString *)key
{
    return [[_data objectForKey:key] floatValue];
}

- (NSInteger)getInteger: (NSString *)key
{
    return [[_data objectForKey:key] integerValue];
}

- (NSString *)getString: (NSString *)key
{
    return [_data objectForKey:key];
}

- (id)init
{
    self = [super init];
    if (self) {
        // Populate config from UserDefaults
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        NSDictionary *storedConfig = [ud dictionaryForKey:kVPUDClientConfigKey];
        NSMutableDictionary *configDefault = [[ud dictionaryForKey:@"ClientConfigDefault"] mutableCopy];
        if (storedConfig) {
            // Each app update, might introduce different defaults, for other legacy config key, we just ignore them
            for (NSString *key in storedConfig) {
                if ([configDefault objectForKey:key]) {
                    [configDefault setValue:[storedConfig objectForKey:key] forKey:key];
                }
            }
        }
        _data = configDefault;
    }
    return self;
}

- (void)importConfig:(NSDictionary *)config
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *configDefault = [[defaults dictionaryForKey:@"ClientConfigDefault"] mutableCopy];
    [configDefault addEntriesFromDictionary:config];
    [defaults setObject:configDefault forKey:kVPUDClientConfigKey];
    
    self.data = configDefault;
}

@end
