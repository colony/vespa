//
//  VPDownloadManager.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/3/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


typedef void(^VPDownloadManagerCompletionBlock)(UIImage *image, NSError *error);

@interface VPDownloadManager : NSObject

+ (instancetype)sharedInstance;

- (void)downloadImage:(NSURL *)url completed:(VPDownloadManagerCompletionBlock)completedBlock;

@end
