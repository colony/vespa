//
//  VPDownloadManager.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/3/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPDownloadManager.h"

#import "FXBlurView.h"
#import "SDWebImageManager.h"


@interface VPDownloadManager ()

// Main download background view
@property (strong, nonatomic) UIView *mainBackgroundView;
@property (strong, nonatomic) UIProgressView *progressView;
@property (strong, nonatomic) id <SDWebImageOperation> downloadOperation;

@end


@implementation VPDownloadManager

+ (instancetype)sharedInstance
{
    static VPDownloadManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)downloadImage:(NSURL *)url completed:(VPDownloadManagerCompletionBlock)completedBlock
{
    @weakify(self);
    self.downloadOperation = [[SDWebImageManager sharedManager] downloadImageWithURL:url
                                                                             options:(SDWebImageHighPriority|SDWebImageAvoidAutoSetImage)
                                                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                                @strongify(self);
                                                                                CGFloat progress = ((CGFloat)receivedSize / (CGFloat)expectedSize);
                                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                                    [self updateProgress:progress];
                                                                                });
                                                                            } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                                                @strongify(self);
                                                                                [self cleanup];
                                                                                if (completedBlock)
                                                                                    completedBlock(image, error);
                                                                            }];
}


#pragma mark - progress
- (void)updateProgress:(CGFloat)progress
{
    UIProgressView *progressView = [self progressView];
    if (progressView)
        progressView.progress = progress;
}

- (void)cleanup
{
    if (_progressView) {
        _progressView.hidden = YES;
        [_progressView removeFromSuperview];
        _progressView = nil;
    }
    
    if (_mainBackgroundView) {
        _mainBackgroundView.hidden = YES;
        [_mainBackgroundView removeFromSuperview];
        _mainBackgroundView = nil;
    }
}


#pragma mark - getter
- (UIView *)mainBackgroundView
{
    if (_mainBackgroundView)
        return _mainBackgroundView;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0)) {
        UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        _mainBackgroundView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    } else {
        FXBlurView *blurView = [[FXBlurView alloc] init];
        blurView.dynamic = NO;
        blurView.blurRadius = 10.0f;
        blurView.blurEnabled = YES;
        blurView.tintColor = nil;
        blurView.underlyingView = [UIApplication sharedApplication].keyWindow;
        
        // https://github.com/nicklockwood/FXBlurView/issues/41
        // FXBlurView is too bright, in order to blend some darkness, we need add a transparent darkview
        UIView *darkOverlay = [[UIView alloc] initWithFrame:blurView.bounds];
        darkOverlay.autoresizingMask = UIViewAutoresizingFill;
        darkOverlay.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        [blurView addSubview:darkOverlay];
        
        _mainBackgroundView = blurView;
    }
    _mainBackgroundView.autoresizingMask = UIViewAutoresizingFill;
    _mainBackgroundView.frame = [UIScreen mainScreen].bounds;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDismiss:)];
    [_mainBackgroundView addGestureRecognizer:tap];
    
    [[UIApplication sharedApplication].keyWindow addSubview:_mainBackgroundView];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_mainBackgroundView];
    
    return _mainBackgroundView;
}

- (UIProgressView *)progressView
{
    if (_progressView) {
        return _progressView;
    }
    
    _progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    _progressView.autoresizingMask = UIViewAutoresizingCentered;
    _progressView.frame = CGRectMake(0, 0, SCREEN_WIDTH / 2, 2);
    _progressView.center = [self mainBackgroundView].center;
    _progressView.progressTintColor = [VPUI colonyColor];
    [[self mainBackgroundView] addSubview:_progressView];
    return _progressView;
}


#pragma mark -
- (void)tapToDismiss:(id)sender
{
    // Cancel the download if user tap while downloading is still in progress
    if (self.downloadOperation)
        [self.downloadOperation cancel];
    [self cleanup];
}

@end

