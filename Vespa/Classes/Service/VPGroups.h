//
//  VPGroups.h
//  Vespa
//
//  Created by Jiayu Zhang on 7/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@class VPGroup;
@class VPGroupList;


@interface VPGroups : NSObject

+ (NSOperation *)fetchGroupWithId:(NSNumber *)groupId
                            param:(NSDictionary *)param
                          success:(void (^)(VPGroup *group))success
                          failure:(void (^)(NSError *error))failure;

+ (NSOperation *)fetchGroupsWithUrl:(NSString *)url
                              param:(NSDictionary *)param
                            success:(void (^)(VPGroupList *groupList))success
                            failure:(void (^)(NSError *error))failure;

+ (BOOL)containsGeneralInGroupPickList;

/**
 * Return a list of VPGroup served as pick list. NOTE, the returned list might be
 * nil or empty, also, each item (VPGroup) contains groupId and name only
 */
+ (NSArray *)fetchGroupPickList;

@end
