//
//  VPGroups.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroups.h"

#import "VPAPIClient.h"
#import "VPGroup.h"


@implementation VPGroups

+ (NSOperation *)fetchGroupWithId:(NSNumber *)groupId
                            param:(NSDictionary *)param
                          success:(void (^)(VPGroup *group))success
                          failure:(void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client fetchGroupWithId:groupId success:success failure:failure];
}

+ (NSOperation *)fetchGroupsWithUrl:(NSString *)url
                              param:(NSDictionary *)param
                            success:(void (^)(VPGroupList *groupList))success
                            failure:(void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    if (url) {
        return [client fetchGroupsWithUrl:url success:success failure:failure];
    } else {
        return [client fetchGroupsWithParam:param success:success failure:failure];
    }
}

+ (BOOL)containsGeneralInGroupPickList
{
    for (VPGroup *g in [self fetchGroupPickList]) {
        if ([g.groupId isEqualToNumber:@1])
            return YES;
    }
    return NO;
}

+ (NSArray *)fetchGroupPickList
{
    NSMutableArray *groupPickList = [NSMutableArray new];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSArray *groupPickListData = [ud stringArrayForKey:kVPUDGroupPickList];
    if (groupPickListData) {
        // Each item is a string in form "id,name", check VPInitManager for detail
        for (NSString *groupPickStr in groupPickListData) {
            NSArray *tokens = [groupPickStr componentsSeparatedByString:@","];
            VPGroup *g = [VPGroup new];
            g.groupId = @([tokens[0] longLongValue]);
            g.name = tokens[1];
            [g setAllowedPostTypesFromCode:[tokens[2] intValue]];
            [groupPickList addObject:g];
        }
    }
    return groupPickList;
}

@end
