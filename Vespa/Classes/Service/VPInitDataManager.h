//
//  VPInitDataManager.h
//  Vespa
//
//  Created by Jay on 2/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VPInitDataManager : NSObject

+ (instancetype)sharedInstance;
- (void) fetchInitData;

@end
