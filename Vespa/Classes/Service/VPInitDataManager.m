//
//  VPInitDataManager.m
//  Vespa
//
//  Created by Jay on 2/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPInitDataManager.h"

#import "NSString+Util.h"
#import "VPAPIClient.h"
#import "VPConfigManager.h"
#import "VPFileManager.h"
#import "VPHTTPClientManager.h"
#import "VPUsers.h"

@implementation VPInitDataManager

#pragma mark - life cycle
+ (instancetype)sharedInstance
{
    static VPInitDataManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        // setup some default value
    }
    return self;
}

#pragma mark - public method
- (void)fetchInitData
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    [client getInitDataWithSuccess:^(NSDictionary *initData) {
        // 1. Client Configuration
        NSString *clientConfigStr = initData[@"client_config"];
        if (![NSString isEmptyString:clientConfigStr]) {
            NSError *err;
            NSDictionary *config = [NSJSONSerialization JSONObjectWithData: [clientConfigStr dataUsingEncoding:NSUTF8StringEncoding]
                                                                   options: NSJSONReadingMutableContainers
                                                                     error: &err];
            [[VPConfigManager sharedInstance] importConfig:config];
        }
        
        // 2. Simple group pick list
        NSArray *groupPickList = initData[@"group_pick_list"];
        if (groupPickList) {
            NSMutableArray *groupPickListSaveArray = [[NSMutableArray alloc] initWithCapacity:[groupPickList count]];
            for (NSDictionary *groupToPick in groupPickList) {
                NSString *groupToPickStr = [NSString stringWithFormat:@"%@,%@,%@", groupToPick[@"id"], groupToPick[@"name"], groupToPick[@"allowed_post_types_code"]];
                [groupPickListSaveArray addObject:groupToPickStr];
            }
            [[NSUserDefaults standardUserDefaults] setObject:groupPickListSaveArray forKey:kVPUDGroupPickList];
        }
        
        // 3. Other data goes here
        
        [self initDataFetched];
    } failure:nil];
}

- (void)initDataFetched
{
    // after initdata fetch, we perform some actions here
    
    // 1. Fetch the link parser javascript (cache-control)
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:[[VPConfigManager sharedInstance] getString:kVPConfigLinkParserJsUrl] parameters:nil];
    [request setValue:@"text/plain; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:nil forHTTPHeaderField:@"Authorization"];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *jsCode = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        VPLog(@"JSCODE %@", jsCode);
        [jsCode writeToFile:[[VPFileManager sharedInstance] pathForFile:kVPFileLinkParserJavascript]
                 atomically:YES
                   encoding:NSUTF8StringEncoding
                      error:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.isCancelled)return;
        VPLog(@"Fail download linkparsejs %@ %@", operation.request.URL, [error localizedDescription]);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
}


@end
