//
//  VPLocationManager.h
//  Vespa
//
//  Created by Jay on 1/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


#import "VPGeo.h"

@interface VPLocationManager : NSObject

+ (instancetype)sharedInstance;

/** nil if there is no */
- (VPGeo *)currentGeo;

- (void)updateGeo;


/** Check if user authorized location permission */
- (BOOL)locationServiceAuthorized;

- (BOOL)locationServiceEnabled;

@end
