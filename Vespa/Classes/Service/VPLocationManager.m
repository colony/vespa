//
//  VPLocationManager.m
//  Vespa
//
//  Created by Jay on 1/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPLocationManager.h"

#import <CoreLocation/CoreLocation.h>
#import "Flurry.h"
#import "VPAPIClient.h"
#import "VPAppWorker.h"
#import "VPConfigManager.h"
#import "VPGeoUtil.h"


static BOOL gate = NO;


@interface VPLocationManager ()<CLLocationManagerDelegate>

@property(strong, nonatomic) VPGeo *curGeo;
@property(strong, nonatomic) CLLocationManager *locMgr;

@end

@implementation VPLocationManager

#pragma mark - life cycle
+ (instancetype)sharedInstance
{
    static VPLocationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Pupulate data from UserDefaults
        _curGeo = [[NSUserDefaults standardUserDefaults] customObjectForKey:kVPUDLocation];
        
        _locMgr = [[CLLocationManager alloc] init];
        _locMgr.delegate = self;
        _locMgr.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return self;
}

#pragma mark - public method
- (VPGeo *)currentGeo
{
    return self.curGeo;
}

- (void)updateGeo
{
    if (![CLLocationManager locationServicesEnabled])
        return;
    
    if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusDenied ||
        CLLocationManager.authorizationStatus == kCLAuthorizationStatusRestricted)
        //TODO: alert user and go to setting menu?
        return;
    
    gate = YES;
    if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusNotDetermined) {
        // ios 8
        if ([self.locMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locMgr requestWhenInUseAuthorization];
        } else {
            // ios 7 and prior, startUpdateLocation will actually trigger authorization prompt
            [self.locMgr startUpdatingLocation];
        }
    } else {
        // Certainly, we have the permission, start loc tracking
        [self.locMgr startUpdatingLocation];
    }
}

- (BOOL)locationServiceAuthorized
{
    CLAuthorizationStatus status = CLLocationManager.authorizationStatus;
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse ||
        status == kCLAuthorizationStatusAuthorized) {
        return true;
    } else {
        return false;
    }
}

- (BOOL)locationServiceEnabled
{
    return [CLLocationManager locationServicesEnabled];
}


#pragma mark - delegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    //http://stackoverflow.com/questions/26582137/core-location-ios8-compatibility-with-ios7
    
    if (status == kCLAuthorizationStatusNotDetermined) {
        [[NSNotificationCenter defaultCenter] postNotificationName: kLocationServiceStatusChangeNotification
                                                            object: self
                                                          userInfo: @{@"available":@(NO),
                                                                      @"determined":@(NO)}];
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse ||
        status == kCLAuthorizationStatusAuthorized) {
        // Either ios8 or ios7, the permission prompt will intercept previous startUpdatingLocation call so that delegate method
        // didUpdateLocations never triggered, here, we call another startUpdatingLocation immediately
        gate = YES;
        [manager startUpdatingLocation];
    }
    else {
        // Denied or restricted
        gate = NO;
        [manager stopUpdatingLocation];
        [[NSNotificationCenter defaultCenter] postNotificationName: kLocationServiceStatusChangeNotification
                                                            object: self
                                                          userInfo: @{@"available":@(NO),
                                                                      @"determined":@(YES)}];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [manager stopUpdatingLocation];
    
    // The delegate method is triggered very frequently, using a static variable
    // to rate-limit the real work (persist, report over http, etc)
    if (!gate)
        return;
    gate = NO;
    
    CLLocation *curCoordinate = [locations lastObject];
    VPLog(@"Update coordinate lat=%.8f lon=%.8f", curCoordinate.coordinate.latitude, curCoordinate.coordinate.longitude);
    
    // HACK BEGIN
    // curCoordinate = [[CLLocation alloc] initWithLatitude:21.04224098382615 longitude:105.8411411103847];
    // HACK END
    
    // Let flurry track geo
    [Flurry setLatitude:curCoordinate.coordinate.latitude
              longitude:curCoordinate.coordinate.longitude
     horizontalAccuracy:curCoordinate.horizontalAccuracy
       verticalAccuracy:curCoordinate.verticalAccuracy];

    // Even though the location has been updated, all other info (state, city, etc) has not been updated
    // They are required for geo displaying logic, it is okay to update them asynchronously
    if (!self.curGeo)
        self.curGeo = [VPGeo new];
    self.curGeo.location = curCoordinate;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setCustomObject:self.curGeo forKey:kVPUDLocation];
    
    // Update api client's GEO header
    [[VPAPIClient sharedInstance] setGeo:self.curGeo];
    
    // Only report the geo location (we don't need reverse geo yet)
    [VPReports reportGeoLocation:self.curGeo];

    // Reverse geo asynchronously
    VPLocationManager * __weak weakSelf = self;
    
    VPLog(@"Starting reverse geo");
    [VPGeoUtil reverseGeocodeLocation:curCoordinate completionHandler:^(VPGeo *geo){
        if (geo) {
            weakSelf.curGeo.location = geo.location;
            weakSelf.curGeo.placeName = geo.placeName;
            weakSelf.curGeo.sublocality = geo.sublocality;
            weakSelf.curGeo.locality = geo.locality;
            weakSelf.curGeo.state = geo.state;
            weakSelf.curGeo.country = geo.country;
            weakSelf.curGeo.countryCode = geo.countryCode;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setCustomObject:weakSelf.curGeo forKey:kVPUDLocation];
            [defaults setObject:[NSDate date] forKey:kVPUDLastTimeReverseGeo];
            VPLog(@"Reverse geo succeed :%@", [geo toHttpParamValue]);
        } else {
            // We have a complete fallback mechanism for reverse geo, if ALL of them failed, report the critical event
            [VPReports reportEvent:kVPEventFailReverseGeo withParam:@{@"status":[NSString stringWithFormat:@"ALL_FAIL %.8f,%.8f", curCoordinate.coordinate.latitude, curCoordinate.coordinate.longitude]} mode:kVPEventModeOnce];
            VPLog(@"Reverse geo failed");
        }
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: kLocationServiceStatusChangeNotification
                                                        object: self
                                                      userInfo: @{@"available":@(YES), @"determined":@(YES)}];
}

@end
