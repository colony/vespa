//
//  VPPosts.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


typedef NS_ENUM(NSInteger, VPPostListFetchType) {
    kVPPostListFetchTypeNearbyRecent = 0,
    kVPPostListFetchTypeNearbyPopular,
    kVPPostListFetchTypeGlobalRecent,
    kVPPostListFetchTypeGlobalPopular,
    kVPPostListFetchTypeHashtag,
    kVPPostListFetchTypeGroupRecent,
    kVPPostListFetchTypeGroupPopular,
    kVPPostListFetchTypeId, /** sorted by ID (or, create_time) */
    kVPPostListFetchTypeAnnouncement, /** Fetch announcement posts */
    kVPPostListFetchTypeThread,
    kVPPostListFetchTypeFeatured
};


@class VPGeo;
@class VPPaginator;
@class VPPost;
@class VPPostList;

@interface VPPosts : NSObject

+ (NSOperation *)createFlagWithPostId: (NSNumber *)postId
                            complaint: (NSString *)complaint
                              success: (void (^)(void))success
                              failure: (void (^)(NSError *error))failure;

/**
 * NOTE, param filterName is only used for reporting (i.e. flurry) purpose
 */
+ (NSOperation *)createPostWithAudio:(NSString *)filePath
                            duration:(NSInteger)durationInMS
                             caption:(NSString *)caption
                  filterNameToReport:(NSString *)filterName
                             groupId:(NSNumber *)groupId
                    createThreadType: (NSString *)createThreadType
                            threadId: (NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure;

+ (NSOperation *)createPostWithCard:(NSString *)cardText
                             deckId:(NSString *)deckId
                          isRequest:(BOOL)isRequest
                            groupId:(NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                            success:(void (^)(VPPost *post))success
                            failure:(void (^)(NSError *error))failure;

/** 
 * filtered - report purpose, YES if the photo image is filtered (not original)
 */
+ (NSOperation *)createPostWithPhoto:(UIImage *)img
                             caption:(NSString *)body
                            uploaded:(BOOL)uploaded
                            filtered:(BOOL)filtered
                             groupId:(NSNumber *)groupId
                    createThreadType: (NSString *)createThreadType
                            threadId: (NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure;

/**
 * Link might or might not comes with an embed image. If youtubeId is set and imageUrl is not set,
 * imageHeight and imageWidth describes the dimension of youtube's hqdefaults thumbnail
 */
+ (NSOperation *)createPostWithLink:(NSString *)linkUrl
                              title:(NSString *)title
                      embedImageUrl:(NSString *)imageUrl
                   embedImageHeight:(NSInteger)height
                    embedImageWidth:(NSInteger)width
                     embedYoutubeId:(NSString *)youtubeId
                          embedType:(NSString *)embedItemType
                            groupId:(NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                            success:(void (^)(VPPost *post))success
                            failure:(void (^)(NSError *error))failure;

+ (NSOperation *)createPostWithText:(NSString *)text
                    backgroundColor:(UIColor *)backgroundColor
                            groupId:(NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                            success:(void (^)(VPPost *post))success
                            failure:(void (^)(NSError *error))failure;

+ (NSOperation *)createPostWithVideo:(NSString *)filePath
                           thumbnail:(UIImage *)thumbnailImg
                             caption:(NSString *)caption
                             groupId:(NSNumber *)groupId
                    createThreadType: (NSString *)createThreadType
                            threadId: (NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure;

+ (NSOperation *)deletePostWithId: (NSNumber *)postId
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure;

+ (NSOperation *)fetchPostWithId: (NSNumber *)postId
                           param: (NSDictionary *)params
                         success: (void (^)(VPPost *post))success
                         failure: (void (^)(NSError *error))failure;

/**
 The URL of next or prev page of results could be got from VPPaginator. If url is nil, the first page is assumed.
 If params are provided and url isn't nil, the same parameter (if any) in url will be overwritten
 */
+ (NSOperation *)fetchMyPostsWithUrl: (NSString *)url
                               param: (NSDictionary *)params
                             success: (void (^)(VPPostList *postList))success
                             failure: (void (^)(NSError *error))failure;

/**
 * type -
 *     Categorize the type of the fetch operation, the impl looksup different data from param dictionary
 *     per type accordingly
 * url -
 *     If set, the impl will query server with the specified url and IGNORE param dictionary
 * param -
 *     pagination http params (i.e. _a,_b,_e,_k,_n,_i)
 *     hashtag,  string, fetch posts by specified hashtag
 *     groupId,  number, fetch posts by specified group ID
 *     threadId, number, fetch posts by specified thread ID
 */
+ (NSOperation *)fetchPostsWithType:(VPPostListFetchType)type
                                url:(NSString *)url
                             param:(NSDictionary *)param
                           success:(void (^)(VPPostList *postList))success
                           failure:(void (^)(NSError *error))failure;

+ (NSOperation *)votePostWithId: (NSNumber *)postId
                         status: (VPVoteStatus)status
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure;

@end
