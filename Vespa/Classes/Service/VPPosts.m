//
//  VPPosts.m
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPPosts.h"

#import "NSString+Util.h"
#import "VPAPIClient.h"
#import "VPConfigManager.h"
#import "VPFileManager.h"
#import "VPGeo.h"
#import "VPLocationManager.h"
#import "VPUsers.h"
#import "VPUtil.h"

@implementation VPPosts

+ (NSOperation *)createFlagWithPostId: (NSNumber *)postId
                            complaint: (NSString *)complaint
                              success: (void (^)(void))success
                              failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client createFlagWithPostId:postId complaint:complaint success:success failure:failure];
}

+ (NSOperation *)createPostWithAudio:(NSString *)filePath
                            duration:(NSInteger)durationInMS
                             caption:(NSString *)caption
                  filterNameToReport:(NSString *)filterName
                             groupId:(NSNumber *)groupId
                    createThreadType: (NSString *)createThreadType
                            threadId: (NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure
{
    [VPReports reportEvent:kVPEventCreateAudioPost withParam:@{@"duration":@(durationInMS), @"caption":[NSString isEmptyString:caption]?@(0):@(1), @"filter":filterName} mode:kVPEventModeOnce];
    
    NSString *audioExt = [filePath pathExtension];
    NSString *audioFmt = ([audioExt isEqualToString:@"wav"]) ? @"WAV" : @"M4A";
    
    VPFileManager *fileMgr = [VPFileManager sharedInstance];
    
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client createPostWithType:@"AUDIO"
                                 body:caption
                                  geo:[[VPLocationManager sharedInstance] currentGeo]
                              groupId:groupId
                     createThreadType:createThreadType
                             threadId:threadId
                                audio:[fileMgr contentsAtFilePath:filePath]
                        audioDuration:durationInMS
                            //No worry about overflow for now, we know it is 30-sec audio clip
                            audioSize:(NSUInteger)[fileMgr sizeAtFilePath:filePath]
                          audioFormat:audioFmt
                                photo:nil
                            photoSize:0
                          photoHeight:0
                           photoWidth:0
                          photoFormat:nil
                        photoUploaded:NO
                                 text:nil
                  textBackgroundColor:nil
                            textColor:nil
                           textFormat:nil
                                 link:nil
                            linkTitle:nil
                         linkEmbedUrl:nil
                      linkEmbedHeight:0
                       linkEmbedWidth:0
                      linkEmbedFormat:nil
                                video:nil
                            videoSize:0
                          videoFormat:nil
                              success:^(VPPost *post) {
                                  // Increment the reward points for CREATE_POST on client side ONLY
                                  // It's okay if the configured points mismatch the one on server side, we will sync up eventually
                                  NSInteger points = [[VPConfigManager sharedInstance] getInteger:kVPConfigCreatePostPoints];
                                  [VPUsers updateUserStatusPropertyKey:@"points" value:points incr:YES zero:YES syncTime:[VPUtil currentSystemTimeInMs]];

                                  [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                                                      object:self
                                                                                    userInfo:@{kPostModificationNotificationInsertKey:@[post]}];
                                  SAFE_BLOCK_RUN(success, post);
                              }
                              failure:failure];
}

+ (NSOperation *)createPostWithCard:(NSString *)cardText
                             deckId:(NSString *)deckId
                          isRequest:(BOOL)isRequest
                            groupId:(NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                            success:(void (^)(VPPost *post))success
                            failure:(void (^)(NSError *error))failure;
{
    // This is generic way to create CARD post
    // For now, we hardcode the data for "Cards Against Humanity" only, all card-related metadata is
    // a serialized string stored in post's body
    
    // The format is:
    // <TYPE>|<VERSION>|deckId|[0,1]
    // 0 means white card (response), while 1 means black card (request)

    [VPReports reportEvent:kVPEventCreateCardPost withParam:@{@"isRequest":@(isRequest)} mode:kVPEventModeOnce];

    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client createPostWithType:@"CARD"
                                 body:[NSString stringWithFormat:@"1|%@|%@", deckId, @(isRequest)]
                                  geo:[[VPLocationManager sharedInstance] currentGeo]
                              groupId:groupId
                     createThreadType:createThreadType
                             threadId:threadId
                                audio:nil
                        audioDuration:0
                            audioSize:0
                          audioFormat:nil
                                photo:nil
                            photoSize:0
                          photoHeight:0
                           photoWidth:0
                          photoFormat:nil
                        photoUploaded:NO
                                 text:cardText
                  textBackgroundColor:(isRequest ? @"000000" : @"ffffff")
                            textColor:(isRequest ? @"ffffff" : @"000000")
                           textFormat:@"PLAIN"
                                 link:nil
                            linkTitle:nil
                         linkEmbedUrl:nil
                      linkEmbedHeight:0
                       linkEmbedWidth:0
                      linkEmbedFormat:nil
                                video:nil
                            videoSize:0
                          videoFormat:nil
                              success:^(VPPost *post) {
                                  // Increment the reward points for CREATE_POST on client side ONLY
                                  // It's okay if the configured points mismatch the one on server side, we will sync up eventually
                                  NSInteger points = [[VPConfigManager sharedInstance] getInteger:kVPConfigCreatePostPoints];
                                  [VPUsers updateUserStatusPropertyKey:@"points" value:points incr:YES zero:YES syncTime:[VPUtil currentSystemTimeInMs]];
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                                                      object:self
                                                                                    userInfo:@{kPostModificationNotificationInsertKey:@[post]}];
                                  SAFE_BLOCK_RUN(success, post);
                              }
                              failure:failure];
}

+ (NSOperation *)createPostWithPhoto:(UIImage *)img
                             caption:(NSString *)body
                            uploaded:(BOOL)uploaded
                            filtered:(BOOL)filtered
                             groupId:(NSNumber *)groupId
                    createThreadType: (NSString *)createThreadType
                            threadId: (NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure;
{
    [VPReports reportEvent:kVPEventCreatePhotoPost withParam:@{@"hasCaption":@(![NSString isEmptyString:body]), @"isUploaded":@(uploaded), @"isFiltered":@(filtered)} mode:kVPEventModeOnce];
    
    float q = [[VPConfigManager sharedInstance] getFloat:kVPConfigJPEGCompressionQuality];
    NSData *imgData = UIImageJPEGRepresentation(img, q);
    
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client createPostWithType:@"PHOTO"
                                 body:body
                                  geo:[[VPLocationManager sharedInstance] currentGeo]
                              groupId:groupId
                     createThreadType:createThreadType
                             threadId:threadId
                                audio:nil
                        audioDuration:0
                            audioSize:0
                          audioFormat:nil
                                photo:imgData
                            photoSize:(imgData != nil ? [imgData length] : 0)
                          photoHeight:img.size.height * img.scale
                           photoWidth:img.size.width * img.scale
                          photoFormat:@"JPG"
                        photoUploaded:uploaded
                                 text:nil
                  textBackgroundColor:nil
                            textColor:nil
                           textFormat:nil
                                 link:nil
                            linkTitle:nil
                         linkEmbedUrl:nil
                      linkEmbedHeight:0
                       linkEmbedWidth:0
                      linkEmbedFormat:nil
                                video:nil
                            videoSize:0
                          videoFormat:nil
                              success:^(VPPost *post) {
                                  // Increment the reward points for CREATE_POST on client side ONLY
                                  // It's okay if the configured points mismatch the one on server side, we will sync up eventually
                                  NSInteger points = [[VPConfigManager sharedInstance] getInteger:kVPConfigCreatePostPoints];
                                  [VPUsers updateUserStatusPropertyKey:@"points" value:points incr:YES zero:YES syncTime:[VPUtil currentSystemTimeInMs]];

                                  [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                                                      object:self
                                                                                    userInfo:@{kPostModificationNotificationInsertKey:@[post]}];
                                  SAFE_BLOCK_RUN(success, post);
                              } failure:failure];
}

+ (NSOperation *)createPostWithLink:(NSString *)linkUrl
                              title:(NSString *)title
                      embedImageUrl:(NSString *)imageUrl
                   embedImageHeight:(NSInteger)height
                    embedImageWidth:(NSInteger)width
                     embedYoutubeId:(NSString *)youtubeId
                          embedType:(NSString *)embedItemType
                            groupId:(NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                            success:(void (^)(VPPost *post))success
                            failure:(void (^)(NSError *error))failure
{
    // If embed item is youtube, the embedUrl contains youtube Id, also there maybe no embed item
    NSString *linkEmbedFormat = embedItemType;
    if (!imageUrl && !youtubeId) {
        linkEmbedFormat = @"NONE";
    }
    NSString *linkEmbedUrl = nil;
    if (imageUrl) {
        linkEmbedUrl = imageUrl;
    } else if (youtubeId) {
        linkEmbedUrl = youtubeId;
    }
    
    [VPReports reportEvent:kVPEventCreateLinkPost withParam:@{@"embedItemType":linkEmbedFormat} mode:kVPEventModeOnce];
    
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client createPostWithType:@"LINK"
                                 body:nil
                                  geo:[[VPLocationManager sharedInstance] currentGeo]
                              groupId:groupId
                     createThreadType:createThreadType
                             threadId:threadId
                                audio:nil
                        audioDuration:0
                            audioSize:0
                          audioFormat:nil
                                photo:nil
                            photoSize:0
                          photoHeight:0
                           photoWidth:0
                          photoFormat:nil
                        photoUploaded:NO
                                 text:nil
                  textBackgroundColor:nil
                            textColor:nil
                           textFormat:nil
                                 link:linkUrl
                            linkTitle:title
                         linkEmbedUrl:linkEmbedUrl
                      linkEmbedHeight:height
                       linkEmbedWidth:width
                      linkEmbedFormat:linkEmbedFormat
                                video:nil
                            videoSize:0
                          videoFormat:nil
                              success:^(VPPost *post) {
                                  // Increment the reward points for CREATE_POST on client side ONLY
                                  // It's okay if the configured points mismatch the one on server side, we will sync up eventually
                                  NSInteger points = [[VPConfigManager sharedInstance] getInteger:kVPConfigCreatePostPoints];
                                  [VPUsers updateUserStatusPropertyKey:@"points" value:points incr:YES zero:YES syncTime:[VPUtil currentSystemTimeInMs]];
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                                                      object:self
                                                                                    userInfo:@{kPostModificationNotificationInsertKey:@[post]}];
                                  SAFE_BLOCK_RUN(success, post);
                              } failure:failure];
}

+ (NSOperation *)createPostWithText:(NSString *)text
                    backgroundColor:(UIColor *)backgroundColor
                            groupId:(NSNumber *)groupId
                   createThreadType: (NSString *)createThreadType
                           threadId: (NSNumber *)threadId
                            success:(void (^)(VPPost *post))success
                            failure:(void (^)(NSError *error))failure
{
    NSString *backgroundColorHex = [VPUtil hexStringFromColor:backgroundColor];
    [VPReports reportEvent:kVPEventCreateTextPost withParam:@{@"color":backgroundColorHex} mode:kVPEventModeOnce];
    
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client createPostWithType:@"TEXT"
                                 body:nil
                                  geo:[[VPLocationManager sharedInstance] currentGeo]
                              groupId:groupId
                     createThreadType:createThreadType
                             threadId:threadId
                                audio:nil
                        audioDuration:0
                            audioSize:0
                          audioFormat:nil
                                photo:nil
                            photoSize:0
                          photoHeight:0
                           photoWidth:0
                          photoFormat:nil
                        photoUploaded:NO
                                 text:text
                  textBackgroundColor:[VPUtil hexStringFromColor:backgroundColor]
                            textColor:@"ffffff"
                           textFormat:@"PLAIN"
                                 link:nil
                            linkTitle:nil
                         linkEmbedUrl:nil
                      linkEmbedHeight:0
                       linkEmbedWidth:0
                      linkEmbedFormat:nil
                                video:nil
                            videoSize:0
                          videoFormat:nil
                              success:^(VPPost *post) {
                                  // Increment the reward points for CREATE_POST on client side ONLY
                                  // It's okay if the configured points mismatch the one on server side, we will sync up eventually
                                  NSInteger points = [[VPConfigManager sharedInstance] getInteger:kVPConfigCreatePostPoints];
                                  [VPUsers updateUserStatusPropertyKey:@"points" value:points incr:YES zero:YES syncTime:[VPUtil currentSystemTimeInMs]];
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                                                      object:self
                                                                                    userInfo:@{kPostModificationNotificationInsertKey:@[post]}];
                                  SAFE_BLOCK_RUN(success, post);
                              } failure:failure];
}

+ (NSOperation *)createPostWithVideo:(NSString *)filePath
                           thumbnail:(UIImage *)thumbnailImg
                             caption:(NSString *)caption
                             groupId:(NSNumber *)groupId
                    createThreadType: (NSString *)createThreadType
                            threadId: (NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure
{
    [VPReports reportEvent:kVPEventCreateVideoPost withParam:nil mode:kVPEventModeOnce];
    
    float q = [[VPConfigManager sharedInstance] getFloat:kVPConfigJPEGCompressionQuality];
    NSData *imgData = UIImageJPEGRepresentation(thumbnailImg, q);
    
    NSString *videoFmt = @"MP4";
    
    VPFileManager *fileMgr = [VPFileManager sharedInstance];
    
    // NOTE, we use photo to pass data of video thumbnail
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client createPostWithType:@"VIDEO"
                                 body:caption
                                  geo:[[VPLocationManager sharedInstance] currentGeo]
                              groupId:groupId
                     createThreadType:createThreadType
                             threadId:threadId
                                audio:nil
                        audioDuration:0
                            audioSize:0
                          audioFormat:nil
                                photo:imgData
                            photoSize:(imgData != nil ? [imgData length] : 0)
                          photoHeight:thumbnailImg.size.height * thumbnailImg.scale
                           photoWidth:thumbnailImg.size.width * thumbnailImg.scale
                          photoFormat:@"JPG"
                        photoUploaded:NO
                                 text:nil
                  textBackgroundColor:nil
                            textColor:nil
                           textFormat:nil
                                 link:nil
                            linkTitle:nil
                         linkEmbedUrl:nil
                      linkEmbedHeight:0
                       linkEmbedWidth:0
                      linkEmbedFormat:nil
                                video:[fileMgr contentsAtFilePath:filePath]
                            videoSize:(NSUInteger)[fileMgr sizeAtFilePath:filePath]
                          videoFormat:videoFmt
                              success:^(VPPost *post) {
                                  // Increment the reward points for CREATE_POST on client side ONLY
                                  // It's okay if the configured points mismatch the one on server side, we will sync up eventually
                                  NSInteger points = [[VPConfigManager sharedInstance] getInteger:kVPConfigCreatePostPoints];
                                  [VPUsers updateUserStatusPropertyKey:@"points" value:points incr:YES zero:YES syncTime:[VPUtil currentSystemTimeInMs]];
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                                                      object:self
                                                                                    userInfo:@{kPostModificationNotificationInsertKey:@[post]}];
                                  SAFE_BLOCK_RUN(success, post);
                              }
                              failure:failure];
}

+ (NSOperation *)deletePostWithId: (NSNumber *)postId
                          success: (void (^)(void))success
                          failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client deletePostWithId:postId success:success failure:failure];
}

+ (NSOperation *)fetchPostWithId: (NSNumber *)postId
                           param: (NSDictionary *)params
                         success: (void (^)(VPPost *post))success
                         failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client fetchPostWithId:postId success:success failure:failure];
}

+ (NSOperation *)fetchMyPostsWithUrl: (NSString *)url
                                param: (NSDictionary *)params
                                success: (void (^)(VPPostList *postList))success
                                failure: (void (^)(NSError *error))failure
{
    //TODO, overwrite parameters in url if both not null
    VPAPIClient *client = [VPAPIClient sharedInstance];
    if (!url) {
        return [client fetchMyPostsWithType:kVPMyAllPosts param:params success:success failure:failure];
    } else {
        return [client fetchMyPostsWithUrl:url success:success failure:failure];
    }
}

+ (NSOperation *)fetchPostsWithType:(VPPostListFetchType)type
                                url:(NSString *)url
                              param:(NSDictionary *)param
                            success:(void (^)(VPPostList *postList))success
                            failure:(void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    if (![NSString isEmptyString:url]) {
        return [client fetchPostsWithUrl:url success:success failure:failure];
    }

    NSMutableDictionary *mparam = [NSMutableDictionary dictionaryWithDictionary:param];
    
    switch (type) {
        case kVPPostListFetchTypeGlobalPopular:{
            [mparam setValue:@"gp" forKey:@"_k"];
            return [client fetchPostsWithParam:mparam success:success failure:failure];
        }
        case kVPPostListFetchTypeGlobalRecent:{
            [mparam setValue:@"gr" forKey:@"_k"];
            return [client fetchPostsWithParam:mparam success:success failure:failure];
        }
        case kVPPostListFetchTypeGroupPopular:{
            NSNumber *groupId = [param objectForKey:@"groupId"];
            [mparam removeObjectForKey:@"groupId"]; //Skip it from http parameters
            [mparam setValue:@"grp" forKey:@"_k"];
            return [client fetchPostsWithParam:mparam groupId:groupId success:success failure:failure];
        }
        case kVPPostListFetchTypeGroupRecent:{
            NSNumber *groupId = [param objectForKey:@"groupId"];
            [mparam removeObjectForKey:@"groupId"]; //Skip it from http parameters
            [mparam setValue:@"grr" forKey:@"_k"];
            return [client fetchPostsWithParam:mparam groupId:groupId success:success failure:failure];
        }
        case kVPPostListFetchTypeHashtag:{
            NSString *hashtag = [param objectForKey:@"hashtag"];
            [mparam removeObjectForKey:@"hashtag"]; //Skip it from http parameters
            return [client fetchPostsWithParam:mparam hashtag:hashtag success:success failure:failure];
        }
        case kVPPostListFetchTypeId:{
            //If we don't specify _k in query, server will assume it is sorted by ID by default
            return [client fetchPostsWithParam:param success:success failure:failure];
        }
        case kVPPostListFetchTypeNearbyPopular:{
            [mparam setValue:@"nbp" forKey:@"_k"];
            return [client fetchPostsWithParam:mparam success:success failure:failure];
        }
        case kVPPostListFetchTypeNearbyRecent:{
            [mparam setValue:@"nbr" forKey:@"_k"];
            return [client fetchPostsWithParam:mparam success:success failure:failure];
        }
        case kVPPostListFetchTypeAnnouncement:{
            return [client fetchAnnouncementPostsWithParam:param success:success failure:failure];
        }
        case kVPPostListFetchTypeThread:{
            NSNumber *threadId = [param objectForKey:@"threadId"];
            [mparam removeObjectForKey:@"threadId"]; //Skip it from http parameters
            return [client fetchThreadWithId:threadId success:success failure:failure];
        }
        case kVPPostListFetchTypeFeatured:{
            return [client fetchFeaturedPostsWithParam:param success:success failure:failure];
        }
        default: //kVPPostListFetchTypeURL already handled above
            return nil;
    }
}

+ (NSOperation *)likePostWithId: (NSNumber *)postId
                         status: (BOOL)status
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client likePostWithId:postId status:status success:success failure:failure];
}

+ (NSOperation *)starPostWithId: (NSNumber *)postId
                            num: (NSUInteger)num
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client starPostWithId:postId num:num success:success failure:failure];
}

+ (NSOperation *)votePostWithId: (NSNumber *)postId
                         status: (VPVoteStatus)status
                        success: (void (^)(void))success
                        failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    switch (status) {
        case kVPVoteStatusDown:
            return [client votePostWithId:postId down:YES up:NO undown:NO unup:NO success:success failure:failure];
            
        case kVPVoteStatusUndown:
            return [client votePostWithId:postId down:NO up:NO undown:YES unup:NO success:success failure:failure];
            
        case kVPVoteStatusUp:
            return [client votePostWithId:postId down:NO up:YES undown:NO unup:NO success:success failure:failure];
            
        case kVPVoteStatusUnup:
            return [client votePostWithId:postId down:NO up:NO undown:NO unup:YES success:success failure:failure];
            
        case kVPVoteStatusDownSwitch:
            return [client votePostWithId:postId down:YES up:NO undown:NO unup:YES success:success failure:failure];
            
        case kVPVoteStatusUpSwitch:
            return [client votePostWithId:postId down:NO up:YES undown:YES unup:NO success:success failure:failure];
    }
}

@end
