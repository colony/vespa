//
//  VPReports.h
//  Vespa
//
//  Created by Jiayu Zhang on 11/20/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


/*
 Report Error name (prefixed by kVPError)
 */
extern NSString * const kVPErrorServerInternalError;
extern NSString * const kVPErrorAppleReverseGeo;
extern NSString * const kVPErrorAACAudioConversion;
extern NSString * const kVPErrorAACAudioConverterNotAvailable;
extern NSString * const kVPErrorGoogleReverseGeo;
extern NSString * const kVPErrorHttpTimeout;
extern NSString * const kVPErrorServerReverseGeo;
extern NSString * const kVPErrorVideoCapture;


/*
 Report Event name (prefixed by kVPEvent)
 */
//-------------------------------
// Related to post
// NOTE1: by default, all event (except CreatePost) here has parameter postId
// NOTE2: since 1.2.0, we change kVPEventCreatePost to kVPEventCreateImagePost
//
// kVPEventCommentPost
//   optionItemId - number, set if the comment attached with photoPickerOptionItem
//   hasImg - bool, set if the comment attached with uploaded/taken photo
// kVPEventFlagPost
//   reason - string, flag reason, same as the text shown in actionsheet
// kVPEventSharePost
//   type - string, activityType of the sharing service, got from UIActivityViewController's completionHandler
//   postType - string, post's type
//-------------------------------
extern NSString * const kVPEventCommentPost;
extern NSString * const kVPEventDownvotePost;
extern NSString * const kVPEventFlagPost;
extern NSString * const kVPEventSharePost;
extern NSString * const kVPEventSharePostAttempt;
extern NSString * const kVPEventUpvotePost;


//-------------------------------
// Audio post
// kVPEventCreateAudioPost
//   duration - number, audio duration in millisecond
//   filter - string, filter name, "original" is one of them
//   caption - number, 1 if user provide caption along with audio, 0 otherwise
//-------------------------------
extern NSString * const kVPEventCreateAudioPost;
extern NSString * const kVPEventPlayAudioPost;

//-------------------------------
// Link post
//   embedItemType - string
//-------------------------------
extern NSString * const kVPEventCreateLinkPost;

//-------------------------------
// Photo post
// kVPEventCreatePhotoPost
//    hasCaption - bool
//    isUplaoded - bool
//    isFiltered - bool
//-------------------------------
extern NSString * const kVPEventCreatePhotoPost;

//-------------------------------
// Text post
//   color - string, hex
//-------------------------------
extern NSString * const kVPEventCreateTextPost;

//-------------------------------
// Video post
//-------------------------------
extern NSString * const kVPEventCreateVideoPost;

//-------------------------------
// Card post
//    isRequest - bool
//-------------------------------
extern NSString * const kVPEventCreateCardPost;


//-------------------------------
// Related to comment
// NOTE: all event has parameter cmtId
//-------------------------------
extern NSString * const kVPEventDownvoteComment;
extern NSString * const kVPEventReplyComment; // Reply a comment successfully
extern NSString * const kVPEventUpvoteComment;


//-------------------------------
// Duration on each page/screen
//
// kVPEventViewGroup
// kVPEventViewGroupRule
//   grpId - group ID
// kVPEventViewPost
//   postId - post ID
// kVPEventViewPostHashtag
//   hashtag - opened hashtag
// kVPEventViewPostThread
//   threadId - thread ID
//-------------------------------
extern NSString * const kVPEventViewAnnouncement;
extern NSString * const kVPEventViewGroup;
extern NSString * const kVPEventViewGroupList;
extern NSString * const kVPEventViewGroupRule;
extern NSString * const kVPEventViewMyPost;
extern NSString * const kVPEventViewNearby;
extern NSString * const kVPEventViewNotification;
extern NSString * const kVPEventViewPost;
extern NSString * const kVPEventViewPostGlobalBest;
extern NSString * const kVPEventViewPostGlobalNew;
extern NSString * const kVPEventViewPostHashtag;
extern NSString * const kVPEventViewPostNearbyBest;
extern NSString * const kVPEventViewPostNearbyNew;
extern NSString * const kVPEventViewPostThread;
extern NSString * const kVPEventViewProfile;


//-------------------------------
// Miscellaneous
//
// kVPEventClickAudioFilter
//   filter - string, audio filter name
// kVPEventInvite
//   type - invitation is done same way as share (kVPEventSharePost)
// kVPEventFailReverseGeo
//   status - the status code returned by reverse geo api
// kVPEventToggleCamera
//   incam - 1 if the toggle happens in camera (when user long press)
//           0, otherwise, user swipe on maskview
// kVPEventReverseGeo
//   via - enum, "Google, Server, Apple"
// kVPEventSearchLink
//   search - bool, 1 if user searchs something, 0 if user directly type or paste a link
//-------------------------------
extern NSString * const kVPEventClickAudioFilter;
extern NSString * const kVPEventClickPoint;
extern NSString * const kVPEventDisableLocation;
extern NSString * const kVPEventFailReverseGeo;
extern NSString * const kVPEventInvite;
extern NSString * const kVPEventInviteAttempt;
extern NSString * const kVPEventReverseGeo;
extern NSString * const kVPEventSearchLink;
extern NSString * const kVPEventSignup;


@class VPGeo;

typedef NS_ENUM(NSInteger, VPEventMode) {
    kVPEventModeOnce = 0,
    kVPEventModeStart,
    kVPEventModeEnd
};

@interface VPReports : NSObject

/**
 * Get saved token from UserDefaults kVPUDDeviceToken, report to server if user already registerd
 * otherwise, do nothing
 */
+ (void)reportDeviceToken;

+ (void)reportError:(NSString *)name message:(NSString *)message error:(NSError*)error;

// This is a generic event reporting proxy function, under the hood
// the real caller could be Flurry, other analytics platform, or even
// our own server (for specific event)
//
// Detail on params per VPEvent:
// kVPEventButtonClicked:
//      name (type) - value
//
+ (void)reportEvent:(NSString *)event withParam:(NSDictionary *)param mode:(VPEventMode)mode;

+ (void)reportGeoLocation:(VPGeo *)geo;

@end
