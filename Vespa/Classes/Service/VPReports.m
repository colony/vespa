//
//  VPReports.m
//  Vespa
//
//  Created by Jiayu Zhang on 11/20/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPReports.h"

#import "Flurry.h"
#import "VPAPIClient.h"
#import "VPConfigManager.h"
#import "VPGeo.h"
#import "VPUsers.h"


NSString * const kVPErrorServerInternalError = @"APIServerInteralError";
NSString * const kVPErrorAppleReverseGeo = @"AppleReverseGeo";
NSString * const kVPErrorAACAudioConversion = @"AACAudioConversion";
NSString * const kVPErrorAACAudioConverterNotAvailable = @"AACAudioConverterNotAvailable";
NSString * const kVPErrorGoogleReverseGeo = @"GoogleReverseGeo";
NSString * const kVPErrorHttpTimeout = @"HttpTimeout";
NSString * const kVPErrorServerReverseGeo = @"ServerReverseGeo";
NSString * const kVPErrorVideoCapture = @"VideoCapture";


NSString * const kVPEventCommentPost = @"CommentPost";
NSString * const kVPEventDownvotePost = @"DownvotePost";
NSString * const kVPEventFlagPost = @"FlagPost";
NSString * const kVPEventSharePost = @"SharePost";
NSString * const kVPEventSharePostAttempt = @"SharePostAttempt";
NSString * const kVPEventUpvotePost = @"UpvotePost";

NSString * const kVPEventClickAudioFilter = @"ClickAudioFilter";
NSString * const kVPEventCreateAudioPost = @"CreateAudioPost";
NSString * const kVPEventPlayAudioPost = @"PlayAudioPost";

NSString * const kVPEventCreateTextPost = @"CreateTextPost";
NSString * const kVPEventCreateLinkPost = @"CreateLinkPost";
NSString * const kVPEventCreatePhotoPost = @"CreatePhotoPost";
NSString * const kVPEventCreateVideoPost = @"CreateVideoPost";
NSString * const kVPEventCreateCardPost = @"CreateCardPost";

NSString * const kVPEventDownvoteComment = @"DownvoteComment";
NSString * const kVPEventReplyComment = @"ReplyComment";
NSString * const kVPEventUpvoteComment = @"UpvoteComment";

NSString * const kVPEventViewAnnouncement = @"ViewAnnouncement";
NSString * const kVPEventViewGroup = @"ViewGroup";
NSString * const kVPEventViewGroupList = @"ViewGroupList";
NSString * const kVPEventViewGroupRule = @"ViewGroupRule";
NSString * const kVPEventViewMyPost = @"ViewMyPost";
NSString * const kVPEventViewNearby = @"ViewNearby";
NSString * const kVPEventViewNotification = @"ViewNotification";
NSString * const kVPEventViewPost = @"ViewPost";
NSString * const kVPEventViewPostGlobalBest = @"ViewPostGlobalBest";
NSString * const kVPEventViewPostGlobalNew = @"ViewPostGlobalNew";
NSString * const kVPEventViewPostHashtag = @"ViewPostHashtag";
NSString * const kVPEventViewPostNearbyBest = @"ViewPostNearbyBest";
NSString * const kVPEventViewPostNearbyNew = @"ViewPostNearbyNew";
NSString * const kVPEventViewPostThread = @"ViewPostThread";
NSString * const kVPEventViewProfile = @"ViewProfile";

NSString * const kVPEventClickPoint = @"ClickPoint";
NSString * const kVPEventDisableLocation = @"DisableLocation";
NSString * const kVPEventFailReverseGeo = @"FailReverseGeo";
NSString * const kVPEventInvite = @"Invite";
NSString * const kVPEventInviteAttempt = @"InviteAttempt";
NSString * const kVPEventReverseGeo = @"ReverseGeo";
NSString * const kVPEventSearchLink = @"SearchLink";
NSString * const kVPEventSignup = @"Signup";



@interface VPReports ()

@end


@implementation VPReports

#pragma mark - public interface
+ (void)reportDeviceToken
{
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:kVPUDDeviceToken];
    if (token) {
        if ([VPUsers hasMe]) {
            VPAPIClient *client = [VPAPIClient sharedInstance];
            [client reportNotificationToken:token success:nil failure:nil];
        }
    }
}

+ (void)reportError:(NSString *)name message:(NSString *)message error:(NSError*)error
{
    [Flurry logError:name message:message error:error];
}

+ (void)reportEvent:(NSString *)event
          withParam:(NSDictionary *)param
               mode:(VPEventMode)mode
{
    switch (mode) {
        case kVPEventModeOnce:
            // Flurry
            [Flurry logEvent:event withParameters:param];
            break;
            
        case kVPEventModeStart:
            // Flurry
            [Flurry logEvent:event withParameters:param timed:YES];
            break;
            
        case kVPEventModeEnd:
            // Flurry
            [Flurry endTimedEvent:event withParameters:param];
            break;
    }
    
    if (event == kVPEventInvite) {
        VPAPIClient *client = [VPAPIClient sharedInstance];
        [client reportInviteWithType:param[@"type"] success:nil failure:nil];
    }
    else if (event == kVPEventSharePost) {
        VPAPIClient *client = [VPAPIClient sharedInstance];
        [client reportShareWithPostId:param[@"postId"] type:param[@"type"] success:nil failure:nil];
    }
}

+ (void)reportGeoLocation:(VPGeo *)geo;
{
    // If we not register yet, don't need to report now
    if (![VPUsers hasMe]) {
        return;
    }
    
    NSInteger threshold = [[VPConfigManager sharedInstance] getInteger:kVPConfigThrottleIntervalReportGeo];
    NSDate *lastTimeReportGeo = [[NSUserDefaults standardUserDefaults] objectForKey:kVPUDLastTimeReportGeo];
    if (lastTimeReportGeo && fabs([lastTimeReportGeo timeIntervalSinceNow]) < threshold)
        return;
    
    VPAPIClient *client = [VPAPIClient sharedInstance];
    [client reportLat:geo.latitude lng:geo.longitude isFirst:(lastTimeReportGeo == nil) success:nil failure:nil];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:kVPUDLastTimeReportGeo];
}

@end
