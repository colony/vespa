//
//  VPThreads.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@class VPPost;

@interface VPThreads : NSObject

+ (NSOperation *)fetchRootPostWithId:(NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure;

@end
