//
//  VPThreads.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPThreads.h"

#import "VPAPIClient.h"


@implementation VPThreads

+ (NSOperation *)fetchRootPostWithId:(NSNumber *)threadId
                             success:(void (^)(VPPost *post))success
                             failure:(void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    return [client fetchRootPostWithId:threadId success:success failure:failure];
}

@end
