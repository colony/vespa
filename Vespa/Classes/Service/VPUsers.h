//
//  VPUsers.h
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@class VPAccount;
@class VPPaginator;
@class VPStatus;


typedef NS_ENUM(NSInteger, VPFetchMeOption) {
    kVPFetchMeRemote = 0,
    kVPFetchMeLocal,
};

@interface VPUsers : NSObject


//+ (NSOperation *)fetchMeWithOption: (VPFetchMeOption)option
//                           success: (void (^)(VPAccount *))success
//                           failure: (void (^)(NSError *error))failure;

/* Fetch me from local, otherwise return nil */
+ (VPAccount *)fetchMe;

+ (NSNumber *)fetchUserId;

+ (BOOL)hasMe;

+ (NSOperation *)signupWithDeviceSuccess: (void (^)(void))success
                                 failure: (void (^)(NSError *error))failure;

#pragma mark - account management


#pragma mark - user status
+ (VPStatus *)fetchUserStatusLocal;

+ (void)fetchAndSyncUserStatusRemote;

+ (BOOL)updateUserStatus:(VPStatus *)newStatus;

+ (BOOL)updateUserStatusFromRemoteNotification:(NSDictionary *)userInfo;

/**
 * incr - YES if the val is increment/decrement to the status property
 * zero - YES if zero out the property value if it is negative
 * RETURN YES if the specified property has been updated, NO otherwise
 */
+ (BOOL)updateUserStatusPropertyKey:(NSString *)propertyKey value:(long)val incr:(BOOL)isIncr zero:(BOOL)isZero syncTime:(long long)syncTime;

@end
