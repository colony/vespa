//
//  VPUsers.m
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPUsers.h"

#import "Flurry.h"
#import "VPConfigManager.h"
#import "VPConstants.h"
#import "VPAccount.h"
#import "VPAPIClient.h"
#import "VPDeviceModelUtil.h"
#import "VPError.h"
#import "VPInitDataManager.h"
#import "VPLocationManager.h"
#import "VPStatus.h"
#import "VPUtil.h"


@interface VPUsers ()
+ (void)deleteMeFromLocal: (VPAccount *)me;
+ (VPAccount *)getMeFromLocal;
+ (void)saveMeToLocal: (VPAccount *)me;
@end

@implementation VPUsers

+ (NSOperation *)fetchMeWithOption: (VPFetchMeOption)option
                           success: (void (^)(VPAccount *))success
                           failure: (void (^)(NSError *error))failure
{
//    if (option == kVPFetchMeLocal) {
//        VPAccount *me = [self getMeFromLocal];
//        if (me) {
//            SAFE_BLOCK_RUN(success, me);
//        } else {
//            NSError *error = [[NSError alloc]initWithDomain:VPDataErrorDomain code:0 userInfo:nil];
//            SAFE_BLOCK_RUN(failure, error);
//        }
//    } else if (option == kVPFetchMeRemote) {
//        VPAPIClient *client = [VPAPIClient sharedInstance];
//        @weakify(self);
//        return [client fetchMeWithParam:nil success:^(VPAccount *me) {
//            @strongify(self);
//            if (!me.accountType) {
//                VPAccount *localMe = [self getMeFromLocal];
//                me.accountId = localMe.accountId;
//                me.accountType = localMe.accountType;
//                me.token = localMe.token;
//            }
//            [self saveMeToLocal:me];
//            SAFE_BLOCK_RUN(success, me);
//        } failure:^(NSError *error) {
//            SAFE_BLOCK_RUN(failure, error);
//        }];
//    }
    return nil;
}

+ (VPAccount *)fetchMe
{
    return [self getMeFromLocal];
}

+ (NSNumber *)fetchUserId
{
    VPAccount *act = [self fetchMe];
    if (!act)
        return nil;
    return act.userId;
}

+ (BOOL)hasMe
{
    VPAccount *localMe = [self getMeFromLocal];
    return localMe ? YES : NO;
}

+ (NSOperation *)signupWithDeviceSuccess: (void (^)(void))success
                                 failure: (void (^)(NSError *error))failure
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
//    NSString *distinctId = [VPUtil defaultDistinctId];
    NSString *idfa = [VPUtil identifierForAds];
    
    return [client signupWithDeviceId:idfa
                          deviceModel:[VPDeviceModelUtil deviceModelName]
                              success:^(VPAccount *account) {
                                  [self saveMeToLocal:account];
                                  [client initSession:account.token];
                                  
                                  // Re-sync initialization data with session token
                                  [[VPInitDataManager sharedInstance]fetchInitData];
                                  
                                  [Flurry setUserID:[account.userId stringValue]];
                                  
                                  // Give user an initial onboarding rewarding points directly
                                  NSInteger points = [[VPConfigManager sharedInstance] getInteger:kVPConfigOnboardPoints];
                                  [VPUsers updateUserStatusPropertyKey:@"points" value:points incr:NO zero:YES syncTime:[VPUtil currentSystemTimeInMs]];
                                  
                                  [VPReports reportEvent:kVPEventSignup withParam:nil mode:kVPEventModeOnce];
                                  SAFE_BLOCK_RUN(success);
                              } failure:^(NSError *error) {
                                  SAFE_BLOCK_RUN(failure, error);
                              }];
}


#pragma mark - user status
+ (VPStatus *)fetchUserStatusLocal
{
    return [[NSUserDefaults standardUserDefaults] customObjectForKey:kVPUDMyStatus];
}

+ (void)fetchAndSyncUserStatusRemote
{
    // We use the request START time as syncTime for new status, so as to avoid including delay time on network
    long long nowSyncTime = [VPUtil currentSystemTimeInMs];
    VPAPIClient *client = [VPAPIClient sharedInstance];
    [client getMyStatusWithSuccess:^(VPStatus *status) {
        // This is extremely rare, sometimes, the RESTKIT mapping the response to VPPostList!!!
        if (![status isKindOfClass:[VPStatus class]])
            return;
        
        VPStatus *newStatus = [VPStatus new];
        for (NSString *key in [self statusPropertyKeys]) {
            [newStatus setValue:[status valueForKey:key] forKey:key syncTime:nowSyncTime];
        }
        if ([self updateUserStatus:newStatus]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserStatusChangeNotification object:nil userInfo:nil];
        }
    } failure:^(NSError *error) {
        VPLog(@"Fail fetch userstatus error %@", [error localizedDescription]);
    }];
}

+ (BOOL)updateUserStatus:(VPStatus *)newStatus
{
    @synchronized(self) {
        VPStatus *oldStatus = [[NSUserDefaults standardUserDefaults] customObjectForKey:kVPUDMyStatus];
        if (!oldStatus) {
            [[NSUserDefaults standardUserDefaults] setCustomObject:newStatus forKey:kVPUDMyStatus];
            return YES;
        } else {
            BOOL updated = NO;
            for (NSString *key in [self statusPropertyKeys]) {
                if ([newStatus syncTimeOf:key] > [oldStatus syncTimeOf:key]) {
                    [oldStatus setValue:[newStatus valueForKey:key] forKey:key syncTime:[newStatus syncTimeOf:key]];
                    updated = YES;
                }
            }
            if (updated) {
                [[NSUserDefaults standardUserDefaults] setCustomObject:oldStatus forKey:kVPUDMyStatus];
            }
            return updated;
        }
    }
}

+ (BOOL)updateUserStatusFromRemoteNotification:(NSDictionary *)userInfo
{
    NSDictionary *ninjr = [userInfo objectForKey:@"ninjr"];
    NSNumber *ts = [ninjr objectForKey:@"ts"];
    
    VPStatus *newStatus = [VPStatus new];
    NSNumber *nuac = [ninjr objectForKey:@"nuac"];
    if (nuac) {
        [newStatus setValue:nuac forKey:@"numUnreadActivities" syncTime:[ts longLongValue]];
    }
    NSNumber *nuan = [ninjr objectForKey:@"nuan"];
    if (nuan) {
        [newStatus setValue:nuan forKey:@"numUnreadAnnouncements" syncTime:[ts longLongValue]];
    }
    
    BOOL updated = [self updateUserStatus:newStatus];
    if (updated) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserStatusChangeNotification object:nil userInfo:nil];
    }
    return updated;
}

+ (BOOL)updateUserStatusPropertyKey:(NSString *)propertyKey value:(long)val incr:(BOOL)isIncr zero:(BOOL)isZero syncTime:(long long)syncTime
{
    BOOL updated = NO;
    @synchronized(self) {
        VPStatus *oldStatus = [[NSUserDefaults standardUserDefaults] customObjectForKey:kVPUDMyStatus];
        if (!oldStatus) {
            oldStatus = [VPStatus new];
        }
        
        if (syncTime > [oldStatus syncTimeOf:propertyKey]) {
            // numeric value for propertyKey:
            //   numUnreadActivities
            //   numUnreadAnnouncements
            //   points
            long oldValue = [[oldStatus valueForKey:propertyKey] longValue];
            long newValue = oldValue;
            newValue = isIncr ? (newValue + val) : val;
            newValue = isZero && newValue < 0 ? 0 : newValue;
            
            // other value types go here...
            
            if (oldValue != newValue) {
                [oldStatus setValue:@(newValue) forKey:propertyKey syncTime:syncTime];
                [[NSUserDefaults standardUserDefaults] setCustomObject:oldStatus forKey:kVPUDMyStatus];
                updated = YES;
            }
        }
    }
    if (updated) {
        if ([propertyKey isEqualToString:@"numUnreadActivities"] ||
            [propertyKey isEqualToString:@"numUnreadAnnouncements"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserStatusChangeNotification object:nil userInfo:nil];
        }
    }
    return updated;
}


#pragma mark - private method
+ (void)deleteMeFromLocal: (VPAccount *)me
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:kVPUDMe];
}

+ (void)saveMeToLocal: (VPAccount *)me
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setCustomObject:me forKey:kVPUDMe];
}

+ (VPAccount *)getMeFromLocal
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    VPAccount *me = (VPAccount *)[defaults customObjectForKey:kVPUDMe];
    return me;
}

+ (NSArray *)statusPropertyKeys
{
    static NSArray *_keys;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _keys = @[@"numUnreadActivities",
                  @"numUnreadAnnouncements",
                  @"points"];
        // Add more property key here
    });
    return _keys;
}


@end
