//
//  VPAppDelegate.h
//  Vespa
//
//  Created by Jiayu Zhang on 10/25/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
