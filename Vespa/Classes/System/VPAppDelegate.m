//
//  VPAppDelegate.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/25/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPAppDelegate.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Flurry.h"
#import <RestKit/RestKit.h>
#import "SDWebImage+Swizzle.h"
#import "VPAPIClient.h"
#import "VPAppWorker.h"
#import "VPConfigManager.h"
#import "VPConstants.h"
#import "VPHTTPClientListener.h"
#import "VPInitDataManager.h"
#import "VPListener.h"
#import "VPLoadUtil.h"
#import "VPLocationManager.h"
#import "VPLocationDisableViewController.h"
#import "VPMain2ViewController.h"
#import "VPNavigationController.h"
#import "VPPaginator.h"
#import "VPPostListViewController.h"
#import "VPReports.h"
#import "VPTutorialViewController.h"
#import "VPViewRedirectionUtil.h"
#import "VPUI.h"
#import "VPUsers.h"
#import "VPUtil.h"



@interface VPAppDelegate ()
@end

@implementation VPAppDelegate

#pragma mark - lifecycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //
    // Initialize app components (ORDER MATTERS)
    //
    
    // 1. UserDefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *defaultPrefsFile = [[NSBundle mainBundle] pathForResource:@"DefaultPreferences" ofType:@"plist"];
    NSDictionary *defaultPrefs = [NSDictionary dictionaryWithContentsOfFile:defaultPrefsFile];
    [defaults registerDefaults:defaultPrefs];
    
    NSString *currentAppVersion = [defaults stringForKey:kVPUDAppVersion];
    if (![currentAppVersion isEqualToString:kVPAppVersion]) {
        if (currentAppVersion) {
            // Upgraded user
            [defaults setBool:YES forKey:kVPUDAppUpgraded];
        } else {
            // Newly installed user
        }
        [defaults setObject:kVPAppVersion forKey:kVPUDAppVersion];
    }

    // 2. Third party API
    [Flurry setDebugLogEnabled:NO];
    [Flurry setCrashReportingEnabled:NO]; // We have Fabric
    [Flurry startSession:kVPFlurryKey];
    [Fabric with:@[CrashlyticsKit]];
    
    // 3. Listeners
    [[VPListener sharedInstance] startListen];
    [[VPHTTPClientListener sharedListener] startListen];
    
    // 4. Managers
    [VPConfigManager sharedInstance];
    [[VPInitDataManager sharedInstance] fetchInitData];
    
    // 5. Others go here...
    [SDImageCache swizzle]; // Swizzle
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[VPUI colonyColor]];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0)) {
        // Before ios8, we set translucent in VPBaseVC
        [[UINavigationBar appearance] setTranslucent:NO];
    }
    [VPUtil registerDeviceToken];
    [[VPAppWorker sharedInstance] perform];
    [SVProgressHUD setForegroundColor:[VPUI colonyColor]];
    [SVProgressHUD setRingThickness:2];

    // 6. Window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    if ([VPUsers hasMe]) {
        NSNumber *userId = [VPUsers fetchUserId];
        [Flurry setUserID:[userId stringValue]];
        
        if ([[VPLocationManager sharedInstance] locationServiceAuthorized]) {
            [[VPLocationManager sharedInstance] updateGeo];

            [VPUsers fetchAndSyncUserStatusRemote];

            self.window.rootViewController = [[VPMain2ViewController alloc] init];
            [self.window makeKeyAndVisible];
        } else {
            self.window.rootViewController = [[VPLocationDisableViewController alloc] init];
            [self.window makeKeyAndVisible];
        } 
    } else {
        VPTutorialViewController *tutorialVC = [VPTutorialViewController new];
        self.window.rootViewController = tutorialVC;
        [self.window makeKeyAndVisible];
    }

    // 7. Preload keyboard to avoid lag on the first time
    dispatch_async(dispatch_get_main_queue(), ^(void){
        UITextField *dummy = [UITextField new];
        dummy.keyboardType = UIKeyboardTypeTwitter;
        [[[[UIApplication sharedApplication] windows] lastObject] addSubview:dummy];
        [dummy becomeFirstResponder];
        [dummy resignFirstResponder];
        [dummy removeFromSuperview];
    });
    
    // TEST cache related stuff
//    SDImageCache *imageCache = [SDImageCache sharedImageCache];
//    [imageCache clearMemory];
//    [imageCache clearDisk];
    
    return YES;
}

//- (void)printLocation:(CLLocation *)location
//{
//    [VPGeoUtil reverseGeocodeLocationApple:location completionHandler:^(VPGeo *geo) {
//        NSString *s = [NSString stringWithFormat:@"1|%f|%f|%@|%@|%@|%@|%@",
//                       location.coordinate.latitude,
//                       location.coordinate.longitude,
//                       placemark.subLocality,
//                       placemark.locality,
//                       [placemark.ISOcountryCode isEqualToString:@"US"] ? [VPGeoUtil convertStateFull:placemark.administrativeArea] : placemark.administrativeArea,
//                       placemark.country,
//                       placemark.ISOcountryCode];
//        NSLog(@"%@", s);
//    }];
//}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[VPListener sharedInstance] stopListen];
    [[VPHTTPClientListener sharedListener] stopListen];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[VPListener sharedInstance] startListen];
    [[VPHTTPClientListener sharedListener] startListen];

    [[VPInitDataManager sharedInstance] fetchInitData];
    
    if ([VPUsers hasMe]) {
        if ([[VPLocationManager sharedInstance] locationServiceAuthorized]) {
            [[VPLocationManager sharedInstance] updateGeo];
            [VPUsers fetchAndSyncUserStatusRemote];

            if ([self.window.rootViewController isKindOfClass:[VPLocationDisableViewController class]]) {
                self.window.rootViewController = [[VPMain2ViewController alloc] init];
                [self.window makeKeyAndVisible];
            } else {
                //TODO: reload feed when foreground?
                //[VPLoadUtil reloadFeedIfPossibleWhenAppForeground];
            }
        } else {
            if ([self.window.rootViewController isKindOfClass:[VPMain2ViewController class]]) {
                self.window.rootViewController = [[VPLocationDisableViewController alloc] init];
                [self.window makeKeyAndVisible];
            }
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if (!url)
        return NO;
    
    if ([[url scheme] isEqualToString:@"tumblr-colony"]) {
//        return [[TMAPIClient sharedInstance]handleOpenURL:url];
    }
    
    if ([[url scheme] hasPrefix:@"fb"]) {
//        return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    }
    
    if ([[url scheme] isEqualToString:@"colony"]) {
        // colony://blahblah
        [VPViewRedirectionUtil handleExternal:url controller:nil];
    }
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    // The delegate method is trigger if the following cases:
    // 1) First time registerDevice called and user grant
    // 2) Everytime after user launch the app and call [VPUtil registerDeviceToken], if the user has previously
    //    grant, the delegate will be immediately triggered
    
    // Raw device token is in binary, we need convert it to hex version
    // Something like "<4bytes 4bytes...>", 32bytes in total
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    VPLog(@"Report device token=%@", hexToken);
    [[NSUserDefaults standardUserDefaults] setObject:hexToken forKey:kVPUDDeviceToken];

    [VPReports reportDeviceToken];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    VPLog(@"Failed to get device token, error: %@", [error localizedDescription]);
}

// Something useful http://stackoverflow.com/questions/16393673/detect-if-the-app-was-launched-opened-from-a-push-notification
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // Recv PN when app is running
    if (application.applicationState == UIApplicationStateActive) {
        NSDictionary *aps = [userInfo objectForKey:@"aps"];

        NSNumber *badge = [aps objectForKey:@"badge"];
        if (badge) {
            [VPUsers updateUserStatusFromRemoteNotification:userInfo];
        }
    }
}

@end
