//
//  VPAppWorker.h
//  Vespa
//
//  Created by Jiayu Zhang on 5/18/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@interface VPAppWorker : NSObject

+ (instancetype)sharedInstance;

// Perform a series of operations on startup
- (void)perform;

- (void)perform:(void (^)(void))block;

@end
