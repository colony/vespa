//
//  VPAppWorker.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/18/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPAppWorker.h"
#import "VPFileManager.h"

@interface VPAppWorker ()

@property (strong, nonatomic) NSOperationQueue *workerQueue;

@end

@implementation VPAppWorker

+ (instancetype)sharedInstance
{
    static VPAppWorker *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (NSOperationQueue *)workerQueue
{
    if (!_workerQueue) {
        _workerQueue = [[NSOperationQueue alloc] init];
        _workerQueue.name = @"com.ninjr.workerqueue";
        _workerQueue.maxConcurrentOperationCount = 1;
    }
    return _workerQueue;
}

- (void)perform:(void (^)(void))block
{
    [self.workerQueue addOperationWithBlock:block];
}

- (void)perform
{
    [self performCleanupLegacyDataFromOldVersion];
    [self performPreloadSomeData];
}

- (void)performCleanupLegacyDataFromOldVersion
{
    [self.workerQueue addOperationWithBlock:^{
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        NSInteger avId = [ud integerForKey:kVPUDLatestCleanAppVersionId];
        if (avId == kVPAppVersionId)
            return; // Clean up-to-date, bail out
        
        // Newly installed user doesn't need cleanup, since we introduce kVPUDAppUpgraded
        // at 1.2.0 (versionId=2), we need take care of it too
        if (![ud boolForKey:kVPUDAppUpgraded] && kVPAppVersionId > 2) {
            [ud setInteger:kVPAppVersionId forKey:kVPUDLatestCleanAppVersionId];
            return;
        }
        
        if (avId < 2) { //1.2.0, TEXT+AUDIO, and relayout to tab-based app
            [ud removeObjectForKey:@"InPlaceTutorialCustomScreenFlag"];
            [ud removeObjectForKey:@"CamMaskLayerTransform"];
            [ud removeObjectForKey:@"InPlaceTutorialSwitchCameraFlag"];
            [ud removeObjectForKey:@"StartTutorial"];
            [ud removeObjectForKey:@"InPlaceTutorialPostFeedFlag"];
            [ud removeObjectForKey:@"CamIndicatorTutorialFlag"];
            [ud removeObjectForKey:@"HonorCodeFlag"];
        }
        
        if (avId < 3) { //1.2.1, Fix audio crash, postVC use Slack, geo issue
            [ud removeObjectForKey:@"ReportGeoSignup"];

            NSFileManager *fileMgr = [NSFileManager defaultManager];
            NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
            NSString *camScreenImgPath = [NSString pathWithComponents:@[
                                                                        documentPath,
                                                                        @"ColonyDocument",
                                                                        @"screen"]];
            if ([fileMgr fileExistsAtPath:camScreenImgPath]) {
                [fileMgr copyItemAtPath:camScreenImgPath toPath:[documentPath stringByAppendingPathComponent:@"cam_screen"] error:nil];
            }
            [fileMgr removeItemAtPath:[documentPath stringByAppendingPathComponent:@"ColonyDocument"] error:nil];
        }
        
        if (avId < 4) { //2.0.0, LINK+GROUP
            [ud removeObjectForKey:@"InPlaceTutorialCustomScreenFlag"];
            [ud removeObjectForKey:@"CamMaskBlueScreenFlag"];
            [ud removeObjectForKey:@"CamMaskNewUserDisabled"];
            [ud removeObjectForKey:@"PromptInfiStar"];
            [ud removeObjectForKey:@"SerialTutorialCamera"];
            [ud removeObjectForKey:@"SerialTutorialHome"];
            
            NSFileManager *fileMgr = [NSFileManager defaultManager];
            NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
            NSString *camScreenImgPath = [documentPath stringByAppendingPathComponent:@"cam_screen"];
            if ([fileMgr fileExistsAtPath:camScreenImgPath]) {
                [fileMgr removeItemAtPath:camScreenImgPath error:nil];
            }
        }
        
        if (avId < 7) { //2.3.0, UI overhaul+IOS 9
            [ud removeObjectForKey:@"InPlaceTutorialChangeTextBackgroundColorFlag"];
            [ud removeObjectForKey:@"TutorialTribe"];
        }
        
        // other_version
        
        [ud setInteger:kVPAppVersionId forKey:kVPUDLatestCleanAppVersionId];
        [ud synchronize];
    }];
}

- (void)performPreloadSomeData
{
    // TODO, check some preload example, to actually DECODE an image
    [self.workerQueue addOperationWithBlock:^{
        [UIImage imageNamed:@"ShareTextBackground"]; // This is used to create an image for text post while sharing
        
        VPFileManager *fileMgr = [VPFileManager sharedInstance];
        if (![fileMgr existsDataWithFilePath:kVPFileLinkParserJavascript]) {
            NSString *bundleJsPath = [[NSBundle mainBundle] pathForResource:@"LinkParser.min" ofType:@"js"];
            [[NSFileManager defaultManager] copyItemAtPath:bundleJsPath toPath:[fileMgr pathForFile:kVPFileLinkParserJavascript] error:nil];
        }
    }];
}

@end
