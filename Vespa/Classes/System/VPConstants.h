//
//  VPConstants.h
//  Vespa
//
//  Created by Jay on 10/26/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

extern NSString *const kVPAppVersion;
extern NSUInteger const kVPAppVersionId; // A monotonically increasing integer ID per AppVersion, starting at zero

//------------------------------------------
// 3rd party
//------------------------------------------
extern NSString *const kVPFlurryKey;
extern NSString *const kVPNewRelicKey;
extern NSString *const kVPTumblrKey;
extern NSString *const kVPTumblrSecret;

//------------------------------------------
// API
//------------------------------------------
extern NSString *const kVPAPIHost;
extern NSString *const kVPAPISecureAPIHost;
extern NSString *const kVPAPIPath;

extern NSString *const kVPClientHeaderKey;
extern NSString *const kVPClientHeaderVal;
extern NSString *const kVPGeoHeaderKey;
extern NSString *const kVPLocaleHeaderKey;


//------------------------------------------
// VPConfigManager key (prefix kVPConfig)
//------------------------------------------
extern NSString * const kVPConfigAudioRecordConvertEnabled;
extern NSString * const kVPConfigAudioRecordTimeInSec; // Maximum seconds allowed to record an audio
extern NSString * const kVPConfigBadReviewCoverEnabled; // If YES, post whose is_bad_reviewed is set will be covered by a tomato picture, user has to dismiss it to reveal the content underneath
extern NSString * const kVPConfigCreatePostPoints;
extern NSString * const kVPConfigFinerGeoDisplayEnabled;
extern NSString * const kVPConfigFlagReason;
extern NSString * const kVPConfigGoogleMapApiKey;
extern NSString * const kVPConfigJPEGCompressionQuality;
extern NSString * const kVPConfigLinkParserJsUrl;
extern NSString * const kVPConfigMaxCaptureDuration;
extern NSString * const KVPConfigNinjrCookieUrl; // If set, we will display a menu item in "Me" tab and user clicks on it will redirect to the corresponding URL, otherwise, the menu item won't be shown
extern NSString * const kVPConfigOnboardPoints;
extern NSString * const kVPConfigPlaceholderWhenGeoMiss; // The placeholder to display when geo info (i.e. country, state, etc) is missing
extern NSString * const kVPConfigSharePostUrlPrefix; // The URL prefix used when share posts to outside platform
extern NSString * const kVPConfigThrottleIntervalReportGeo;  // Throttle interval, wait at least the number of seconds between geo reporting
extern NSString * const kVPConfigThrottleIntervalReverseGeo; // Throttle interval, wait at least the number of seconds between geo reversing


//------------------------------------------
// VPPromptManager key (prefix kVPPrompt)
// NOTE, all prompt key will also in UserDefaults to store a bool value
// to indicate if the prompt has been before
//------------------------------------------
extern NSString * const kVPPromptGroupIdea;
extern NSString * const kVPPromptHonorCode;
extern NSString * const kVPPromptPoint;
extern NSString * const kVPPromptWeb;


//------------------------------------------
// UserDefaults key (prefix kVPUD)
//------------------------------------------
extern NSString *const kVPUDClientConfigKey;
extern NSString *const kVPUDDeviceToken;
extern NSString *const kVPUDLastTimeReportGeo;
extern NSString *const kVPUDLastTimeReverseGeo;
extern NSString *const kVPUDLatestCleanAppVersionId; // Per app version, we might have something left over on disk that no longer needs in future. There is an operation in VPAppWorker to cleanup legacy data upon launch. The key is to track the latest AppVersionId has performed cleanup
extern NSString *const kVPUDLocation;
extern NSString *const kVPUDGroupPickList; // An array of "groupId,groupName" served as pick list when creating post
extern NSString *const kVPUDTumblrOAuthToken;
extern NSString *const kVPUDTumblrOAuthSecret;

/* Related to ME */
extern NSString *const kVPUDMe; // VPAccount obj of me
extern NSString *const kVPUDMyStatus;

/* Related to App */
extern NSString *const kVPUDAppVersion; // The current appVersion running on, by comparing this stored value with kVPAppVersion per update, we could determine if this is an upgraded user or new user
extern NSString *const kVPUDAppUpgraded; // 1 means, the user (not new) has upgraded to current app version (kVPUDAppVersion)


//------------------------------------------
// Notification (suffix Notification)
//------------------------------------------
/*
 VPListener Notification data (userinfo):
 kHostNotReachableNotification:
    serverMaintenance - BOOL, YES if host is not reachable because server is in maintenance, return 503 response code specifically
 kLoadingSpinnerShowNotification:
    status - the status to be displayed on spinner, if not set, only spinner is displayed
    allowUserInteraction - BOOL, YES if allow user interaction while the spinner is showing
 kPostModifiationNotification
    type - enum string (CHANGE, DELETE, CREATE)
    post - VPPost, the post
    from - enum string, (VPPostListVC, VPMyPostVC, VPPostVC) when type=CHANGE, this is set as VC's name indicating where change comes from
    like - number with bool, YES liked, NO unliked, set if type==CHNAGE
 kTabbarHideNotification
 kTabbarShowNotification
    animated - number with bool
 kLocationServiceStatusChangeNotification
    determined - BOOL, YES if user has either rejected or authorized the location permission, NO if user hasn't made decision
    available - BOOL, YES if geo has been updated and reversed, user can call VOLocationManager.currengeGeo, NO if user rejected the permission
 kTabbarCurrentTapNotification
    tab - enum string (GLOBAL, NEARBY, GROUP, PROFILE)
 */
extern NSString * const kHostNotReachableNotification;
extern NSString * const kInternetNotReachableNotification;
extern NSString * const kLoadingSpinnerShowNotification;
extern NSString * const kLoadingSpinnerHideNotification;
extern NSString * const kLocationServiceStatusChangeNotification;
extern NSString * const kPresentAddPostNotification;
extern NSString * const kTabbarCurrentTapNotification; // User click the tab they currently on
extern NSString * const kTabbarHideNotification;
extern NSString * const kTabbarShowNotification;
extern NSString * const kUserStatusChangeNotification;

extern NSString * const kPostModificationNotification;
extern NSString * const kPostModificationNotificationDeleteKey;
extern NSString * const kPostModificationNotificationInsertKey;
extern NSString * const kPostModificationNotificationUpdateKey;



//------------------------------------------
// Miscellaneous
//------------------------------------------
extern CGFloat const kVPDefaultLoadMoreFooterHeight;
extern CGFloat const kVPDefaultPullToRefreshExpandHeight;
extern CGFloat const kVPDefaultPreloadHeight;
extern CGFloat const kVPMinOffsetToggleTabbar; // Drag minimum to hide/show tabbar
extern CGFloat const kVPTabbarHeight;
extern NSInteger const kVPDefaultNumberOfItemsPerPage;
extern NSString * const kVPAssetTypeBlueScreen; // Used for custom mask screen


typedef NS_ENUM(NSInteger, VPVoteStatus) {
    kVPVoteStatusDown = 0,
    kVPVoteStatusUp,
    kVPVoteStatusUndown,
    kVPVoteStatusUnup,
    // These 2 status mean user down/upvotes the post and ALSO undone his previous vote (switch over)
    kVPVoteStatusDownSwitch,
    kVPVoteStatusUpSwitch
};


