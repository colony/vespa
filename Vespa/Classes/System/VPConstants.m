//
//  VPConstants.m
//  Vespa
//
//  Created by Jay on 10/26/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPConstants.h"

NSString *const kVPAppVersion = @"2.3.0";
NSUInteger const kVPAppVersionId = 7;

#pragma mark - 3rd
NSString *const kVPFlurryKey = @"Y5VCVFB2B2XGR95X7DZQ";   // Ninjr Dev
//NSString *const kVPFlurryKey = @"8PY52D5V2KVH84PX6243";   // Ninjr Prod
NSString *const kVPNewRelicKey = @"AA39530f073e7c03b4fd1ea4060d84868f135f174c";
NSString *const kVPTumblrKey = @"lA64nhHvS8EcgrM6AdCUyqSUf09cm0dRBWXIiJEReu6R46Qdj2";
NSString *const kVPTumblrSecret = @"7KutBXtAOrnc2V6AAXFJcPNptMcn7ZWonrhOlACPWnKqpGXngC";


#pragma mark - API related
//NSString *const kVPAPIHost = @"http://dev.colonyapp.com:8080";
//NSString *const kVPAPIPath = @"/gd/v1";
//NSString *const kVPAPIHost = @"http://localhost:8080";
//NSString *const kVPAPIPath = @"/gd/v1";
//NSString *const kVPAPIHost = @"http://192.168.1.120:8080"; // ubuntu
//NSString *const kVPAPIPath = @"/gd/v1";
//NSString *const kVPAPIHost = @"http://server.ninjrapp.com:8080";
//NSString *const kVPAPIPath = @"/gd/v1";
NSString *const kVPAPIHost = @"http://192.168.1.118:8080";   // laptop
NSString *const kVPAPIPath = @"/gd/v1";
//NSString *const kVPAPIHost = @"http://192.168.1.136:8080";   // jin laptop
NSString *const kVPAPISecureAPIHost = @"";

NSString *const kVPClientHeaderKey = @"X-GD-CLIENT";
NSString *const kVPGeoHeaderKey = @"X-GD-GEO";

//Format is "ClientType:version"
//Also note, the version is not same as the one on AppStore, it is mainly used as version
//control on features, server will behave/respond differently according to this value for
//certain use case
NSString *const kVPClientHeaderVal = @"IPHONE:5";
NSString *const kVPLocaleHeaderKey = @"X-GD-LOCALE";


#pragma mark - VPConfigManager key
NSString * const kVPConfigAudioRecordConvertEnabled = @"AudioRecordConvertEnabled";
NSString * const kVPConfigAudioRecordTimeInSec = @"AudioRecordTimeInSec";
NSString * const kVPConfigBadReviewCoverEnabled = @"BadReviewCoverEnabled";
NSString * const kVPConfigCreatePostPoints = @"CreatePostPoints";
NSString * const kVPConfigFinerGeoDisplayEnabled = @"FinerGeoDisplayEnabled";
NSString * const kVPConfigFlagReason = @"FlagReason";
NSString * const kVPConfigGoogleMapApiKey = @"GoogleMapApiKey";
NSString * const kVPConfigJPEGCompressionQuality = @"JPEGCompressionQuality";
NSString * const kVPConfigLinkParserJsUrl = @"LinkParserJsUrl";
NSString * const kVPConfigMaxCaptureDuration = @"MaxCaptureDuration";
NSString * const KVPConfigNinjrCookieUrl = @"NinjrCookieUrl";
NSString * const kVPConfigOnboardPoints = @"OnboardPoints";
NSString * const kVPConfigPlaceholderWhenGeoMiss = @"PlaceholderWhenGeoMiss";
NSString * const kVPConfigSharePostUrlPrefix = @"SharePostUrlPrefix";
NSString * const kVPConfigThrottleIntervalReportGeo = @"ThrottleIntervalReportGeo";
NSString * const kVPConfigThrottleIntervalReverseGeo = @"ThrottleIntervalReverseGeo";
NSString * const kVPConfigVideoBitRate = @"VideoBitRate";


#pragma mark - VPPromptManager key
NSString * const kVPPromptGroupIdea = @"PromptGroupIdea";
NSString * const kVPPromptHonorCode = @"PromptHonorCode";
//NSString * const kVPPromptInfiStar = @"PromptInfiStar";
NSString * const kVPPromptPoint = @"PromptPoint";
NSString * const kVPPromptWeb = @"PromptWeb";


#pragma mark - UserDefaults key
//NSString *const kVPUDCamMaskBlueScreenFlag = @"CamMaskBlueScreenFlag";
//NSString *const kVPUDCamMaskNewUserDisabled = @"CamMaskNewUserDisabled";
NSString *const kVPUDClientConfigKey = @"ClientConfig";
NSString *const kVPUDDeviceToken = @"DeviceToken";
NSString *const kVPUDLastTimeReportGeo = @"LastTimeReportGeo";
NSString *const kVPUDLastTimeReverseGeo = @"LastTimeReverseGeo";
NSString *const kVPUDLatestCleanAppVersionId = @"LatestCleanAppVersionId";
NSString *const kVPUDLocation = @"Location";
NSString *const kVPUDMe = @"Me"; //VPAccount obj of me.
NSString *const kVPUDMyStatus = @"MyStatus";
NSString *const kVPUDGroupPickList = @"GroupPickList";
NSString *const kVPUDTumblrOAuthToken = @"TumblrOAuthT";
NSString *const kVPUDTumblrOAuthSecret = @"TumblrOAuthS";

//NSString *const kVPUDInPlaceTutorialChangeTextBackgroundColorFlag = @"InPlaceTutorialChangeTextBackgroundColorFlag";
//NSString *const kVPUDInPlaceTutorialCustomScreenFlag = @"InPlaceTutorialCustomScreenFlag";
//NSString *const kVPUDSerialTutorialCameraFlag = @"SerialTutorialCamera";
//NSString *const kVPUDSerialTutorialHomeFlag = @"SerialTutorialHome";

NSString *const kVPUDAppVersion = @"AppVersion";
NSString *const kVPUDAppUpgraded = @"AppUpgraded";


#pragma mark - Notification key
NSString * const kLoadingSpinnerShowNotification = @"LoadingSpinnerShowNotification";
NSString * const kLoadingSpinnerHideNotification = @"LoadingSpinnerHideNotification";
NSString * const kHostNotReachableNotification = @"HostNotReachableNotification";
NSString * const kInternetNotReachableNotification = @"InternetNotReachableNotification";
NSString * const kLocationServiceStatusChangeNotification = @"LocationServiceStatusChangeNotification";
NSString * const kPresentAddPostNotification = @"PresentAddPostNotification";
NSString * const kTabbarCurrentTapNotification = @"TabbarCurrentTapNotification";
NSString * const kTabbarHideNotification = @"TabbarHideNotification";
NSString * const kTabbarShowNotification = @"TabbarShowNotification";
NSString * const kPostModificationNotification = @"PostModificationNotification";
NSString * const kPostModificationNotificationDeleteKey = @"PostModificationNotificationKeyD";
NSString * const kPostModificationNotificationInsertKey = @"PostModificationNotificationKeyI";
NSString * const kPostModificationNotificationUpdateKey = @"PostModificationNotificationKeyU";
NSString * const kUserStatusChangeNotification = @"UserStatusChangeNotification";
//NSString * const kWebNotification = @"WebNotification";


#pragma mark - Miscellaneous
NSInteger const kVPDefaultNumberOfItemsPerPage = 20;
CGFloat const kVPDefaultPullToRefreshExpandHeight = 60.0f;
CGFloat const kVPDefaultPreloadHeight = 100.0;
CGFloat const kVPDefaultLoadMoreFooterHeight = 49.0; // Same as tabbar height
CGFloat const kVPMinOffsetToggleTabbar = 30.0f;
CGFloat const kVPTabbarHeight = 49.0f;
NSString * const kVPAssetTypeBlueScreen = @"VPAssetTypeBlueScreen";
NSString * const kVPLastSyncPoints = @"LastSyncPoints";


