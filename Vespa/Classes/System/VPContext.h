//
//  VPContext.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


// Predefined context domain, prefix by 'CD'
extern NSString * const kVPCDApp; // App-wise, global context domain


/**
 * The domain contains data necessary for ViewControllers that create and upload post (w/ media content)
 * For now, VPMediaSubmitViewController and VPMediaCardViewController will consult this domain
 * Since, this is not ViewController-specific domain, the caller should clear it RIGHT after fetch all
 * needed contents, otherwise, it won't be cleaned at dealloc
 *
 * KEYS:
 *     groupId - NSNumber, the ID of group that the user is under
 *     groupName - NSString, the name of group that the user is under
 *     createThreadType - NSString, set if the post is created, also create the thread of specified type
 *     threadId - NSNumber, the ID of thread to which the post belongs
 */
extern NSString * const kVPCDMediaSubmission;

/**
 * KEYS:
 *     optionItemId - NSNumber, the ID of the picked photoOptionItem when creating comment
 *     optionItemMedia - UIImage, object, the picked media
 */
extern NSString * const kVPCDPostViewController;



@interface VPContext : NSObject

+ (instancetype)sharedInstance;

/**
 * Remove all stored data associated with specified domain, ONLY predefined key whose values are cleared
 */
- (void)clearDomain:(NSString *)domain;

/**
 * Fetch the value associated with specified domain and key
 */
- (id)getDomain:(NSString *)domain key:(NSString *)key;

/**
 * If value is nil, the method takes no effect
 */
- (void)putDomain:(NSString *)domain key:(NSString *)key val:(id)value;

/**
 * Remove the specified domain and key, return YES if found and removed, NO otherwise
 */
- (BOOL)removeDomain:(NSString *)domain key:(NSString *)key;

@end
