//
//  VPContext.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPContext.h"
#import "NSString+Util.h"


// Context domain
NSString * const kVPCDApp = @"AppCD";
NSString * const kVPCDMediaSubmission = @"MediaSubmissionCD";
NSString * const kVPCDPostViewController = @"PostViewControllerCD";


@interface VPContext ()

@property (strong, nonatomic) NSMutableDictionary *context;

@end


@implementation VPContext

#pragma mark - initialization
+ (instancetype)sharedInstance
{
    static VPContext *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        _context = [NSMutableDictionary dictionary];
    }
    return self;
}


#pragma mark - public method
- (void)clearDomain:(NSString *)domain
{
    if ([NSString isEmptyString:domain])
        return;
    
    NSString *keyPrefix = [domain stringByAppendingString:@"."];
    
    NSMutableArray *keysToDelete = [NSMutableArray array];
    for (NSString *key in [self.context keyEnumerator]) {
        if ([key hasPrefix:keyPrefix]) {
            [keysToDelete addObject:key];
        }
    }
    
    [self.context removeObjectsForKeys:keysToDelete];
}

- (id)getDomain:(NSString *)domain key:(NSString *)key
{
    if ([NSString isEmptyString:domain])
        return nil;
    
    NSString *k = [NSString stringWithFormat:@"%@.%@", domain, key];
    return [self.context objectForKey:k];
}

- (void)putDomain:(NSString *)domain key:(NSString *)key val:(id)value
{
    [self.context setValue:value forKey:[NSString stringWithFormat:@"%@.%@", domain, key]];
}

- (BOOL)removeDomain:(NSString *)domain key:(NSString *)key
{
    NSString *k = [NSString stringWithFormat:@"%@.%@", domain, key];
    id obj = [self.context objectForKey:k];
    if (obj) {
        [self.context removeObjectForKey:k];
        return YES;
    }
    return NO;
}

@end
