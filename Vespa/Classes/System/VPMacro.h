//
//  VPMacro.h
//  Vespa
//
//  Created by Jay on 11/2/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#ifndef Vespa_VPMacro_h
#define Vespa_VPMacro_h

// strongify obj and return if nil.
#define strongifyAndReturnIfNil(VAR) \
strongify(VAR) \
if(!(VAR)){ return; }

// run the block if block is not nil
#define SAFE_BLOCK_RUN(block, ...)  block ? block(__VA_ARGS__) : 0

// app log
#ifdef DEBUG
#   define VPLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define VPLog(...)
#endif

#define ASYNC_MAIN(...) dispatch_async(dispatch_get_main_queue(), ^{ __VA_ARGS__ })

#if CGFLOAT_IS_DOUBLE
#define CGFMAX(x,y) fmax(x,y)
#else
#define CGFMAX(x,y) fmaxf(x,y)
#endif

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define ObjectOrNull(o) o ?: [NSNull null]


// NOTE: since ios8, screen dimensions are interface-oriented, here, we adapt the prior version to behave the same
#define SCREEN_HEIGHT (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1 ? (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width) : [[UIScreen mainScreen] bounds].size.height)
#define SCREEN_WIDTH (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1 ? (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height) : [[UIScreen mainScreen] bounds].size.width)
#define SCREEN_SCALE    [UIScreen mainScreen].scale

#define UIViewAutoresizingCentered (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin)
#define UIViewAutoresizingFill (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth)


#endif