//
//  VPDeviceModelUtil.h
//  Vespa
//
//  Created by Jiayu Zhang on 3/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


// For complete list check
// http://stackoverflow.com/questions/8292246/how-to-programmatically-differentiate-between-iphone-4-and-iphone-4s
// https://github.com/InderKumarRathore/DeviceUtil/blob/master/DeviceUtil.m

typedef NS_ENUM(NSInteger, VPDeviceModel) {
    kIPhoneUnknow = 0, // sentinel value
    
    kIPhone,
    kIPhone3G,
    kIPhone3GS,
    kIPhone4,
    kIPhone4_RevA,
    kIPhone4_CDMA,
    kIPhone4S,
    kIPhone5_GSM,
    kIPhone5_GSM_CDMA,
    kIPhone5c_GSM,
    kIPhone5c_GSM_CDMA,
    kIPhone5s_GSM,
    kIPhone5s_GSM_CDMA,
    kIPhone6_GSM_CDMA,
    kIPhone6Plus_GSM_CDMA,
    
    kIPod1G,
    kIPod2G,
    kIPod3G,
    kIPod4G,
    kIPod5G,

    kIPad,
    kIPad2,
    kIPad3,
    kIPad4,
    kIPad_MINI,
    kIPad_MINI_RETINA, //ipad2
    kIPad_MINI3,
    kIPad_AIR,
    kIPad_AIR2
};

@interface VPDeviceModelUtil : NSObject

+ (VPDeviceModel)deviceModel;

/**
 * Returns the device model name. Return nil if it is unidentifiable
 * Available name: 
 * IPHONE, IPHONE_3G, IPHONE_3GS, IPHONE_4, IPHONE_4S, IPHONE_5, IPHONE_5C, IPHONE_5S, IPHONE_6, IPHONE_6PLUS
 */
+ (NSString *)deviceModelName;

@end
