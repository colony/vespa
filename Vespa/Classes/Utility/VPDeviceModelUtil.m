//
//  VPDeviceModelUtil.m
//  Vespa
//
//  Created by Jiayu Zhang on 3/4/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPDeviceModelUtil.h"
#import <sys/utsname.h>



@implementation VPDeviceModelUtil

+ (VPDeviceModel)deviceModel
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *machineName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    //MARK: More official list is at
    //http://theiphonewiki.com/wiki/Models
    //MARK: You may just return machineName. Following is for convenience
    
    static NSDictionary *deviceModels = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        deviceModels = @{
                         @"iPhone1,1":    @(kIPhone),
                         @"iPhone1,2":    @(kIPhone3G),
                         @"iPhone2,1":    @(kIPhone3GS),
                         @"iPhone3,1":    @(kIPhone4),
                         @"iPhone3,2":    @(kIPhone4_RevA),
                         @"iPhone3,3":    @(kIPhone4_CDMA),
                         @"iPhone4,1":    @(kIPhone4S),
                         @"iPhone5,1":    @(kIPhone5_GSM),
                         @"iPhone5,2":    @(kIPhone5_GSM_CDMA),
                         @"iPhone5,3":    @(kIPhone5c_GSM),
                         @"iPhone5,4":    @(kIPhone5c_GSM_CDMA),
                         @"iPhone6,1":    @(kIPhone5s_GSM),
                         @"iPhone6,2":    @(kIPhone5s_GSM_CDMA),
                         @"iPhone7,1":    @(kIPhone6Plus_GSM_CDMA),
                         @"iPhone7,2":    @(kIPhone6_GSM_CDMA),
                         
                         @"iPod1,1":      @(kIPod1G),
                         @"iPod2,1":      @(kIPod2G),
                         @"iPod3,1":      @(kIPod3G),
                         @"iPod4,1":      @(kIPod4G),
                         @"iPod5,1":      @(kIPod5G),
                         
                         @"iPad1,1":      @(kIPad),
                         @"iPad1,2":      @(kIPad),
                         @"iPad2,1":      @(kIPad2),
                         @"iPad2,2":      @(kIPad2),
                         @"iPad2,3":      @(kIPad2),
                         @"iPad2,4":      @(kIPad2),
                         @"iPad2,5":      @(kIPad_MINI),
                         @"iPad2,6":      @(kIPad_MINI),
                         @"iPad2,7":      @(kIPad_MINI),
                         @"iPad3,1":      @(kIPad3),
                         @"iPad3,2":      @(kIPad3),
                         @"iPad3,3":      @(kIPad3),
                         @"iPad3,4":      @(kIPad4),
                         @"iPad3,5":      @(kIPad4),
                         @"iPad3,6":      @(kIPad4),
                         @"iPad4,1":      @(kIPad_AIR),
                         @"iPad4,2":      @(kIPad_AIR),
                         @"iPad4,3":      @(kIPad_AIR),
                         @"iPad4,4":      @(kIPad_MINI_RETINA),
                         @"iPad4,5":      @(kIPad_MINI_RETINA),
                         @"iPad4,6":      @(kIPad_MINI_RETINA),
                         @"iPad4,7":      @(kIPad_MINI3),
                         @"iPad4,8":      @(kIPad_MINI3),
                         @"iPad5,3":      @(kIPad_AIR2),
                         @"iPad5,4":      @(kIPad_AIR2)
                         };
    });
    
    NSNumber *n = deviceModels[machineName];
    if (n == nil)
        return kIPhoneUnknow;
    
    return [n integerValue];
}

+ (NSString *)deviceModelName
{
    VPDeviceModel dm = [self deviceModel];
    if (dm == kIPhoneUnknow)
        return @"UNIDENTIFIED";
    
    switch (dm) {
        case kIPhone:
            return @"IPHONE";
        case kIPhone3G:
            return @"IPHONE_3G";
        case kIPhone3GS:
            return @"IPHONE_3GS";
        case kIPhone4:
        case kIPhone4_RevA:
        case kIPhone4_CDMA:
            return @"IPHONE_4";
        case kIPhone4S:
            return @"IPHONE_4S";
        case kIPhone5_GSM:
        case kIPhone5_GSM_CDMA:
            return @"IPHONE_5";
        case kIPhone5c_GSM:
        case kIPhone5c_GSM_CDMA:
            return @"IPHONE_5C";
        case kIPhone5s_GSM:
        case kIPhone5s_GSM_CDMA:
            return @"IPHONE_5S";
        case kIPhone6_GSM_CDMA:
            return @"IPHONE_6";
        case kIPhone6Plus_GSM_CDMA:
            return @"IPHONE_6PLUS";
        case kIPod1G:
            return @"IPOD_1G";
        case kIPod2G:
            return @"IPOD_2G";
        case kIPod3G:
            return @"IPOD_3G";
        case kIPod4G:
            return @"IPOD_4G";
        case kIPod5G:
            return @"IPOD_5G";
        case kIPad:
            return @"IPAD";
        case kIPad2:
            return @"IPAD2";
        case kIPad3:
            return @"IPAD3";
        case kIPad4:
            return @"IPAD4";
        case kIPad_MINI:
            return @"IPAD_MINI";
        case kIPad_MINI_RETINA: //ipad2
            return @"IPAD_MINI_RETINA";
        case kIPad_MINI3:
            return @"IPAD_MINI3";
        case kIPad_AIR:
            return @"IPAD_AIR";
        case kIPad_AIR2:
            return @"IPAD_AIR2";

        default:
            return nil;
    }
}

@end
