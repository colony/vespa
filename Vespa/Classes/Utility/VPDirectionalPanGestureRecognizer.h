//
//  VPDirectionalPanGestureRecognizer.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/29/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//  https://github.com/fastred/SloppySwiper
//


typedef NS_ENUM(NSInteger, VPPanDirection) {
    kVPPanDirectionRight = 0,
    kVPPanDirectionDown,
    kVPPanDirectionLeft,
    kVPPanDirectionUp
};

@interface VPDirectionalPanGestureRecognizer : UIPanGestureRecognizer

@property (assign, nonatomic) VPPanDirection direction;

@end
