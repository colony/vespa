//
//  VPDirectionalPanGestureRecognizer.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/29/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//  https://github.com/fastred/SloppySwiper
//

#import "VPDirectionalPanGestureRecognizer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>

@interface VPDirectionalPanGestureRecognizer ()

@property (assign, nonatomic) BOOL dragging;

@end


@implementation VPDirectionalPanGestureRecognizer

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    if (self.state == UIGestureRecognizerStateFailed) return;
    
    CGPoint velocity = [self velocityInView:self.view];
    
    // check direction only on the first move
    if (!self.dragging) {
        NSDictionary *velocities = @{
                                     @(kVPPanDirectionRight) : @(velocity.x),
                                     @(kVPPanDirectionDown) : @(velocity.y),
                                     @(kVPPanDirectionLeft) : @(-velocity.x),
                                     @(kVPPanDirectionUp) : @(-velocity.y)
                                     };
        NSArray *keysSorted = [velocities keysSortedByValueUsingSelector:@selector(compare:)];
        
        // Fails the gesture if the highest velocity isn't in the same direction as `direction` property.
        if ([[keysSorted lastObject] integerValue] != self.direction) {
            self.state = UIGestureRecognizerStateFailed;
        }
        
        self.dragging = YES;
    }
}

- (void)reset
{
    [super reset];
    self.dragging = NO;
}

@end
