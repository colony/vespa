//
//  VPError.h
//  Vespa
//
//  Created by Jay on 11/5/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const VPDataErrorDomain = @"VPDataErrorDomain";

@interface VPError : NSObject

@end
