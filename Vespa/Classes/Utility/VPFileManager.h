//
//  VPFileManager.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/26/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

// Audio related
extern NSString * const kVPFileAudioRecordM4A; // The recorded audio output, from VPMediaMicView, aac m4a
extern NSString * const kVPFileAudioFilterMonsterWAV; // The offline-filtered audio output, wav format
extern NSString * const kVPFileAudioFilterChipmunkWAV; // The offline-filtered audio output, wav format
extern NSString * const kVPFileAudioAACConvertM4A; // The converted-AAC-encoded audio output, m4a format

// Video related
extern NSString * const kVPFileVideoRecordMP4; // The recorded video output

// Misc
extern NSString * const kVPFileLinkParserJavascript;


@interface VPFileManager : NSObject

@property (strong, nonatomic, readonly) NSString *documentDirectoryPath;

+ (instancetype)sharedInstance;

- (NSData *)contentsAtFilePath:(NSString *)filePath;
- (void)deleteDataWithFilePath:(NSString *)filePath;
- (BOOL)existsDataWithFilePath:(NSString *)filePath;
- (NSData *)getDataWithFilePath:(NSString *)filePath;
/** Return an absolute path for the relative filepath in the ROOT document of the app */
- (NSString *)pathForFile:(NSString *)filePath;
- (unsigned long long)sizeAtFilePath:(NSString *)filePath; //in bytes
- (void)saveData:(NSData *)data withFilePath:(NSString *)filePath;

@end
