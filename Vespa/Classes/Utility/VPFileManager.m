//
//  VPFileManager.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/26/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPFileManager.h"

#define HTTP @"http"


NSString * const kVPFileAudioRecordM4A = @"audio/record.m4a";
NSString * const kVPFileAudioFilterMonsterWAV = @"audio/moster_filter.wav";
NSString * const kVPFileAudioFilterChipmunkWAV = @"audio/chipmunk_filter.wav";
NSString * const kVPFileAudioAACConvertM4A = @"audio/convert.m4a";

NSString * const kVPFileVideoRecordMP4 = @"video/record.mp4";

NSString * const kVPFileLinkParserJavascript = @"link_parser.js";


@implementation VPFileManager

+ (instancetype)sharedInstance
{
    static VPFileManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        _documentDirectoryPath = [paths lastObject];
       
        // Create sub-directory if necessary
        NSFileManager* fileManager = [NSFileManager defaultManager];
        [fileManager createDirectoryAtPath:[_documentDirectoryPath stringByAppendingPathComponent:@"audio"] withIntermediateDirectories:YES attributes:nil error:nil];
        [fileManager createDirectoryAtPath:[_documentDirectoryPath stringByAppendingPathComponent:@"video"] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return self;
}

- (NSData *)contentsAtFilePath:(NSString *)filePath
{
    return [[NSFileManager defaultManager] contentsAtPath:[self.documentDirectoryPath stringByAppendingPathComponent:filePath]];
}

- (void)deleteDataWithFilePath:(NSString *)filePath
{
    if (![self existsDataWithFilePath:filePath])
        return;
    
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:[self.documentDirectoryPath stringByAppendingPathComponent:(filePath)] error:&error];
    if (!success) {
        VPLog(@"Error removing document path: %@", error.localizedDescription);
    }
}

- (BOOL)existsDataWithFilePath:(NSString *)filePath
{
    return [[NSFileManager defaultManager] fileExistsAtPath:[self.documentDirectoryPath stringByAppendingPathComponent:(filePath)]];
}

- (NSData *)getDataWithFilePath:(NSString *)filePath
{
    return [[NSFileManager defaultManager] contentsAtPath:[self.documentDirectoryPath stringByAppendingPathComponent:(filePath)]];
}

- (NSString *)pathForFile:(NSString *)filePath
{
    return [self.documentDirectoryPath stringByAppendingPathComponent:filePath];
}

- (unsigned long long)sizeAtFilePath:(NSString *)filePath
{
    NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[self.documentDirectoryPath stringByAppendingPathComponent:(filePath)] error:nil];
    return [fileDictionary fileSize];
}

- (void)saveData:(NSData *)data withFilePath:(NSString *)filePath
{
    [data writeToFile:[self.documentDirectoryPath stringByAppendingPathComponent:(filePath)] atomically:YES];
}

@end
