//
//  VPGeoUtil.h
//  Vespa
//
//  Created by Jiayu Zhang on 4/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@class VPGeo;
@class VPPost;

@interface VPGeoUtil : NSObject

/**
 * Helper to convert from state (United States) abbreviation to full name
 */
+ (NSString *)convertStateFull:(NSString *)abbrev;

/**
 * The inverse version of convertStateFull:
 */
+ (NSString *)convertStateShort:(NSString *)fullname;

/**
 * Return a format string describing distance between the specified geo and current geo
 * Note, it returns empty string if either the parameter or current geo doesn't exist
 */
+ (NSString *)formatDistanceFromHere:(VPGeo *)geo;

/**
 * Same as formatDistanceFromHere:, but, take account into the containing VPPost,
 * because the geo display might be different on special post (i.e. 101 post)
 */
+ (NSString *)formatDistanceFromHere:(VPGeo *)geo withPost:(VPPost *)post;

+ (void)reverseGeocodeLocation:(CLLocation *)location completionHandler:(void (^)(VPGeo *geo))handler;
/** 
 * Reverse geo a location by using Apple Built-in CLGeocoder. The callback might pass
 * a nil param if something wrong happens under the hood
 */
//+ (void)reverseGeocodeLocation:(CLLocation *)location completionHandler:(void (^)(CLPlacemark *placemark))handler DEPRECATED_ATTRIBUTE;

/**
 * Reverse geo a location by using Google Map API. The callback might pass a nil
 * param if something wrong happens under the hood
 */
//+ (void)reverseGeocodeLocation2:(CLLocation *)location completionHandler:(void (^)(VPGeo *geo))handler;

@end
