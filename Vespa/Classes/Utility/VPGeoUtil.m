//
//  VPGeoUtil.m
//  Vespa
//
//  Created by Jiayu Zhang on 4/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGeoUtil.h"

#import "NSString+Util.h"
#import "VPAPIClient.h"
#import "VPConfigManager.h"
#import "VPGeo.h"
#import "VPHTTPClientManager.h"
#import "VPLocationManager.h"
#import "VPPost.h"
#import "VPReports.h"
#import "VPUtil.h"


static NSString * const _kGoogleGeocodingAPI = @"https://maps.googleapis.com/maps/api/geocode/json";

static NSString *const kUnitMeter = @"m";
static NSString *const kUnitKiloMeter = @"km";
static NSString *const kUnitFeet = @"ft";
static NSString *const kUnitMile = @"mi";

static double kMeterToFeet = 3.2808399;
static double kFeetToMiles = 5280;
static double kMeterCutoff = 1000;
static double kFeetCutoff = 3281;

@implementation VPGeoUtil

+ (NSString *)convertStateFull:(NSString *)abbrev
{
    if (abbrev.length <= 2) {
        return [[[self USStateAbbreviations] allKeysForObject:[abbrev uppercaseString]] firstObject];
    }
    return abbrev; // return itself if it is not abbreviation
}

+ (NSString *)convertStateShort:(NSString *)fullname
{
    if (fullname.length <= 2)
        return fullname;
    return [[self USStateAbbreviations] objectForKey:fullname];
}

+ (NSString *)formatDistanceFromHere:(VPGeo *)geo
{
    // 1. Display placeholder if incomplete geo data
    if (!geo || ![geo hasEnoughInfoToDisplay]) {
        return [[VPConfigManager sharedInstance] getString:kVPConfigPlaceholderWhenGeoMiss];
    }
    
    // 2. Display place name directly if exists
    if ([geo hasPlaceName]) {
        return geo.placeName;
    }

    // 3. If the client doesn't have geo
    VPGeo *curGeo = [[VPLocationManager sharedInstance] currentGeo];
    if (!curGeo) {
        return [[VPConfigManager sharedInstance] getString:kVPConfigPlaceholderWhenGeoMiss];
    }
    
    // 4. Same city, "neighborhood, city", *if configured*
    if ([[VPConfigManager sharedInstance] getBool:kVPConfigFinerGeoDisplayEnabled]) {
        if ([NSString isEqualButNotEmpty:curGeo.locality and:geo.locality caseSensitive:NO]) {
            if ([geo hasSublocality]) {
                return [NSString stringWithFormat:@"%@, %@", [geo sublocality], geo.locality];
            }
        }
    }
    
    // 5. Same country, "city,state"
    if ([NSString isEqualButNotEmpty:curGeo.countryCode and:geo.countryCode caseSensitive:NO]) {
        if ([geo hasState] && [geo hasLocality]) {
            return [NSString stringWithFormat:@"%@, %@", geo.locality, ([geo isUS] ? [self convertStateShort:geo.state] : geo.state)];
        }
    }
    
    // 6. Diff country
    if ([geo hasState]) {
        return [NSString stringWithFormat:@"%@, %@", geo.state, geo.countryCode];
    } else {
        return geo.country;
    }
}

+ (NSString *)formatDistanceFromHere:(VPGeo *)geo withPost:(VPPost *)post
{
    // For now, we do nothing special about the post itself
    return [self formatDistanceFromHere:geo];
}

+ (void)reverseGeocodeLocation:(CLLocation *)location completionHandler:(void (^)(VPGeo *geo))handler
{
    NSInteger threshold = [[VPConfigManager sharedInstance] getInteger:kVPConfigThrottleIntervalReverseGeo];
    NSDate *lastTimeReverseGeo = [[NSUserDefaults standardUserDefaults] objectForKey:kVPUDLastTimeReverseGeo];
    if (lastTimeReverseGeo && fabs([lastTimeReverseGeo timeIntervalSinceNow]) < threshold)
        return;
    
    // If it is WIFI or we have api KEY, use Google reversegeo API
    // If it is WWAN, if device lang is en, use Apple CLLocation
    // If it is WWAN, if device lang is not en, use Server as fallback
    
    NSString *mapKey = [[VPConfigManager sharedInstance] getString:kVPConfigGoogleMapApiKey];
    
    NSInteger it = [VPUtil checkNetworkConnection];
    if (![NSString isEmptyString:mapKey] || it == 2) { // WIFI
        [self reverseGeocodeLocationGoogle:location completionHandler:^(VPGeo *geo) {
            if (geo) {
                [VPReports reportEvent:kVPEventReverseGeo withParam:@{@"via":@"Google"} mode:kVPEventModeOnce];
                SAFE_BLOCK_RUN(handler, geo);
                return;
            }
            [self reverseGeocodeLocationFallback:location completionHandler:handler];
        }];
    } else {
        [self reverseGeocodeLocationFallback:location completionHandler:handler];
    }
}

+ (void)reverseGeocodeLocationFallback:(CLLocation *)location completionHandler:(void (^)(VPGeo *))handler
{
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([[language lowercaseString] isEqualToString:@"en"]) {
        [self reverseGeocodeLocationApple:location completionHandler:^(VPGeo *geo) {
            if (geo) {
                [VPReports reportEvent:kVPEventReverseGeo withParam:@{@"via":@"Apple"} mode:kVPEventModeOnce];
                SAFE_BLOCK_RUN(handler, geo);
            } else
                [self reverseGeocodeLocationServer:location completionHandler:handler];
        }];
    } else {
        [self reverseGeocodeLocationServer:location completionHandler:handler];
    }
}

+ (void)reverseGeocodeLocationApple:(CLLocation *)location completionHandler:(void (^)(VPGeo *geo))handler
{
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray* placemarks, NSError* error){
                       if (error) {
                           VPLog(@"Fail to reverse geo:%@", [error localizedDescription]);
                           [VPReports reportError:kVPErrorAppleReverseGeo message:[error localizedDescription] error:error];
                           SAFE_BLOCK_RUN(handler, nil);
                       } else {
                           CLPlacemark *placemark = [placemarks firstObject];
                           
                           VPGeo *geo = [VPGeo new];
                           geo.location = location;
                           geo.sublocality = placemark.subLocality;
                           geo.locality = placemark.locality;
                           geo.state = [placemark.ISOcountryCode isEqualToString:@"US"] ? [VPGeoUtil convertStateFull:placemark.administrativeArea] : placemark.administrativeArea;
                           geo.country = placemark.country;
                           geo.countryCode = placemark.ISOcountryCode;
                           SAFE_BLOCK_RUN(handler, geo);
                       }
                   }];
}

+ (void)reverseGeocodeLocationGoogle:(CLLocation *)location completionHandler:(void (^)(VPGeo *geo))handler
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[NSString stringWithFormat:@"%.8f,%.8f", location.coordinate.latitude, location.coordinate.longitude] forKey:@"latlng"];
    [param setValue:@"en" forKey:@"language"];
    
    NSString *mapKey = [[VPConfigManager sharedInstance] getString:kVPConfigGoogleMapApiKey];
    if (![NSString isEmptyString:mapKey]) {
        [param setValue:mapKey forKey:@"key"];
    }
    
    VPHTTPClientManager *httpClient = [VPHTTPClientManager sharedClient];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                            path:_kGoogleGeocodingAPI
                                                      parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                            NSString *status = JSON[@"status"];
                                                                                            if ([status isEqualToString:@"OK"]) {
                                                                                                VPGeo *geo = [VPGeo new];
                                                                                                geo.location = location;
                                                                                                @try {
                                                                                                    // NOTE: for geo.sublocality, neighborhood's long_name has priority over sublocality's
                                                                                                    BOOL hasNeighborhood = NO;
                                                                                                    NSArray *results = JSON[@"results"];
                                                                                                    for (NSUInteger i = 0; i < [results count]; ++i) {
                                                                                                        NSArray *addrComponents = results[i][@"address_components"];
                                                                                                        for (NSUInteger j = 0; j < [addrComponents count]; ++j) {
                                                                                                            NSDictionary *component = addrComponents[j];
                                                                                                            NSArray *types = component[@"types"];
                                                                                                            if (!hasNeighborhood && [types containsObject:@"neighborhood"]) {
                                                                                                                geo.sublocality = component[@"long_name"];
                                                                                                                hasNeighborhood = YES;
                                                                                                            } else if (![geo hasSublocality] && [types containsObject:@"sublocality"]) {
                                                                                                                geo.sublocality = component[@"long_name"];
                                                                                                            } else if (![geo hasLocality] && [types containsObject:@"locality"]) {
                                                                                                                geo.locality = component[@"long_name"];
                                                                                                            } else if (![geo hasState] && [types containsObject:@"administrative_area_level_1"]) {
                                                                                                                geo.state = component[@"long_name"];
                                                                                                            } else if (![geo hasCountry] && [types containsObject:@"country"]) {
                                                                                                                geo.country = component[@"long_name"];
                                                                                                                geo.countryCode = component[@"short_name"];
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } @catch (NSException *ex) {
                                                                                                    // Access nested structure of nsdictionary might cause trouble by sending
                                                                                                    // selector to nil value, we catch all exception here
                                                                                                    [VPReports reportEvent:kVPEventFailReverseGeo withParam:@{@"status":@"FAIL_JSON_PARSE"} mode:kVPEventModeOnce];
                                                                                                    SAFE_BLOCK_RUN(handler, nil);
                                                                                                    return;
                                                                                                }
                                                                                                SAFE_BLOCK_RUN(handler, geo);
                                                                                            } else {
                                                                                                [VPReports reportEvent:kVPEventFailReverseGeo withParam:@{@"status":status} mode:kVPEventModeOnce];
                                                                                                SAFE_BLOCK_RUN(handler, nil);
                                                                                            }
                                                                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            VPLog(@"Fail to reverse geo:%@", [error localizedDescription]);
                                                                                            [VPReports reportError:kVPErrorGoogleReverseGeo message:[error localizedDescription] error:error];
                                                                                            SAFE_BLOCK_RUN(handler, nil);
                                                                                        }];
    [httpClient enqueueHTTPRequestOperation:operation];
}

+ (void)reverseGeocodeLocationServer:(CLLocation *)location completionHandler:(void (^)(VPGeo *geo))handler
{
    VPAPIClient *client = [VPAPIClient sharedInstance];
    [client reverseGeo:location success:^(VPGeo *geo) {
        [VPReports reportEvent:kVPEventReverseGeo withParam:@{@"via":@"Server"} mode:kVPEventModeOnce];
        SAFE_BLOCK_RUN(handler, geo);
    } failure:^(NSError *error) {
        [VPReports reportError:kVPErrorServerReverseGeo message:[error localizedDescription] error:error];
        SAFE_BLOCK_RUN(handler, nil);
    }];
}


#pragma mark -
/**
 * Return 'X [UNIT]' by converting the input distance (in meter) by taking account into system metric preference
 */
+ (NSString *)formatDistance:(NSNumber *)distanceInMeter
{
    BOOL isMetric = [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
    double distance = distanceInMeter.doubleValue;
    NSString *unit;
    if (isMetric) {
        if (distance < kMeterCutoff) {
            unit = kUnitMeter;
        } else {
            unit = kUnitKiloMeter;
            distance = distance / 1000;
        }
    } else { // assume Imperial / U.S.
        distance = distance * kMeterToFeet;
        if (distance < kFeetCutoff) {
            unit = kUnitFeet;
        } else {
            unit = kUnitMile;
            distance = distance / kFeetToMiles;
        }
    }
    return [NSString stringWithFormat:@"%.2f %@", ceil(distance), unit];
}

+ (NSDictionary *)USStateAbbreviations
{
    static NSDictionary *nameAbbreviations = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        nameAbbreviations = [NSDictionary dictionaryWithObjectsAndKeys:
                             @"AL",@"Alabama",
                             @"AK",@"Alaska",
                             @"AZ",@"Arizona",
                             @"AR",@"Arkansas",
                             @"CA",@"California",
                             @"CO",@"Colorado",
                             @"CT",@"Connecticut",
                             @"DE",@"Delaware",
                             @"DC",@"District of Columbia",
                             @"FL",@"Florida",
                             @"GA",@"Georgia",
                             @"HI",@"Hawaii",
                             @"ID",@"Idaho",
                             @"IL",@"Illinois",
                             @"IN",@"Indiana",
                             @"IA",@"Iowa",
                             @"KS",@"Kansas",
                             @"KY",@"Kentucky",
                             @"LA",@"Louisiana",
                             @"ME",@"Maine",
                             @"MD",@"Maryland",
                             @"MA",@"Massachusetts",
                             @"MI",@"Michigan",
                             @"MN",@"Minnesota",
                             @"MS",@"Mississippi",
                             @"MO",@"Missouri",
                             @"MT",@"Montana",
                             @"NE",@"Nebraska",
                             @"NV",@"Nevada",
                             @"NH",@"New Hampshire",
                             @"NJ",@"New Jersey",
                             @"NM",@"New Mexico",
                             @"NY",@"New York",
                             @"NC",@"North Carolina",
                             @"ND",@"North Dakota",
                             @"OH",@"Ohio",
                             @"OK",@"Oklahoma",
                             @"OR",@"Oregon",
                             @"PA",@"Pennsylvania",
                             @"RI",@"Rhode Island",
                             @"SC",@"South Carolina",
                             @"SD",@"South Dakota",
                             @"TN",@"Tennessee",
                             @"TX",@"Texas",
                             @"UT",@"Utah",
                             @"VT",@"Vermont",
                             @"VA",@"Virginia",
                             @"WA",@"Washington",
                             @"WV",@"West Virginia",
                             @"WI",@"Wisconsin",
                             @"WY",@"Wyoming",
                             nil];
    });
    
    return nameAbbreviations;
}

@end
