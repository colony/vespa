//
//  VPImageFilterUtil.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@interface VPImageFilterUtil : NSObject

+ (UIImage *)filterImage:(UIImage *)image withName:(NSString *)filterName;

@end
