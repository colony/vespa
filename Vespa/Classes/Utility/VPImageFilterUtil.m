//
//  VPImageFilterUtil.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPImageFilterUtil.h"

#import "GPUImage.h"


@implementation VPImageFilterUtil

+ (UIImage *)filterImage:(UIImage *)image withName:(NSString *)filterName
{
    // Pro
    if ([filterName isEqualToString:@"Pro"]) {
        return [[[GPUImageToneCurveFilter alloc] initWithACV:@"xpro"] imageByFilteringImage:image];
    }
    
    // Azure
    else if ([filterName isEqualToString:@"Azure"]) {
        return [[[GPUImageToneCurveFilter alloc] initWithACV:@"aqua"] imageByFilteringImage:image];
    }
    
    // Mature
    else if ([filterName isEqualToString:@"Mature"]) {
        return [[[GPUImageMonochromeFilter alloc] init] imageByFilteringImage:image];
    }
    
    // B&W
    else if ([filterName isEqualToString:@"B&W"]) {
        return [[[GPUImageGrayscaleFilter alloc] init] imageByFilteringImage:image];
    }
    
    // Fade
    else if ([filterName isEqualToString:@"Fade"]) {
        return [[[GPUImageToneCurveFilter alloc] initWithACV:@"fade"] imageByFilteringImage:image];
    }
    
    //    // Linear
    //    else if ([filterName isEqualToString:@"Linear"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CISRGBToneCurveToLinear"];
    //    }
    //
    //    // Vignette
    //    else if ([filterName isEqualToString:@"Vignette"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIVignetteEffect"];
    //    }
    //
    //    // Earlybird
    //    //    else if ([filterName isEqualToString:@"Earlybird"]) {
    //    //        return [self filterViaGPUImage:image withFilter:@"IFEarlybirdFilter"];
    //    //    }
    //
    //    // Instant
    //    else if ([filterName isEqualToString:@"Instant"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIPhotoEffectInstant"];
    //    }
    //
    //    // Process
    //    else if ([filterName isEqualToString:@"Process"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIPhotoEffectProcess"];
    //    }
    //
    //    // Transfer
    //    else if ([filterName isEqualToString:@"Transfer"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIPhotoEffectTransfer"];
    //    }
    //
    //    // Sepia
    ////    else if ([filterName isEqualToString:@"Sepia"]) {
    ////        //        return [self filterViaCoreImage:image withFilter:@"CISepiaTone"];
    ////        return [self filterViaGPUImage:image withFilter:@"GPUImageSepiaFilter"];
    ////    }
    //
    //    // Chrome
    //    else if ([filterName isEqualToString:@"Chrome"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIPhotoEffectChrome"];
    //    }
    //
    //    // Fade
    //    else if ([filterName isEqualToString:@"Fade"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIPhotoEffectFade"];
    //    }
    //
    //    // Curve
    //    else if ([filterName isEqualToString:@"Curve"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CILinearToSRGBToneCurve"];
    //    }
    //
    //    // Tonal
    //    else if ([filterName isEqualToString:@"Tonal"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIPhotoEffectTonal"];
    //    }
    //
    //    // Noir
    //    else if ([filterName isEqualToString:@"Noir"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIPhotoEffectNoir"];
    //    }
    //    
    //    // Mono
    //    else if ([filterName isEqualToString:@"Mono"]) {
    //        return [self filterViaCoreImage:image withFilter:@"CIPhotoEffectMono"];
    //    }
    
    else {
        VPLog(@"Error: Unrecognized filter name %@", filterName);
        return image; // Return back the original one
    }
}

+ (UIImage *)filterViaCoreImage:(UIImage *)image withFilter:(NSString *)ciFilterName
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:ciFilterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    if([ciFilterName isEqualToString:@"CIVignetteEffect"]){
        // parameters for CIVignetteEffect
        CGFloat R = MIN(image.size.width, image.size.height)*image.scale/2;
        CIVector *vct = [[CIVector alloc] initWithX:image.size.width*image.scale/2 Y:image.size.height*image.scale/2];
        [filter setValue:vct forKey:@"inputCenter"];
        [filter setValue:[NSNumber numberWithFloat:0.9] forKey:@"inputIntensity"];
        [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    }
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    return result;
}

@end
