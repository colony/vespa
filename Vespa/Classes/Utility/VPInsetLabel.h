//
//  VPInsetLabel.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/24/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VPInsetLabel : UILabel

@property (assign, nonatomic) UIEdgeInsets insets;

@end
