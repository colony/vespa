//
//  VPInsetLabel.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/24/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPInsetLabel.h"

@implementation VPInsetLabel

- (void)setInsets:(UIEdgeInsets)insets
{
    _insets = insets ;
    [self invalidateIntrinsicContentSize] ;
}

- (void)drawTextInRect:(CGRect)rect
{
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.insets)];
}

//- (void)resizeHeightToFitText
//{
//    CGRect frame = [self bounds];
//    CGFloat textWidth = frame.size.width - (self.insets.left + self.insets.right);
//    
//    CGSize newSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(textWidth, MAXFLOAT) lineBreakMode:self.lineBreakMode];
//    
//            CGRect rect = [post.body boundingRectWithSize:CGSizeMake(width - 30, MAXFLOAT)
//                                                  options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
//                                               attributes:@{NSFontAttributeName : self.font}
//                                                  context:nil];
//    
//    frame.size.height = newSize.height + self.insets.top + self.insets.bottom;
//    self.frame = frame;
//}

- (CGSize)intrinsicContentSize
{
    CGSize superSize = [super intrinsicContentSize] ;
    superSize.height += self.insets.top + self.insets.bottom ;
    superSize.width += self.insets.left + self.insets.right ;
    return superSize ;
}

@end