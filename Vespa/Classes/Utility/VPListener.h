//
//  VPListener.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/13/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@interface VPListener : NSObject

+ (instancetype)sharedInstance;
- (void)startListen;
- (void)stopListen;

@end
