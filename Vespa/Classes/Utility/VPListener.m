//
//  VPListener.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/13/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPListener.h"

#import "SVProgressHUD.h"
#import "VPMain2ViewController.h"
#import "VPPost.h"
#import "VPStatus.h"
#import "VPUsers.h"


@interface VPListener ()

@property (strong, nonatomic) UIView *bannerView;

@end


@implementation VPListener

#pragma mark - lifecycle
+ (instancetype)sharedInstance;
{
    static VPListener *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)startListen
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self];
    [center addObserver:self selector:@selector(showLoadingSpinner:) name:kLoadingSpinnerShowNotification object:nil];
    [center addObserver:self selector:@selector(hideLoadingSpinner:) name:kLoadingSpinnerHideNotification object:nil];
    [center addObserver:self selector:@selector(hostNotReachable:) name:kHostNotReachableNotification object:nil];
    [center addObserver:self selector:@selector(internetNotReachable:) name:kInternetNotReachableNotification object:nil];
    [center addObserver:self selector:@selector(presentAddPost:) name:kPresentAddPostNotification object:nil];
    [center addObserver:self selector:@selector(showTabbar:) name:kTabbarShowNotification object:nil];
    [center addObserver:self selector:@selector(hideTabbar:) name:kTabbarHideNotification object:nil];
    [center addObserver:self selector:@selector(userStatusChanged:) name:kUserStatusChangeNotification object:nil];
}

- (void)stopListen
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - notification handle
- (void)showLoadingSpinner:(NSNotification *)notification
{
    NSString *status;
    BOOL allowUserInteraction = NO;
    if (notification.userInfo) {
        allowUserInteraction = [notification.userInfo[@"allowUserInteraction"] boolValue];
        status = notification.userInfo[@"status"];
    }
    
    if (status) {
        [SVProgressHUD showWithStatus:status maskType:allowUserInteraction ? SVProgressHUDMaskTypeNone : SVProgressHUDMaskTypeClear];
    } else {
        [SVProgressHUD showWithMaskType:allowUserInteraction ? SVProgressHUDMaskTypeNone : SVProgressHUDMaskTypeClear];
    }
}

- (void)hideLoadingSpinner:(NSNotification *)notification
{
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
}

- (void)internetNotReachable:(NSNotification *)notification
{
    [self displayBannerOnTop:@"Oops, could not connect to server. Please check your network settings."];
}

- (void)hostNotReachable:(NSNotification *)notification
{
    if(notification.userInfo && [notification.userInfo[@"serverMaintenance"] boolValue]) {
        [self displayBannerOnTop:@"We're in the middle of server maintenance. We'll be back in a few minutes."];
    } else {
        [self displayBannerOnTop:@"Oops, could not connect to server. Please Check your network settings."];
    }
}

- (void)presentAddPost:(NSNotification *)notification
{
    [[VPMain2ViewController accessInstance] presentAddPostViewController];
}

- (void)showTabbar:(NSNotification *)notification
{
    BOOL animated = NO;
    if (notification.userInfo)
        animated = [[[notification userInfo] valueForKey:@"animated"] boolValue];
    [[VPMain2ViewController accessInstance] showTabbarAnimated:animated];
}

- (void)hideTabbar:(NSNotification *)notification
{
    BOOL animated = NO;
    if (notification.userInfo)
        animated = [[[notification userInfo] valueForKey:@"animated"] boolValue];
    [[VPMain2ViewController accessInstance] hideTabbarAnimated:animated];
}

- (void)userStatusChanged:(NSNotification *)notification
{
    VPStatus *status = [VPUsers fetchUserStatusLocal];
    [UIApplication sharedApplication].applicationIconBadgeNumber = [status computeApplicationBadgeNumber];
}


#pragma mark - private method
/* Display an self-dismissed alert from top */
- (void)displayBannerOnTop:(NSString *)message
{
    if (self.bannerView) {
        [self.bannerView removeFromSuperview];
        self.bannerView = nil;
    }
    
    __block UIView *view = [UIView new];
    view.opaque = YES;
    view.backgroundColor = [UIColor redColor];
    
    UILabel *alertLabel = [UILabel new];
    alertLabel.font = [VPUI fontOpenSans:13.0f thickness:1];
    alertLabel.numberOfLines = 0;
    alertLabel.lineBreakMode = NSLineBreakByWordWrapping;
    alertLabel.text = message;
    alertLabel.textColor = [UIColor whiteColor];
    alertLabel.textAlignment = NSTextAlignmentCenter;
    
    CGSize expectLabelSize = [alertLabel sizeThatFits:CGSizeMake(SCREEN_WIDTH - 20, MAXFLOAT)];
    alertLabel.frame = CGRectMake(10.0, 20.0, SCREEN_WIDTH - 20, expectLabelSize.height);
    view.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, expectLabelSize.height + 5 + 20);
    [view addSubview:alertLabel];
    
    self.bannerView = view;
    
    [[UIApplication sharedApplication].keyWindow addSubview:view];
    
    view.y = -view.height;
    [UIView animateWithDuration:0.5 delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         view.y = 0.0f;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.5
                                               delay:4.0 // The delay means how long the alert will last on top
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              view.y = -view.height;
                                          } completion:^(BOOL finished) {
                                              [view removeFromSuperview];
                                              view = nil;
                                          }];
                     }];
}


@end
