//
//  VPLoadUtil.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/20/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>

//extern NSString *const kVPUDAppEnterTime; // The last time app enters (foregrounded or launched)
//extern NSString *const kVPUDLoadTimePostListViewController; // The last load time in seconds since epoc


@interface VPLoadUtil : NSObject

//+ (void)loadMyPostViewController:(BOOL)always load:(void (^)(void))callback;

//+ (void)loadNotificationViewController:(BOOL)always load:(void (^)(void))callback;

//+ (void)loadPostListViewController:(BOOL)always load:(void (^)(void))callback;

//+ (void)reloadFeedIfPossibleWhenAppForeground;

@end
