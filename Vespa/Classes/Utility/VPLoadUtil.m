//
//  VPLoadUtil.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/20/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPLoadUtil.h"

#import "VPContext.h"
#import "VPStatus.h"

// BE CAUTION, NOT overwrite keys defined in VPConstants!!
//NSString *const kVPUDAppEnterTime = @"AppEnterTime";
//NSString *const kVPUDLoadTimePostListViewController = @"LoadTimePostListVC";


@implementation VPLoadUtil

+ (void)loadNotificationViewController:(BOOL)always load:(void (^)(void))callback
{
//    if (always) { // This happens when the VC is FIRST time load
//        SAFE_BLOCK_RUN(callback);
//    } else {
//        VPStatus *status = [[NSUserDefaults standardUserDefaults] customObjectForKey:kVPUDMyStatus];
//        if (status && [status.numUnreadNotifications intValue] > 0) {
//            SAFE_BLOCK_RUN(callback);
//        }
//    }
}

//+ (void)reloadFeedIfPossibleWhenAppForeground
//{
//    VPMain3ViewController *main3VC = [VPMain3ViewController myself];
//    UIViewController *vc = [main3VC topViewController];
//    if ([vc isKindOfClass:[VPMain2ViewController class]]) {
//        VPMain2ViewController *main2VC = (VPMain2ViewController *)vc;
//        [main2VC reloadIndexIfCurrent:kVPPageIndexFeed];
//    }
//}

//+ (void)loadPostListViewController:(BOOL)always load:(void (^)(void))callback
//{
//    if (always) { // This happens when the VC is FIRST time load
//        [self saveNowForKey:kVPUDLoadTimePostListViewController];
//        SAFE_BLOCK_RUN(callback);
//    } else {
//        // Reload once after app launched or foregrounded
//        double appEnterTime = [[self ud] doubleForKey:kVPUDAppEnterTime];
//        double loadTime = [[self ud] doubleForKey:kVPUDLoadTimePostListViewController];
//        if (loadTime < appEnterTime) {
//            [self saveNowForKey:kVPUDLoadTimePostListViewController];
//            SAFE_BLOCK_RUN(callback);
//        }
//    }
//}

//+ (void)recordAppEnterTime
//{
//    [self saveNowForKey:kVPUDAppEnterTime];
//}


#pragma mark - private methods
+ (void) saveNowForKey:(NSString *)key
{
    // the number of seconds since epoch as a double
    [[NSUserDefaults standardUserDefaults] setDouble:[[NSDate date] timeIntervalSince1970] forKey:key];
}

+ (NSUserDefaults *)ud
{
    return [NSUserDefaults standardUserDefaults];
}

@end
