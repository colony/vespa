//
//  VPColor.h
//  Vespa
//
//  Created by Jiayu Zhang on 12/16/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@interface VPUI : NSObject

+ (UIColor *)backgroundColor;

/* Colony's company COLOR, widely used on components like navbar, tabbar, etc */
+ (UIColor *)colonyColor;

+ (UIColor *)darkGrayColor;

/* Used for background usually */
+ (UIColor *)lightGrayColor;

/* Used for gray font usually */
+ (UIColor *)mediumGrayColor;

/**
 * thickness - 0 (light), 1 (regular), 2 (semibold), 3 (bold)
 */
+ (UIFont *)fontOpenSans:(CGFloat)size thickness:(NSInteger)thickness;

@end
