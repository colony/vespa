//
//  VPColor.m
//  Vespa
//
//  Created by Jiayu Zhang on 12/16/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPUI.h"

@implementation VPUI

+ (UIColor *)backgroundColor
{
    return UIColorFromRGB(0xe9eaed);
}

+ (UIColor *)colonyColor
{
    return UIColorFromRGB(0x1088fe);
}

+ (UIColor *)darkGrayColor
{
    return UIColorFromRGB(0x66696d);
}

+ (UIColor *)lightGrayColor
{
   return UIColorFromRGB(0xf6f6f6);
}

+ (UIColor *)mediumGrayColor
{
    return UIColorFromRGB(0xb0b0b0);
}

+ (UIFont *)fontOpenSans:(CGFloat)size thickness:(NSInteger)thickness
{
    switch (thickness) {
        case 0:
            return [UIFont fontWithName:@"OpenSans-Light" size:size];
        case 1:
            return [UIFont fontWithName:@"OpenSans" size:size];
        case 2:
            return [UIFont fontWithName:@"OpenSans-Semibold" size:size];
        case 3:
            return [UIFont fontWithName:@"OpenSans-Bold" size:size];
        default:
            return nil;
    }
}

@end
