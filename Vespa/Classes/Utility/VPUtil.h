//
//  VPUtil.h
//  Vespa
//
//  Created by Jiayu Zhang on 11/22/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@class VPGeo;
@class VPPost;

@interface VPUtil : NSObject

typedef NS_ENUM(NSInteger, VPAccess) {
    kVPAccessAllowed = 0,
    kVPAccessDenied,
    kVPAccessUnknown
};

+ (VPAccess)checkCameraAvailability;
+ (VPAccess)checkMicrophoneAvailability; // NOTE, always return kVPAccessUnknown for ios7 prior
+ (VPAccess)checkPhotoRollAvailability;


#pragma mark - formation
/**
 Format the specified date to an user-friendly term since current time, i.e. 2s ago, 2min ago,
 The terms are interpreted as: 
    s - second, min - minute, hr - hour, d - day, wk - week
 If the date is more than a month older than now, the date is directly returned with NSDateFormatterMediumStyle
 If the date is equal or greater than now, "Just now" is returned
 NOTE: the parameter must conform to ISO standard (i.e. 2014-12-22T02:04:21+0000)
 */
+ (NSString *)formatDateSinceNow:(NSDate *)date;
/**
 Shorten number to the closest unit (K,M,B,T)
 */
+ (NSString *)formatNumber:(NSNumber *)number;
+ (NSString *)formatNumber:(NSNumber *)number singular:(NSString *)singularTerm plural:(NSString *)pluralTerm;


#pragma mark - view related
+ (void)showCameraAccessDenialAlert;
+ (void)showInvitationView:(UIViewController *)viewController;
+ (UIViewController *)getTopMostController;
/**
 * Present a mediaVC (from media package) based on the specified media type
 * For now, the allowed media types are - AUDIO,LINK,PHOTO,TEXT,VIDEO (! CARD not supported for now)
 * The method does nothing if passed mediaType is invalid
 */
+ (void)presentMediaViewController:(UIViewController *)presentingViewController mediaType:(NSString *)type;
/** if appStyle, reset navigation bar appearance to app's blue theme. otherwise, reset navigation to normal (this is useful before presenting UIActivityViewController) */
+ (void)resetNavigationBar:(BOOL)appStyle;


#pragma mark - misc.
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (long long)currentSystemTimeInMs;
+ (NSString *)hexStringFromColor:(UIColor *)color;
+ (NSString *)identifierForAds;
+ (NSString *)defaultDistinctId;
+ (CGSize)sizeForImageWithAspectRatio:(CGSize)imageSize parentViewSize:(CGSize)parentSize; // Compute the proper size for image in parent frame but also maintain aspect ratio
+ (NSDictionary *)parseUrlQuery:(NSURL *)url;
/**
 * 0 - NO, 1 - WWAN, 2 - WIFI
 */
+ (NSInteger)checkNetworkConnection;

+ (void)registerDeviceToken;

/**
 Restkit won't inject HTTP headers for request created outside. Only the request created from objectRequestOperationWithRequest has headers.
 To circumvent that, we generate the raw request and add all default headers from RestClientManager
 NOTE: return nil, if the url is malformed or nil
 */
+ (NSMutableURLRequest *)requestWithURL:(NSString *)urlStr;

+ (UIViewController *)rootViewController;

/**
 * Return a list of supported PostType in string, NOTE, the index of each PostType is as SAME as the ordinal of server's enum
 */
+ (NSArray *)supportedPostTypes;

/** Return the topmost viewcontroller is currently being presented (modal not navigation stack) */
+ (UIViewController *)topmostPresentedViewController;

@end
