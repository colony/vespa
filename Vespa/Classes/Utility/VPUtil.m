//
//  VPUtil.m
//  Vespa
//
//  Created by Jiayu Zhang on 11/22/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPUtil.h"

#import <AdSupport/ASIdentifierManager.h> 
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"
#import "VPGeo.h"
#import "VPHTTPClientManager.h"
#import "VPLocationManager.h"
#import "VPNavigationController.h"
#import "VPRestClientManager.h"
#import "VPUI.h"

//#import <AdSupport/AdSupport.h>

static NSString *const kUnitNone = @"";
static NSString *const kUnitThousand = @"K";
static NSString *const kUnitMillion = @"M";
static NSString *const kUnitBillion = @"B";
static NSString *const kUnitTrillion = @"T";

static NSString *const kUnitMeter = @"m";
static NSString *const kUnitKiloMeter = @"km";
static NSString *const kUnitFeet = @"ft";
static NSString *const kUnitMile = @"mi";

@implementation VPUtil


#pragma mark - public
+ (VPAccess)checkCameraAvailability
{
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    switch (authStatus) {
        case AVAuthorizationStatusDenied:
        case AVAuthorizationStatusRestricted:
            return kVPAccessDenied;
            
        case AVAuthorizationStatusAuthorized:
            return kVPAccessAllowed;
            
        case AVAuthorizationStatusNotDetermined:
        default:
            return kVPAccessUnknown;
    }
}

+ (VPAccess)checkMicrophoneAvailability
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0)) {
        switch ([[AVAudioSession sharedInstance] recordPermission]) {
            case AVAudioSessionRecordPermissionDenied:
                return kVPAccessDenied;
            case AVAudioSessionRecordPermissionGranted:
                return kVPAccessAllowed;
            case AVAudioSessionRecordPermissionUndetermined:
                return kVPAccessUnknown;
        }
    } else {
        // IOS 7 or prior, we always return Unknown, caller should call
        // requestRecordPermission to check which will display an alert
        // view at first time
        return kVPAccessUnknown;
    }
}

+ (VPAccess)checkPhotoRollAvailability
{
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    switch (status) {
        case ALAuthorizationStatusDenied:
        case ALAuthorizationStatusRestricted:
            return kVPAccessDenied;
            
        case ALAuthorizationStatusAuthorized:
            return kVPAccessAllowed;
            
        case ALAuthorizationStatusNotDetermined:
        default:
            return kVPAccessUnknown;
    }
    //TODO: ios8 Photos framework
}


#pragma mark - formation
+ (NSString *)formatDateSinceNow:(NSDate *)date
{
    static NSDateFormatter *userVisibleDateFmt;
    long interval = [date timeIntervalSinceNow] * -1;
    if (interval < 0) interval = 0;
    
    if (interval == 0) {
        return @"Just now";

    } else if (interval < 60) {
        return [NSString stringWithFormat:@"%ld %@", interval, @"s ago"];
    } else if (interval/60 < 60) {
        return [NSString stringWithFormat:@"%.f %@", floor(interval/60), @"m ago"];
    } else if (interval/3600 < 24) {
        return [NSString stringWithFormat:@"%.f %@", floor(interval/3600), @"h ago"];
    } else if (interval/86400 < 7) {
        return [NSString stringWithFormat:@"%.f %@", floor(interval/86400), @"d ago"];
    } else if (interval/604800 < 4) {
        return [NSString stringWithFormat:@"%.f %@", floor(interval/604800), @"wk ago"];
    } else if (interval/2419200 < 12) {
        return [NSString stringWithFormat:@"%.f %@", floor(interval/2419200), @"mo ago"];
    } else if (interval/29030400 < 2) {
        return [NSString stringWithFormat:@"%.f %@", floor(interval/29030400), @"yr ago"];
    } else {
        // 1+ year ago, display actual date
        // TODO: there might be a problem with NSDateFormatterMediumStyle, it will display
        // date and year, which might not fit in small rect
        // If a year older, format and return the date directly
        if (!userVisibleDateFmt) {
            // The default dateformat will take account into phone's setting (like Calendar)
            // https://developer.apple.com/library/ios/qa/qa1480/_index.html
            userVisibleDateFmt = [NSDateFormatter new];
            [userVisibleDateFmt setDateStyle:NSDateFormatterMediumStyle];
            [userVisibleDateFmt setTimeStyle:NSDateFormatterNoStyle];
        }
        return [userVisibleDateFmt stringFromDate:date];
    }
}

+ (NSString *)formatNumber:(NSNumber *)number
{
    NSString *shortNumber = @"0";
    if (number) {
        NSArray *suffixArray = @[kUnitNone, kUnitThousand, kUnitMillion, kUnitBillion, kUnitTrillion];
        NSUInteger theIntegerLength = [number stringValue].length;
        NSInteger displaySuffix = theIntegerLength / 3;
        displaySuffix = theIntegerLength % 3 == 0 ? displaySuffix - 1 : displaySuffix;
        double result = number.longValue / pow(10, 3 * displaySuffix);
        if (result > 0) {
            shortNumber = [NSString stringWithFormat:@"%.*f%@", (result < 10 && fmod(round(result * 10), 10)) ? 1 : 0, result,suffixArray[displaySuffix]];
        } else {
            shortNumber = number.stringValue;
        }
    }
    return shortNumber;
}

+ (NSString *)formatNumber:(NSNumber *)number singular:(NSString *)singularTerm plural:(NSString *)pluralTerm
{
    if (!number || [number intValue] == 0)
        return [NSString stringWithFormat:@"0 %@", singularTerm];
    if ([number intValue] == 1)
        return [NSString stringWithFormat:@"1 %@", singularTerm];
    return [NSString stringWithFormat:@"%@ %@", [self formatNumber:number], pluralTerm];
}


#pragma mark - view related
+ (void)showCameraAccessDenialAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Yo, there must've been a mix-up because you don't have access to use this camera. Fix it in your settings!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

+ (void)showInvitationView:(UIViewController *)viewController
{
    [VPReports reportEvent:kVPEventInviteAttempt withParam:nil mode:kVPEventModeOnce];
    
    NSString *textToShare = [NSString stringWithFormat:@"You should be a ninja. Check out the Ninjr App"];
    NSString *url = @"http://ninjrapp.com";
    NSArray *objectsToShare = @[textToShare, url];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAirDrop];
    if ([activityVC respondsToSelector:@selector(setCompletionWithItemsHandler:)]) { // IOS 8
        [activityVC setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
            if (completed && !activityError) {
                [VPReports reportEvent:kVPEventInvite withParam:@{@"type":activityType} mode:kVPEventModeOnce];
            }
        }];
    } else {
        [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
            if (completed) {
                [VPReports reportEvent:kVPEventInvite withParam:@{@"type":activityType} mode:kVPEventModeOnce];
            }
        }];
    }
    [viewController presentViewController:activityVC animated:YES completion:nil];
}

+ (UIViewController *)getTopMostController
{
    UIViewController *rootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [self getTopViewControllerWithRootViewController:rootVC];
}

+ (UIViewController*)getTopViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self getTopViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self getTopViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self getTopViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

+ (void)presentMediaViewController:(UIViewController *)presentingViewController mediaType:(NSString *)type
{
    NSString *clazz;
    if ([type isEqualToString:@"AUDIO"]) {
        clazz = @"VPMediaAudioViewController";
    } else if ([type isEqualToString:@"LINK"]) {
        clazz = @"VPMediaLinkViewController";
    } else if ([type isEqualToString:@"PHOTO"]) {
        clazz = @"VPMediaPhoto3ViewController";
    } else if ([type isEqualToString:@"TEXT"]) {
        clazz = @"VPMediaTextViewController";
    } else if ([type isEqualToString:@"VIDEO"]) {
        clazz = @"VPMediaPhoto4ViewController";
    } else if ([type isEqualToString:@"CARD"]) {
        VPLog(@"Unable to present VPMediaCardViewController here, need more init data");
        //clazz = @"VPMediaCardViewController";
    }
    
    if (!clazz) return;
    
    UIViewController *vc = [[NSClassFromString(clazz) alloc] init];
    VPNavigationController *nc = [[VPNavigationController alloc] initWithRootViewController:vc];
    nc.customNavigatonGestureEnabled = NO;
    [nc configureDefaultStyle];
    nc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    nc.modalPresentationCapturesStatusBarAppearance = YES;
    [presentingViewController presentViewController:nc animated:YES completion:nil];
}

+ (void)resetNavigationBar:(BOOL)appStyle
{
    // Reset the navigationbar default appearance
    if (appStyle) {
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setBarTintColor:[VPUI colonyColor]];
    } else {
        [[UINavigationBar appearance] setTintColor:nil];
        [[UINavigationBar appearance] setBarTintColor:nil];
    }
}


#pragma mark - misc.
+ (NSDictionary *)parseUrlQuery:(NSURL *)url
{
    NSString *query = [url query];
    if (!query)
        return nil;
    
    NSArray *queryPairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *pairs = [NSMutableDictionary dictionary];
    for (NSString *queryPair in queryPairs) {
        NSArray *bits = [queryPair componentsSeparatedByString:@"="];
        if ([bits count] != 2) { continue; }
        
        NSString *key = [[bits objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *value = [[bits objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [pairs setObject:value forKey:key];
    }
    return pairs;
}

+ (void)registerDeviceToken
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        // iOS 8 Notifications
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else {
        // iOS < 8 Notifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
}

+ (NSMutableURLRequest *)requestWithURL:(NSString *)urlStr
{
    NSURL *url = [NSURL URLWithString:urlStr];
    if (!url) {
        return nil;
    }
    
    NSMutableURLRequest *r = [NSMutableURLRequest requestWithURL:url];
    NSDictionary *headers = [[VPRestClientManager sharedClient] defaultHeaders];
    [r setAllHTTPHeaderFields:headers];
    return r;
}

+ (UIColor *)colorFromHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (long long)currentSystemTimeInMs
{
    // Incidentally, a Java Long is the same length as an Objective-C long long: 64 bits. An Objective-C long is a pseudonym for int
    
    NSDate *now = [NSDate date];
    NSTimeInterval i = [now timeIntervalSince1970];
    
    return (long long)(i * 1000);
}

+ (NSString *)hexStringFromColor:(UIColor *)color
{
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}

+ (NSString *)identifierForAds
{
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}

+ (NSString *)defaultDistinctId
{
    NSString *distinctId = nil;
    if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled])
    {
        NSUUID *IDFA = [[ASIdentifierManager sharedManager] advertisingIdentifier];
        distinctId = [IDFA UUIDString];
    }
    if (!distinctId) {
        distinctId = [[UIDevice currentDevice].identifierForVendor UUIDString];
    }
    if (!distinctId) {
        distinctId = [[NSUUID UUID] UUIDString];
    }
    return distinctId;
}

+ (CGSize)sizeForImageWithAspectRatio:(CGSize)imageSize parentViewSize:(CGSize)parentSize;
{
    CGFloat screenWidth = parentSize.width;
    CGFloat screenHeight = parentSize.height;
    CGFloat targetWidth = screenWidth;
    CGFloat targetHeight = screenHeight;
    CGFloat nativeHeight = screenHeight;
    CGFloat nativeWidth = screenWidth;
    if (imageSize.width > 0 && imageSize.height > 0) {
        nativeHeight = (imageSize.height > 0) ? imageSize.height : screenHeight;
        nativeWidth = (imageSize.width > 0) ? imageSize.width : screenWidth;
    }
    if (nativeHeight > nativeWidth) {
        if (screenHeight/screenWidth < nativeHeight/nativeWidth) {
            targetWidth = screenHeight / (nativeHeight / nativeWidth);
        } else {
            targetHeight = screenWidth / (nativeWidth / nativeHeight);
        }
    } else {
        if (screenWidth/screenHeight < nativeWidth/nativeHeight) {
            targetHeight = screenWidth / (nativeWidth / nativeHeight);
        } else {
            targetWidth = screenHeight / (nativeHeight / nativeWidth);
        }
    }
    return CGSizeMake(targetWidth, targetHeight);
}

+ (UIViewController *)rootViewController
{
    return [[UIApplication sharedApplication] keyWindow].rootViewController;
}

+ (NSArray *)supportedPostTypes
{
    static NSArray *types;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        types = @[@"AUDIO",@"CARD",@"LINK",@"PHOTO",@"TEXT",@"VIDEO"];
    });
    return types;
}

+ (UIViewController *)topmostPresentedViewController
{
    UIViewController *presentingVC = [self rootViewController];
    UIViewController *presentedVC = presentingVC.presentedViewController;
    
    while (presentedVC) {
        presentingVC = presentedVC;
        presentedVC = presentingVC.presentedViewController;
    }
    return presentingVC;
}

+ (NSInteger)checkNetworkConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    NSInteger i = status;
    
    [reachability stopNotifier];
    return i;
}

@end
