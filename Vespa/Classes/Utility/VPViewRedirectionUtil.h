//
//  VPViewRedirectionUtil.h
//  Vespa
//
//  Created by Jiayu Zhang on 12/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, VPRedirectAction)
{
    // Redirect to feed, optionally, the param could contain a VPPost (JUST created by user) with key "post"
    kVPFeedRedirectAction = 0,
    
    // Redirect to notification
    kVPNotificationRedirectAction,
    
    // Redirect to hashtag page
    kVPHashtagRedirectAction
};


@interface VPViewRedirectionUtil : NSObject

/**
 Below redirect action is allowed to build external url
 kVPHashtagRedirectAction
    id - the hashtag to open with
 */
+ (NSString *)buildExternalURL:(VPRedirectAction)action param:(NSDictionary *)param;

+ (void)handleExternal:(NSURL *)url controller:(UIViewController *)controller;

/**
 kVPFeedRedirectAction
    post - VPPost object (if any) created by user that want to be added on top of postlist feed
 kVPHashtagRedirectAction
    id - the hashtag
 */
+ (void)handleInternal:(VPRedirectAction)action param:(NSDictionary *)param controller:(UIViewController *)controller;

@end
