//
//  VPViewRedirectionUtil.m
//  Vespa
//
//  Created by Jiayu Zhang on 12/17/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPViewRedirectionUtil.h"

#import "NSUserDefaults+CustomObject.h"
#import "VPMain2ViewController.h"
#import "VPPostListViewController.h"
#import "VPUtil.h"


@implementation VPViewRedirectionUtil

+ (NSString *)buildExternalURL:(VPRedirectAction)action param:(NSDictionary *)param
{
    if (action == kVPHashtagRedirectAction) {
        return [NSString stringWithFormat:@"colony://hashtag?id=%@", param[@"id"]];
    }
    
    return nil;
}

+ (void)handleExternal:(NSURL *)url controller:(UIViewController *)controller
{
    // scheme://host?query
    if (![[url scheme] isEqualToString:@"colony"]) {
        return;
    }
    
    NSString *host = [url host];
    NSDictionary *param = [VPUtil parseUrlQuery:url];
    controller = controller ? controller : [VPUtil getTopMostController];
    
    if ([host isEqualToString:@"hashtag"]) {
        [self handleInternal:kVPHashtagRedirectAction param:param controller:controller];
    }
}

+ (void)handleInternal:(VPRedirectAction)target param:(NSDictionary *)param controller:(UIViewController *)controller;
{
    // The view controller in charge of entire app
    VPMain2ViewController *rootVC = (VPMain2ViewController *)[VPUtil rootViewController];

#pragma mark - Feed
    if (target == kVPFeedRedirectAction) {
        if ([rootVC presentedViewController]) {
            [rootVC dismissViewControllerAnimated:NO completion:nil];
        }
        
//        UINavigationController *nc = (UINavigationController *)[rootVC viewControllerAtTab:kVPTabIndexHome];
//        [nc popToRootViewControllerAnimated:NO];
//        [rootVC gotoTab:kVPTabIndexHome];
    }

#pragma mark - Hashtag
    else if (target == kVPHashtagRedirectAction) {
        VPPostListViewController *vc = [[VPPostListViewController alloc] initWithHashtag:param[@"id"]];
        [controller.navigationController pushViewController:vc animated:YES];
    }
}

@end
