//
//  VPWebViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/10/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

@interface VPWebViewController : VPBaseViewController
/**
 * customTitle - 
 *     Title of navigationbar, if not set, host extracted from url is used instead
 */
- (id)initWithURL:(NSURL *)url customTitle:(NSString *)customTitle;

/**
 * Under most cases, VPWebVC should be within a navigationController stack, so that we could setup the view, layout and navbar
 * properly. If the property is YES, VPWebVC is root of a navigationController and the navVC is presented. Otherwise, the
 * VPWebVC is pushed to a navigation stack
 */
@property (assign, nonatomic) BOOL isPresenting;

@end
