//
//  VPWebViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/10/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPWebViewController.h"

#import "UINavigationItem+Util.h"


@interface VPWebViewController ()<UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UIActivityIndicatorView *loadingIndicator;

@property (strong, nonatomic) NSURLRequest *requestToBeLoaded;
@property (strong, nonatomic) NSString *customTitle;

@end

@implementation VPWebViewController

#pragma mark - lifecycle
- (id)initWithURL:(NSURL *)url customTitle:(NSString *)customTitle
{
    if (self = [super init]) {
        _requestToBeLoaded = [NSURLRequest requestWithURL:url];
        _customTitle = customTitle;
        if (!_customTitle) {
            _customTitle = [[url host] lowercaseString];
            if ([_customTitle hasPrefix:@"www."]) {
                _customTitle = [_customTitle substringFromIndex:4];
            }
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.autoresizingMask = UIViewAutoresizingFill;
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    [self.view addSubview:self.webView];
    
    self.loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.loadingIndicator.autoresizingMask = UIViewAutoresizingCentered;
    self.loadingIndicator.frame = CGRectMake(0, 0, 20, 20);
    self.loadingIndicator.hidesWhenStopped = YES;
    self.loadingIndicator.userInteractionEnabled = NO;
    self.loadingIndicator.center = self.view.center;
    [self.view addSubview:self.loadingIndicator];
    
    [self.navigationItem setCustomTitle:self.customTitle textColor:nil font:nil];
    if (self.isPresenting) {
        UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        closeBtn.frame = CGRectMake(0, 0, 30, 30);
        closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [closeBtn setImage:[UIImage imageNamed:@"close_button_white"] forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setCustomLeftWithView:closeBtn];
        [self.navigationItem tweakLeftWithSpace:-8];
    }

    if (self.requestToBeLoaded) {
        [self.webView loadRequest:self.requestToBeLoaded];
    }
}

- (void)dealloc
{
    self.webView.delegate = nil;
    [self.webView stopLoading];
}


#pragma mark - action handling
- (void)closeButtonPressed:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return self.isPresenting;
}


#pragma mark - web view delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_loadingIndicator stopAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_loadingIndicator startAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    VPLog(@"loading web page error: %@", error);
    [_loadingIndicator stopAnimating];
}

@end
