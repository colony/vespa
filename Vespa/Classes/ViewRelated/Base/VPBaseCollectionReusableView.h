//
//  VPBaseCollectionReusableView.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@interface VPBaseCollectionReusableView : UICollectionReusableView

- (void)setupUI;
- (void)resetView;
+ (NSValue *)sizeOfView;

@end
