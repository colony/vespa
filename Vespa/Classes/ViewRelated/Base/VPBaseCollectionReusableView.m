//
//  VPBaseCollectionReusableView.m
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseCollectionReusableView.h"

@implementation VPBaseCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)prepareForReuse
{
    [self resetView];
    [super prepareForReuse];
}

- (void)setupUI
{
    
}

- (void)resetView
{
    
}

//default zero. sub class need to implement.
+ (NSValue *)sizeOfView
{
    return [NSValue valueWithCGSize:CGSizeZero];
}

@end
