//
//  VPBaseCollectionView.h
//  Vespa
//
//  Created by Jiayu Zhang on 5/18/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VPBaseCollectionView : UICollectionView

/** 
 * Minish the scrollview delay by cancelling touch in contentView that subclasses UIButton
 * To make it fully works, it will only set delaysContentTouches to NO
 */
@property (assign, nonatomic) BOOL shouldCancelUIButton;

@end
