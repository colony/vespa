//
//  VPBaseCollectionView.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/18/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseCollectionView.h"

@implementation VPBaseCollectionView

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    if (_shouldCancelUIButton) {
        if ([view isKindOfClass:[UIButton class]])
            return YES;
    }
    return [super touchesShouldCancelInContentView:view];
}

- (void)setShouldCancelUIButton:(BOOL)shouldCancelUIButton
{
    _shouldCancelUIButton = shouldCancelUIButton;
    self.delaysContentTouches = !_shouldCancelUIButton;
}

@end
