//
//  VPBaseCollectionViewCell.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@interface VPBaseCollectionViewCell : UICollectionViewCell

- (void)setupUI;
- (void)resetCell;
+ (NSValue *)sizeOfCell:(id)obj;
/** 
 This method is useful, when we register multiple cell classes in collectionview,
 After we load a list of objects of different type, each of them will be first
 check against this method before sizeOfCell:
 */
+ (BOOL)isAcceptable:(id)obj;

@end
