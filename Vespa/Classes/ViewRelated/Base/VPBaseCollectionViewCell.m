//
//  VPBaseCollectionViewCell.m
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseCollectionViewCell.h"

@implementation VPBaseCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)prepareForReuse
{
    [self resetCell];
    [super prepareForReuse];
}

- (void)setupUI
{
    
}

- (void)resetCell
{
    
}

//default zero. sub class need to implement.
+ (NSValue *)sizeOfCell:(id)obj
{
    return [NSValue valueWithCGSize:CGSizeZero];
}

//default NO, sub class need to implement
+ (BOOL)isAcceptable:(id)obj
{
    return NO;
}

@end
