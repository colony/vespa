//
//  VPBaseCollectionViewController.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseViewController.h"
#import "SSPullToRefreshView.h"

@interface VPBaseCollectionViewController : VPBaseViewController<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) SSPullToRefreshView *refreshView;

@property (nonatomic, assign) NSInteger currentPageNumber;
@property (nonatomic, assign) NSInteger numberOfItemsPerPage;
@property (nonatomic, assign) BOOL hasNextPage;

/** 
 * IMPORTANT, the cellClasses must have same length as cellNibNames, each one in cellNibNames corresponds to
 * the one in cellClasses, if nil is in cellNibNames, collectionview will registerClass, otherwise registerNib
 */
@property (nonatomic, strong) NSArray *cellNibNames;
@property (nonatomic, strong) NSArray *cellClasses;

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *cellSizes;
// use for store section information, sectionInfo.count = # of section, sectionInfo[x] = # of items in section x.
@property (strong) NSMutableArray *sectionInfo;

@property (assign, nonatomic) BOOL enableTabbarHandle; // If YES, when scrolling, tabbar will show/hide
@property (nonatomic, assign) BOOL enablePullToRefresh;
@property (nonatomic, assign) BOOL enableLoadMore;
@property (assign, nonatomic) BOOL enableScrollViewSmoothOptimization; // If YES, we will delaysContentTouches to NO, and cancel touches on contentView (like UIButton) during scrolling

@property (nonatomic, strong) Class sectionHeaderClass;
@property (nonatomic, strong) Class sectionFooterClass;

#pragma mark -
- (void)calculateSectionInfo;

//get size from cache
//- (CGSize)collectionView:(UICollectionView *)collectionView sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

//get indexpath from item index, assume item index is VALID
- (NSIndexPath *)getIndexPathFromFromItemIndex: (NSInteger)itemIndex;

//get item index from index path. use for multi section. default setting is for single section.
- (NSInteger)getItemIndexFromIndexPath: (NSIndexPath *)indexPath;

//to be overrided,load data from remote server
//parameters:
//  expandRefreshView - NSNumber with bool, true if expand refreshview
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *) param;

//when page was load,call this method
- (void)onObjectsDidLoaded:(NSArray *)objects pageNo:(NSInteger)pageNo error:(NSError *)error;

//Called to determine property hasNextPage everytime onObjectsDidLoaded
- (BOOL)determineHasNextPage:(NSArray *)objects pageNo:(NSInteger)pageNo;

- (void)pullDownRefresh;

/*
 By default, the base collectionView is (0,0,0,0), and footerview's frame is calculated and added based on that if _enableLoadmore is YES.
 If the child class re-frame the collectionView, the method needs to be called right after to re-layout the footerview
 */
//- (void)resetFooterView;

/*
 Called at setupUI. Child class should override this method to customize frame or userinterface.
 By default, the collection view is (0,0,0,0)
 */
- (void)setupCollectionView:(UICollectionView *)collectionView;

//need to override
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForHeaderOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath;
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForFooterOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath;
@end
