//
//  VPBaseCollectionViewController.m
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseCollectionViewController.h"

#import "VPBaseCollectionReusableView.h"
#import "VPBaseCollectionView.h"
#import "VPBaseCollectionViewCell.h"
#import "VPBaseCollectionViewFlowLayout.h"
#import "VPPullToRefreshContentView.h"




@interface VPBaseCollectionViewController () <SSPullToRefreshViewDelegate>

@property (assign, nonatomic) BOOL isLoading;
@property (strong, nonatomic) UIActivityIndicatorView *loadMoreIndicator;
@property (assign, nonatomic) CGFloat collectionViewBeginDragingY;

@end

@implementation VPBaseCollectionViewController

#pragma mark - initialization
- (void)commonInit
{
    [super commonInit];
    //setup default value
    _enablePullToRefresh = NO;
    _enableLoadMore = NO;
    _enableTabbarHandle = NO;
    _enableScrollViewSmoothOptimization = NO;
    _numberOfItemsPerPage = kVPDefaultNumberOfItemsPerPage;
    
    //initilize variable
    _items = [[NSMutableArray alloc]init];
    _cellSizes = [[NSMutableArray alloc]init];
    _sectionInfo = [[NSMutableArray alloc]init];
    [_sectionInfo addObject:@(0)];
}

- (void)setupCollectionView:(UICollectionView *)collectionView
{
    collectionView.autoresizingMask = UIViewAutoresizingFill;
    collectionView.opaque = YES;
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.alwaysBounceVertical = YES;
    collectionView.delegate = self;
    collectionView.dataSource = self;
}

- (void)setupUI
{
    [super setupUI];
    
    self.automaticallyAdjustsScrollViewInsets = NO; // important!
    
    // By default, use vertical FlowLayout
    VPBaseCollectionViewFlowLayout *layout = [[VPBaseCollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    VPBaseCollectionView *baseCollectionView = [[VPBaseCollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    baseCollectionView.shouldCancelUIButton = _enableScrollViewSmoothOptimization;
    _collectionView = baseCollectionView;
    
    // register cell reuse identifier
    for (NSUInteger i=0; i<[_cellClasses count]; i++) {
        Class cellClass = [_cellClasses objectAtIndex:i];
        NSString *cellNibName = [_cellNibNames objectAtIndex:i];
        if (![cellNibName isEqual:[NSNull null]]) {
            UINib *nib = [UINib nibWithNibName:cellNibName bundle:nil];
            [_collectionView registerNib:nib forCellWithReuseIdentifier:NSStringFromClass(cellClass)];
        } else {
            [_collectionView registerClass:cellClass forCellWithReuseIdentifier:NSStringFromClass(cellClass)];
        }
    }
    
    // register supplementary view
    if (self.sectionHeaderClass) {
        [_collectionView registerClass:self.sectionHeaderClass forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass(self.sectionHeaderClass)];
    }
    if (self.sectionFooterClass) {
        [_collectionView registerClass:self.sectionFooterClass forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass(self.sectionFooterClass)];
    }
    
    // setup loadmore footer
    UIView *footerView;
    if (_enableLoadMore) {
        _collectionView.contentInset = UIEdgeInsetsMake(0, 0, kVPDefaultLoadMoreFooterHeight, 0);
        
        footerView = [[UIView alloc] initWithFrame:CGRectMake(0, _collectionView.top + _collectionView.height - kVPDefaultLoadMoreFooterHeight, _collectionView.width, kVPDefaultLoadMoreFooterHeight)];
        footerView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        footerView.backgroundColor = [UIColor clearColor];
        _loadMoreIndicator =  [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _loadMoreIndicator.center = CGPointMake(footerView.width / 2, footerView.height / 2);
        [footerView addSubview:_loadMoreIndicator];
        [self.view addSubview:footerView];
    }
    
    [self setupCollectionView:_collectionView];
    [self.view addSubview:_collectionView];

    if (_enableLoadMore)
        [self.view insertSubview:footerView aboveSubview:_collectionView];
    
    // NOTE: We setup RefreshControl after collectionView has been setup, the reason is that
    //       SSPullToRefreshView might save/manipulate some collectionview's attributes upon
    //       initialization, for example, contentInsets
    if (_enablePullToRefresh) {
        _refreshView = [[SSPullToRefreshView alloc] initWithScrollView:_collectionView delegate:self];
        _refreshView.expandedHeight = kVPDefaultPullToRefreshExpandHeight;
        _refreshView.contentView = [[VPPullToRefreshContentView alloc] initWithFrame:CGRectMake(0, 0, _collectionView.width, kVPDefaultPullToRefreshExpandHeight)];
        _refreshView.backgroundColor = UIColorFromRGB(0x0c68c3);
    }
}


#pragma mark - public method
- (void)pullDownRefresh
{
    [_collectionView setContentOffset:CGPointZero animated:NO];
    _currentPageNumber = 0;
    [self loadObjectsOfPageNo:_currentPageNumber parameters:nil];
}

- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *) param
{
    //cancel previous un-finished request.
    if (self.ongoingRequestOperation && [self.ongoingRequestOperation isExecuting]) {
        [self.ongoingRequestOperation cancel];
        self.ongoingRequestOperation = nil;
    }
    _isLoading = YES;
    if (pageNo == 0) {
        if (param) {
            NSNumber *v = [param objectForKey:@"expandRefreshView"];
            if (v && [v boolValue]) {
                [_refreshView startLoading];
//                [_refreshView startLoadingAndExpand:YES animated:YES];
            }
        }
    } else if (pageNo > 0) {
      //  [_loadMoreIndicator startAnimating];
    }
    //put other custom stuff here
}

- (void)onObjectsDidLoaded:(NSArray *)objects pageNo:(NSInteger)pageNo error:(NSError *)error
{
    if (!error) {
        if (pageNo == 0) {
            [_items removeAllObjects];
            [_cellSizes removeAllObjects];
            _currentPageNumber = 0;
        } else {
            _currentPageNumber ++;
        }

        _hasNextPage = [self determineHasNextPage:objects pageNo:pageNo];
        
        [_items addObjectsFromArray:objects];
        // NOTE!!, we assume the registered cellClass is type of VPBaseCollectionViewCell
        if (_cellClasses) {
            for (id obj in objects) {
                for (Class clz in _cellClasses) {
                    if ([clz isSubclassOfClass:[VPBaseCollectionViewCell class]] && [clz performSelector:@selector(isAcceptable:) withObject:obj]) {
                        NSValue *cellSizeVale = [clz performSelector:@selector(sizeOfCell:) withObject:obj];
                        [_cellSizes addObject:cellSizeVale];
                        break;
                    }
                }
            }
        }
        [self calculateSectionInfo];
        
        @try {
            // disable animation when reloading collection view
            [CATransaction begin];
            [CATransaction setDisableActions:YES];
            [_collectionView reloadData];
            [CATransaction commit];
        } @catch (NSException *e) {
            VPLog(@"Fail to reload collectionview : %@", [e description]);
        }
    }
    _isLoading = NO;
    [_refreshView finishLoading];
    [_loadMoreIndicator stopAnimating];
}

- (BOOL)determineHasNextPage:(NSArray *)objects pageNo:(NSInteger)pageNo
{
    if (!_enableLoadMore || objects.count < _numberOfItemsPerPage) {
        return NO;
    }
    return YES;
}

- (void)calculateSectionInfo
{
    [_sectionInfo removeAllObjects];
    [_sectionInfo addObject:[NSNumber numberWithInteger:_items.count]];
}

- (NSInteger)getItemIndexFromIndexPath: (NSIndexPath *)indexPath
{
    NSInteger itemIndex = 0;
    
    for (NSInteger i = 0; i < indexPath.section; ++i) {
        itemIndex += [self collectionView:_collectionView numberOfItemsInSection:i];
    }
    itemIndex += indexPath.row;
    
    return itemIndex;
}

- (NSIndexPath *)getIndexPathFromFromItemIndex: (NSInteger)itemIndex
{
    NSInteger section = 0;
    NSInteger startIndex = 0;
    
    for (; section < [self numberOfSectionsInCollectionView:_collectionView]; ++section) {
        startIndex += [self collectionView:_collectionView numberOfItemsInSection:section];
        if (startIndex > itemIndex)
            break;
    }
    
    NSInteger num = [self collectionView:_collectionView numberOfItemsInSection:section];
    return [NSIndexPath indexPathForRow:(itemIndex - startIndex + num) inSection:section];
}

- (void)setItems:(NSMutableArray *)items
{
    _items = items ? items : [@[] mutableCopy];
    [self refreshCellSizeCache];
}

//need to override
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForHeaderOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

//need to override
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForFooterOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}


#pragma mark - collection view delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    // need to at least one section
    return _sectionInfo.count > 0 ? _sectionInfo.count : 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section < _sectionInfo.count) {
        return [[_sectionInfo objectAtIndex:section]integerValue];
    } else {
        VPLog(@"ERROR: WRONG SECTION NUMBER %ld, TOTAL SECTION: %lu", (long)section, (unsigned long)_sectionInfo.count);
        return 0;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (!self.sectionHeaderClass)
        return CGSizeZero;
    
    if ([self.sectionHeaderClass isSubclassOfClass:[VPBaseCollectionReusableView class]]) {
        NSValue *size = [self.sectionHeaderClass performSelector:@selector(sizeOfView)];
        return [size CGSizeValue];
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    if (!self.sectionFooterClass)
        return CGSizeZero;
    
    if ([self.sectionFooterClass isSubclassOfClass:[VPBaseCollectionReusableView class]]) {
        NSValue *size = [self.sectionFooterClass performSelector:@selector(sizeOfView)];
        return [size CGSizeValue];
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self isNeedToRefreshCellSizeCache])
        [self refreshCellSizeCache];
    CGSize size = CGSizeZero;
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    if(index < _cellSizes.count)
    {
        size = [[_cellSizes objectAtIndex: index] CGSizeValue];
    }
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f; // This is the minimum inter item spacing
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
//{
//    return 0.0;
//}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        reusableview = [self collectionView:collectionView viewForHeaderOfKind:kind atIndexPath:indexPath];
        if (reusableview == nil) {
            reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass(self.sectionHeaderClass) forIndexPath:indexPath];
        }
    } else if (kind == UICollectionElementKindSectionFooter) {
        reusableview = [self collectionView:collectionView viewForFooterOfKind:kind atIndexPath:indexPath];
        if (reusableview == nil) {
            reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass(self.sectionFooterClass) forIndexPath:indexPath];
        }
    }
    
    if (reusableview == nil) {
        VPLog(@"WARNING: NO VALID REUSABLE VIEW of %@", kind);
        reusableview = [[VPBaseCollectionReusableView alloc] init];
    }
    return reusableview;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Child class MUST override this because we have register different cells as different identifier
    return nil;
}


#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.enableTabbarHandle && scrollView == self.collectionView) {
        // If content (not much) FITs in scrollview, don't bother with tabbar
        if (scrollView.contentSize.height > scrollView.frame.size.height) {
            CGFloat offsetY = self.collectionViewBeginDragingY - scrollView.contentOffset.y;
            
            if(offsetY < -kVPMinOffsetToggleTabbar){
                [[NSNotificationCenter defaultCenter] postNotificationName:kTabbarHideNotification object:nil userInfo:@{@"animated":@(YES)}];
            } else if(offsetY > kVPMinOffsetToggleTabbar){
                [[NSNotificationCenter defaultCenter] postNotificationName:kTabbarShowNotification object:nil userInfo:@{@"animated":@(YES)}];
            }
        }
    }
    
    if (!_enableLoadMore || !_hasNextPage) {
        //this view doesn't have LoadMore capability
        return;
    }
    
    // when reaching bottom, load a new page
    CGFloat preloadButtomOffset = kVPDefaultPreloadHeight;
    if (_enableLoadMore)
        preloadButtomOffset += kVPDefaultLoadMoreFooterHeight;
    if (scrollView.contentOffset.y >0 && scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.bounds.size.height - preloadButtomOffset) {
        if (!_isLoading) {
            [self loadObjectsOfPageNo:(_currentPageNumber + 1) parameters:nil];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.enableTabbarHandle && scrollView == self.collectionView)
        self.collectionViewBeginDragingY = scrollView.contentOffset.y;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float bottomEdge = scrollView.contentOffset.y + scrollView.bounds.size.height;
    if (bottomEdge >= scrollView.contentSize.height) {
        // We are at the end
        if (_isLoading) {
            [self.loadMoreIndicator startAnimating];
        }
    }
}


#pragma mark - pull to refresh delegate
- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view
{
    _currentPageNumber = 0;
    [self loadObjectsOfPageNo:_currentPageNumber parameters:@{@"expandRefreshView":@(YES)}];
}

#pragma mark - private method
- (void)refreshCellSizeCache
{
    [_cellSizes removeAllObjects];
    if (_cellClasses) {
        for (id obj in _items) {
            for (Class clz in _cellClasses) {
                if ([clz isSubclassOfClass:[VPBaseCollectionViewCell class]] && [clz performSelector:@selector(isAcceptable:) withObject:obj]) {
                    NSValue *cellSizeVale = [clz performSelector:@selector(sizeOfCell:) withObject:obj];
                    [_cellSizes addObject:cellSizeVale];
                }
            }
        }
    }
}

- (BOOL)isNeedToRefreshCellSizeCache
{
    return _cellSizes.count != _items.count;
}

@end
