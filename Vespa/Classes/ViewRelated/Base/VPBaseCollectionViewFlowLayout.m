//
//  VPBaseCollectionViewFlowLayout.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/19/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseCollectionViewFlowLayout.h"

@interface VPBaseCollectionViewFlowLayout ()

@property (strong, nonatomic) NSMutableArray *insertedIndexPaths;
@property (strong, nonatomic) NSMutableArray *removedIndexPaths;

@end

@implementation VPBaseCollectionViewFlowLayout

- (void)prepareForCollectionViewUpdates:(NSArray *)updateItems
{
    [super prepareForCollectionViewUpdates:updateItems];
    
    // Keep track of updates to items and sections so we can use this information to create nifty animations
    self.insertedIndexPaths     = [NSMutableArray array];
    self.removedIndexPaths      = [NSMutableArray array];
    
    [updateItems enumerateObjectsUsingBlock:^(UICollectionViewUpdateItem *updateItem, NSUInteger idx, BOOL *stop) {
        if (updateItem.updateAction == UICollectionUpdateActionInsert)
        {
            // If the update item's index path has an "item" value of NSNotFound,
            // it means it was a section update, not an individual item.
            // This is 100% undocumented but 100% reproducible.
            
            [self.insertedIndexPaths addObject:updateItem.indexPathAfterUpdate];
            
        }
        else if (updateItem.updateAction == UICollectionUpdateActionDelete)
        {
            [self.removedIndexPaths addObject:updateItem.indexPathBeforeUpdate];
        }
    }];
}

- (void)finalizeCollectionViewUpdates
{
    [super finalizeCollectionViewUpdates];
    self.insertedIndexPaths     = nil;
    self.removedIndexPaths      = nil;
}

// These layout attributes are applied to a cell that is "appearing" and will be eased into the nominal layout attributes for that cell
// Cells "appear" in several cases:
//  - Inserted explicitly or via a section insert
//  - Moved as a result of an insert at a lower index path
//  - Result of an animated bounds change repositioning cells
- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath
{
    UICollectionViewLayoutAttributes *attributes = [super initialLayoutAttributesForAppearingItemAtIndexPath:itemIndexPath];
    if ([self.insertedIndexPaths containsObject:itemIndexPath]) {
        attributes.transform3D = CATransform3DMakeScale(0.3, 0.3, 1.0);
    }
    return attributes;
}

// These layout attributes are applied to a cell that is "disappearing" and will be eased to from the nominal layout attribues prior to disappearing
// Cells "disappear" in several cases:
//  - Removed explicitly or via a section removal
//  - Moved as a result of a removal at a lower index path
//  - Result of an animated bounds change repositioning cells
- (UICollectionViewLayoutAttributes*)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath
{
    UICollectionViewLayoutAttributes *attributes = [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    if ([self.removedIndexPaths containsObject:itemIndexPath]) {
        attributes.transform3D = CATransform3DMakeScale(0.3, 0.3, 1.0);
        attributes.alpha = 0.0f;
    }
    
    return attributes;
}

@end
