//
//  VPBaseTableViewCell.h
//  Vespa
//
//  Created by Jay on 11/2/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPUIKit.h"

@interface VPBaseTableViewCell : UITableViewCell

- (void)setupUI;
- (void)resetCell;
+ (NSNumber *)heightOfCell:(id)obj;

@end
