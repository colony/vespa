//
//  VPBaseTableViewCell.m
//  Vespa
//
//  Created by Jay on 11/2/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseTableViewCell.h"

@implementation VPBaseTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)prepareForReuse
{
    [self resetCell];
    [super prepareForReuse];
}

- (void)setupUI
{
    
}

- (void)resetCell
{
    
}

+ (NSNumber *)heightOfCell:(id)obj
{
    return [NSNumber numberWithInt:0];
}
@end
