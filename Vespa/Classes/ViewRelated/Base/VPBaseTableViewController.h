//
//  VPBaseTableViewController.h
//  Vespa
//
//  Created by Jay on 11/2/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseViewController.h"
#import "SSPullToRefreshView.h"

@interface VPTableCellInfo : NSObject

@property(nonatomic) Class cellClass;
@property(nonatomic) NSString *cellNib;
@property(nonatomic) NSString *identifier;

-(id)initWithClass:(Class)cls nib:(NSString*)nib identifier:(NSString*)identifier;

@end


@interface VPBaseTableViewController : VPBaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) SSPullToRefreshView *refreshView;

/* Pagination attributes */
@property (nonatomic, assign) NSInteger currentPageNumber;
@property (nonatomic, assign) NSInteger numberOfItemsPerPage;

/* Table cell info, when registering cells, cellNib (if not nil) will considered first */
@property (nonatomic, strong) NSMutableArray *cellInfos;

/* Table section info, sectionInfo.count = # of section, sectionInfo[x] = # of items in section x */
@property (nonatomic, strong) NSMutableArray *sectionInfos;

/* Data source backed the table */
@property (nonatomic, strong) NSMutableArray *items;

/* Internal cache for cell heights */
@property (nonatomic, strong) NSMutableArray *cellHeights;

/* Internal flag to customize */
@property (nonatomic, assign) BOOL enablePullToRefresh;
@property (nonatomic, assign) BOOL enableLoadMore;
@property (nonatomic, assign) BOOL hasNextPage;

/**
 * If YES, when scrolling, tabbar will show/hide. By default it is NO
 */
@property (assign, nonatomic) BOOL enableTabbarHandle;

/*
 A special flag, if set true, the loadMore will prepend to head of items instead of append to tail of items per page
 */
@property (assign, nonatomic) BOOL reversePagination;

/** YES if UITableViewStyleGrouped, otherwise UITableViewStylePlain */
@property (assign, nonatomic) BOOL isGroupStyle;

- (id)getItemFromIndexPath:(NSIndexPath *)indexPath;

- (void)pullDownRefresh;

/**
 Send the loaded (remote) data to underlying data source. 
 The method will either prepend or append data to the underlying array _items.
 If you want more granular control on manipulating data on sections, use onObjectsDidLoaded2.
 Also, block is used to provide customized animation or else after objects inputed, if not provided, by default, [tableview reloadData] is called.
 */
- (void)onObjectsDidLoaded:(NSArray *)objects pageNo:(NSInteger)pageNo error:(NSError *)error block:(void (^)(void))block;

/**
 Advanced version of onObjectsDidLoaded that takes additional param that specifies section num to load into objects array
 In such way, you could add data to any section. Also note, if pageNo=0, it will only wipe reset data for specified section
 */
- (void)onObjectsDidLoaded2:(NSArray *)objects pageNo:(NSInteger)pageNo section:(NSInteger)sec error:(NSError *)error block:(void (^)(void))block;

/**
 To be overrided, load objects from remote server
 parameters:
    expandRefreshView - NSNumber with bool, true if expand refreshview
 */
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *) param;

/**
 Called at setupUI. Child class should override this method to customize frame or userinterface.
 By default, the collection view is (0,0,0,0)
 */
- (void)setupTableView:(UITableView *)tableView;

/**
 Obtain CellInfo at specified index path, the default implementation will try to access cellInfos array by using indexPath.section as index, otherwise, return nil 
 */
- (VPTableCellInfo *)cellInfoForIndexPath:(NSIndexPath *)indexPath;

/**
 Everytime new item added to the items array, we need re-compute section info.
 The default impl will assume single section whose size is equivalent to items.count
 */
- (void)calculateSectionInfo;

@end
