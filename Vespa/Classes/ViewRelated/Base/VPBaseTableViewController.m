//
//  VPBaseTableViewController.m
//  Vespa
//
//  Created by Jay on 11/2/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseTableViewController.h"
#import "VPBaseTableViewCell.h"
#import "VPPullToRefreshContentView.h"

@interface VPTableCellInfo ()

@end


@implementation VPTableCellInfo

- (id)initWithClass:(Class)cls nib:(NSString*)nib identifier:(NSString*)identifier
{
    if (self = [super init]) {
        _cellClass = cls;
        _cellNib = nib;
        _identifier = identifier;
    }
    return self;
}

@end

@interface VPBaseTableViewController ()<SSPullToRefreshViewDelegate>

@property (assign, nonatomic) BOOL isLoading;
@property (strong, nonatomic) UIActivityIndicatorView *loadMoreIndicator;
@property (assign, nonatomic) CGFloat tableViewBeginDragingY;

@end


@implementation VPBaseTableViewController

#pragma mark - initialization
- (void)commonInit
{
    [super commonInit];
    //setup default value
    _enablePullToRefresh = NO;
    _enableLoadMore = NO;
    _enableTabbarHandle = NO;
    _hasNextPage = NO;
    _numberOfItemsPerPage = kVPDefaultNumberOfItemsPerPage;
    _reversePagination = NO;
    
    _sectionInfos = [NSMutableArray new];
    _items = [NSMutableArray new];
    _cellHeights = [NSMutableArray new];
}

- (void)setupUI
{
    [super setupUI];
    
    //setup table view
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:(self.isGroupStyle?UITableViewStyleGrouped:UITableViewStylePlain)];
    [self setupTableView:_tableView];
    [self.view addSubview:_tableView];
    
    // register cell reuse identifier
    if (_cellInfos) {
        for (VPTableCellInfo *c in _cellInfos) {
            if (c) {
                if (c.cellNib) {
                    UINib *nib = [UINib nibWithNibName:c.cellNib bundle:nil];
                    if (nib) [_tableView registerNib:nib forCellReuseIdentifier:c.identifier];
                } else if (c.cellClass) {
                    [_tableView registerClass:c.cellClass forCellReuseIdentifier:c.identifier];
                }
            }
        }
    }
    
    // setup pull down refresh control
    if (_enablePullToRefresh) {
        _refreshView = [[SSPullToRefreshView alloc] initWithScrollView:_tableView delegate:self];
        _refreshView.expandedHeight = 60.f;
        _refreshView.contentView = [[VPPullToRefreshContentView alloc] initWithFrame:CGRectMake(0, 0, _tableView.width, _refreshView.expandedHeight)];
        _refreshView.backgroundColor = UIColorFromRGB(0x0c68c3);
    }
    
    // setup load more footer
    if (_enableLoadMore) {
        _hasNextPage = YES;
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, kVPDefaultLoadMoreFooterHeight)];
        _loadMoreIndicator =  [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _loadMoreIndicator.center = CGPointMake(footerView.bounds.size.width / 2, footerView.bounds.size.height / 2);
        [footerView addSubview:_loadMoreIndicator];
        _tableView.tableFooterView = footerView;
    } else {
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
}

- (void)setupTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorInset = UIEdgeInsetsZero;
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    tableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.delaysContentTouches = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tableView.scrollsToTop = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.tableView.scrollsToTop = NO;
}


#pragma mark - public interface
- (VPTableCellInfo *)cellInfoForIndexPath:(NSIndexPath *)indexPath
{
    // By default, we assume cellinfo per each section
    if (_cellInfos && _cellInfos.count > indexPath.section) {
        return _cellInfos[indexPath.section];
    }
    return nil;
}

- (id)getItemFromIndexPath:(NSIndexPath *)indexPath
{
    NSInteger idx = [self getItemIndexForSection:indexPath.section] + indexPath.row;
    if (_items.count <= idx)
    {
        VPLog(@"Try to access out of bound idx %ld", (long)idx);
        return nil;
    }
    return [_items objectAtIndex:idx];
}

- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *) param
{
    //cancel previous un-finished request.
    if (self.ongoingRequestOperation && [self.ongoingRequestOperation isExecuting]) {
        [self.ongoingRequestOperation cancel];
        self.ongoingRequestOperation = nil;
    }
    _isLoading = YES;
    if (pageNo == 0) {
        if (param) {
            NSNumber *v = [param objectForKey:@"expandRefreshView"];
            if (v && [v boolValue]) {
                [_refreshView startLoadingAndExpand:YES animated:YES];
            }
        }
    }
    if (_enableLoadMore && pageNo > 0) {
        [_loadMoreIndicator startAnimating];
    }
    //put other custom stuff here
}

- (void)onObjectsDidLoaded:(NSArray *)objects pageNo:(NSInteger)pageNo error:(NSError *)error block:(void (^)(void))block
{
    if (!error) {
        if (pageNo == 0) {
            [_items removeAllObjects];
            [_cellHeights removeAllObjects];
            _currentPageNumber = 0;
        } else {
            _currentPageNumber++;
        }
        
        if (_reversePagination)
            [_items insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, objects.count)]];
        else
            [_items addObjectsFromArray:objects];
        
        // Re-compute sectioninfo after _items is updated
        [self calculateSectionInfo];
        
        for (int i = 0; i < objects.count; ++i) {
            if (_reversePagination)
                [_cellHeights insertObject:[NSNull null] atIndex:0];
            else
                [_cellHeights addObject:[NSNull null]];
        }
        
        if (!block) {
            [_tableView reloadData];
        } else {
            SAFE_BLOCK_RUN(block);
        }
        
        if (!_enableLoadMore || objects.count < _numberOfItemsPerPage) {
            _hasNextPage = NO;
        } else {
            _hasNextPage = YES;
        }
    }
    
    _isLoading = NO;
    [_refreshView finishLoading];
    [_loadMoreIndicator stopAnimating];
}

// NOT TESTED YET
- (void)onObjectsDidLoaded2:(NSArray *)objects pageNo:(NSInteger)pageNo section:(NSInteger)section error:(NSError *)error block:(void (^)(void))block
{
    if (!error) {
        // The specified
        NSInteger idx = [self getItemIndexForSection:section];
        NSInteger len = 0;
        NSIndexSet *is;
        
        BOOL isLastSection = (section == _sectionInfos.count - 1);
        BOOL isNewSection = (section >= _sectionInfos.count);
        if (!isNewSection) {
            len = [[_sectionInfos objectAtIndex:section] integerValue];
            is = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(idx, len)];
        }
        
        if (pageNo == 0) {
            if (isNewSection) {
                // Do nothing, because of this is new section
            } else {
                if (len > 0) {
                    [_items removeObjectsAtIndexes:is];
                    [_cellHeights removeObjectsAtIndexes:is];
                }
            }
            _currentPageNumber = 0;
        } else {
            _currentPageNumber++;
        }
        
        if (isNewSection) {
            // Append data since it is new section, we don't worry about reversePagination here
            [_items addObjectsFromArray:objects];
            for (int i = 0; i < objects.count; ++i) {
                [_cellHeights addObject:[NSNull null]];
            }
        } else if (isLastSection) {
            if (_reversePagination) {
                [_items insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(idx, objects.count)]];
                for (int i = 0; i < objects.count; ++i) {
                    [_cellHeights insertObject:[NSNull null] atIndex:idx];
                }
            } else {
                // Same as append as isNewSection
                [_items addObjectsFromArray:objects];
                for (int i = 0; i < objects.count; ++i) {
                    [_cellHeights addObject:[NSNull null]];
                }
            }
        } else {
            if (_reversePagination) {
                [_items insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(idx, objects.count)]];
                for (int i = 0; i < objects.count; ++i) {
                    [_cellHeights insertObject:[NSNull null] atIndex:idx];
                }
            } else {
                // Apparently, we need insert data in the middle
                [_items insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(idx + len, objects.count)]];
                for (int i = 0; i < objects.count; ++i) {
                    [_cellHeights insertObject:[NSNull null] atIndex:(idx+len)];
                }
            }
        }
        
        [self calculateSectionInfo];
        
        if (!block) {
            [_tableView reloadData];
        } else {
            SAFE_BLOCK_RUN(block);
        }
        
        if (!_enableLoadMore || objects.count < _numberOfItemsPerPage) {
            _hasNextPage = NO;
        } else {
            _hasNextPage = YES;
        }
    }
    
    _isLoading = NO;
    [_refreshView finishLoading];
    [_loadMoreIndicator stopAnimating];
}

- (void)calculateSectionInfo
{
    [_sectionInfos removeAllObjects];
    [_sectionInfos addObject:[NSNumber numberWithInteger:_items.count]];
}

- (void)pullDownRefresh
{
    [_tableView setContentOffset:CGPointZero animated:NO];
    _currentPageNumber = 0;
    [self loadObjectsOfPageNo:_currentPageNumber parameters:nil];
}

- (void)setItems:(NSMutableArray *)items
{
    _items = items ? items : [[NSMutableArray alloc]init];
    [self calculateSectionInfo];
    [self resetCellHeightCache];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section < _sectionInfos.count) {
        return [[_sectionInfos objectAtIndex:section] integerValue];
    } else {
        VPLog(@"Wrong sectio number %ld, total section: %lu", (long)section, (unsigned long)_sectionInfos.count);
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionInfos.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    if (_items.count <= index)
        return 0;
    
    id h = [_cellHeights objectAtIndex:index];
    if (h == [NSNull null]) {
        // Apparently, the cell height is nil, not cached yet, compute and save into cache
        VPTableCellInfo *ci = [self cellInfoForIndexPath:indexPath];
        if (!ci)
            return 0;
        
        if (!ci.cellClass || ![ci.cellClass isSubclassOfClass:[VPBaseTableViewCell class]])
            return 0;
        
        h = [ci.cellClass performSelector:@selector(heightOfCell:) withObject:[_items objectAtIndex:index]];
        _cellHeights[index] = h;
    }
    return [h floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VPTableCellInfo *ci = [self cellInfoForIndexPath:indexPath];
    if (ci)
        return [_tableView dequeueReusableCellWithIdentifier:ci.identifier forIndexPath:indexPath];
    return nil;
}

//http://stackoverflow.com/questions/25770119/ios-8-uitableview-separator-inset-0-not-working
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}


#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.enableTabbarHandle && scrollView == self.tableView) {
        // If content (not much) FITs in scrollview, don't bother with tabbar
        if (scrollView.contentSize.height > scrollView.frame.size.height) {
            CGFloat offsetY = self.tableViewBeginDragingY - scrollView.contentOffset.y;
            
            if(offsetY < -kVPMinOffsetToggleTabbar){
                [[NSNotificationCenter defaultCenter] postNotificationName:kTabbarHideNotification object:nil userInfo:@{@"animated":@(YES)}];
            } else if(offsetY > kVPMinOffsetToggleTabbar){
                [[NSNotificationCenter defaultCenter] postNotificationName:kTabbarShowNotification object:nil userInfo:@{@"animated":@(YES)}];
            }
        }
    }
    
    if (!_enableLoadMore || !_hasNextPage) {
        //this view doesn't have LoadMore capability or reach to the end of list
        return;
    }
    
    // when reaching bottom, load a new page
    if (scrollView.contentOffset.y >0 && scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.bounds.size.height - kVPDefaultPreloadHeight) {
        if (!_isLoading) {
            [self loadObjectsOfPageNo: _currentPageNumber + 1 parameters:nil];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.enableTabbarHandle && scrollView == self.tableView)
        self.tableViewBeginDragingY = scrollView.contentOffset.y;
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    return YES;
}


#pragma mark - pull to refresh delegate
- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view
{
    _currentPageNumber = 0;
    [self loadObjectsOfPageNo:_currentPageNumber parameters:@{@"expandRefreshView":@(YES)}];
}


#pragma mark - private method
- (void)resetCellHeightCache
{
    [_cellHeights removeAllObjects];
    for (int i = 0; i < _items.count; ++i) {
        [_cellHeights addObject:[NSNull null]];
    }
}

- (NSInteger)getItemIndexForSection:(NSInteger)section
{
    NSInteger sum = 0;
    for (int i = 0; i < MIN(_sectionInfos.count, section); ++i) {
        sum += [[_sectionInfos objectAtIndex:i] integerValue];
    }
    return sum;
}

- (NSInteger)getItemIndexFromIndexPath: (NSIndexPath *)indexPath
{
    NSInteger itemIndex = 0;

    for (NSInteger i = 0; i < indexPath.section; ++i) {
        itemIndex += [self tableView:_tableView numberOfRowsInSection:i];
    }
    itemIndex += indexPath.row;

    return itemIndex;
}

@end
