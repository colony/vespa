//
//  VPBaseView.h
//  Vespa
//
//  Created by Jay on 12/21/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPUIKit.h"

@interface VPBaseView : UIView

- (void)setupUI;

/**
 True if stop sending event to super view or next Responder, false otherwise. By default, it is false.
 */
@property(nonatomic, assign) BOOL stopEventPropagation;

@end
