//
//  VPBaseView.m
//  Vespa
//
//  Created by Jay on 12/21/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseView.h"

@implementation VPBaseView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setupUI
{
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.stopEventPropagation)
        return;
    [super touchesBegan:touches withEvent:event];
}

@end
