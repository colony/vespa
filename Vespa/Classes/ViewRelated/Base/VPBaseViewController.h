//
//  VPBaseViewController.h
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


#import <SVProgressHUD/SVProgressHUD.h>
#import "VPUIKit.h"

@interface VPBaseViewController : UIViewController

//Need to cancel on-going request operation when the view disappered.
@property (atomic, strong) NSOperation *ongoingRequestOperation;
//A flag to determine if the current VC didAppeared (present modally or pushed on top of navigation)
@property (nonatomic, assign, readonly) BOOL isTop;

/*
 Initialize some custom variable when view controller is instanized.
 */
- (void)commonInit;

/*
 If the current VC is in navigation stack, the method return previous vc on same stack.
 If the current one is root or not in stack, nil is returned
 */
- (UIViewController *)previous;

/*
 Customize or initialize some UI for the view controller.
 This method will be called after viewDidLoad.
 */
- (void)setupUI;

/**
 The domain per ViewController used by VPContext
 */
- (NSString *)contextDomain;

/**
 * Dismiss all presented VC till hitting the top presentingVC
 */
- (void)dismissViewControllerAncestor:(BOOL)animated completion:(void (^)(void))completion;

@end
