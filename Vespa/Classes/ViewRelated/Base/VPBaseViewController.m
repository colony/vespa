//
//  VPBaseViewController.m
//  Vespa
//
//  Created by Jay on 11/1/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

#import "UINavigationItem+Util.h"
#import "VPContext.h"

@interface VPBaseViewController () {
    BOOL _didSetupUI;
}
@end

@implementation VPBaseViewController


#pragma mark - lifecycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _didSetupUI = NO;
        [self commonInit];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.navigationController) {
        if (SYSTEM_VERSION_LESS_THAN(_iOS_8_0)) {
            [self.navigationController.navigationBar setTranslucent:NO];
        }
        [self.navigationController setNavigationBarHidden:NO];
        [self.navigationItem setCustomBackTitle:@""];
    }
    
    self.view.clearsContextBeforeDrawing = NO;
    self.view.opaque = YES;
    //http://stackoverflow.com/questions/5169517/autoresizing-mask-flexible-width-of-an-image-with-fixed-height
    [self.view setAutoresizingMask:UIViewAutoresizingFill];
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.extendedLayoutIncludesOpaqueBars = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_didSetupUI) {
        [self setupUI];
        _didSetupUI = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _isTop = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    _isTop = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_ongoingRequestOperation && [_ongoingRequestOperation isExecuting]){
        [_ongoingRequestOperation cancel];
        _ongoingRequestOperation = nil;
    }
}

- (void)dealloc
{
    // The context lifecycle will only sustain until it destroyed
    [[VPContext sharedInstance] clearDomain:[self contextDomain]];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)commonInit
{
}

- (void)setupUI
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
- (UIViewController *)previous
{
    if (self.navigationController) {
        NSInteger myIndex = [self.navigationController.viewControllers indexOfObject:self];
        if (myIndex != 0 && myIndex != NSNotFound) {
            return [self.navigationController.viewControllers objectAtIndex:myIndex-1];
        }
    }
    return nil;
}

- (NSString *)contextDomain
{
    return nil;
}

- (void)dismissViewControllerAncestor:(BOOL)animated completion:(void (^)(void))completion
{
    UIViewController *ancestorVC;
    UIViewController *presentingVC = self.presentingViewController;
    while (presentingVC) {
        ancestorVC = presentingVC;
        presentingVC = presentingVC.presentingViewController;
    }
    
    if (ancestorVC) {
        // We only animate the dismissal at top level
        [ancestorVC dismissViewControllerAnimated:animated completion:completion];
    }
}

@end
