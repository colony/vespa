//
//  VPButton.h
//  Vespa
//
//  Created by Jay on 11/9/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPUIKit.h"

@interface VPButton : UIButton

@property (assign, nonatomic) BOOL highlightAnimationEnabled;

@end
