//
//  VPButton.m
//  Vespa
//
//  Created by Jay on 11/9/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPButton.h"

@implementation VPButton

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        self.exclusiveTouch = YES;
        [self setupEvent];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.exclusiveTouch = YES;
        [self setupEvent];
    }
    return self;
}

- (void)setupEvent
{
    [self addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(buttonTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(buttonTouchUp:) forControlEvents:UIControlEventTouchUpOutside];
}

- (void)buttonTouchDown:(id)sender
{
    if (self.highlightAnimationEnabled) {
        [UIView animateWithDuration:0.1 animations:^{
            self.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
}

- (void)buttonTouchUp:(id)sender
{
    if (self.highlightAnimationEnabled) {
        [UIView animateWithDuration:0.5
                              delay:0.0
             usingSpringWithDamping:0.2
              initialSpringVelocity:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.transform = CGAffineTransformIdentity;
                         } completion:nil];
    }
}

@end
