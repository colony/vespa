//
//  VPNavigationController.h
//  Vespa
//
//  Created by Jay on 11/8/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


@interface VPNavigationController : UINavigationController

/* Convenient method to return previous vc, return nil either specified viewController not found or it is root vc */
- (UIViewController *)prevViewController:(UIViewController *)viewController;

/* Convenient method to return root vc */
- (UIViewController *)rootViewController;

/* Convenient method to return vc on top */
- (UIViewController *)topViewController;

/** Configure the Navigationbar to default UI style */
- (void)configureDefaultStyle;

/*
 Inherited from UINavigationController
 */
//- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated;


/**
 First, this will disable the system's interactivePopGesture (or EdgeGesture), replace that
 with
 */
@property (assign, nonatomic) BOOL customNavigatonGestureEnabled;

@end
