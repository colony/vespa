//
//  VPNavigationController.m
//  Vespa
//
//  Created by Jay on 11/8/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//


/*
 Things to look up
 http://stackoverflow.com/questions/19054625/changing-back-button-in-ios-7-disables-swipe-to-navigate-back/20330647#20330647
 http://keighl.com/post/ios7-interactive-pop-gesture-custom-back-button/
 https://github.com/fastred/SloppySwiper/
 */
#import "VPNavigationController.h"

#import "VPDirectionalPanGestureRecognizer.h"
#import "VPNavigationControllerAnimator.h"
#import "VPPostListViewController.h"


static CGFloat const _kVPMinimumPanToInteractiveTransition = 90.0f;


@interface VPNavigationController ()<UINavigationControllerDelegate,UIGestureRecognizerDelegate>

/* True if the previous pushed view controller hasn't finished yet, in this case, append the vc to our pendingQueue */
@property (assign, nonatomic) BOOL shouldIgnorePushingViewController;

/* A queue of view controller pending on push, the queue ease the pressure that a large seq of push (with animation) occurs at same time */
@property (strong, nonatomic) NSMutableArray *pendingViewControllerQueue;

/* Things related to custom interaction */
@property (strong, nonatomic) VPDirectionalPanGestureRecognizer *panRecognizer;
@property (strong, nonatomic) UIPercentDrivenInteractiveTransition *interactionController;
@property (strong, nonatomic) VPNavigationControllerAnimator *animator;

@property (assign, nonatomic) BOOL duringAnimation;

@end


@implementation VPNavigationController

#pragma mark - lifecycle
- (id)init
{
    if (self = [super init]) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    // Default configurations
    _customNavigatonGestureEnabled = NO;
    _duringAnimation = NO;
    _shouldIgnorePushingViewController = NO;
    _pendingViewControllerQueue = [NSMutableArray new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - public interface
- (void)setCustomNavigatonGestureEnabled:(BOOL)customNavigatonGestureEnabled
{
    _customNavigatonGestureEnabled = customNavigatonGestureEnabled;
    __weak UINavigationController *weakSelf = self;
    self.delegate = (id<UINavigationControllerDelegate>)weakSelf;
    if (customNavigatonGestureEnabled) {
        self.panRecognizer = [[VPDirectionalPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        self.panRecognizer.direction = kVPPanDirectionRight;
        self.panRecognizer.delegate = (id<UIGestureRecognizerDelegate>)weakSelf;
        [self.view addGestureRecognizer:self.panRecognizer];
        
        self.animator = [VPNavigationControllerAnimator new];
    } else {
        self.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)weakSelf;
        self.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (UIViewController *)prevViewController:(UIViewController *)viewController
{
    NSInteger idx = [self.viewControllers indexOfObject:viewController];
    if (idx != 0 && idx != NSNotFound) {
        return [self.viewControllers objectAtIndex:idx-1];
    } else {
        return nil;
    }
}

- (UIViewController *)rootViewController
{
    return [self.viewControllers firstObject];
}

- (UIViewController *)topViewController
{
    return [self.viewControllers lastObject];
}

- (void)configureDefaultStyle
{
    self.navigationBar.translucent = NO;
    self.navigationBar.tintColor = [UIColor blackColor];
    self.navigationBar.barTintColor = nil;
    [self.navigationBar setShadowImage:[UIImage new]];
}


#pragma mark - UIGestureRecognizerDelegate delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO; // Disallow others when VPDirectionalPanGestureRecognizer wakes up
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return self.viewControllers.count > 1;
}


#pragma mark - action handling
- (void)handlePan:(UIPanGestureRecognizer *)sender
{
    UIView *view = self.view;
    if (sender.state == UIGestureRecognizerStateBegan) {
        if (!self.duringAnimation && self.viewControllers.count > 1) {
            self.interactionController = [[UIPercentDrivenInteractiveTransition alloc] init];
            self.interactionController.completionCurve = UIViewAnimationCurveEaseOut;
            [self popViewControllerAnimated:YES];
        }
    } else if (sender.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [sender translationInView:view];
        // Cumulative translation.x can be less than zero because user can pan slightly to the right and then back to the left.
        CGFloat d = translation.x > 0 ? translation.x / CGRectGetWidth(view.bounds) : 0;
        // Divide the touch translation by two, as a way to add resistance on movement
        [self.interactionController updateInteractiveTransition:(d/2)];
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        if ([sender velocityInView:view].x > 0 && [sender translationInView:view].x > _kVPMinimumPanToInteractiveTransition) {
            [self.interactionController finishInteractiveTransition];
        } else {
            [self.interactionController cancelInteractiveTransition];
            // When the transition is cancelled, `navigationController:didShowViewController:animated:` isn't called, so we have to maintain `duringAnimation`'s state here too.
            self.duringAnimation = NO;
        }
        self.interactionController = nil;
    } else if (sender.state == UIGestureRecognizerStateCancelled) {
        [self.interactionController cancelInteractiveTransition];
        self.duringAnimation = NO;
        self.interactionController = nil;
    }
}


#pragma mark - override
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if(!self.shouldIgnorePushingViewController) {
        self.interactivePopGestureRecognizer.enabled = NO;
        [super pushViewController:viewController animated:animated];
    } else {
        [self.pendingViewControllerQueue addObject:viewController];
    }
    self.shouldIgnorePushingViewController = YES;
}


#pragma mark - delegate
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    if (operation == UINavigationControllerOperationPop && self.customNavigatonGestureEnabled) {
        return self.animator;
    }
    return nil;
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController
{
    if (self.customNavigatonGestureEnabled)
        return self.interactionController;
    else
        return nil;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (animated) {
        self.duringAnimation = YES;
    }
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    self.duringAnimation = NO;
    self.shouldIgnorePushingViewController = NO;
    
    if (self.pendingViewControllerQueue.count > 0){
        UIViewController *vc = self.pendingViewControllerQueue.firstObject;
        [self.pendingViewControllerQueue removeObjectAtIndex:0];
        [self pushViewController:vc animated:NO];
    }
    
    if (self.viewControllers.count <= 1) {
        if (self.customNavigatonGestureEnabled) {
            self.panRecognizer.enabled = NO;
        } else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_7_0)) {
            self.interactivePopGestureRecognizer.enabled = NO;
        }
    } else {
        if (self.customNavigatonGestureEnabled) {
            self.panRecognizer.enabled = YES;
        } else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_7_0)) {
            self.interactivePopGestureRecognizer.enabled = YES;
//            self.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)viewController;
        }
    }
}

@end



@interface UINavigationController (StatusBarStyle)

@end



@implementation UINavigationController (StatusBarStyle)

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

@end
