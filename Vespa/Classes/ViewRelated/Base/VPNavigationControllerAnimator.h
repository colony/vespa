//
//  VPNavigationControllerAnimator.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@interface VPNavigationControllerAnimator : NSObject<UIViewControllerAnimatedTransitioning>

@end
