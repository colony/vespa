//
//  VPPageViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 3/24/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

@protocol VPPageViewControllerDataSource;
@protocol VPPageViewControllerDelegate;

@interface VPPageViewController : VPBaseViewController

/** Constructor that initialize with initial start page number(index) which by default is 0 (the first page) */
- (instancetype)initWithInitialPage:(NSUInteger)page;

- (void)reloadPageAtIndex:(NSUInteger)index;
- (void)navigateToPageAtIndex:(NSUInteger)index animated:(BOOL)animated completion:(void(^)())completion;
- (UIViewController *)viewControllerAtIndex:(NSUInteger)index;

@property (weak, nonatomic) id<VPPageViewControllerDataSource> dataSource;
@property (weak, nonatomic) id<VPPageViewControllerDelegate> delegate;

@property (nonatomic, readonly) NSUInteger currentPage;

/**
 * The total number of pages, returned by dataSource numberOfPagesInPageViewController:
 */
@property (nonatomic, readonly) NSUInteger numberOfPages;

/**
 * Boolean value that controls whether the pageController's scrollView bounces past
 * the edge of content and back again.
 * 
 * Default value is set to true
 */
@property (nonatomic, assign) BOOL bounces;

/** A Boolean value that determines whether scrolling is enabled for the pageController's
 * scrollView.
 *
 * Default value is set to true
 */
@property (nonatomic, assign) BOOL scrollEnabled;

@end


@protocol VPPageViewControllerDataSource <NSObject>

- (NSUInteger)numberOfPagesInPageViewController:(VPPageViewController *)pageViewController;

/**
 * To make reloadPageAtIndex: works. Please return a new instance every time the method is called.
 * Internally, a cache is maintained so as to avoid call this method as much as possible
 */
- (UIViewController *)pageViewController:(VPPageViewController *)pageViewController viewControllerAtIndex:(NSUInteger)index;

@end

@protocol VPPageViewControllerDelegate <NSObject>

@optional
- (void)pageViewController:(VPPageViewController *)pageViewController
     didShowViewController:(UIViewController *)controller
                   atIndex:(NSUInteger)index;

/**
 * A view controller is hidden when (1) it is previously visible and (2) its frame
 * has moved outside the pageController's scrollview bounds (off-screen).
 *
 * NOTE, navigateToPageAtIndex:animated:completion will only call didHide delegate method for
 * current view controller and didShow delegate method for TARGET view controller (the one 
 * specified by index), every controllers in between are not touched.
 */
- (void)pageViewController:(VPPageViewController *)pageViewController
     didHideViewController:(UIViewController *)controller
                   atIndex:(NSUInteger)index;

@end

