//
//  VPPageViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 3/24/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPageViewController.h"

@interface VPPageViewController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) NSMutableArray *viewControllers;
@property (assign, nonatomic) NSUInteger currentPage;
@property (assign, nonatomic) NSUInteger numberOfPages;
/** The internal scrollview inside UIPageViewController */
@property (strong, nonatomic) UIScrollView *scrollView;

@end

@implementation VPPageViewController

#pragma mark - lifecycle
- (instancetype)initWithInitialPage:(NSUInteger)page
{
    if (self = [super init]) {
        _currentPage = page;
    }
    return self;
}

- (void)commonInit
{
    [super commonInit];
    _viewControllers = [NSMutableArray new];
    _currentPage = 0;
    _numberOfPages = 0;
    _bounces = YES;
    _scrollEnabled = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageController.dataSource = self;
    self.pageController.delegate = self;
    self.pageController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pageController.view.frame = self.view.bounds;
    
    self.numberOfPages = [self.dataSource numberOfPagesInPageViewController:self];
    for (NSUInteger index = 0; index < self.numberOfPages; index++) {
        [self.viewControllers addObject:[self.dataSource pageViewController:self viewControllerAtIndex:index]];
    }
    
    // Start by displaying the current-page-indexed viewcontroller
    [self.pageController setViewControllers:@[[self.viewControllers objectAtIndex:self.currentPage]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
    
    // Hack to disable UIPageViewController bounce
    // http://stackoverflow.com/questions/21798218/disable-uipageviewcontroller-bounce
    for (UIView *view in self.pageController.view.subviews ) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            // Based on observation, there is ONLY ONE scrollView inside pageController
            self.scrollView = (UIScrollView *)view;
            self.scrollView.delegate = self;
            
            // Check NOTES for detail
            self.scrollView.delaysContentTouches = NO;
        }
    }
}


#pragma mark - public method
- (void)reloadPageAtIndex:(NSUInteger)index
{
    //TODO
    //NOTE: since we cache all view controllers, navigate among them WON'T trigger viewDidLoad
    //      in order to reload, we need call datasource to re-instantiate a new view controller
}

- (void)navigateToPageAtIndex:(NSUInteger)index animated:(BOOL)animated completion:(void(^)())completion
{
    if (self.currentPage == index)
        return;
    NSUInteger fromIndex = self.currentPage;
    UIViewController *fromVC = [self currentViewController];
    UIViewController *toVC = self.viewControllers[index];
    if (toVC) {
        @weakify(self);
        [self.pageController setViewControllers:@[toVC] direction:UIPageViewControllerNavigationDirectionForward animated:animated completion:^(BOOL finished) {
            @strongify(self);
            
            self.currentPage = index;
            
            if ([self.delegate respondsToSelector:@selector(pageViewController:didHideViewController:atIndex:)]) {
                [self.delegate pageViewController:self didHideViewController:fromVC atIndex:fromIndex];
            }
            if ([self.delegate respondsToSelector:@selector(pageViewController:didShowViewController:atIndex:)]) {
                [self.delegate pageViewController:self didShowViewController:toVC atIndex:index];
            }
            SAFE_BLOCK_RUN(completion);
        }];
    }
}

- (void)setScrollEnabled:(BOOL)scrollEnabled
{
    self.scrollView.scrollEnabled = scrollEnabled;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    return [self.viewControllers objectAtIndex:index];
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.bounces)
        return; // Don't bother with hack if we allow page bounce which is default behaviour
    
    if (self.currentPage == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width) {
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
    if (self.currentPage == ([self.viewControllers count]-1) && scrollView.contentOffset.x > scrollView.bounds.size.width) {
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (self.bounces)
        return; // Don't bother with hack if we allow page bounce which is default behaviour
    
    if (self.currentPage == 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width) {
        velocity = CGPointZero;
        *targetContentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
    if (self.currentPage == ([self.viewControllers count]-1) && scrollView.contentOffset.x >= scrollView.bounds.size.width) {
        velocity = CGPointZero;
        *targetContentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
    
}


#pragma mark - UIPageViewControllerDataSource
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self.viewControllers indexOfObject:viewController];
    if (index == 0) {
        return nil;
    }
    // Decrease the index by 1 to return
    index--;
    return [self.viewControllers objectAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self.viewControllers indexOfObject:viewController];
    index++;
    if (index == self.viewControllers.count) {
        return nil;
    }
    return [self.viewControllers objectAtIndex:index];
}


#pragma mark - UIPageViewControllerDelegate
- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
    if (completed) {
        NSUInteger currentIndex = [self.viewControllers indexOfObject:[pageViewController.viewControllers lastObject]];
        if (currentIndex != NSNotFound) {
            self.currentPage = currentIndex;
        } else {
            return;
        }
        
        if (previousViewControllers && [self.delegate respondsToSelector:@selector(pageViewController:didHideViewController:atIndex:)]) {
            UIViewController *vc = [previousViewControllers lastObject];
            NSUInteger idx = [self.viewControllers indexOfObject:vc];
            [self.delegate pageViewController:self didHideViewController:vc atIndex:idx];
        }
        if ([self.delegate respondsToSelector:@selector(pageViewController:didShowViewController:atIndex:)]) {
            [self.delegate pageViewController:self didShowViewController:[self currentViewController] atIndex:self.currentPage];
        }
    }
}


#pragma mark - private method
- (UIViewController *)currentViewController
{
    return self.viewControllers[self.currentPage];
}

@end
