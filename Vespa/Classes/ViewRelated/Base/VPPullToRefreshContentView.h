//
//  VPPullToRefreshContentView.h
//  Vespa
//
//  Created by Jay on 2/15/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSPullToRefreshView.h"

@interface VPPullToRefreshContentView : UIView<SSPullToRefreshContentView>

@property (nonatomic, strong) UIImageView *backgroundImgView;
@property (nonatomic, strong) UIImageView *refreshImgView;
@end
