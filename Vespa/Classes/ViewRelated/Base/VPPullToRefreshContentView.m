//
//  VPPullToRefreshContentView.m
//  Vespa
//
//  Created by Jay on 2/15/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPullToRefreshContentView.h"

@implementation VPPullToRefreshContentView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        _refreshImgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ninjahead_icon"]];
        CGRect f = _refreshImgView.frame;
        f.size.width = 50.0f;
        f.size.height = 50.0f;
        _refreshImgView.frame = f;
        _refreshImgView.center = self.center;
        
        [self addSubview:_refreshImgView];
    }
    return self;
}


//- (void)layoutSubviews {
//    CGSize size = self.bounds.size;
//    self.statusLabel.frame = CGRectMake(20.0f, roundf((size.height - 30.0f) / 2.0f), size.width - 40.0f, 30.0f);
//    self.activityIndicatorView.frame = CGRectMake(roundf((size.width - 20.0f) / 2.0f), roundf((size.height - 20.0f) / 2.0f), 20.0f, 20.0f);
//}


#pragma mark - SSPullToRefreshContentView

- (void)setState:(SSPullToRefreshViewState)state withPullToRefreshView:(SSPullToRefreshView *)view {
    switch (state) {
        case SSPullToRefreshViewStateReady: {
//            self.statusLabel.text = NSLocalizedString(@"Release to refresh", nil);
//            [self.activityIndicatorView startAnimating];
//            self.activityIndicatorView.alpha = 0.0f;
            break;
        }
            
        case SSPullToRefreshViewStateNormal: {
            [self stopRefreshAnimation];
//            self.statusLabel.text = NSLocalizedString(@"Pull down to refresh", nil);
//            self.statusLabel.alpha = 1.0f;
//            [self.activityIndicatorView stopAnimating];
//            self.activityIndicatorView.alpha = 1.0f;
            break;
        }
            
        case SSPullToRefreshViewStateLoading: {
            [self startRefreshAnimation];
            break;
        }
            
        case SSPullToRefreshViewStateClosing: {
            
            break;
        }
    }
}

- (void)setPullProgress:(CGFloat)pullProgress
{
    if (pullProgress <= 1) {
        self.refreshImgView.transform = CGAffineTransformMakeRotation(M_PI * pullProgress * 2.0);
    } else {
        self.refreshImgView.transform = CGAffineTransformMakeRotation(0);
    }
}

#pragma mark - private
- (void)startRefreshAnimation
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI];
    rotationAnimation.duration = 0.2f;
    rotationAnimation.repeatCount = HUGE_VALF;
    rotationAnimation.cumulative = YES;
    
    [self.refreshImgView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)stopRefreshAnimation
{
    [self.refreshImgView.layer removeAllAnimations];
    self.refreshImgView.transform = CGAffineTransformMakeRotation(0);
}
@end
