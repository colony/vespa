//
//  VPBehaviour.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//
//  http://www.objc.io/issue-13/behaviors.html
//  https://github.com/krzysztofzablocki/BehavioursExample

#import <Foundation/Foundation.h>
#ifndef IBInspectable
    #define IBInspectable
#endif

@interface VPBehaviour : UIControl

/* Object that this behaviour life will be bound to */
@property (weak, nonatomic) IBOutlet id owner;

- (void)vp_bindLifetimeToObject:(id)object;
- (void)vp_releaseLifetimeFromObject:(id)object;

@end


// We could add delegate (later) for more fine-grain control
//@protocol VPBehaviourDelegate <NSObject>
//@optional
//
//- (void)behaviourWillBegin:(VPBehaviour *)behaviour;
//- (void)behaviourDidFinish:(VPBehaviour *)behaviour;
//
//@end