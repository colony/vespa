//
//  VPBehaviour.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBehaviour.h"
#import "objc/runtime.h"

@implementation VPBehaviour

- (void)setOwner:(id)owner
{
    if (_owner != owner) {
        [self vp_releaseLifetimeFromObject:_owner];
        _owner = owner;
        [self vp_bindLifetimeToObject:_owner];
    }
}

- (void)vp_bindLifetimeToObject:(id)object
{
    objc_setAssociatedObject(object, (__bridge void *)self, self, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)vp_releaseLifetimeFromObject:(id)object
{
    objc_setAssociatedObject(object, (__bridge void *)self, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
