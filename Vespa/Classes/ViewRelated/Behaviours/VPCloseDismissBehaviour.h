//
//  VPCloseDismissBehaviour.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VPBehaviour.h"

@interface VPCloseDismissBehaviour : VPBehaviour

- (IBAction)closeButtonPressed:(id)sender;

@property (assign, nonatomic) IBInspectable BOOL animated;

@end
