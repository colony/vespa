//
//  VPCloseDismissBehaviour.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCloseDismissBehaviour.h"

@implementation VPCloseDismissBehaviour

- (IBAction)closeButtonPressed:(id)sender
{
    if ([self.owner respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]) {
        [self.owner dismissViewControllerAnimated:self.animated completion:nil];
    }
}

@end
