//
//  VPCommentCell.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseTableViewCell.h"

@class VPComment;
@class VPCommentCellInfo;

@protocol VPCommentCellDelegate;

@interface VPCommentCell : VPBaseTableViewCell

@property (weak, nonatomic) id<VPCommentCellDelegate> delegate;
@property (strong, nonatomic, readonly) VPComment *comment;
@property (strong, nonatomic, readonly) VPCommentCellInfo *info;

- (void)setComment:(VPComment *)comment postOwnerId:(NSNumber *)postOwnerId info:(VPCommentCellInfo *)info;

- (void)setSelectedForReply:(BOOL)selected;

/** 
 * Use this method instead of heightOfCell:
 * The outExtraInfo is an OUT parameter, its property will be filled up when return
 */
+ (void)heightOfComment:(VPComment *)cmt withInfo:(VPCommentCellInfo *)outInfo;

@end

@protocol VPCommentCellDelegate <NSObject>

@optional
- (void)commentReplyButtonPressed:(VPCommentCell *)cell;
- (void)commentParentCommentViewTapped:(VPCommentCell *)cell;

@end
