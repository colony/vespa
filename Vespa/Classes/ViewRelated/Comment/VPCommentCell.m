//
//  VPCommentCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentCell.h"

#import "NSString+Util.h"
#import "UIView+Frame.h"
#import "VPComment.h"
#import "VPCommentCellInfo.h"
#import "VPComments.h"
#import "VPGeoUtil.h"
#import "VPMedia.h"
#import "VPPosts.h"
#import "VPUI.h"
#import "VPUser.h"
#import "VPUtil.h"
#import "VPViewRedirectionUtil.h"


#define VOTEDOWN_BUTTON_TAG 1
#define VOTEUP_BUTTON_TAG 2


@interface VPCommentCell ()

// Base comment view
@property (weak, nonatomic) IBOutlet UIImageView *anonImageView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;

// Parent comment view
@property (weak, nonatomic) IBOutlet UIView *parentWrapperView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *parentWrapperViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *parentCommentView; // the one within wrapperView
@property (weak, nonatomic) IBOutlet UIImageView *parentAnonImageView;
@property (weak, nonatomic) IBOutlet UILabel *parentBodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *expandAndCloseLabel;

@end


@implementation VPCommentCell

- (void)setupUI
{
    [super setupUI];

    self.contentView.autoresizingMask = UIViewAutoresizingFill;
    
    // See XIB for details
    self.bodyLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 55.0f - 10.0f;
    self.parentBodyLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 55.0f - 5.0f - 30.0f - 5.0f - 10.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(parentCommentViewTapped:)];
    [self.parentCommentView addGestureRecognizer:tap];
}

- (void)setComment:(VPComment *)comment postOwnerId:(NSNumber *)postOwnerId info:(VPCommentCellInfo *)info
{
    _comment = comment;
    _info = info;
    
    // parent comment view
    if (comment.parentComment) {
        self.parentWrapperView.hidden = NO;
        self.parentWrapperViewHeightConstraint.constant = info.isExpanded ? info.parentExpandHeight : info.parentCollapseHeight;
        
        self.expandAndCloseLabel.hidden = !info.expandible;
        if (info.expandible && info.isExpanded) {
            self.expandAndCloseLabel.hidden = NO;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"COLLAPSE"];
            [attributedString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, 8)];
            self.expandAndCloseLabel.attributedText = attributedString;
        } else if (info.expandible && !info.isExpanded) {
            self.expandAndCloseLabel.hidden = NO;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"EXPAND"];
            [attributedString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, 6)];
            self.expandAndCloseLabel.attributedText = attributedString;
        } else {
            self.expandAndCloseLabel.hidden = YES;
        }
        
        VPComment *parentComment = comment.parentComment;
        if ([parentComment.owner.userId isEqualToNumber:postOwnerId]) {
            self.parentAnonImageView.image = [UIImage imageNamed:@"op"];
        } else {
            [self loadIcon:self.parentAnonImageView icon:parentComment.ownerIcon];
            self.parentAnonImageView.backgroundColor = [VPUtil colorFromHexString:parentComment.ownerIcon.iconColorHex];
        }
        self.parentBodyLabel.numberOfLines = info.isExpanded ? 0 : 2;
        self.parentBodyLabel.text = [parentComment properBodyForParentComment];
    } else {
        self.parentWrapperView.hidden = YES;
        self.parentWrapperViewHeightConstraint.constant = 0;
    }
    
    // base comment view
    self.bodyLabel.text = comment.body;
    self.locationLabel.text = [VPGeoUtil formatDistanceFromHere:comment.geo];
    self.timeLabel.text = [VPUtil formatDateSinceNow:comment.createTime];
    if ([comment.owner.userId isEqualToNumber:postOwnerId]) {
        self.anonImageView.image = [UIImage imageNamed:@"op"];
    } else {
        [self loadIcon:self.anonImageView icon:comment.ownerIcon];
        self.anonImageView.backgroundColor = [VPUtil colorFromHexString:comment.ownerIcon.iconColorHex];
    }
    self.downButton.selected = [comment isDownvote];
    [self.downButton setTitle:[comment.numOfDownvotes stringValue] forState:UIControlStateNormal];
    self.upButton.selected = [comment isUpvote];
    [self.upButton setTitle:[comment.numOfUpvotes stringValue] forState:UIControlStateNormal];

//    [self setNeedsUpdateConstraints];
}

- (void)setSelectedForReply:(BOOL)selected
{
    if (selected) {
        self.backgroundColor = UIColorFromRGB(0xb7dbff);
        self.parentCommentView.backgroundColor = UIColorFromRGB(0xb7dbff);
    } else {
        self.backgroundColor = [UIColor whiteColor];
        self.parentCommentView.backgroundColor = [VPUI lightGrayColor];
    }
}

+ (void)heightOfComment:(VPComment *)cmt withInfo:(VPCommentCellInfo *)outInfo
{
    UIFont *font = [VPUI fontOpenSans:14.0f thickness:1]; 
    
    // body label
    // 55 - leading space to cell (super)
    // 10 - trailing space to cell (super)
    CGFloat bodyWidth = SCREEN_WIDTH - 55.0f - 10.0f;
    CGFloat bodyHeight = [cmt.body boundingRectWithSize:CGSizeMake(bodyWidth, MAXFLOAT)
                                                options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                             attributes:@{NSFontAttributeName:font}
                                                context:nil].size.height;
    bodyHeight = ceilf(bodyHeight);

    CGFloat parentCommentHeight = 0.0f;
    if (cmt.parentComment) {
        CGFloat parentCommentBodyWidth = SCREEN_WIDTH - 55.0f - 5.0f - 24.0f - 5.0f - 5.0f - 10.0f;
        CGFloat parentCommentBodyHeight = [[cmt.parentComment properBodyForParentComment] boundingRectWithSize:CGSizeMake(parentCommentBodyWidth, MAXFLOAT)
                                                                               options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                                            attributes:@{NSFontAttributeName:font}
                                                                               context:nil].size.height;
//        int charSize = lroundf(font.lineHeight);
//        int height = lroundf(parentCommentBodyHeight);
        NSInteger lineCount = (int)ceilf(parentCommentBodyHeight / font.lineHeight);
        
        if (lineCount == 1) {
            parentCommentHeight = 40.0f;
            outInfo.expandible = NO;
        } else if (lineCount == 2) {
            parentCommentHeight = 60.0f;
            outInfo.expandible = NO;
        } else {
            parentCommentHeight = 80.0f;
            outInfo.expandible = YES;
        }
        outInfo.parentCollapseHeight = parentCommentHeight;
        outInfo.parentExpandHeight = 5.0f + ceilf(parentCommentBodyHeight) + (outInfo.expandible ? 25.0f : 5.0f);
    }
    
    // parentCommentHeight -
    // 10.0f - vertical spacing between parent comment and body label
    // bodyHeight -
    // 20.0f - stats height
    // 10.0f - vertical spacing between stats and bottom
    // 25.0f - bottom height
    outInfo.height = (parentCommentHeight + 10.0f + bodyHeight + 20.0f + 10.0f + 25.0f);
}

+ (NSNumber *)heightOfCell:(id)obj
{
    // Unused!
    return @0;
}


#pragma mark - action handling
- (IBAction)replyButtonPressed:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(commentReplyButtonPressed:)]) {
        [self.delegate commentReplyButtonPressed:self];
    }
}

- (void)parentCommentViewTapped:(id)sender
{
    if (self.info.expandible) {
        if ([self.delegate respondsToSelector:@selector(commentParentCommentViewTapped:)]) {
            [self.delegate commentParentCommentViewTapped:self];
        }
    }
}

- (IBAction)voteButtonPressed:(UIButton *)sender
{
    BOOL isDownvoteClicked = (sender.tag == VOTEDOWN_BUTTON_TAG);
    
    VPVoteStatus status;
    if ([self.comment.voteStatus isEqualToString:@"DOWNVOTE"]) {
        if (isDownvoteClicked) {
            self.comment.voteStatus = @"NOVOTE";
            self.comment.numOfDownvotes = [NSNumber numberWithLong:(self.comment.numOfDownvotes.longValue - 1)];
            status = kVPVoteStatusUndown;
        } else {
            self.comment.voteStatus = @"UPVOTE";
            self.comment.numOfUpvotes = [NSNumber numberWithLong:(self.comment.numOfUpvotes.longValue + 1)];
            self.comment.numOfDownvotes = [NSNumber numberWithLong:(self.comment.numOfDownvotes.longValue - 1)];
            status = kVPVoteStatusUpSwitch;
        }
    } else if ([self.comment.voteStatus isEqualToString:@"UPVOTE"]) {
        if (isDownvoteClicked) {
            self.comment.voteStatus = @"DOWNVOTE";
            self.comment.numOfDownvotes = [NSNumber numberWithLong:(self.comment.numOfDownvotes.longValue + 1)];
            self.comment.numOfUpvotes = [NSNumber numberWithLong:(self.comment.numOfUpvotes.longValue - 1)];
            status = kVPVoteStatusDownSwitch;
        } else {
            self.comment.voteStatus = @"NOVOTE";
            self.comment.numOfUpvotes = [NSNumber numberWithLong:(self.comment.numOfUpvotes.longValue - 1)];
            status = kVPVoteStatusUnup;
        }
    } else { // NOVOTE
        if (isDownvoteClicked) {
            self.comment.voteStatus = @"DOWNVOTE";
            self.comment.numOfDownvotes = [NSNumber numberWithLong:(self.comment.numOfDownvotes.longValue + 1)];
            status = kVPVoteStatusDown;
        } else {
            self.comment.voteStatus = @"UPVOTE";
            self.comment.numOfUpvotes = [NSNumber numberWithLong:(self.comment.numOfUpvotes.longValue + 1)];
            status = kVPVoteStatusUp;
        }
    }
    
    self.comment.numOfDownvotes = @(MAX(0, self.comment.numOfDownvotes.longValue));
    self.comment.numOfUpvotes = @(MAX(0, self.comment.numOfUpvotes.longValue));
    
    self.downButton.selected = [self.comment isDownvote];
    [self.downButton setTitle:[self.comment.numOfDownvotes stringValue] forState:UIControlStateNormal];
    self.upButton.selected = [self.comment isUpvote];
    [self.upButton setTitle:[self.comment.numOfUpvotes stringValue] forState:UIControlStateNormal];
    
    [VPComments voteCommentWithId:self.comment.commentId
                           status:status
                          success:nil
                          failure:nil];
    
    if (status == kVPVoteStatusDown || status == kVPVoteStatusDownSwitch) {
        [VPReports reportEvent:kVPEventDownvoteComment withParam:@{@"cmtId":self.comment.commentId} mode:kVPEventModeOnce];
    } else if (status == kVPVoteStatusUp || status == kVPVoteStatusUpSwitch) {
        [VPReports reportEvent:kVPEventUpvoteComment withParam:@{@"cmtId":self.comment.commentId} mode:kVPEventModeOnce];
    }
}


#pragma mark - private
- (NSArray *)localSupportIconList
{
    static NSArray *array;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        array = @[
                  @"AngryNinja",
                  @"Duck",
                  @"HappyNinja",
                  @"MonkeyFace",
                  @"Ninja",
                  @"NinjaStar",
                  @"Bee",
                  @"Cat",
                  @"Dragon",
                  @"FishBones",
                  @"Lotus",
                  @"Seahorse",
                  @"Sushi"
                  ];
    });
    return array;
}

- (void)loadIcon:(UIImageView *)iconImageView icon:(VPMedia *)iconMedia
{
    if ([[self localSupportIconList] containsObject:iconMedia.iconSystemId]) {
        iconImageView.image = [UIImage imageNamed:iconMedia.iconSystemId];
    } else {
        // Remote load icon, respect the scale, i.e. //some_path/Duck_3 in iphone6+
        [iconImageView sd_setImageWithURL:[NSURL URLWithString:[iconMedia.iconUrl stringByAppendingString:[NSString stringWithFormat:@"_%d", (int)SCREEN_SCALE]]]
                         placeholderImage:nil
                                  options:SDWebImageRetryFailed|SDWebImageLowPriority];
    }
}

@end
