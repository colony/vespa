//
//  VPCommentCellInfo.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/3/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

@interface VPCommentCellInfo : NSObject

/** The current displaying height of comment cell, needed by tableView */
@property (assign, nonatomic) CGFloat height;

/** Per commentCell, height of parent comment view (if any) when collapsed */
@property (assign, nonatomic) CGFloat parentCollapseHeight;

/** Per commentCell height of parent comment view (if any) when expanded */
@property (assign, nonatomic) CGFloat parentExpandHeight;

/** Boolean set if the parent comment view has been expanded */
@property (assign, nonatomic) BOOL isExpanded;

/** Boolean set if the comment is bad reviewed and has been covered */
@property (assign, nonatomic) BOOL isCovered;

/** NO if the parentCommentView fits (<=2 lines of comments), YES otherwise */
@property (assign, nonatomic) BOOL expandible;

- (void)toggle;

@end
