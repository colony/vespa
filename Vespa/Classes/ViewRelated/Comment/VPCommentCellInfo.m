//
//  VPCommentCellInfo.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/3/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentCellInfo.h"

@implementation VPCommentCellInfo

- (id)init
{
    if (self = [super init]) {
        _height = 0.0f;
        _parentCollapseHeight = 0.0f;
        _parentExpandHeight = 0.0f;
        _isExpanded = NO;
    }
    return self;
}

- (void)toggle
{
    if (!self.expandible)
        return;
    
    if (self.isExpanded) {
        self.height -= (self.parentExpandHeight - self.parentCollapseHeight);
        self.isExpanded = NO;
    } else {
        self.height += (self.parentExpandHeight - self.parentCollapseHeight);
        self.isExpanded = YES;
    }
}

@end
