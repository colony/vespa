//
//  VPCommentCoverCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/3/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentCoverCell.h"

@implementation VPCommentCoverCell

- (void)setupUI
{
    [super setupUI];
    
    UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
    label.autoresizingMask = UIViewAutoresizingFill;
    label.font = [VPUI fontOpenSans:15.0 thickness:2];
    label.textColor = [VPUI darkGrayColor];
    label.text = @"Comment buried beneath tomatoes.";
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    
    self.backgroundColor = [VPUI lightGrayColor];
}

@end
