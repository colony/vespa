//
//  VPCommentPhotoCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentPhotoCell.h"

#import "FLAnimatedImageView.h"
#import "UIImageView+WebCacheAddition.h"
#import "VPComment.h"
#import "VPCommentCellInfo.h"
#import "VPMedia.h"


@interface VPCommentPhotoCell ()

@property (weak, nonatomic) IBOutlet FLAnimatedImageView *photoImageView;

@end

@implementation VPCommentPhotoCell

#pragma mark - override
- (void)setupUI
{
    [super setupUI];
    self.photoImageView.shouldStartAnimatingAutomatically = YES;
}

- (void)resetCell
{
    [super resetCell];
    [self.photoImageView vp_cancelCurrentLHImageLoad];
    self.photoImageView.image = nil;
    self.photoImageView.animatedImage = nil;
}

- (void)setComment:(VPComment *)comment postOwnerId:(NSNumber *)postOwnerId info:(VPCommentCellInfo *)info
{
    [super setComment:comment postOwnerId:postOwnerId info:info];
    
    __weak __typeof(self)wself = self;
    [self.photoImageView vp_setImageWithLowURL:[NSURL URLWithString:comment.photo.photoThumbnailUrl]
                                    lowOptions:(SDWebImageRetryFailed|SDWebImageHighPriority)
                                       highURL:[NSURL URLWithString:comment.photo.photoUrl]
                                   highOptions:(SDWebImageRetryFailed|SDWebImageLowPriority)
                              placeholderImage:nil
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL, BOOL isLowQuality) {
                                         if (!wself)return;
                                         if (isLowQuality) {
                                             wself.photoImageView.image = image;
                                             wself.photoImageView.alpha = 0.6f;
                                         } else {
                                             wself.photoImageView.image = image;
                                             wself.photoImageView.alpha = 1.0f;
                                         }
                                     }];
    
//    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:comment.photo.photoUrl]
//                           placeholderImage:nil
//                                    options:(SDWebImageRetryFailed|SDWebImageLowPriority)];
}

+ (void)heightOfComment:(VPComment *)cmt withInfo:(VPCommentCellInfo *)outInfo
{
    [super heightOfComment:cmt withInfo:outInfo];
    VPMedia *photoMedia = cmt.photo;
    
    CGFloat photoWidth = photoMedia.photoWidth * 1.0f;
    CGFloat photoHeight = photoMedia.photoHeight * 1.0f;
    CGFloat targetHeight = (SCREEN_WIDTH - 55 - 10) * (photoHeight / photoWidth);
    
    // We need addup the photo height
    outInfo.height += targetHeight;
}

@end
