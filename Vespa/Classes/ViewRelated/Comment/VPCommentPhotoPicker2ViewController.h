//
//  VPCommentPhotoPicker2ViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseCollectionViewController.h"

@interface VPCommentPhotoPicker2ViewController : VPBaseCollectionViewController

- (id)initWithOptionId:(NSNumber *)optionId optionName:(NSString *)optionName;

@end
