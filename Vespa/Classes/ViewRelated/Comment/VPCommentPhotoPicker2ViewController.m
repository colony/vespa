//
//  VPCommentPhotoPicker2ViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentPhotoPicker2ViewController.h"

#import "FLAnimatedImage.h"
#import "UIImage+Addition.h"
#import "UINavigationItem+Util.h"
#import "VPAPIClient.h"
#import "VPBaseCollectionViewCell.h"
#import "VPCommentPhotoPickerOption.h"
#import "VPDownloadManager.h"
#import "VPMedia.h"
#import "VPMediaSubmitPreviewViewController.h"
#import "VPPaginator.h"


@interface _VPCommentPhotoPickerItemCell : VPBaseCollectionViewCell

@property (strong, nonatomic) UIImageView *imageView;

@end

@implementation _VPCommentPhotoPickerItemCell

- (void)setupUI
{
    [super setupUI];
    
    self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.imageView.autoresizingMask = UIViewAutoresizingFill;
    [self addSubview:self.imageView];
    
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = SCREEN_SCALE;
    self.layer.cornerRadius = 5.0f;
    self.layer.masksToBounds = YES;
}

- (void)resetCell
{
    [super resetCell];
    [self.imageView sd_cancelCurrentImageLoad];
    self.imageView.image = nil;
}

+ (BOOL)isAcceptable:(id)obj
{
    return [obj isKindOfClass:[VPCommentPhotoPickerOptionItem class]];
}

+ (NSValue *)sizeOfCell:(id)obj
{
    CGFloat width = (SCREEN_WIDTH - 30.0f)/2;
    return [NSValue valueWithCGSize:CGSizeMake(width, width)];
}

- (void)setItem:(VPCommentPhotoPickerOptionItem *)item
{
    if ([item.media isGif]) {
        // We only load the gif thumbnail which is the posterImageUrl
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:item.media.photoThumbnailUrl]
                          placeholderImage:nil
                                   options:(SDWebImageRetryFailed|SDWebImageLowPriority)
                                 completed:nil];
    } else {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:item.media.photoUrl]
                          placeholderImage:nil
                                   options:(SDWebImageRetryFailed|SDWebImageLowPriority)
                                 completed:nil];
    }
}

@end



@interface VPCommentPhotoPicker2ViewController ()<VPMediaSubmitPreviewViewControllerDelegate>

@property (strong, nonatomic) VPPaginator *paginator;
@property (assign, nonatomic) BOOL loadWhenDidAppear;
@property (strong, nonatomic) NSNumber *optionId;
@property (strong, nonatomic) NSString *optionName;

@property (strong, nonatomic) UIImage *currentPreviewingItemImg;
@property (strong, nonatomic) NSNumber *currentPreviewingItemId;

@end


@implementation VPCommentPhotoPicker2ViewController

#pragma mark - lifecycle
- (id)initWithOptionId:(NSNumber *)optionId optionName:(NSString *)optionName
{
    if (self = [super init]) {
        _optionId = optionId;
        _optionName = optionName;
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [super commonInit];
    self.cellClasses = @[[_VPCommentPhotoPickerItemCell class]];
    self.cellNibNames = @[[NSNull null]];
    self.enablePullToRefresh = YES;
    self.enableLoadMore = YES;
    self.enableTabbarHandle = NO;
}

- (void)setupCollectionView:(UICollectionView *)collectionView
{
    [super setupCollectionView:collectionView];
    collectionView.backgroundColor = [VPUI lightGrayColor];
    collectionView.contentInset = UIEdgeInsetsMake(0, 0, kVPDefaultLoadMoreFooterHeight, 0);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setCustomTitle:self.optionName];
    self.loadWhenDidAppear = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.collectionView.scrollsToTop = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.loadWhenDidAppear) {
        self.loadWhenDidAppear = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
        [self pullDownRefresh];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.collectionView.scrollsToTop = NO;
}


#pragma mark - override
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    @weakify(self);
    VPAPIClient *client = [VPAPIClient sharedInstance];
    self.ongoingRequestOperation = [client fetchCommentPhotoPickerOptionItemsWithUrl:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                                            optionId:self.optionId
                                                                             success:^(VPCommentPhotoPickerOptionItemList *itemList) {
                                                                                 @strongify(self);
                                                                                 self.paginator = itemList.paginator;
                                                                                 [self onObjectsDidLoaded:itemList.items pageNo:pageNo error:nil];
                                                                                 [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                                             } failure:^(NSError *error) {
                                                                                 @strongify(self);
                                                                                 VPLog(@"Fail to fetch comment photo picker options: %@", [error localizedDescription]);
                                                                                 [self onObjectsDidLoaded:nil pageNo:pageNo error:error];
                                                                                 [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                                             }];
}


#pragma mark - collectionview delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    VPCommentPhotoPickerOptionItem *item = [self.items objectAtIndex:index];
    
    _VPCommentPhotoPickerItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"_VPCommentPhotoPickerItemCell" forIndexPath:indexPath];
    [cell setItem:item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _VPCommentPhotoPickerItemCell *cell = (_VPCommentPhotoPickerItemCell *)[self collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    VPCommentPhotoPickerOptionItem *item = [self.items objectAtIndex:index];
    VPMedia *media = item.media;
    
    self.currentPreviewingItemId = item.itemId;
    self.currentPreviewingItemImg = cell.imageView.image; // Even if it is GIF, we've already load the posterImage in cell
    
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellRect = attributes.frame;
    CGRect cellRectInWindow = [collectionView convertRect:cellRect toView:nil];
    
    __block VPMediaSubmitPreviewViewController *previewVC;
    if ([media isGif]) {
        [[VPDownloadManager sharedInstance] downloadImage:[NSURL URLWithString:media.photoUrl] completed:^(UIImage *image, NSError *error) {
            // The image should be FLAnimatedImage, because VPDownloadManager depends on SDWebImageManager which also be
            // modified by SDWebImage+Swizzle to support FLAnimatedImage integration
            FLAnimatedImage *flImage = (FLAnimatedImage *)image;
            previewVC = [[VPMediaSubmitPreviewViewController alloc] initWithReferenceRect:cellRectInWindow
                                                                                    image:cell.imageView.image
                                                                              previewType:@"GIF"
                                                                                     data:flImage
                                                                             enableScroll:NO
                                                                             enablePicker:YES
                                                                                   picked:NO];
            previewVC.delegate = self;
            [previewVC showFromViewController:self.navigationController];
        }];
    } else { //JPG or PNG or static image
        previewVC = [[VPMediaSubmitPreviewViewController alloc] initWithReferenceRect:cellRectInWindow
                                                                                image:cell.imageView.image
                                                                          previewType:@"IMAGE"
                                                                                 data:nil
                                                                         enableScroll:NO
                                                                         enablePicker:YES
                                                                               picked:NO];
         previewVC.delegate = self;
        [previewVC showFromViewController:self.navigationController];
    }
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.0f; // minimum, one less than 10 to make calculation less strict
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10.0f, 10.0f, 0.0f, 10.0f);
}


#pragma mark - VPMediaSubmitPreviewViewControllerDelegate
- (void)previewViewController:(VPMediaSubmitPreviewViewController *)viewController picked:(BOOL)picked
{
    UIImage *resultImg = [self.currentPreviewingItemImg resizedImage:CGSizeMake(25, 25) interpolationQuality:kCGInterpolationLow];
    
    [[VPContext sharedInstance] putDomain:kVPCDPostViewController key:@"optionItemId" val:self.currentPreviewingItemId];
    [[VPContext sharedInstance] putDomain:kVPCDPostViewController key:@"optionItemMedia" val:resultImg];
    
    [viewController dismissViewControllerAnimated:NO completion:nil];
    // We need pop twice to VPPostViewController
    NSArray *vcs = self.navigationController.viewControllers;
    UIViewController *toVC = vcs[vcs.count - 3];
    [self.navigationController popToViewController:toVC animated:YES];
}

@end
