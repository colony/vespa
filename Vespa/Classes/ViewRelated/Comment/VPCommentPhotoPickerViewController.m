//
//  VPCommentPhotoPickerViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 8/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCommentPhotoPickerViewController.h"

#import "UINavigationItem+Util.h"
#import "VPAPIClient.h"
#import "VPBaseCollectionViewCell.h"
#import "VPCommentPhotoPickerOption.h"
#import "VPCommentPhotoPicker2ViewController.h"
#import "VPComments.h"


@interface _VPCommentPhotoPickerOptionCell : VPBaseCollectionViewCell

@property (strong, nonatomic) VPCommentPhotoPickerOption *option;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UIImageView *coverImageView;

@end

@implementation _VPCommentPhotoPickerOptionCell

- (void)setupUI
{
    [super setupUI];
    
    self.coverImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.coverImageView.autoresizingMask = UIViewAutoresizingFill;
    [self addSubview:self.coverImageView];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:self.bounds];
    self.nameLabel.autoresizingMask = UIViewAutoresizingFill;
    self.nameLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.5f];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.textColor = [UIColor whiteColor];
    self.nameLabel.font = [VPUI fontOpenSans:18.0 thickness:3];
    [self addSubview:self.nameLabel];
    
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = SCREEN_SCALE;
    self.layer.cornerRadius = 5.0f;
    self.clipsToBounds = YES;
}

- (void)resetCell
{
    [super resetCell];
    [self.coverImageView sd_cancelCurrentImageLoad];
    self.coverImageView.image = nil;
}

+ (BOOL)isAcceptable:(id)obj
{
    return [obj isKindOfClass:[VPCommentPhotoPickerOption class]];
}

+ (NSValue *)sizeOfCell:(id)obj
{
    CGFloat width = (SCREEN_WIDTH - 30.0f)/2;
    return [NSValue valueWithCGSize:CGSizeMake(width, width)];
}

- (void)setOption:(VPCommentPhotoPickerOption *)option
{
    _option = option;
    self.nameLabel.text = option.name;
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:option.coverImageUrl]
                           placeholderImage:nil
                                    options:(SDWebImageRetryFailed|SDWebImageLowPriority)
                                  completed:nil];
}

@end


@interface VPCommentPhotoPickerViewController ()

@property (assign, nonatomic) BOOL loadWhenDidAppear;

@end


@implementation VPCommentPhotoPickerViewController

#pragma mark - lifecycle
- (void)commonInit
{
    [super commonInit];
    self.cellClasses = @[[_VPCommentPhotoPickerOptionCell class]];
    self.cellNibNames = @[[NSNull null]];
    self.enablePullToRefresh = YES;
    self.enableLoadMore = NO;
    self.enableTabbarHandle = NO;
}

- (void)setupCollectionView:(UICollectionView *)collectionView
{
    [super setupCollectionView:collectionView];
    collectionView.backgroundColor = [VPUI mediumGrayColor];
    collectionView.contentInset = UIEdgeInsetsMake(0, 0, kVPDefaultLoadMoreFooterHeight, 0);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setCustomTitle:@"Reactions"];
    self.loadWhenDidAppear = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.collectionView.scrollsToTop = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.loadWhenDidAppear) {
        self.loadWhenDidAppear = NO;
//        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
        [self pullDownRefresh];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.collectionView.scrollsToTop = NO;
}


#pragma mark - override
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    @weakify(self);
    VPAPIClient *client = [VPAPIClient sharedInstance];
    self.ongoingRequestOperation = [client fetchCommentPhotoPickerOptionsWithParam:nil
                                                                           success:^(NSArray *options) {
                                                                               @strongify(self);
                                                                               [self onObjectsDidLoaded:options pageNo:pageNo error:nil];
//                                                                               [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                                           } failure:^(NSError *error) {
                                                                               @strongify(self);
                                                                               VPLog(@"Fail to fetch comment photo picker options: %@", [error localizedDescription]);
                                                                               [self onObjectsDidLoaded:nil pageNo:pageNo error:error];
//                                                                               [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                                           }];
}


#pragma mark - collectionview delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    VPCommentPhotoPickerOption *option = [self.items objectAtIndex:index];
    
    _VPCommentPhotoPickerOptionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"_VPCommentPhotoPickerOptionCell" forIndexPath:indexPath];
    cell.option = option;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    VPCommentPhotoPickerOption *option = [self.items objectAtIndex:index];
    VPCommentPhotoPicker2ViewController *picker2VC = [[VPCommentPhotoPicker2ViewController alloc] initWithOptionId:option.optionId optionName:option.name];
    [self.navigationController pushViewController:picker2VC animated:YES];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.0f; // minimum, one less than 10 to make calculation less strict
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10.0f, 10.0f, 0.0f, 10.0f);
}

@end
