//
//  VPLocationDisableViewController.m
//  Vespa
//
//  Created by Jay on 2/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPLocationDisableViewController.h"

@implementation VPLocationDisableViewController

- (void)setupUI
{
    [super setupUI];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    imgView.image = [UIImage imageNamed:@"location_off_page"];
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:imgView];
}
@end
