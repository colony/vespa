//
//  VPTutorialViewController.m
//  Vespa
//
//  Created by Jay on 1/18/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPTutorialViewController.h"
#import "VPInitDataManager.h"
#import "VPLocationDisableViewController.h"
#import "VPLocationManager.h"
#import "VPMain2ViewController.h"
#import "VPPromptManager.h"
#import "VPUsers.h"
#import "VPWebViewController.h"


static NSString *kCaptionKey = @"captionKey";
static NSString *kImageKey = @"imageKey";

@interface VPTutorialViewController () <UIScrollViewDelegate,UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewPages;
@property (nonatomic, strong) NSArray *contentList;
@property (nonatomic, assign) BOOL hasInitCam;

@end

@implementation VPTutorialViewController

#pragma mark - life cycle
- (void)commonInit
{
    [super commonInit];
    
    NSString *s;
    if (DEVICE_IS_IPHONE_4) {
        s = @"_iphone4";
    } else {
        CGFloat scale = [UIScreen mainScreen].scale;
        s = [NSString stringWithFormat:@"@%dx", (int)scale];
    }
    
    VPLog(@"The tutorial into extension %@", s);
    
    self.contentList = @[
                         [@"Intro1" stringByAppendingString:s],
                         [@"Intro2" stringByAppendingString:s],
                         [@"Intro3" stringByAppendingString:s],
                         [@"Intro4" stringByAppendingString:s],
                         [@"Intro5" stringByAppendingString:s],
                         [@"Intro6" stringByAppendingString:s]
                         ];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)setupUI
{
    [super setupUI];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    NSUInteger numberPages = self.contentList.count;
    
    // view controllers are created lazily
    // in the meantime, load the array with placeholders which will be replaced on demand
    NSMutableArray *viewPages = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < numberPages; i++)
    {
        [viewPages addObject:[NSNull null]];
    }
    self.viewPages = viewPages;
    
    // a page is the width of the scroll view
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.width * numberPages, self.scrollView.height);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.bounces = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    self.pageControl.numberOfPages = numberPages;
    self.pageControl.currentPage = 0;
    self.pageControl.pageIndicatorTintColor = UIColorFromRGB(0x5fb6ff);
    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    
    
    //pre load all page
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    [self loadScrollViewWithPage:2];
    [self loadScrollViewWithPage:3];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveLocationServiceChange:)
                                                 name:kLocationServiceStatusChangeNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - action
- (IBAction)changePage:(id)sender
{
    [self gotoPage:YES];    // YES = animate
}

- (void)continueButtonPressed:(id)sender
{
    [[VPLocationManager sharedInstance] updateGeo];
}

#pragma mark - private
- (void)loadScrollViewWithPage:(NSUInteger)page
{
    if (page >= self.contentList.count)
        return;
    
    // replace the placeholder if necessary
    UIView *view = [self.viewPages objectAtIndex:page];
    if ((NSNull *)view == [NSNull null]) {
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:self.contentList[page] ofType:@"png" inDirectory:@"CarouselTutorial"];
        
        //TODO: init own page view here
        view = [[UIView alloc]initWithFrame:self.scrollView.bounds];
        view.x = self.scrollView.width * page;
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:view.bounds];
        imgView.image = [[UIImage alloc] initWithContentsOfFile:imagePath];
        imgView.center = self.scrollView.center;
        [view addSubview:imgView];
        
        // last page need to add button and agreement text
        if (page == self.contentList.count - 1) {
            UIButton *continueButton = [self configureContinueButton:view];
            [view addSubview:continueButton];
            
            UITextView *agreeTextView = [self configureAgreeTextView:view];
            [view addSubview:agreeTextView];
        }
        
        [self.viewPages replaceObjectAtIndex:page withObject:view];
        [self.scrollView addSubview:view];
    }
}

- (UIButton *)configureContinueButton:(UIView *)view
{
    VPButton *continueButton = [[VPButton alloc]initWithFrame:CGRectMake(0, 0, 200, 67)];
    continueButton.centerX = view.width / 2;
    continueButton.bottom = view.height - 50;
    [continueButton setBackgroundImage:[UIImage imageNamed:@"continue_btn"] forState:UIControlStateNormal];
    [continueButton addTarget:self action:@selector(continueButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return continueButton;
}

- (UITextView *)configureAgreeTextView:(UIView *)view
{
    UITextView *agreementTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 258, 50)];
    agreementTextView.centerX = view.width / 2;
    agreementTextView.bottom = view.height;
    agreementTextView.backgroundColor = [UIColor clearColor];
    agreementTextView.editable = NO;
    agreementTextView.showsHorizontalScrollIndicator = NO;
    agreementTextView.showsVerticalScrollIndicator = NO;
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@"You also agree to Ninjr's Terms of Service and Privacy Policy."];
    NSRange range1 = [attrStr.string rangeOfString:@"Terms of Service"];
    NSRange range2 = [attrStr.string rangeOfString:@"Privacy Policy"];
    
    [attrStr beginEditing];
    [attrStr addAttribute:NSFontAttributeName
                    value:[VPUI fontOpenSans:11 thickness:3]
                    range:range1];
    [attrStr addAttribute:NSFontAttributeName
                    value:[VPUI fontOpenSans:11 thickness:3]
                    range:range2];
    [attrStr addAttribute:NSLinkAttributeName
                    value:[NSURL URLWithString:@"http://ninjrapp.com/terms"]
                    range:range1];
    [attrStr addAttribute:NSLinkAttributeName
                    value:[NSURL URLWithString:@"http://ninjrapp.com/privacy"]
                    range:range2];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [attrStr.string length])];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment                = NSTextAlignmentCenter;
    [attrStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attrStr.string length])];
    [attrStr endEditing];
    
    agreementTextView.attributedText = attrStr;
    agreementTextView.linkTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    agreementTextView.delegate = self;
    
    return agreementTextView;
}

// at the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.width;
    NSUInteger page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    //Disable page controll and scroll view for last page
    if(page == self.contentList.count - 1) {
        self.pageControl.hidden = YES;
        self.scrollView.scrollEnabled = NO;
    }
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // a possible optimization would be to unload the views+controllers which are no longer visible
}

- (void)gotoPage:(BOOL)animated
{
    NSInteger page = self.pageControl.currentPage;
    
    //Disable page controll and scroll view for last page
    if(page == self.contentList.count - 1) {
        self.pageControl.hidden = YES;
        self.scrollView.scrollEnabled = NO;
    }
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // update the scroll view to the appropriate page
    CGRect bounds = self.scrollView.bounds;
    bounds.origin.x = CGRectGetWidth(bounds) * page;
    bounds.origin.y = 0;
    [self.scrollView scrollRectToVisible:bounds animated:animated];
}

- (void)didReceiveLocationServiceChange: (NSNotification *)notification
{
    BOOL isAvailable = [notification.userInfo[@"available"]boolValue];
    BOOL isDetermined = [notification.userInfo[@"determined"]boolValue];
    if (isAvailable) {
        [self registerUserAndShowMainPage];
    } else if(isDetermined) {
        [self showLocationSettingPage];
    }
}

- (void)registerUserAndShowMainPage
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil userInfo:@{@"status":@"Registering"}];
    [VPUsers signupWithDeviceSuccess:^{
        // De-duty ourself from location status change notification
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        // Right after registration, we immediately report something to server
        [VPReports reportGeoLocation:[VPLocationManager sharedInstance].currentGeo];
        [VPReports reportDeviceToken];
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        window.rootViewController = [[VPMain2ViewController alloc] init];
        [window makeKeyAndVisible];
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
    } failure:^(NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                        message:@"Fail to register at this moment, plz try later"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }];
}

- (void)showLocationSettingPage
{
    [VPUsers signupWithDeviceSuccess:^{
        [VPReports reportEvent:kVPEventDisableLocation withParam:nil mode:kVPEventModeOnce];

        VPLocationDisableViewController *locationVC = [VPLocationDisableViewController new];
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        window.rootViewController = locationVC;
        [window makeKeyAndVisible];
    } failure:nil];
}


#pragma mark - status bar hiding
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    [[VPPromptManager sharedInstance] prompt:kVPPromptWeb data:[URL absoluteString] onlyOnce:NO overContext:NO animated:YES];
    
//    VPWebViewController *webVC = [[VPWebViewController alloc] initWithURL:URL customTitle:@""];
//    webVC.isPresenting = YES;
//    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:webVC];
//    [self presentViewController:nc animated:YES completion:nil];
    
    // Avoid opening the URL in browser
    return NO;
}

@end
