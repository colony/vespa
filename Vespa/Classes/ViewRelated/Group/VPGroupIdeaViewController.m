//
//  VPGroupIdeaViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroupIdeaViewController.h"

#import "OLGhostAlertView.h"
#import "UIView+EventIntercept.h"
#import "VPApiClient.h"


@interface VPGroupIdeaViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation VPGroupIdeaViewController

#pragma mark - lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentView.stopEventPropagation = YES;
    
    self.textView.layer.borderColor = [VPUI mediumGrayColor].CGColor;
    self.textView.layer.borderWidth = 1.0f;
    self.textView.layer.cornerRadius = 5;
    self.textView.clipsToBounds = YES;
    self.textView.delegate = self;
}


#pragma mark - UITextView delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    self.placeholderLabel.hidden = YES;
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    self.placeholderLabel.hidden = [textView hasText];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - action handling
- (IBAction)submitButtonPressed:(id)sender
{
    if ([self.textView hasText]) {
        [[VPAPIClient sharedInstance] reportFeedback:[@"Group:" stringByAppendingString:self.textView.text] success:nil failure:nil];
        [self dismissViewControllerAnimated:NO completion:^{
            OLGhostAlertView *alertView = [[OLGhostAlertView alloc] initWithTitle:@"Thank You"
                                                                          message:nil
                                                                          timeout:0.3
                                                                      dismissible:NO];
            alertView.style = OLGhostAlertViewStyleLight;
            alertView.position = OLGhostAlertViewPositionCenter;
            [alertView show];
        }];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
