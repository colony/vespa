//
//  VPGroupListCell.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseCollectionViewCell.h"

@class VPGroup;

@interface VPGroupListCell : VPBaseCollectionViewCell

@property (strong, nonatomic) VPGroup *group;

@end
