//
//  VPGroupListCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroupListCell.h"

#import "VPGroup.h"


@interface VPGroupListCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@end


@implementation VPGroupListCell

- (void)setupUI
{
    [super setupUI];
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = SCREEN_SCALE;
    
    self.coverImageView.layer.cornerRadius = 17.0f;
    self.coverImageView.layer.masksToBounds = YES;
    
    self.nameLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 10.0f - 35.0f - 10.0f - 10.0f;
}

+ (BOOL)isAcceptable:(id)obj
{
    return [obj isKindOfClass:[VPGroup class]];
}

+ (NSValue *)sizeOfCell:(id)obj
{
    VPGroup *group = obj;
    
    // group name label
    // 10 - leading space to cell (super)
    // 35 - width of group cover icon
    // 10 - space between cover and name
    // 10 - trailing space to cell (super)
    CGFloat w = SCREEN_WIDTH - 10.0f - 35.0f - 10.0f - 10.0f;
    CGFloat h = [group.name boundingRectWithSize:CGSizeMake(w, MAXFLOAT)
                                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                      attributes:@{NSFontAttributeName:[VPUI fontOpenSans:14.0f thickness:2]}
                                         context:nil].size.height;
    h = ceilf(h);
    
    // 35 - height of group cover icon
    // 15 - height of stats
    // 14 - extra space surrounding each cell
    CGFloat result = MAX(35, h+15);
    return [NSValue valueWithCGSize:CGSizeMake(SCREEN_WIDTH, result + 14.0f)];
}

- (void)setGroup:(VPGroup *)group
{
    _group = group;
    if ([group.groupId isEqualToNumber:@2]) {
        self.statLabel.text = @""; // We hide stats for block group
    } else {
        self.statLabel.text = [NSString stringWithFormat:@"%@ stars  %@ tomatoes", group.numOfUpVotes, group.numOfDownVotes];
    }
    self.nameLabel.text = group.name;
    
    @weakify(self);
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:group.coverImageUrl]
                           placeholderImage:nil
                                    options:(SDWebImageRetryFailed|SDWebImageLowPriority)
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      @strongify(self);
                                      self.coverImageView.alpha = 0.6f;
                                      [UIView animateWithDuration:0.2
                                                            delay:0.0
                                                          options:UIViewAnimationOptionCurveLinear
                                                       animations:^{
                                                           self.coverImageView.alpha = 1.0f;
                                                       }
                                                       completion:nil];
                                  }];
}

@end
