//
//  VPGroupListViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroupListViewController.h"

#import "UINavigationItem+Util.h"
#import "VPBaseCollectionReusableView.h"
#import "VPGroup.h"
#import "VPGroupIdeaViewController.h"
#import "VPGroupListCell.h"
#import "VPGroups.h"
#import "VPGroupViewController.h"
#import "VPPaginator.h"
#import "VPPost.h"
#import "VPPosts.h"
#import "VPPostThreadViewController.h"
#import "VPPostViewController.h"
#import "VPPromptManager.h"
#import "VPUtil.h"


static CGFloat const _kVPHeaderViewHeight = 30.0f;

@interface _VPHeaderView : VPBaseCollectionReusableView

@property (strong, nonatomic) UILabel *label;

@end


@interface _VPFeaturedListCell : VPBaseCollectionViewCell

@property (strong, nonatomic) VPPost *featuredPost;

@end


@interface VPGroupListViewController ()

@property (assign, nonatomic) NSInteger numFeatured;
@property (strong, nonatomic) VPPaginator *paginator;
@property (assign, nonatomic) BOOL loadWhenDidAppear;

@end


@implementation VPGroupListViewController

#pragma mark - lifecycle
- (void)commonInit
{
    [super commonInit];
    self.numFeatured = 0;
    self.cellClasses = @[[VPGroupListCell class], [_VPFeaturedListCell class]];
    self.cellNibNames = @[@"VPGroupListCell", [NSNull null]];
    self.sectionHeaderClass = [_VPHeaderView class];
    self.enablePullToRefresh = YES;
    self.enableLoadMore = YES;
    self.enableTabbarHandle = YES;
    
    // NOTE, the parent class dealloc will remove from notificationCenter
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTabbarCurrentTapNotification:) name:kTabbarCurrentTapNotification object:nil];
}

- (void)setupCollectionView:(UICollectionView *)collectionView
{
    [super setupCollectionView:collectionView];
    collectionView.backgroundColor = [UIColor whiteColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setCustomTitle:@"Explore Tribes"];
    self.loadWhenDidAppear = YES;
    
    UIButton *addPostButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addPostButton.frame = CGRectMake(0, 0, 40, 40);
    addPostButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [addPostButton addTarget:self action:@selector(groupIdeaButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [addPostButton setImage:[UIImage imageNamed:@"group_lightbulb"] forState:UIControlStateNormal];
    [self.navigationItem setCustomRightWithView:addPostButton];
    [self.navigationItem tweakRightWithSpace:-8];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.collectionView.scrollsToTop = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.loadWhenDidAppear) {
        self.loadWhenDidAppear = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
        [self pullDownRefresh];
    }
    [VPReports reportEvent:kVPEventViewGroupList withParam:nil mode:kVPEventModeStart];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.collectionView.scrollsToTop = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [VPReports reportEvent:kVPEventViewGroupList withParam:nil mode:kVPEventModeEnd];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - action handling
- (void)groupIdeaButtonPressed:(id)sender
{
    [[VPPromptManager sharedInstance] prompt:kVPPromptGroupIdea data:nil onlyOnce:NO overContext:YES animated:NO];
}


#pragma mark - override
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_group_t requestGroup = dispatch_group_create();
        
        __block NSError *bError = nil;
        __block VPPostList *bFeaturedList = nil;
        __block VPGroupList *bGroupList = nil;
        
        if (pageNo == 0) {
            dispatch_group_enter(requestGroup);
            [VPPosts fetchPostsWithType:kVPPostListFetchTypeFeatured url:nil param:nil
                                success:^(VPPostList *postList) {
                                    bFeaturedList = postList;
                                    dispatch_group_leave(requestGroup);
                                } failure:^(NSError *error) {
                                    VPLog(@"Fail fetchFeatured: %@", [error localizedDescription]);
                                    bError = error;
                                    dispatch_group_leave(requestGroup);
                                }];
        }
        
        dispatch_group_enter(requestGroup);
        self.ongoingRequestOperation = [VPGroups fetchGroupsWithUrl:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                              param:nil
                                                            success:^(VPGroupList *groupList) {
                                                                bGroupList = groupList;
//                                                                [self onObjectsDidLoaded:groupList.groups pageNo:pageNo error:nil];
                                                                
                                                                // Upon first page loading, we might start the tutorial
//                                                                if (pageNo == 0) {
//                                                                    [[VPTutorialManager sharedInstance] start:kVPTutorialTribe];
//                                                                }
                                                                dispatch_group_leave(requestGroup);
                                                            } failure:^(NSError *error) {
                                                                VPLog(@"Fail fetchGroups: %@", [error localizedDescription]);
                                                                bError = error;
                                                                dispatch_group_leave(requestGroup);
                                                            }];
        
        dispatch_group_wait(requestGroup, DISPATCH_TIME_FOREVER);
        dispatch_async(dispatch_get_main_queue(), ^{
            if (bError) {
                [self onObjectsDidLoaded:nil pageNo:pageNo error:bError];
            } else {
                self.paginator = bGroupList.paginator;
                NSMutableArray *newItems = [NSMutableArray new];
                if (pageNo == 0) {
                    self.numFeatured = [bFeaturedList.posts count];
                    [newItems addObjectsFromArray:bFeaturedList.posts];
                }
                [newItems addObjectsFromArray:bGroupList.groups];
                [self onObjectsDidLoaded:newItems pageNo:pageNo error:nil];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
        });
    });
}

- (void)calculateSectionInfo
{
    [self.sectionInfo removeAllObjects];
    [self.sectionInfo addObject:@(self.numFeatured)];
    [self.sectionInfo addObject:@(self.items.count - self.numFeatured)];
}


#pragma mark - collectionview delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    if (indexPath.section == 0) {
        VPPost *post = [self.items objectAtIndex:index];
        _VPFeaturedListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"_VPFeaturedListCell" forIndexPath:indexPath];
        cell.featuredPost = post;
        return cell;
    } else {
        VPGroup *group = [self.items objectAtIndex:index];
        VPGroupListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VPGroupListCell" forIndexPath:indexPath];
        cell.group = group;
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    UIViewController *toController = nil;
    if (indexPath.section == 0) {
        // Featured post is selected
        VPPost *post = [self.items objectAtIndex:index];
        if ([post isThreadRoot]) {
            toController = [[VPPostThreadViewController alloc] initWithThreadRoot:post];
        } else {
            toController = [[VPPostViewController alloc] initWithPost:post];
        }
    } else {
        VPGroup *group = [self.items objectAtIndex:index];
        toController = [[VPGroupViewController alloc] initWithGroupId:group.groupId groupName:group.name];
    }

    if (toController) {
        toController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toController animated:YES];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionFooter)
        return [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    
    UICollectionReusableView *view = [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    _VPHeaderView *headerView = (_VPHeaderView *)view;
    if (indexPath.section == 0) {
        headerView.label.text = @"Featuring";
    } else {
        headerView.label.text = @"Tribes";
    }
    return view;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//    return 9.0f; // minimum, one less than 10 to make calculation less strict
//}
//
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f);
//}


#pragma mark - notification
- (void)handleTabbarCurrentTapNotification:(NSNotification *)noti
{
    if (noti.userInfo && [noti.userInfo[@"tab"] isEqualToString:@"GROUP"]) {
        if (self.isTop) {
            [self.collectionView setContentOffset:CGPointMake(0.0f, 0.0f) animated:YES];
        }
    }
}

@end


@implementation _VPFeaturedListCell

- (void)setupUI
{
    self.opaque = YES;
    self.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10.0f, 4.0f, self.width - 20.0f, self.height - 10.0f);
    label.autoresizingMask = UIViewAutoresizingFill;
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [VPUI fontOpenSans:14.0 thickness:1];
    label.tag = 999;
    [self addSubview:label];
    
    UIView *divider = [[UIView alloc] init];
    divider.frame = CGRectMake(0.0f, self.height - 1.0f, self.width, 1.0f);
    divider.backgroundColor = [VPUI lightGrayColor];
    divider.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    [self addSubview:divider];
}

+ (BOOL)isAcceptable:(id)obj
{
    return [obj isKindOfClass:[VPPost class]];
}

+ (NSValue *)sizeOfCell:(id)obj
{
    VPPost *post = (VPPost *)obj;
    CGSize size = [post.featuredTitle boundingRectWithSize:CGSizeMake(SCREEN_WIDTH - 20.0f, MAXFLOAT)
                                                   options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                attributes:@{NSFontAttributeName:[VPUI fontOpenSans:14.0f thickness:1]}
                                                   context:nil].size;
    return [NSValue valueWithCGSize:CGSizeMake(SCREEN_WIDTH, ceilf(size.height) + 10.0f)];
}

- (void)setFeaturedPost:(VPPost *)featuredPost
{
    _featuredPost = featuredPost;
    UILabel *label = (UILabel *)[self viewWithTag:999];
    label.text = featuredPost.featuredTitle;
}

@end


@implementation _VPHeaderView

- (void)setupUI
{
    [super setupUI];
    self.userInteractionEnabled = NO;
    
    self.backgroundColor = [VPUI lightGrayColor];
    
    self.label = [[UILabel alloc] init];
    self.label.numberOfLines = 1;
    self.label.textAlignment = NSTextAlignmentLeft;
    self.label.font = [VPUI fontOpenSans:12.0 thickness:3];
    self.label.textColor = [VPUI mediumGrayColor];
    self.label.frame = CGRectMake(10.0f, 10.0f, self.width, 20.0f);
    [self addSubview:self.label];
}

+ (NSValue *)sizeOfView
{
    return [NSValue valueWithCGSize:CGSizeMake(SCREEN_WIDTH, _kVPHeaderViewHeight)];
}

@end
