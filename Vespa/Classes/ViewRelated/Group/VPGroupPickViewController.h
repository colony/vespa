//
//  VPGroupPickViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 7/5/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseTableViewController.h"

@protocol VPGroupPickViewControllerDelegate;

@interface VPGroupPickViewController : VPBaseTableViewController

@property (weak, nonatomic) id<VPGroupPickViewControllerDelegate> delegate;

/**
 * currentGroupId
 *     The ID of group that the user is currently under, it is nil if user create post from global place
 * mediaType
 *     Filter the group pick list that supports the specified media type
 * pickedGroupId
 *     The ID and name of the group has already been picked
 */
- (instancetype)initWithMediaType:(NSString *)mediaType
                   currentGroupId:(NSNumber *)currentGroupId
                    pickedGroupId:(NSNumber *)pickedGroupId;

@end


@protocol VPGroupPickViewControllerDelegate <NSObject>
@optional
- (void)groupPickViewControllerDidPicked:(VPGroupPickViewController *)viewController groupId:(NSNumber *)groupId groupName:(NSString *)groupName;
- (void)groupPickViewControllerDidCancelled:(VPGroupPickViewController *)viewController;

@end