//
//  VPGroupPickViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/5/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroupPickViewController.h"

#import "UINavigationItem+Util.h"
#import "VPBaseTableViewCell.h"
#import "VPGroup.h"
#import "VPGroups.h"


@interface VPGroupPickCell : VPBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkmarkImageView;

@end

@implementation VPGroupPickCell

- (void)setGroup:(VPGroup *)group current:(BOOL)isCurrent picked:(BOOL)isPicked
{
    self.groupNameLabel.text = isCurrent ? [group.name stringByAppendingString:@" (Current)"] : group.name;
    self.groupNameLabel.textColor = isPicked ? [VPUI colonyColor] : [VPUI mediumGrayColor];
    self.checkmarkImageView.hidden = !isPicked;
}

+ (NSNumber *)heightOfCell:(id)obj
{
    return @(44.0f);
}

@end


@interface VPGroupPickViewController ()

@property (strong, nonatomic) NSNumber *currentGroupId;
@property (strong, nonatomic) NSNumber *pickedGroupId;
@property (strong, nonatomic) NSString *pickedMediaType;

@end

@implementation VPGroupPickViewController

#pragma mark - lifecycle
- (instancetype)initWithMediaType:(NSString *)mediaType
                   currentGroupId:(NSNumber *)currentGroupId
                    pickedGroupId:(NSNumber *)pickedGroupId
{
    if (self = [super init]) {
        _pickedMediaType = mediaType;
        _currentGroupId = currentGroupId;
        _pickedGroupId = pickedGroupId;
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [super commonInit];
    self.enablePullToRefresh = NO;
    self.enableLoadMore = NO;
    self.enableTabbarHandle = NO;
    self.cellInfos = [NSMutableArray arrayWithObject:[[VPTableCellInfo alloc] initWithClass:[VPGroupPickCell class] nib:@"VPGroupPickCell" identifier:@"VPGroupPickCell"]];
}

- (void)setupTableView:(UITableView *)tableView
{
    [super setupTableView:tableView];
    tableView.backgroundColor = [UIColor whiteColor];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    [self.navigationItem setCustomTitle:@"Choose a Tribe" textColor:[UIColor blackColor] font:nil];
    [self.navigationItem setCustomLeftWithTitle:@"Cancel" textColor:[UIColor blackColor] font:nil target:self action:@selector(cancelButtonPressed:)];
    [self pullDownRefresh];
}


#pragma mark - override
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    NSArray *groupPickList = [VPGroups fetchGroupPickList];
    
    // We need reorder the pick list so that the current-already-picked one should be on top
    // Also, we need filter the groups that supports current media type
    NSMutableArray *groupPickMutableList = [NSMutableArray arrayWithCapacity:[groupPickList count]];
    for (VPGroup *g in groupPickList) {
        if (self.pickedGroupId && [g.groupId isEqualToNumber:self.pickedGroupId]) {
            [groupPickMutableList insertObject:g atIndex:0];
        } else if ([g.allowedPostTypes containsObject:self.pickedMediaType]) {
            [groupPickMutableList addObject:g];
        }
    }
    [self onObjectsDidLoaded:groupPickMutableList pageNo:0 error:nil block:nil];
}


#pragma mark - table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    VPGroup *group = [self getItemFromIndexPath:indexPath];
    [((VPGroupPickCell *)cell) setGroup:group
                                current:(self.currentGroupId && [group.groupId isEqualToNumber:self.currentGroupId])
                                 picked:(self.pickedGroupId && [group.groupId isEqualToNumber:self.pickedGroupId])];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VPGroup *group = [self getItemFromIndexPath:indexPath];
    self.pickedGroupId = group.groupId;
    [self.tableView reloadData];
    
    // reloadData is not sync, schedule the completion code in dispatch_async
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(groupPickViewControllerDidPicked:groupId:groupName:)]) {
            [self.delegate groupPickViewControllerDidPicked:self groupId:group.groupId groupName:group.name];
        }
    });
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


#pragma mark - action handling
- (void)cancelButtonPressed:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(groupPickViewControllerDidCancelled:)]) {
        [self.delegate groupPickViewControllerDidCancelled:self];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
