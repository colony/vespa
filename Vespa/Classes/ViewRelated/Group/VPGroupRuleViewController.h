//
//  VPGroupRuleViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 7/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

@interface VPGroupRuleViewController : VPBaseViewController

- (id)initWithGroupId:(NSNumber *)groupId textRule:(NSString *)text;

- (id)initWithGroupId:(NSNumber *)groupId imageRule:(NSString *)imageUrl;

@end
