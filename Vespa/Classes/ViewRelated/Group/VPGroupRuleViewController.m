//
//  VPGroupRuleViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroupRuleViewController.h"

#import "UIImageView+Addition.h"
#import "UINavigationItem+Util.h"


@interface VPGroupRuleViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textRuleTextView;
@property (weak, nonatomic) IBOutlet UIImageView *imageRuleImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *imageRuleScrollView;

@property (strong, nonatomic) NSString *ruleContent;
@property (assign, nonatomic) BOOL isTextRule;

@property (strong, nonatomic) NSNumber *groupId;

@end

@implementation VPGroupRuleViewController

- (id)initWithGroupId:(NSNumber *)groupId textRule:(NSString *)text
{
    if (self = [super init]) {
        _groupId = groupId;
        _isTextRule = YES;
        _ruleContent = text;
    }
    return self;
}

- (id)initWithGroupId:(NSNumber *)groupId imageRule:(NSString *)imageUrl
{
    if (self = [super init]) {
        _groupId = groupId;
        _isTextRule = NO;
        _ruleContent = imageUrl;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setCustomTitle:@"RULE"];
    
    if (self.isTextRule) {
        self.imageRuleScrollView.hidden = YES;
        self.textRuleTextView.text = self.ruleContent;
    } else {
        self.textRuleTextView.hidden = YES;
        [self.imageRuleImageView sd_setImageWithURL:[NSURL URLWithString:self.ruleContent]
                                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                              // NOTE, in IB, it is aspect fill, we compute the frame size proportionally
                                              CGSize imageSize = image.size;
                                              
                                              CGFloat w = SCREEN_WIDTH;
                                              CGFloat h = SCREEN_WIDTH/imageSize.width * imageSize.height;
                                              
                                              self.imageRuleImageView.frame = CGRectMake(0, 0, w, h);
                                              self.imageRuleScrollView.contentSize = self.imageRuleImageView.bounds.size;
                                          }];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [VPReports reportEvent:kVPEventViewGroupRule withParam:@{@"grpId":self.groupId} mode:kVPEventModeStart];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [VPReports reportEvent:kVPEventViewGroupRule withParam:@{@"grpId":self.groupId} mode:kVPEventModeEnd];
}

@end
