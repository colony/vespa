//
//  VPGroupViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


#import "VPPostCollectionViewController.h"

@class VPGroup;

@interface VPGroupViewController : VPPostCollectionViewController

- (instancetype)initWithGroup:(VPGroup *)group;

- (instancetype)initWithGroupId:(NSNumber *)groupId groupName:(NSString *)groupName;

@end
