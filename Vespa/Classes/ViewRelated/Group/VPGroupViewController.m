//
//  VPGroupViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGroupViewController.h"

#import "HMSegmentedControl.h"
#import "UIViewController+ScrollingNavbar.h"
#import "UINavigationItem+Util.h"
#import "VPAddPostMenuView.h"
#import "VPBaseCollectionReusableView.h"
#import "VPGroup.h"
#import "VPGroups.h"
#import "VPGroupRuleViewController.h"
#import "VPMediaCardViewController.h"
#import "VPNavigationController.h"
#import "VPPaginator.h"
#import "VPPost.h"
#import "VPPosts.h"
#import "VPUtil.h"


#define FilterNew 0
#define FilterBest 1

static CGFloat const _kVPGroupRuleCellHeight = 40.0f;


@interface _VPGroupRuleHeaderView : VPBaseCollectionReusableView

@property (weak, nonatomic) UIViewController *containerController;

- (void)setGroupName:(NSString *)groupName;

@end


@interface VPGroupViewController ()<VPAddPostMenuViewDelegate>

@property (strong, nonatomic) VPPaginator *paginator;
@property (strong, nonatomic) VPGroup *group;
@property (strong, nonatomic) NSNumber *groupId;
@property (strong, nonatomic) NSString *groupName;

@property (strong, nonatomic) UILabel *noDataIndicatorLabel;

// 0 - New, 1 - Best
@property (assign, nonatomic) NSInteger currentFilterType;
@property (strong, nonatomic) UISegmentedControl *filterControl;

@end

@implementation VPGroupViewController

#pragma mark - lifecycle
- (instancetype)initWithGroup:(VPGroup *)group
{
    if (self = [super init]) {
        _group = group;
        _groupId = _group.groupId;
        _groupName = _group.name;
    }
    return self;
}

- (instancetype)initWithGroupId:(NSNumber *)groupId groupName:(NSString *)groupName;
{
    if (self = [super init]) {
        _groupId = groupId;
        _groupName = groupName;
    }
    return self;
}

- (void)commonInit
{
    [super commonInit];
    self.showNavbarWhenScrollToTop = YES;
    self.enableScrollingNavbar = YES;
    self.enableTabbarHandle = NO;
    self.sectionHeaderClass = [_VPGroupRuleHeaderView class];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *addPostButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addPostButton.frame = CGRectMake(0, 0, 40, 40);
    addPostButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [addPostButton addTarget:self action:@selector(addPostButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [addPostButton setImage:[UIImage imageNamed:@"compose_post_button"] forState:UIControlStateNormal];
    [self.navigationItem setCustomRightWithView:addPostButton];
    [self.navigationItem tweakRightWithSpace:-8];

    HMSegmentedControl *segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"NEW", @"HOT"]];
    segmentedControl.frame = CGRectMake(0.0f, 0.0f, 130.0f, 44.0f);
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.backgroundColor = [UIColor clearColor];
    segmentedControl.titleTextAttributes = @{
                                             NSFontAttributeName: [VPUI fontOpenSans:16.0f thickness:3],
                                             NSForegroundColorAttributeName : [UIColor colorWithRed:255 green:255 blue:255 alpha:0.5]
                                             };
    segmentedControl.selectedTitleTextAttributes = @{
                                                     NSFontAttributeName: [VPUI fontOpenSans:16.0f thickness:3],
                                                     NSForegroundColorAttributeName : [UIColor whiteColor]
                                                     };
    segmentedControl.selectionIndicatorColor = [UIColor whiteColor];
    segmentedControl.selectedSegmentIndex = FilterNew;
    [segmentedControl addTarget:self action:@selector(filterSegmentControlChanged:) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = segmentedControl;
    
    [self loadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [VPReports reportEvent:kVPEventViewGroup withParam:@{@"grpId":self.groupId} mode:kVPEventModeStart];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [VPReports reportEvent:kVPEventViewGroup withParam:@{@"grpId":self.groupId} mode:kVPEventModeEnd];
}


#pragma mark - collection view delegate
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionFooter)
        return [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    
    UICollectionReusableView *view = [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    ((_VPGroupRuleHeaderView *)view).containerController = self;
    [((_VPGroupRuleHeaderView *)view) setGroupName:self.groupName];
    return view;
}


#pragma mark - override function
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    
    // Some group has special 'REVERSE' rule, in this case, we need prepare the query parameters well
    NSDictionary *fetchParam;
//    if (self.group.isReversed && pageNo == 0) {
//        fetchParam = @{
//                       @"_e"     : @"0",          // Inclusive
//                       @"_o"     : @"1",          // Ascending
//                       @"_a"     : [NSNull null], // After null, it means start of the list
//                       @"groupId": self.group.groupId
//                       };
//    } else {
    fetchParam = @{@"groupId": self.group.groupId};
    
    VPPostListFetchType fetchType = self.currentFilterType == FilterNew ? kVPPostListFetchTypeGroupRecent : kVPPostListFetchTypeGroupPopular;
    @weakify(self);
    self.ongoingRequestOperation = [VPPosts fetchPostsWithType:fetchType
                                                           url:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                         param:fetchParam
                                                       success:^(VPPostList *postList) {
                                                           @strongify(self);
                                                           self.paginator = postList.paginator;
                                                           [self onObjectsDidLoaded:postList.posts pageNo:pageNo error:nil];
                                                           [self attemptShowNoDataIndicator];
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                       } failure:^(NSError *error) {
                                                           @strongify(self);
                                                           VPLog(@"Fail fetchPostsByGroup: %@", [error localizedDescription]);
                                                           [self onObjectsDidLoaded:nil pageNo:pageNo error:error];
                                                           [self attemptShowNoDataIndicator];
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                       }];
}


#pragma mark - action handling
- (void)addPostButtonPressed:(id)sender
{
    [[VPContext sharedInstance] putDomain:kVPCDMediaSubmission key:@"groupId" val:self.group.groupId];
    [[VPContext sharedInstance] putDomain:kVPCDMediaSubmission key:@"groupName" val:self.group.name];
    if (self.group.threadType) {
        [[VPContext sharedInstance] putDomain:kVPCDMediaSubmission key:@"createThreadType" val:self.group.threadType];
    }
    
    // Special case:
    // If "CARD" is the ONLY one allowed, we will open the VPMediaCardViewController instead of VPAddPostMenuView
    // Also, the cardType stored in group's rule data
    if ([self.group.allowedPostTypes count] == 1 && [self.group.allowedPostTypes[0] isEqualToString:@"CARD"]) {
        VPMediaCardViewController *vc = [[VPMediaCardViewController alloc] initRequestCard];
        VPNavigationController *nc = [[VPNavigationController alloc] initWithRootViewController:vc];
        nc.customNavigatonGestureEnabled = NO;
        [nc configureDefaultStyle];
        nc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        nc.modalPresentationCapturesStatusBarAppearance = YES;
        [self presentViewController:nc animated:YES completion:nil];
        return;
    }
    
    VPAddPostMenuView *menuView = (VPAddPostMenuView *)[[[NSBundle mainBundle] loadNibNamed:@"VPAddPostMenuView" owner:nil options:nil] objectAtIndex:0];
    menuView.allowedTypes = self.group.allowedPostTypes;
    menuView.frame = [UIScreen mainScreen].bounds;
    menuView.opaque = YES;
    menuView.delegate = self;
    [[UIApplication sharedApplication].keyWindow addSubview:menuView];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:menuView];
}

- (void)filterSegmentControlChanged:(id)sender
{
    // http://stackoverflow.com/questions/18894772/uisegmentedcontrol-in-ios-7-divider-image-is-wrong-during-animation
    // The problem is, when segmentControlValueChanged, the animation of background image (segment) and divider
    // is out of sync for a tiny second, which leads to an noticeable flashing
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    for (UIView *segment in [segmentedControl subviews]) {
        [segment.layer removeAllAnimations];
        for (UIView *view in [segment subviews]) {
            [view.layer removeAllAnimations];
        }
    }
    
    [self setFilter:segmentedControl.selectedSegmentIndex];
}


#pragma mark - VPAddPostMenuViewDelegate
- (void)addPostSelected:(VPAddPostMenuView *)view mediaType:(NSString *)type
{
    view.hidden = YES;
    [view removeFromSuperview];
    [VPUtil presentMediaViewController:self mediaType:type];
}


#pragma mark - private method
- (void)loadData
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
    
    if (self.group) {
        [self setFilter:FilterNew];
    } else {
        @weakify(self);
        [VPGroups fetchGroupWithId:self.groupId
                             param:nil
                           success:^(VPGroup *group) {
                               @strongify(self);
                               self.group = group;
                               [self setFilter:FilterNew];
                           } failure:^(NSError *error) {
                               VPLog(@"Fetch Group: %@", [error localizedDescription]);
                               [[NSNotificationCenter defaultCenter] postNotificationName: kLoadingSpinnerHideNotification object:nil];
                           }];
    }
}

- (void)ruleHeaderTapped
{
    VPGroupRuleViewController *ruleVC = [[VPGroupRuleViewController alloc] initWithGroupId:self.groupId textRule:self.group.ruleText];
    if (self.group.ruleText) {
        ruleVC = [[VPGroupRuleViewController alloc] initWithGroupId:self.groupId textRule:self.group.ruleText];
    } else {
        ruleVC = [[VPGroupRuleViewController alloc] initWithGroupId:self.groupId imageRule:self.group.ruleImageUrl];
    }
    ruleVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:ruleVC animated:YES];
}

- (void)attemptShowNoDataIndicator
{
    if ([self.items count] > 0) {
        self.noDataIndicatorLabel.hidden = YES;
        [self.noDataIndicatorLabel removeFromSuperview];
        self.noDataIndicatorLabel = nil;
    } else {
        if (!self.noDataIndicatorLabel) {
            self.noDataIndicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.collectionView.width, 20.0f)];
            self.noDataIndicatorLabel.textAlignment = NSTextAlignmentCenter;
            self.noDataIndicatorLabel.textColor = [VPUI darkGrayColor];
            self.noDataIndicatorLabel.text = @"No posts yet.";
            self.noDataIndicatorLabel.center = self.collectionView.center;
            
            [self.collectionView addSubview:self.noDataIndicatorLabel];
            [self.collectionView bringSubviewToFront:self.noDataIndicatorLabel];
        }
        self.noDataIndicatorLabel.hidden = NO;
    }
}

- (void)setFilter:(NSInteger)type
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
    self.currentFilterType = type;
    [self pullDownRefresh];
}

@end


/**
 * The section header
 */
@implementation _VPGroupRuleHeaderView

- (void)setupUI
{
    self.backgroundColor = [UIColor whiteColor];
    self.userInteractionEnabled = YES;
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(5, 0, self.width, _kVPGroupRuleCellHeight);
    label.autoresizingMask = UIViewAutoresizingFill;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [VPUI darkGrayColor];
    label.font = [VPUI fontOpenSans:16.0 thickness:1];
    label.tag = 999;
    [self addSubview:label];
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_arrow"]];
    arrow.frame = CGRectMake(self.width - 25, 10, 20, 20);
    arrow.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self addSubview:arrow];
    
    UIView *divider = [[UIView alloc] initWithFrame:CGRectMake(0, self.height - 1, self.width, 1)];
    divider.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    divider.backgroundColor = UIColorFromRGB(0xE8E8E8);
    [self addSubview:divider];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    [tap addTarget:self action:@selector(tapped:)];
    [self addGestureRecognizer:tap];
}

- (void)setGroupName:(NSString *)groupName
{
    UILabel *label = (UILabel *)[self viewWithTag:999];
    label.text = groupName;
}

- (void)tapped:(id)sender
{
    [((VPGroupViewController *)self.containerController) ruleHeaderTapped];
}

+ (NSValue *)sizeOfView
{
    return [NSValue valueWithCGSize:CGSizeMake(SCREEN_WIDTH, _kVPGroupRuleCellHeight)];
}

@end
