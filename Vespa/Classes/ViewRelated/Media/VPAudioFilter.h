//
//  VPAudioFilter.h
//  Vespa
//
//  Created by Jiayu Zhang on 5/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VPAudioFilter : NSObject

+ (instancetype)sharedInstance;

/**
 * param:
 *   newRate, float. required
 *   newShiftCents, float, required
 */
- (void)filterAudioByTimeStrech:(NSString *)srcFilePath destFilePath:(NSString *)destFilePath param:(NSDictionary *)param;

/**
 * param:
 *   roomSize, float, required
 *   mix, float, required
 */
- (void)filterAudioByReverb:(NSString *)srcFilePath destFilePath:(NSString *)destFilePath param:(NSDictionary *)param;

- (double)duration:(NSString *)filePath;

@end
