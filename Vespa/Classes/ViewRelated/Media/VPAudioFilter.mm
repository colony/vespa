//
//  VPAudioFilter.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/31/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPAudioFilter.h"

#import "SuperpoweredDecoder.h"
#import "SuperpoweredRecorder.h"
#import "SuperpoweredReverb.h"
#import "SuperpoweredSimple.h"
#import "SuperpoweredTimeStretching.h"


@implementation VPAudioFilter

+ (instancetype)sharedInstance
{
    static VPAudioFilter *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
    }
    return self;
}

- (void)filterAudioByTimeStrech:(NSString *)srcFilePath destFilePath:(NSString *)destFilePath param:(NSDictionary *)param
{
    float newRate = [param[@"newRate"] floatValue];
    float newShiftCents = [param[@"newShiftCents"] floatValue];
    
    SuperpoweredDecoder *decoder = new SuperpoweredDecoder(false);
    decoder->open([srcFilePath fileSystemRepresentation]);
    
    SuperpoweredAudiobufferPool *bufferPool = new SuperpoweredAudiobufferPool(4, 1024 * 1024); // Allow 1 MB max. memory for the buffer pool.
    SuperpoweredTimeStretching *timeStretch = new SuperpoweredTimeStretching(bufferPool, decoder->samplerate);
    timeStretch->setRateAndPitchShiftCents(newRate, newShiftCents);
    
    SuperpoweredAudiopointerList *outputBuffers = new SuperpoweredAudiopointerList(bufferPool);

    FILE *fd = createWAV([destFilePath fileSystemRepresentation], decoder->samplerate, 2);
    
    // Create a buffer for the 16-bit integer samples.
    short int *intBuffer = (short int *)malloc(decoder->samplesPerFrame * 2 * sizeof(short int) + 16384);
    
    // Processing.
    while (true) {
        // Decode one frame. samplesDecoded will be overwritten with the actual decoded number of samples.
        unsigned int samplesDecoded = decoder->samplesPerFrame;
        if (decoder->decode(intBuffer, &samplesDecoded) != SUPERPOWEREDDECODER_OK) break;
        
        // Create an input buffer for the time stretcher.
        SuperpoweredAudiobufferlistElement inputBuffer;
        bufferPool->createSuperpoweredAudiobufferlistElement(&inputBuffer, decoder->samplePosition, samplesDecoded + 8);
        
        // Convert the decoded PCM samples from 16-bit integer to 32-bit floating point.
        SuperpoweredShortIntToFloat(intBuffer, bufferPool->floatAudio(&inputBuffer), samplesDecoded);
        inputBuffer.endSample = samplesDecoded; // <-- Important!
        
        // Time stretching.
        timeStretch->process(&inputBuffer, outputBuffers);
        
        // Do we have some output?
        if (outputBuffers->makeSlice(0, outputBuffers->sampleLength)) {
            
            while (true) { // Iterate on every output slice.
                float *timeStretchedAudio = NULL;
                int samples = 0;
                
                // Get pointer to the output samples.
                if (!outputBuffers->nextSliceItem(&timeStretchedAudio, &samples)) break;
                
                // Convert the time stretched PCM samples from 32-bit floating point to 16-bit integer.
                SuperpoweredFloatToShortInt(timeStretchedAudio, intBuffer, samples);
                
                // Write the audio to disk.
                fwrite(intBuffer, 1, samples * 4, fd);
            };
            
            // Clear the output buffer list.
            outputBuffers->clear();
        };
    };
    
    // Cleanup.
    closeWAV(fd);
    delete decoder;
    delete timeStretch;
    delete bufferPool;
    free(intBuffer);
}

- (void)filterAudioByReverb:(NSString *)srcFilePath destFilePath:(NSString *)destFilePath param:(NSDictionary *)param
{
    float roomSize = [param[@"roomSize"] floatValue];
    float mix = [param[@"mix"] floatValue];
    
    SuperpoweredDecoder *decoder = new SuperpoweredDecoder(false);
    decoder->open([srcFilePath fileSystemRepresentation]);

    FILE *fd = createWAV([destFilePath fileSystemRepresentation], decoder->samplerate, 2);
    
    SuperpoweredReverb *reverb = new SuperpoweredReverb(decoder->samplerate);
    reverb->setRoomSize(roomSize);
    reverb->setMix(mix);
    reverb->enable(true);
    
    // Create a buffer for the 16-bit integer samples.
    short int *intBuffer = (short int *)malloc(decoder->samplesPerFrame * 2 * sizeof(short int) + 16384);
    float *stereoBufferr = (float *)malloc(decoder->samplesPerFrame * 2 * sizeof(float));
    
    while (true) {
        unsigned int samplesDecoded = decoder->samplesPerFrame;
        if(decoder->decode(intBuffer, &samplesDecoded) != SUPERPOWEREDDECODER_OK) break;
        
        SuperpoweredShortIntToFloat(intBuffer, stereoBufferr, samplesDecoded);
        reverb->process(stereoBufferr, stereoBufferr, samplesDecoded);
        
        // Convert the time stretched PCM samples from 32-bit floating point to 16-bit integer.
        SuperpoweredFloatToShortInt(stereoBufferr, intBuffer, samplesDecoded);
        
        // Write the audio to disk.
        fwrite(intBuffer, 1, samplesDecoded * 4, fd);
    }

    // Cleanup
    closeWAV(fd);
    free(intBuffer);
    free(stereoBufferr);
    delete decoder;
}

- (double)duration:(NSString *)filePath
{
    SuperpoweredDecoder *decoder = new SuperpoweredDecoder(false);
    decoder->open([filePath fileSystemRepresentation]);
    double duration = decoder->durationSeconds;
    delete decoder;
    return duration;
}

@end
