//
//  VPAudioManager.h
//  Vespa
//
//  Created by Jiayu Zhang on 5/16/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


typedef NS_ENUM(NSInteger, VPAudioFilterType)
{
    kVPAudioFilterMonster = 1,
    kVPAudioFilterOrigin = 2,
    kVPAudioFilterChipmunk = 3
};


@protocol VPAudioManagerDelegate;

@interface VPAudioManager : NSObject

+ (instancetype)sharedInstance;

// filter
- (void)filter:(VPAudioFilterType)type src:(NSString *)srcPath dst:(NSString *)dstPath;

// convert
- (void)convert:(NSString *)srcPath dst:(NSString *)dstPath delegate:(id<VPAudioManagerDelegate>)delegate;
- (BOOL)isConverterAvailable;

// play
- (void)playFile:(NSString *)file delegate:(id<VPAudioManagerDelegate>)delegate;
- (void)playURL:(NSURL *)url delegate:(id<VPAudioManagerDelegate>)delegate;
- (void)stop;

// record


// misc
- (NSTimeInterval) durationOfAudio:(NSString *)file; // inseconds

@end


@protocol VPAudioManagerDelegate <NSObject>
@optional
/** The URL has been played to the end */
- (void)VPAudioManagerEnd:(VPAudioManager *)manager;

/** The URL has been started */
- (void)VPAudioManagerStart:(VPAudioManager *)manager;

/** The URL has been paused/stopped in the middle */
- (void)VPAudioManagerStop:(VPAudioManager *)manager;

/** The URL has made progress */
//- (void)VPAudioManagerProgress:(VPAudioManager *)manager current:(CGFloat)current duration:(CGFloat)duration;

/** Audio conversion complete or fail */
- (void)VPAudioManagerConvert:(VPAudioManager *)manager fail:(NSError *)error;

@end