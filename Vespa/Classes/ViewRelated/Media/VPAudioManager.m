//
//  VPAudioManager.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/16/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPAudioManager.h"

#import <AVFoundation/AVFoundation.h>
#import "HysteriaPlayer.h"
#import "TPAACAudioConverter.h"
#import "VPAudioFilter.h"


@interface VPAudioManager ()<HysteriaPlayerDelegate, HysteriaPlayerDataSource, TPAACAudioConverterDelegate>

@property (weak, nonatomic) id<VPAudioManagerDelegate> delegate;

@property (strong, nonatomic) NSURL *currentURL;
@property (assign, nonatomic) BOOL isStopping;


@property (strong, nonatomic) HysteriaPlayer *player;
@property (strong, nonatomic) TPAACAudioConverter *converter;

// YES if the manager starts or in between converting process, NO otherwise
@property (assign, nonatomic) BOOL isConverting;

@end


@implementation VPAudioManager

+ (instancetype)sharedInstance
{
    static VPAudioManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        // Initialize player
        self.player = [HysteriaPlayer sharedInstance];
        self.player.datasource = self;
        self.player.delegate = self;
        [self.player enableMemoryCached:NO];
        [self.player setPlayerRepeatMode:HysteriaPlayerRepeatModeOff];
        [self.player setPlayerShuffleMode:HysteriaPlayerShuffleModeOff];
        self.player.disableLogs = YES;
#ifdef DEBUG
        self.player.disableLogs = NO;
#endif
        
        // Initialize AV session
        NSError *error = nil;
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&error];
        if (error) {
            VPLog(@"AVAudioSession fail setCategory: %@", [error localizedDescription]);
        }
        [session setActive:YES error:&error];
        if (error) {
            VPLog(@"AVAudioSession fail setActive: %@", [error localizedDescription]);
        }
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(audioSessionInterrupted:)
                                                     name:AVAudioSessionInterruptionNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - public interface
- (void)filter:(VPAudioFilterType)type src:(NSString *)srcPath dst:(NSString *)dstPath
{
    if (type == kVPAudioFilterOrigin)
        return; // Do nothing
    
    if (type == kVPAudioFilterChipmunk) {
        [[VPAudioFilter sharedInstance] filterAudioByTimeStrech:srcPath destFilePath:dstPath param:@{@"newRate":@(1.0), @"newShiftCents":@(700.0)}];
    } else if (type == kVPAudioFilterMonster) {
        [[VPAudioFilter sharedInstance] filterAudioByTimeStrech:srcPath destFilePath:dstPath param:@{@"newRate":@(1.0), @"newShiftCents":@(-400.0)}];
    }
}

- (void)convert:(NSString *)srcPath dst:(NSString *)dstPath delegate:(id<VPAudioManagerDelegate>)delegate
{
    if (self.isConverting)
        return;
    
    if ([self isConverterAvailable]) {
        self.delegate = delegate;
        self.isConverting = YES;
        self.converter = [[TPAACAudioConverter alloc] initWithDelegate:self source:srcPath destination:dstPath];
        [self.converter start];
    }
}

- (BOOL)isConverterAvailable
{
    return [TPAACAudioConverter AACConverterAvailable];
}

- (void)playFile:(NSString *)file delegate:(id<VPAudioManagerDelegate>)delegate
{
    [self stop];
    NSURL *localUrl = [[NSURL alloc] initFileURLWithPath:file];
    self.currentURL = localUrl;
    self.delegate = delegate;
    [self play];
}

- (void)playURL:(NSURL *)url delegate:(id<VPAudioManagerDelegate>)delegate
{
    [self stop];
    self.currentURL = url;
    self.delegate = delegate;
    [self play];
}

- (void)play
{
    [self.player pausePlayerForcibly:NO];
    [self.player fetchAndPlayPlayerItem:0];
}

- (void)stop
{
    self.isStopping = YES;
    
    if ([self.player isPlaying])
    {
        [self.player pausePlayerForcibly:YES];
        [self.player pause];
        // We have to forcibly stop previous delegate
        if ([self.delegate respondsToSelector:@selector(VPAudioManagerStop:)]) {
            [self.delegate VPAudioManagerStop:self];
        }
    }

    [self.player removeAllItems];
    self.delegate = nil;
    
    self.isStopping = NO;
}

//- (void)updateProgress
//{
//    if (self.delegate && !self.isStopping) {
//        HysteriaPlayer *player = [HysteriaPlayer sharedInstance];
//        CGFloat current = [player getPlayingItemCurrentTime];
//        CGFloat duration = [player getPlayingItemDurationTime];
//        if ([self.delegate respondsToSelector:@selector(VPAudioManagerProgress:current:duration:)]) {
//            [self.delegate VPAudioManagerProgress:self current:current duration:duration];
//        }
//    }
//}

- (NSTimeInterval) durationOfAudio:(NSString *)file
{
    return [[VPAudioFilter sharedInstance] duration:file];
}


#pragma mark - HysteriaPlayerDataSource
- (NSUInteger)hysteriaPlayerNumberOfItems
{
    return 1;
}

- (NSURL *)hysteriaPlayerURLForItemAtIndex:(NSUInteger)index preBuffer:(BOOL)preBuffer
{
    return self.currentURL;
}


#pragma mark - HysteriaPlayerDelegate
- (void)hysteriaPlayerReadyToPlay:(HysteriaPlayerReadyToPlay)identifier
{
    if (identifier == HysteriaPlayerReadyToPlayPlayer) {
        // It will be called when Player is ready to play at the first time
//        if (!self.playerObserver) {
//            @weakify(self);
//            self.playerObserver = [[HysteriaPlayer sharedInstance] addPeriodicTimeObserverForInterval:CMTimeMake(30, 1000)  //30fps
//                                                                                                queue:NULL // main queue
//                                                                                           usingBlock:^(CMTime time) {
//                                                                                               @strongify(self);
//                                                                                               [self updateProgress];
//                                                                                           }];
//        }
    }
    else if (identifier == HysteriaPlayerReadyToPlayCurrentItem) {
        if ([self.delegate respondsToSelector:@selector(VPAudioManagerStart:)]) {
            [self.delegate VPAudioManagerStart:self];
        }
    }
}

- (void)hysteriaPlayerDidFailed:(HysteriaPlayerFailed)identifier error:(NSError *)error
{
    VPLog(@"HysteriaPlayer fail:(%lu) %@", (long)identifier, [error localizedDescription]);
}

- (void)hysteriaPlayerDidReachEnd
{
    if ([self.delegate respondsToSelector:@selector(VPAudioManagerEnd:)]) {
        [self.delegate VPAudioManagerEnd:self];
    }
    self.delegate = nil;
}


#pragma mark - TPAACAudioConverter
-(void)AACAudioConverterDidFinishConversion:(TPAACAudioConverter *)converter
{
    self.converter = nil;
    if ([self.delegate respondsToSelector:@selector(VPAudioManagerConvert:fail:)]) {
        [self.delegate VPAudioManagerConvert:self fail:nil];
    }
    self.delegate = nil;
    self.isConverting = NO;
}

- (void)AACAudioConverter:(TPAACAudioConverter *)converter didFailWithError:(NSError *)error
{
    self.converter = nil;
    if ([self.delegate respondsToSelector:@selector(VPAudioManagerConvert:fail:)]) {
        [self.delegate VPAudioManagerConvert:self fail:error];
    }
    self.delegate = nil;
    self.isConverting = NO;
}


#pragma mark - audio session interruption
- (void)audioSessionInterrupted:(NSNotification *)notification
{
    AVAudioSessionInterruptionType type = (AVAudioSessionInterruptionType)[notification.userInfo[AVAudioSessionInterruptionTypeKey] integerValue];
    if (type == AVAudioSessionInterruptionTypeEnded) {
        if (self.isConverting && self.converter)
            [self.converter resume];
    } else if ( type == AVAudioSessionInterruptionTypeBegan ) {
        if (self.isConverting && self.converter)
            [self.converter interrupt];
    }
}

@end
