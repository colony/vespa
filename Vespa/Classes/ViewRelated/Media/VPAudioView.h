//
//  VPAudioView.h
//  Vespa
//
//  Created by Jiayu Zhang on 10/1/15.
//  Copyright © 2015 Colony. All rights reserved.
//


#import "VPBaseView.h"

@interface VPAudioView : VPBaseView

- (void)play;
- (void)stop;
- (void)resetUI;

@property (strong, nonatomic) NSString *audioUrl;
@property (assign, nonatomic) NSInteger audioDuration; // ms

@end
