//
//  VPAudioView.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/1/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPAudioView.h"

#import "VPAudioManager.h"

@interface VPAudioView ()<VPAudioManagerDelegate>

@property (strong, nonatomic) UIButton *playButton;
//@property (strong, nonatomic) DACircularProgressView *progressView;

@end


@implementation VPAudioView

- (void)setupUI
{
    self.backgroundColor = UIColorFromRGB(0xE8E8E8);
//    if (!self.progressView) {
//        self.progressView = [[DACircularProgressView alloc] initWithFrame:CGRectZero];
//        self.progressView.backgroundColor = [UIColor whiteColor];
//        self.progressView.opaque = YES;
//        self.progressView.roundedCorners = NO;
//        self.progressView.clockwiseProgress = YES;
//        self.progressView.thicknessRatio = 0.2f;
//        self.progressView.autoresizingMask = UIViewAutoresizingCentered;
//        [self addSubview:self.progressView];
//    }
    
    if (!self.playButton) {
        self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.playButton setImage:[UIImage imageNamed:@"audio_play"] forState:UIControlStateNormal];
        [self.playButton setImage:[UIImage imageNamed:@"audio_stop"] forState:UIControlStateSelected];
        [self.playButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        self.playButton.autoresizingMask = UIViewAutoresizingCentered;
        [self addSubview:self.playButton];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat side = MIN(100.0f, MIN(self.width, self.height));
    CGFloat innerSide = side * (1-0.2f);
//    if (self.progressView) {
//        self.progressView.width = side;
//        self.progressView.height = side;
//        self.progressView.center = CGPointMake(self.width/2.0f, self.height/2.0f);
//    }
    if (self.playButton) {
        self.playButton.width = innerSide;
        self.playButton.height = innerSide;
        self.playButton.center = CGPointMake(self.width/2.0f, self.height/2.0f);
    }
}

- (void)play
{
    [[VPAudioManager sharedInstance] playURL:[NSURL URLWithString:self.audioUrl] delegate:self];
    [VPReports reportEvent:kVPEventPlayAudioPost withParam:nil mode:kVPEventModeOnce];
}

- (void)stop
{
    [[VPAudioManager sharedInstance] stop];
}

- (void)resetUI
{
    [self.playButton setSelected:NO];
//    [self.progressView stopProgress];
}


#pragma mark - action handling
- (void)playButtonPressed:(UIButton *)btn
{
    [btn setSelected:!btn.selected];
    
    if (btn.selected) {
        [self play];
    } else {
        [self stop];
    }
}


#pragma mark - VPAudioManager delegate
- (void)VPAudioManagerStop:(VPAudioManager *)manager
{
    [self.playButton setSelected:NO];
//    [self.progressView stopProgress];
}

- (void)VPAudioManagerStart:(VPAudioManager *)manager
{
//    [self.progressView setProgress:1.0f animated:YES initialDelay:0 withDuration:(self.audioDuration * 1.0f)/1000];
}

//- (void)VPAudioManagerProgress:(VPAudioManager *)manager current:(CGFloat)current duration:(CGFloat)duration
//{
//    CGFloat progress = current / duration;
//    VPLog(@"progress = %f", progress);
//
//    if (progress >= 1.0f || progress <= FLT_EPSILON) {
//        [self.progressView setProgress:0.0f animated:NO];
//    } else {
//        [self.progressView setProgress:progress animated:YES];
//    }
//}

- (void)VPAudioManagerEnd:(VPAudioManager *)manager
{
    [self.playButton setSelected:NO];
//    [self.progressView stopProgress];
}

@end
