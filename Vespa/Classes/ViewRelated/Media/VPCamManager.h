//
//  VPCamManager.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/3/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef NS_ENUM(NSInteger, VPCamState) {
    kVPCamStateInitial = 0,
    kVPCamStateFail,  // setup error
    kVPCamStateSetup,
    kVPCamStateRun,
    kVPCamStateStop,
    kVPCamStateRuntimeError // session runtime error
};

@protocol VPCamManagerDelegate <NSObject>
@optional

- (void)subjectAreaDidChange;

/** A cam is considered as launch when in Fail,Run,Stop,RuntimeError states */
- (void)camDidLaunch:(VPCamState)state;

@end


@interface VPCamManager : NSObject

@property (weak, nonatomic) id<VPCamManagerDelegate> delegate;

/* True if current front-facing camera, also, the first time the manager initialize with back-facing camera */
@property (assign, nonatomic) BOOL isFront;

+ (instancetype)sharedInstance;

/** 
TODO: comment!!!
 */
- (void)launchImmediateOrDelegate:(id<VPCamManagerDelegate>)delegate;

- (void)startRunning;
- (void)stopRunning;
- (void)captureStillImage:(void (^)(UIImage *image))success failure:(void (^)(NSError *error))failure;
- (void)cameraToggle;
- (AVCaptureDevicePosition)position;
- (void)focus:(AVCaptureFocusMode)focusMode atDevicePoint:(CGPoint)point;

@property (assign, readonly, nonatomic) VPCamState state;

/**
 Camera device configuration
 */
@property (assign, nonatomic) AVCaptureExposureMode exposureMode;
@property (assign, nonatomic) AVCaptureFlashMode flashMode;
@property (assign, nonatomic) AVCaptureFocusMode focusMode;
@property (assign, nonatomic) AVCaptureTorchMode torchMode;
@property (assign, nonatomic) AVCaptureWhiteBalanceMode whiteBalanceMode;

/**
 AVCapture props (no audio yet)
 */
@property (strong, readonly, nonatomic) AVCaptureSession *session;
@property (strong, readonly, nonatomic) AVCaptureDeviceInput *videoInput;

/**
 Convenient checkers
 */
- (BOOL)hasExposure;
- (BOOL)hasFlash;
- (BOOL)hasFocus;
- (BOOL)hasTorch;
- (BOOL)hasWhiteBalance;

@end
