//
//  VPCamManager.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/3/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCamManager.h"

@interface VPCamManager ()

@property (strong, nonatomic) dispatch_queue_t queue;
@property (strong, nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (strong, nonatomic) NSObject *syncObj;

@property (assign, nonatomic) NSInteger numOfCamToggleTaskRequested;
@property (assign, nonatomic) NSInteger numOfCamToggleTaskInQueue;

@end

@implementation VPCamManager

#pragma mark - lifecycle
+ (instancetype)sharedInstance
{
    static VPCamManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        _state = kVPCamStateInitial;
        _syncObj = [NSObject new];
        _queue = dispatch_queue_create("VPCam Queue", DISPATCH_QUEUE_SERIAL);
        _numOfCamToggleTaskRequested = 0;
        _numOfCamToggleTaskInQueue = 0;
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self stopRunning];
    _session = nil;
    _videoInput = nil;
    _stillImageOutput = nil;
    _queue = nil;
}

- (void)setup
{
    dispatch_async(self.queue, ^{
        @synchronized(self.syncObj) {
            if (self.state >= kVPCamStateSetup) {
                return; // Bail out
            }
        }
        
        // 1. Setup session and input and output
        NSError *error = nil;
        _videoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self backFacingCamera] error:&error];
        _isFront = NO; // initialize with back cam
        
        self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        self.stillImageOutput.outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:AVVideoCodecJPEG, AVVideoCodecKey, nil];
        
        _session = [[AVCaptureSession alloc] init];
        if ([self.session canAddInput:self.videoInput]) {
            [self.session addInput:self.videoInput];
        }
        if ([self.session canAddOutput:self.stillImageOutput]) {
            [self.session addOutput:self.stillImageOutput];
        }
        [self.session setSessionPreset:AVCaptureSessionPresetHigh];
        
        if (error) {
            VPLog(@"Setup session failed with %@", [error userInfo]);
            [self setState:kVPCamStateFail];
            return;
        }
        
        // 2. Configure device
        AVCaptureDevice *device = self.videoInput.device;
        if ([device lockForConfiguration:&error]) {
            if ([device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
                device.focusMode = AVCaptureFocusModeContinuousAutoFocus;
            }
            
            if ([device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
                device.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
            }
            
            if ([device isFlashModeSupported:AVCaptureFlashModeOff]) {
                device.flashMode = AVCaptureFlashModeOff; // By default, we TURN OFF FLASH!!!
            }
            
            // This enable us to track focus changed
            device.subjectAreaChangeMonitoringEnabled = YES;
            [device unlockForConfiguration];
            
            // Register interested notification
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureSessionDidStopRunning:) name:AVCaptureSessionDidStopRunningNotification object:self.session];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureSessionDidStartRunning:) name:AVCaptureSessionDidStartRunningNotification object:self.session];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureSessionRuntimeError:) name:AVCaptureSessionRuntimeErrorNotification object:self.session];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[self.videoInput device]];
        } else {
            VPLog(@"LockForConfiguration failed with %@", [error userInfo]);
            // We still allow camera session proceed
        }
        
        // 3. Portrait
        AVCaptureConnection *videoConnection = [VPCamManager connectionWithMediaType:AVMediaTypeVideo fromConnections:self.stillImageOutput.connections];
        if ([videoConnection isVideoOrientationSupported]) {
            [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
        }
        
        [self setState:kVPCamStateSetup];
    });
}


#pragma mark - public methods
- (void)launchImmediateOrDelegate:(id<VPCamManagerDelegate>)delegate
{
    self.delegate = delegate;
    @synchronized(self.syncObj) {
        if (self.state == kVPCamStateInitial || self.state == kVPCamStateSetup) {
            return; // Bail out
        }
    }
    
    VPLog(@"Launch immediately");
    // If we reach here, the cam did launched with a state
    if ([self.delegate respondsToSelector:@selector(camDidLaunch:)]) {
        [self.delegate camDidLaunch:self.state];
    }
}

- (void)startRunning
{
    // Setup first if previous setup failed
    @synchronized(self.syncObj) {
        if (self.state == kVPCamStateInitial || self.state == kVPCamStateFail) {
            [self setup]; // Returns immediately
        }
    }
    
    dispatch_async(self.queue, ^{
        @synchronized(self.syncObj) {
            if (self.state == kVPCamStateInitial || self.state == kVPCamStateFail || self.state == kVPCamStateRun) {
                return; // Bail out
            }
        }
        if (!self.session.isRunning)
            [self.session startRunning];
    });
}

- (void)stopRunning
{
    dispatch_async(self.queue, ^{
        @synchronized(self.syncObj) {
            if (self.state == kVPCamStateInitial || self.state == kVPCamStateFail) {
                return; // Bail out
            }
        }
        [self.session stopRunning];
    });
}

- (void)captureStillImage:(void (^)(UIImage *image))success failure:(void (^)(NSError *error))failure
{
    dispatch_async(self.queue, ^{
        AVCaptureConnection *videoConnection = [VPCamManager connectionWithMediaType:AVMediaTypeVideo fromConnections:self.stillImageOutput.connections];
        if ([videoConnection isVideoOrientationSupported]) {
            [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
        }

        [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                           completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error)
         {
             if (imageDataSampleBuffer) {
                 NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                 UIImage *image = [[UIImage alloc] initWithData:imageData scale:1];
                 SAFE_BLOCK_RUN(success, image);
             } else if (error) {
                 SAFE_BLOCK_RUN(failure, error);
             }
         }];
    });
}

- (void)cameraToggle
{
    self.numOfCamToggleTaskRequested++;
    self.numOfCamToggleTaskInQueue++;
    dispatch_async(self.queue, ^{
        // The algorithm here makes sure we toggle the least odd or even times but still get correct times of toggle
        // The bug could be extremely rare, we do the if condition here while the two increment above
        // (taskRequest++, taskInQueue++) only finish taskRequest++
        if (self.numOfCamToggleTaskInQueue <= 2 &&
            self.numOfCamToggleTaskRequested % self.numOfCamToggleTaskInQueue == 0) {
            NSError *error;
            if ([self hasMultipleCameras]) {
                AVCaptureDeviceInput *newVideoInput = nil;
                AVCaptureDevicePosition position = self.videoInput.device.position;
                BOOL toFront = NO;
                if (position == AVCaptureDevicePositionBack) {
                    newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self frontFacingCamera] error:&error];
                    toFront = YES;
                } else if (position == AVCaptureDevicePositionFront) {
                    newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self backFacingCamera] error:&error];
                    toFront = NO;
                } else {
                    newVideoInput = nil;
                }
                
                if (newVideoInput != nil) {
                    [self.session beginConfiguration];
                    [self.session removeInput:self.videoInput];
                    if ([self.session canAddInput:newVideoInput]) {
                        [self.session addInput:newVideoInput];
                        _videoInput = newVideoInput;
                    } else {
                        [self.session addInput:self.videoInput];
                    }
                    [self.session commitConfiguration];
                    
                    self.isFront = toFront; // Toggle the flag after commit
                }
            }
            
            if (error) {
                VPLog(@"cameraToggle failed %@", [error localizedDescription]);
            }
            
            self.numOfCamToggleTaskInQueue--;
            self.numOfCamToggleTaskRequested--;
        }
        else {
            self.numOfCamToggleTaskInQueue--;
        }
    });
}

- (AVCaptureDevicePosition)position
{
    return self.videoInput.device.position;
}

- (void)focus:(AVCaptureFocusMode)focusMode atDevicePoint:(CGPoint)point
{
    dispatch_async(self.queue, ^{
        AVCaptureDevice *device = self.videoInput.device;
        NSError *error = nil;
        if ([device lockForConfiguration:&error]) {
            if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode]) {
                device.focusMode = focusMode;
                device.focusPointOfInterest = point;
            }
            [device unlockForConfiguration];
        } else {
            VPLog(@"Error when lockForConfiguration %@", [error userInfo]);
        }
    });
}


#pragma mark - notification
- (void)captureSessionRuntimeError:(NSNotification *)notification
{
    //TODO: retry?
    NSError *error = [notification userInfo][AVCaptureSessionErrorKey];
    VPLog(@"VPCamManager captureSessionRuntimeError: %@", [error userInfo]);
    
    [self setState:kVPCamStateRuntimeError];

    if ([self.delegate respondsToSelector:@selector(camDidLaunch:)]) {
        [self.delegate camDidLaunch:kVPCamStateRuntimeError];
    }
    
    dispatch_async(self.queue, ^{
        @synchronized(self.syncObj) {
            if (self.state == kVPCamStateInitial ||
                self.state == kVPCamStateFail ||
                self.state == kVPCamStateRun) {
                return; // Bail out
            }
        }
        if (!self.session.isRunning)
            [self.session startRunning];
    });
}

- (void)captureSessionDidStartRunning:(NSNotification *)notification
{
    VPLog(@"VPCamManager captureSessionDidStartRunning");
    [self setState:kVPCamStateRun];
    if ([self.delegate respondsToSelector:@selector(camDidLaunch:)]) {
        [self.delegate camDidLaunch:kVPCamStateRun];
    }
}

- (void)captureSessionDidStopRunning:(NSNotification *)notification
{
    VPLog(@"VPCamManager captureSessionDidStopRunning");
    [self setState:kVPCamStateStop];
    if ([self.delegate respondsToSelector:@selector(camDidLaunch:)]) {
        [self.delegate camDidLaunch:kVPCamStateStop];
    }
}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
    if ([self.delegate respondsToSelector:@selector(subjectAreaDidChange)]) {
        [self.delegate subjectAreaDidChange];
    }
}


#pragma mark - getter & setter for device configuration
- (BOOL)hasFlash {
    return self.videoInput.device.hasFlash;
}

- (AVCaptureFlashMode)flashMode {
    return self.videoInput.device.flashMode;
}

- (void)setFlashMode:(AVCaptureFlashMode)flashMode {
    AVCaptureDevice *device = self.videoInput.device;
    if ([device isFlashModeSupported:flashMode] && device.flashMode != flashMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.flashMode = flashMode;
            [device unlockForConfiguration];
        } else {
            VPLog(@"LockForConfiguration failed with %@", [error userInfo]);
        }
    }
}

- (BOOL)hasTorch {
    return self.videoInput.device.hasTorch;
}

- (AVCaptureTorchMode)torchMode {
    return self.videoInput.device.torchMode;
}

- (void)setTorchMode:(AVCaptureTorchMode)torchMode {
    AVCaptureDevice *device = self.videoInput.device;
    if ([device isTorchModeSupported:torchMode] && device.torchMode != torchMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.torchMode = torchMode;
            [device unlockForConfiguration];
        } else {
            VPLog(@"LockForConfiguration failed with %@", [error userInfo]);
        }
    }
}

- (BOOL)hasFocus {
    AVCaptureDevice *device = self.videoInput.device;
    
    return  [device isFocusModeSupported:AVCaptureFocusModeLocked] ||
    [device isFocusModeSupported:AVCaptureFocusModeAutoFocus] ||
    [device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus];
}

- (AVCaptureFocusMode)focusMode {
    return self.videoInput.device.focusMode;
}

- (void)setFocusMode:(AVCaptureFocusMode)focusMode {
    AVCaptureDevice *device = self.videoInput.device;
    if ([device isFocusModeSupported:focusMode] && device.focusMode != focusMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.focusMode = focusMode;
            [device unlockForConfiguration];
        } else {
            VPLog(@"LockForConfiguration failed with %@", [error userInfo]);
        }
    }
}

- (BOOL)hasExposure {
    AVCaptureDevice *device = self.videoInput.device;
    
    return  [device isExposureModeSupported:AVCaptureExposureModeLocked] ||
    [device isExposureModeSupported:AVCaptureExposureModeAutoExpose] ||
    [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure];
}

- (AVCaptureExposureMode)exposureMode {
    return self.videoInput.device.exposureMode;
}

- (void)setExposureMode:(AVCaptureExposureMode)exposureMode {
    if (exposureMode == AVCaptureExposureModeAutoExpose) {
        exposureMode = AVCaptureExposureModeContinuousAutoExposure;
    }
    AVCaptureDevice *device = self.videoInput.device;
    if ([device isExposureModeSupported:exposureMode] && device.exposureMode != exposureMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.exposureMode = exposureMode;
            [device unlockForConfiguration];
        } else {
            VPLog(@"LockForConfiguration failed with %@", [error userInfo]);
        }
    }
}

- (BOOL)hasWhiteBalance {
    AVCaptureDevice *device = self.videoInput.device;
    
    return  [device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeLocked] ||
    [device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeAutoWhiteBalance];
}

- (AVCaptureWhiteBalanceMode)whiteBalanceMode {
    return self.videoInput.device.whiteBalanceMode;
}

- (void)setWhiteBalanceMode:(AVCaptureWhiteBalanceMode)whiteBalanceMode {
    if (whiteBalanceMode == AVCaptureWhiteBalanceModeAutoWhiteBalance) {
        whiteBalanceMode = AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance;
    }
    AVCaptureDevice *device = self.videoInput.device;
    if ([device isWhiteBalanceModeSupported:whiteBalanceMode] && device.whiteBalanceMode != whiteBalanceMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.whiteBalanceMode = whiteBalanceMode;
            [device unlockForConfiguration];
        } else {
            VPLog(@"LockForConfiguration failed with %@", [error userInfo]);
        }
    }
}


#pragma mark - private methods
+ (AVCaptureConnection *)connectionWithMediaType:(NSString *)mediaType fromConnections:(NSArray *)connections
{
    for (AVCaptureConnection *connection in connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([port.mediaType isEqual:mediaType] ) {
                return connection;
            }
        }
    }
    return nil;
}

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}

- (AVCaptureDevice *)frontFacingCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionFront];
}

- (AVCaptureDevice *)backFacingCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionBack];
}

- (void)setState:(VPCamState)state
{
    @synchronized(self.syncObj) {
        _state = state;
    }
}

- (BOOL)hasMultipleCameras
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count] > 1 ? YES : NO;
}

@end
