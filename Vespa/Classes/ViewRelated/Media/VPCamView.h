//
//  VPCamView.h
//  Vespa
//
//  Created by Jiayu Zhang on 7/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseView.h"
#import "PBJVision.h"
#import "PBJVisionUtilities.h"


@interface VPCamView : VPBaseView

- (id)initWithPhotoFrame:(CGRect)frame vision:(PBJVision *)vision;
- (id)initWithVideoFrame:(CGRect)frame vision:(PBJVision *)vision;

/**
 * VPCamView is highly coupled with PBJVision, all PBJVisionDelegate's message will 
 * be proxied thru VPCamView to this delegate property. The reason for this is that
 * VPCamView might have specific logic for certain delegate message (i.e. focus)
 */
@property (weak, nonatomic) id<PBJVisionDelegate> delegate;

@property (assign, nonatomic) BOOL tapToFocusEnabled;

@end
