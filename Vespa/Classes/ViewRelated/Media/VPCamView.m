//
//  VPCamView.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/30/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCamView.h"

#import "VPConfigManager.h"
#import "VPFileManager.h"


static CGFloat const _kVPFocusBoxWidth = 50.0f;

typedef NS_ENUM(NSInteger, _kVPCamViewMode) {
    kVPCamViewModePhoto = 0,
    kVPCamViewModeVideo
};

@interface VPCamView ()<PBJVisionDelegate>

@property (assign, nonatomic) _kVPCamViewMode mode;
@property (assign, nonatomic) BOOL isCamReady;
@property (strong, nonatomic) CALayer *focusBox;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;
@property (assign, nonatomic) BOOL isAnimatingFocus;

@property (strong, nonatomic) PBJVision *vision;

@end

@implementation VPCamView

#pragma mark - lifecycle
- (id)initWithPhotoFrame:(CGRect)frame vision:(PBJVision *)vision
{
    if (self = [super initWithFrame:frame]) {
        self.tapToFocusEnabled = YES;
        self.isCamReady = NO;
        self.mode = kVPCamViewModePhoto;
        self.autoresizingMask = UIViewAutoresizingFill;
        
        //- Configure focusbox
        self.focusBox = [CALayer layer];
        NSDictionary *unanimatedActions = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"bounds", [NSNull null], @"frame", [NSNull null], @"position", nil];
        self.focusBox.actions = unanimatedActions;
        self.focusBox.borderWidth = 2.0f;
        self.focusBox.borderColor = [[UIColor whiteColor] CGColor];
        self.focusBox.opacity = 0.0f;
        
        //- Configure camera setting
        self.vision = vision;
        self.vision.delegate = self;
        self.vision.cameraDevice = PBJCameraDeviceBack;
        self.vision.cameraMode = PBJCameraModePhoto;
        self.vision.autoUpdatePreviewOrientation = NO;
        self.vision.cameraOrientation = PBJCameraOrientationPortrait;
        self.vision.thumbnailEnabled = NO; //TODO: Do we need create thumbnail of the image?
        // Seems like, the focusMode is not set at this time, since _currentDevice is nil, since it is ContAutoFocus by default, don't bother then
//        vision.focusMode = AVCaptureFocusModeContinuousAutoFocus;
        
        //- Configure preview
        self.previewLayer = [self.vision previewLayer];
        self.previewLayer.frame = self.bounds;
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [self.previewLayer addSublayer:self.focusBox];
        [self.layer insertSublayer:self.previewLayer atIndex:0];
        
        //- Tap-to-focus
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
        [tap addTarget:self action:@selector(camViewTapped:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (id)initWithVideoFrame:(CGRect)frame vision:(PBJVision *)vision
{
    if (self = [super initWithFrame:frame]) {
        self.isCamReady = NO;
        self.mode = kVPCamViewModeVideo;
        self.autoresizingMask = UIViewAutoresizingFill;
        
        //- Configure focusbox
        self.focusBox = [CALayer layer];
        NSDictionary *unanimatedActions = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"bounds", [NSNull null], @"frame", [NSNull null], @"position", nil];
        self.focusBox.actions = unanimatedActions;
        self.focusBox.borderWidth = 2.0f;
        self.focusBox.borderColor = [[UIColor whiteColor] CGColor];
        self.focusBox.opacity = 0.0f;
        
        //- Configure camera setting
        self.vision = vision;
        vision.delegate = self;
        vision.cameraDevice = PBJCameraDeviceBack;
        vision.cameraMode = PBJCameraModeVideo;
        vision.autoUpdatePreviewOrientation = NO;
        vision.cameraOrientation = PBJCameraOrientationPortrait;
        vision.captureSessionPreset = AVCaptureSessionPresetInputPriority;
        vision.videoFrameRate = 30;
        vision.videoRenderingEnabled = NO;
        vision.thumbnailEnabled = YES;
        vision.defaultVideoThumbnails = YES;
        vision.usesApplicationAudioSession = NO;
        vision.maximumCaptureDuration = CMTimeMakeWithSeconds([[VPConfigManager sharedInstance] getFloat:kVPConfigMaxCaptureDuration], 30);
        vision.captureDirectory = [VPFileManager sharedInstance].documentDirectoryPath;
        vision.videoDimensionWidth = 320; //SCREEN_WIDTH;
        vision.videoDimensionHeight = 568; //SCREEN_HEIGHT;
        // Check Apple's RosyWriter project, the magic number here approximately matches the quality produced by AVCaptureSessionPresetMedium or Low
        vision.videoBitRate = SCREEN_WIDTH * SCREEN_HEIGHT * 4.05;
        // vision.videoBitRate = 700000;
        
        //- Configure preview
        self.previewLayer = [vision previewLayer];
        self.previewLayer.frame = self.bounds;
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [self.previewLayer addSublayer:self.focusBox];
        [self.layer insertSublayer:self.previewLayer atIndex:0];
        
        //- Tap-to-focus
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
        [tap addTarget:self action:@selector(camViewTapped:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    // We need relayout the sublayer cuz the frame might not be correct at initWithFrame
    self.previewLayer.frame = self.bounds;
}


#pragma mark - message forwarding
- (BOOL)respondsToSelector:(SEL)aSelector
{
    NSString *selectorName = NSStringFromSelector(aSelector);
    if ([selectorName hasPrefix:@"vision"]) { // All PBJVisionDelegate's messages start with "vision"
        static NSArray *interalPJVisionDelegateMessages;
        static dispatch_once_t onceToken = 0;
        dispatch_once(&onceToken, ^{
            interalPJVisionDelegateMessages = @[
                                                @"visionSessionDidStart:",
                                                @"visionWillStartFocus:",
                                                @"vision:willStartVideoCaptureToFile:"
                                                ];
        });
        if (![interalPJVisionDelegateMessages containsObject:selectorName]) {
            return [self.delegate respondsToSelector:aSelector];
        }
    }
    return [super respondsToSelector:aSelector];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    if ([self.delegate respondsToSelector:[anInvocation selector]]) {
        [anInvocation invokeWithTarget:self.delegate];
    } else {
        [super forwardInvocation:anInvocation];
    }
}


#pragma mark - PBJVisionDelegate
- (void)visionSessionDidStart:(PBJVision * __nonnull)vision
{
    self.isCamReady = YES;
    
    if ([self.delegate respondsToSelector:@selector(visionSessionDidStart:)]) {
        [self.delegate visionSessionDidStart:vision];
    }
}

- (void)visionWillStartFocus:(PBJVision * __nonnull)vision
{
    if (vision.cameraDevice == PBJCameraDeviceFront)
        return;
    
    [self drawFocusBoxAtCenter];
    
    if ([self.delegate respondsToSelector:@selector(visionWillStartFocus:)]) {
        [self.delegate visionSessionDidStart:vision];
    }
}

//- (void)visionDidStopFocus:(PBJVision * __nonnull)vision
//{
//    VPLog(@"visionDidStopFocus");
//    [self animateFocusBox:NO];
//    
//    if ([self.delegate respondsToSelector:@selector(visionDidStopFocus:)]) {
//        [self.delegate visionSessionDidStart:vision];
//    }
//}

- (NSString *)vision:(PBJVision *)vision willStartVideoCaptureToFile:(NSString *)fileName
{
    return kVPFileVideoRecordMP4;
}


#pragma mark - action handling
- (void)camViewTapped:(UITapGestureRecognizer *)sender
{
    if (!self.tapToFocusEnabled)
        return;
    
    if (!self.isCamReady)
        return;
    
//    PBJVision *vision = [PBJVision sharedInstance];
    if (!self.vision.isFocusPointOfInterestSupported)
        return;
    
    CGPoint tapPoint = [sender locationInView:self];
    
    CGPoint adjustPoint = [PBJVisionUtilities convertToPointOfInterestFromViewCoordinates:tapPoint inFrame:self.frame vision:self.vision];
    [self.vision focusExposeAndAdjustWhiteBalanceAtAdjustedPoint:adjustPoint];
    
    [self drawFocusBoxAtPointOfInterest:tapPoint];
}


#pragma mark - private
- (void)drawFocusBoxAtCenter
{
    [self drawFocusBoxAtPointOfInterest:CGPointMake(self.bounds.size.width / 2.0f, self.bounds.size.height / 2.0f)];
}

- (void)drawFocusBoxAtPointOfInterest:(CGPoint)point
{
    [self.focusBox setFrame:CGRectMake(0.0f, 0.0f, _kVPFocusBoxWidth, _kVPFocusBoxWidth)];
    [self.focusBox setPosition:point];
    [self animateFocusBox];
}

- (void)animateFocusBox
{
    if (self.isAnimatingFocus)
        return;
    self.isAnimatingFocus = YES;
    
    CALayer *layer = self.focusBox;
    [layer setHidden:NO];
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [opacityAnimation setDuration:0.3f];
    [opacityAnimation setRepeatCount:1];
    [opacityAnimation setAutoreverses:YES];
    [opacityAnimation setFromValue:[NSNumber numberWithFloat:1.0f]];
    [opacityAnimation setToValue:[NSNumber numberWithFloat:0.0f]];
    opacityAnimation.delegate = self;
    [layer addAnimation:opacityAnimation forKey:@"animateOpacity"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self.focusBox removeAnimationForKey:@"animateOpacity"];
    self.isAnimatingFocus = NO;
}

@end
