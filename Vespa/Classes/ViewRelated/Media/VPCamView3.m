//
//  VPCamView3.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/10/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPCamView3.h"

#import "VPCamManager.h"


static CGFloat const _kVPFocusBoxWidth = 50.0f;

@interface VPCamView3 ()<VPCamManagerDelegate>

@property (assign, nonatomic) BOOL isCamReady;

@property (strong, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;
@property (strong, nonatomic) CALayer *focusBox;
@property (assign, nonatomic) BOOL isFocusSupport;

@end


@implementation VPCamView3

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.isCamReady = NO;
        
        //- Configure focusbox
        self.focusBox = [CALayer layer];
        NSDictionary *unanimatedActions = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"bounds", [NSNull null], @"frame", [NSNull null], @"position", nil];
        self.focusBox.actions = unanimatedActions;
        self.focusBox.borderWidth = 2.0f;
        self.focusBox.borderColor = [[UIColor whiteColor] CGColor];
        self.focusBox.opacity = 0.0f;
        
        //- Configure previewLayer
        self.previewLayer = [AVCaptureVideoPreviewLayer new];
        self.previewLayer.frame = self.bounds;
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [self.previewLayer addSublayer:self.focusBox];
        [self.layer insertSublayer:self.previewLayer atIndex:0]; // Put the sublayer behind
        
        // Attach the camera whenever it is ready, also attach self as delegate
        [[VPCamManager sharedInstance] launchImmediateOrDelegate:self];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    // We need relayout the sublayer cuz the frame might not be finalized at initWithFrame
    self.previewLayer.frame = self.bounds;
}


#pragma mark - VPCamManagerDelegate
- (void)camDidLaunch:(VPCamState)state
{
    if (state == kVPCamStateRun) {
        VPCamManager *camManager = [VPCamManager sharedInstance];
        self.previewLayer.session = camManager.session;
        self.isCamReady = YES;
        self.isFocusSupport = [camManager hasFocus];
    }
    //TODO: other state handling
}

- (void)subjectAreaDidChange
{
    CGPoint p = CGPointMake(.5f,.5f);
    [[VPCamManager sharedInstance] focus:AVCaptureFocusModeContinuousAutoFocus atDevicePoint:p];
    [self drawFocusBoxAtCenter];
}


#pragma mark - private
- (void)drawFocusBoxAtCenter
{
    [self drawFocusBoxAtPointOfInterest:CGPointMake(self.bounds.size.width / 2.0f, self.bounds.size.height / 2.0f)];
}

- (void)drawFocusBoxAtPointOfInterest:(CGPoint)point
{
    if (self.isFocusSupport) {
        [self.focusBox setFrame:CGRectMake(0.0f, 0.0f, _kVPFocusBoxWidth, _kVPFocusBoxWidth)];
        [self.focusBox setPosition:point];
        [self addAdjustingAnimationToLayer:self.focusBox removeAnimation:YES];
    }
}

- (void)addAdjustingAnimationToLayer:(CALayer *)layer removeAnimation:(BOOL)remove {
    if (remove) {
        [layer removeAnimationForKey:@"animateOpacity"];
    }
    if ([layer animationForKey:@"animateOpacity"] == nil) {
        [layer setHidden:NO];
        CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [opacityAnimation setDuration:0.3f];
        [opacityAnimation setRepeatCount:1.0f];
        [opacityAnimation setAutoreverses:YES];
        [opacityAnimation setFromValue:[NSNumber numberWithFloat:1.0f]];
        [opacityAnimation setToValue:[NSNumber numberWithFloat:0.0f]];
        [layer addAnimation:opacityAnimation forKey:@"animateOpacity"];
    }
}

@end
