//
//  VPDrawView.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/25/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseView.h"

@protocol VPDrawViewDelegate;
@interface VPDrawView : VPBaseView

/** The color used for NEXT stroke */
@property (strong, nonatomic) UIColor *color;

@property (weak, nonatomic) id<VPDrawViewDelegate> delegate;

/** Remove the last stroke if any */
- (void)reset;

/** Check if any (unreset) stroke on draw view */
- (BOOL)hasStroke;

@end


@protocol VPDrawViewDelegate <NSObject>
@optional
- (BOOL)drawViewShouldBeginDrawing:(VPDrawView *)drawView;
- (void)drawViewDidEndDrawing:(VPDrawView *)drawView;

@end