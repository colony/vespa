//
//  VPDrawView.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/25/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPDrawView.h"

@interface VPDrawView ()

/** A list of UIBezierPath stroke, each stroke occupies two indexes, first the path obj, second the associated color */
@property (strong, nonatomic) NSMutableArray *paths;

@property (strong, nonatomic) UIBezierPath *curPath;

@property (assign, nonatomic) BOOL enabled;

@end


@implementation VPDrawView

- (void)setupUI
{
    _paths = [NSMutableArray new];
    _color = [UIColor blackColor]; // The default color
    _enabled = YES;
}

- (void)reset
{
    if ([self.paths count] >= 2) {
        [self.paths removeLastObject];
        [self.paths removeLastObject];
        [self setNeedsDisplay];
    }
}

- (BOOL)hasStroke
{
    return [self.paths count] >= 2;
}

#pragma mark - touch and draw
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(drawViewShouldBeginDrawing:)]) {
        if (![self.delegate drawViewShouldBeginDrawing:self]) {
            self.enabled = NO;
            return; // Delegate tells us to abort
        }
    }
    
    self.curPath = [UIBezierPath bezierPath];
    self.curPath.lineCapStyle = kCGLineCapRound;
    self.curPath.lineWidth = 4.0f;
    
    CGPoint p = [[touches anyObject] locationInView:self];
    [self.curPath moveToPoint:p];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!self.enabled)
        return;
    
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    [self.curPath addLineToPoint:p];
    
    [self.paths addObject:self.curPath];
    [self.paths addObject:self.color];
    self.curPath = nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(drawViewDidEndDrawing:)]) {
        [self.delegate drawViewDidEndDrawing:self];
    }
    
    [self setNeedsDisplay];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!self.enabled)
        return;
    
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    [self.curPath addLineToPoint:p];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    for (int i = 0; i < [self.paths count]; i+=2) {
        UIBezierPath *path = self.paths[i];
        UIColor *color = self.paths[i+1];
        
        [color setStroke];
        [path stroke];
    }
    
    if (self.curPath) {
        [self.color setStroke];
        [self.curPath stroke];
    }
}

@end
