//
//  VPMediaAccessDenyVIew.h
//  Vespa
//
//  Created by Jiayu Zhang on 5/12/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseView.h"

@interface VPMediaAccessDenyView : VPBaseView

/** 1-Camera 2-Microphone */
- (void)configureLabel:(NSInteger)i;

@end
