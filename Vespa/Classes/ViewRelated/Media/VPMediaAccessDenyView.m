//
//  VPMediaAccessDenyVIew.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/12/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaAccessDenyView.h"


@interface VPMediaAccessDenyView ()

@property (weak, nonatomic) IBOutlet UIButton *accessSettingsButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end

@implementation VPMediaAccessDenyView

- (void)setupUI
{
    [super setupUI];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0)) {
        self.accessSettingsButton.hidden = NO;
    } else {
        self.accessSettingsButton.hidden = YES;
    }
}

- (void)configureLabel:(NSInteger)i
{
    if (i == 1) { //Camera
        self.messageLabel.text = @"Ninjr needs your permission to use Camera. Please change privacy settings";
    } else if (i == 2) { //Microphone
        self.messageLabel.text = @"Ninjr needs your permission to use Microphone. Please change privacy settings";
    }
}

- (IBAction)accessSettingsButtonPressed:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end
