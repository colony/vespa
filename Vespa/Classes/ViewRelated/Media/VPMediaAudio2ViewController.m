//
//  VPMediaAudio2ViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/19/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaAudio2ViewController.h"

#import "DACircularProgressView.h"
#import "UINavigationItem+Util.h"
#import "VPAudioManager.h"
#import "VPConfigManager.h"
#import "VPFileManager.h"
#import "VPMediaSubmitViewController.h"


@interface VPMediaAudio2ViewController ()<VPAudioManagerDelegate>

// Start or attempt playing, this doesn't necessary mean player->playing is true
// It is a flag indicating the current status to help us change the view correspondingly
@property (assign, nonatomic) BOOL isPlaying;

@property (assign, nonatomic) NSTimeInterval duration; // in seconds

@property (assign, nonatomic) VPAudioFilterType currentFilterType;

// View components
@property (weak, nonatomic) IBOutlet DACircularProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *currentFilterButton; // Initially, this is original filter
@property (weak, nonatomic) IBOutlet UIView *filterView;

@property (assign, nonatomic) BOOL isNexting;

@end

@implementation VPMediaAudio2ViewController

#pragma mark - lifecycle
- (void)commonInit
{
    [super commonInit];
    // Quick dirty way to get duration of audio file
    NSString *path = [[VPFileManager sharedInstance] pathForFile:kVPFileAudioRecordM4A];
    self.duration = [[VPAudioManager sharedInstance] durationOfAudio:path];
}

- (void)setupUI
{
    [super setupUI];
    
    // Configure progressView
    self.progressView.roundedCorners = NO;
    self.progressView.clockwiseProgress = YES;
    self.progressView.thicknessRatio = 0.2f;
    
    // Configure filter view
    CGFloat space = (SCREEN_WIDTH - 3 * 60) / 4; //each filter button 60x60
    [self.filterView viewWithTag:1].x = space;
    [self.filterView viewWithTag:2].x = 2*space + 60.0f;
    [self.filterView viewWithTag:3].x = 3*space + 120.0f;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.isPlaying = NO;
    self.currentFilterType = kVPAudioFilterOrigin;
    
    // Pre-clean the audio filter cache leftover
    [[VPFileManager sharedInstance] deleteDataWithFilePath:kVPFileAudioFilterChipmunkWAV];
    [[VPFileManager sharedInstance] deleteDataWithFilePath:kVPFileAudioFilterMonsterWAV];
    
    // Configure navbar
    [self.navigationItem setCustomRightWithTitle:@"NEXT" textColor:[UIColor blackColor] font:[VPUI fontOpenSans:15.0f thickness:3] target:self action:@selector(nextButtonPressed:)];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopPlaying];
}


#pragma mark - action handling
- (IBAction)filterButtonPressed:(UIButton *)sender
{
    if (self.currentFilterButton != sender) {
        self.currentFilterButton.selected = NO;
        self.currentFilterButton = sender;
        self.currentFilterButton.selected = YES;
        // NOTE, we assign each filter button a tag in IB that maps to VPAudioFilterType
        NSInteger i = self.currentFilterButton.tag;
        if (i == 1) {
            self.currentFilterType = kVPAudioFilterMonster;
            [VPReports reportEvent:kVPEventClickAudioFilter withParam:@{@"filter":@"monster"} mode:kVPEventModeOnce];
        } else if (i == 2) {
            self.currentFilterType = kVPAudioFilterOrigin;
            [VPReports reportEvent:kVPEventClickAudioFilter withParam:@{@"filter":@"original"} mode:kVPEventModeOnce];
        } else if (i == 3) {
            self.currentFilterType = kVPAudioFilterChipmunk;
            [VPReports reportEvent:kVPEventClickAudioFilter withParam:@{@"filter":@"chipmunk"} mode:kVPEventModeOnce];
        }
    }
    // Stop current playing and reset (if necessary)
    [self stopPlaying];
    [self startPlaying];
}

- (IBAction)playButtonPressed:(UIButton *)sender
{
    if (self.isPlaying) {
        [self stopPlaying];
    } else {
        [self startPlaying];
    }
}

- (void)nextButtonPressed:(id)sender
{
    if (self.isNexting)
        return;
    self.isNexting = YES;
    
    [self stopPlaying];
    
    // Convert filtered audio from WAV to AAC/M4A
    BOOL isConverting = NO;
    if (self.currentFilterType != kVPAudioFilterOrigin) {
        if([[VPConfigManager sharedInstance] getBool:kVPConfigAudioRecordConvertEnabled]) {
            VPAudioManager *audioMgr = [VPAudioManager sharedInstance];
            if (![audioMgr isConverterAvailable]) {
                VPLog(@"TPAACAudioConverter not available");
                [VPReports reportError:kVPErrorAACAudioConverterNotAvailable message:nil error:nil];
            } else {
                VPFileManager *fileMgr = [VPFileManager sharedInstance];
                NSString *srcAudioPath = [fileMgr pathForFile:[self fileForCurrentFilter]];
                NSString *dstAudioPath = [fileMgr pathForFile:kVPFileAudioAACConvertM4A];
#ifdef DEBUG
                NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:srcAudioPath error:nil];
                VPLog(@"start conversion %@, original file size = %llu", [[NSDate date] description], [fileDictionary fileSize]);
#endif
                isConverting = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil userInfo:@{@"status":@"Compressing"}];
                [audioMgr convert:srcAudioPath dst:dstAudioPath delegate:self];
            }
        }
    }
    
    if (!isConverting) {
        [self proceedToNext:[self fileForCurrentFilter]];
    }
}


#pragma mark - playing
- (void)startPlaying
{
    self.isPlaying = YES;
    
    VPAudioManager *audioMgr = [VPAudioManager sharedInstance];
    VPFileManager *fileMgr = [VPFileManager sharedInstance];
    
    // We will play the offline-filtered audio file
    NSString *audioFilePath = [self fileForCurrentFilter];
    if (self.currentFilterType != kVPAudioFilterOrigin && ![fileMgr existsDataWithFilePath:audioFilePath]) {
        NSString *srcPath = [fileMgr pathForFile:kVPFileAudioRecordM4A];
        NSString *dstPath = [fileMgr pathForFile:audioFilePath];
        [audioMgr filter:self.currentFilterType src:srcPath dst:dstPath];
    }
    
    [audioMgr playFile:[fileMgr pathForFile:audioFilePath] delegate:self];
}

- (void)stopPlaying
{
    self.isPlaying = NO;
    [[VPAudioManager sharedInstance] stop];
}


#pragma mark - VPAudioManagerDelegate
- (void)VPAudioManagerStart:(VPAudioManager *)manager
{
    [self.playButton setImage:[UIImage imageNamed:@"audio_record_stop"] forState:UIControlStateNormal];
    [self.progressView setProgress:1.0f animated:YES initialDelay:0.0 withDuration:self.duration];
}

- (void)VPAudioManagerStop:(VPAudioManager *)manager
{
    [self.playButton setImage:[UIImage imageNamed:@"audio_record_play"] forState:UIControlStateNormal];
    [self.progressView stopProgress];
    self.isPlaying = NO;
}

- (void)VPAudioManagerEnd:(VPAudioManager *)manager
{
    [self.playButton setImage:[UIImage imageNamed:@"audio_record_play"] forState:UIControlStateNormal];
    [self.progressView stopProgress];
    self.isPlaying = NO;
}

- (void)VPAudioManagerConvert:(VPAudioManager *)manager fail:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
    if (error) {
        VPLog(@"AACAudioConverter fail: %@, we have to post original", [error localizedDescription]);
        [VPReports reportError:kVPErrorAACAudioConversion message:[error localizedDescription] error:error];
        
        // In this case, we have to post the original file
        [self proceedToNext:[self fileForCurrentFilter]];
    } else {
#ifdef DEBUG
        NSString *path = [[VPFileManager sharedInstance] pathForFile:kVPFileAudioAACConvertM4A];
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
        VPLog(@"complete conversion %@, output file size = %llu", [[NSDate date] description], [fileDictionary fileSize]);
#endif
        [self proceedToNext:kVPFileAudioAACConvertM4A];
    }
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - private
- (NSString *)fileForCurrentFilter
{
    switch (self.currentFilterType) {
        case kVPAudioFilterOrigin:
            return kVPFileAudioRecordM4A;
        case kVPAudioFilterMonster:
            return kVPFileAudioFilterMonsterWAV;
        case kVPAudioFilterChipmunk:
            return kVPFileAudioFilterChipmunkWAV;
    }
    return nil;
}

- (void)proceedToNext:(NSString *)file
{
    NSString *filterNameToReport;
    switch (self.currentFilterType) {
        case kVPAudioFilterChipmunk:
            filterNameToReport = @"chipmunk";
            break;
        case kVPAudioFilterMonster:
            filterNameToReport = @"monster";
            break;
        default:
            filterNameToReport = @"original";
            break;
    }
    
    VPMediaSubmitViewController *submitVC = [[VPMediaSubmitViewController alloc] initWithData:@{
                                                                                                @"type":@"AUDIO",
                                                                                                @"audioDuration":@(self.duration),
                                                                                                @"audioFile":file,
                                                                                                @"audioFilterName":filterNameToReport
                                                                                                }];
    [self.navigationController pushViewController:submitVC animated:YES];
    self.isNexting = NO;
}

@end
