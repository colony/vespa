//
//  VPMediaAudioViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/19/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaAudioViewController.h"

#import <AVFoundation/AVFoundation.h>
#import "DACircularProgressView.h"
#import "UINavigationItem+Util.h"
#import "VPAudioManager.h"
#import "VPConfigManager.h"
#import "VPFileManager.h"
#import "VPMediaAccessDenyView.h"
#import "VPMediaAudio2ViewController.h"
#import "VPUtil.h"


@interface VPMediaAudioViewController ()<AVAudioRecorderDelegate>

/**
 * Recording could be finished in different case. If user presses stop button or timer expires (timeMax)
 * it is done NORMALLY. If user close go back, we will stop the recorder and set the value to NO
 */
@property (assign, nonatomic) BOOL isDoneRecordingNormally;

@property (weak, nonatomic) IBOutlet DACircularProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timerCountLabel;

/** Progress, Timer related */
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) NSInteger timeElapse;
@property (assign, nonatomic) NSInteger timeMax;

@property (strong, nonatomic) AVAudioRecorder *recorder;
@property (strong, nonatomic) AVAudioPlayer *player;

@property (strong, nonatomic) VPMediaAccessDenyView *accessDenyOverlayView;

@end

@implementation VPMediaAudioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];

    // Configure navbar
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0, 0, 30, 30);
    closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [closeBtn setImage:[UIImage imageNamed:@"close_button_black"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setCustomLeftWithView:closeBtn];
    [self.navigationItem tweakLeftWithSpace:-8];
    
    // Configure progressView
    self.progressView.roundedCorners = NO;
    self.progressView.clockwiseProgress = YES;
    self.progressView.thicknessRatio = 0.2f;
    
    // Configure Recorder
    [VPAudioManager sharedInstance]; // Internally initialize the avaudiosession
//    NSError *error = nil;
//    AVAudioSession *session = [AVAudioSession sharedInstance];
//    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
//    if (error) {
//        VPLog(@"AVAudioSession fail: %@", [error localizedDescription]);
//    }
    
    //http://www.raywenderlich.com/69365/audio-tutorial-ios-file-data-formats-2014-edition
    //The most important thing to know here is the preferred variant of linear PCM on the iPhone is little-endian integer 16-bit, or LEI16 for short.
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC]  forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityMedium]  forKey:AVEncoderAudioQualityKey];
    [recordSetting setValue:[NSNumber numberWithFloat:16000.0]             forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt:1]                     forKey:AVNumberOfChannelsKey];
    
    //    [recordSetting setObject:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    //    [recordSetting setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    //    [recordSetting setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
    //    [recordSetting setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsNonInterleaved];
    //    [recordSetting setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
    
    NSString *outputPath = [[VPFileManager sharedInstance] pathForFile:kVPFileAudioRecordM4A];
    NSURL *outputFileURL = [NSURL fileURLWithPath:outputPath];
    
    NSError *error = nil;
    self.recorder = nil;
    self.recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:&error];
    if (error) {
        VPLog(@"Recorder fail: %@", [error localizedDescription]);
    }
    
    self.recorder.delegate = self;
    self.recorder.meteringEnabled = NO;
    [self.recorder prepareToRecord];
    
    self.timeMax = [[VPConfigManager sharedInstance] getInteger:kVPConfigAudioRecordTimeInSec];
}

- (void)setupUI
{
    [super setupUI];
    self.timerCountLabel.y = self.view.centerY + 75 + 30;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.isDoneRecordingNormally = NO;
    
    // Audio access handling
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
        ASYNC_MAIN({
            [self showAccessDenyOverlayView:!granted];
        });
    }];
    [self setupStopRecording];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Right before we leave the vc, we need stop the recorder if necessary. There could be different reason that
    // we leaves current controller. Either we finish recorder and push to next vc or we click close/go back
    [self stopRecording];
}


#pragma mark - action handling
- (IBAction)recordButtonPressed:(id)sender
{
    if (self.recorder.recording) {
        self.isDoneRecordingNormally = YES;
        [self stopRecording];
    } else {
        [self startRecording];
    }
}

- (void)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - record, progress and timer
- (void)startRecording
{
    if (!self.recorder.recording) {
        NSError *error = nil;
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:&error];
        
        if (error) {
            NSLog(@"Session fail: %@", [error localizedDescription]);
        }
        
        [self.recorder record];
        [self setupStartRecording];
    }
}

- (void)setupStartRecording
{
    [self.recordButton setImage:[UIImage imageNamed:@"audio_record_stop"] forState:UIControlStateNormal];
    self.messageLabel.hidden = YES;
    self.timerCountLabel.hidden = NO;
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self
                                                selector:@selector(progressChange)
                                                userInfo:nil
                                                 repeats:YES];
    [self.progressView setProgress:1.0f animated:YES initialDelay:0.0 withDuration:self.timeMax];
}

- (void)stopRecording
{
    if (self.recorder.recording) {
        [self.recorder stop];
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:NO error:nil];
        
        [self.progressView pauseProgress];
        
        if (self.timer) {
            [self.timer invalidate];
            self.timer = nil;
        }
        
        // We only stop the timer here, instead of change view to reflect stop status
        // Because, right after record finish, we will present to next editorVC, in some case,
        // we will see a flashy change of view before presenting
        // [self setupStopRecording];
    }
}

- (void)setupStopRecording
{
    [self.recordButton setImage:[UIImage imageNamed:@"audio_record"] forState:UIControlStateNormal];
    self.messageLabel.hidden = NO;
    self.timerCountLabel.hidden = YES;
    self.timerCountLabel.text = @"0s";
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    [self.progressView setProgress:0.0f];
    self.timeElapse = 0;
}

- (void)progressChange
{
    self.timeElapse += 1;
    
    if ([self.timer isValid]) {
        self.timerCountLabel.text = [NSString stringWithFormat:@"%lds", (long)self.timeElapse];
    }
    
    if (self.timeElapse >= self.timeMax) {
        self.isDoneRecordingNormally =YES;
        [self stopRecording];
    }
}


#pragma mark - AVAudioRecorderDelegate
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    if (!flag) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                          message:@"Fail to record your audio. Retry?"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        [self setupStopRecording];
        return;
    }
    
    if (!self.isDoneRecordingNormally)
        return;
    
    VPMediaAudio2ViewController *nextStepVC = [[VPMediaAudio2ViewController alloc] init];
    [self.navigationController pushViewController:nextStepVC animated:YES];
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
    VPLog(@"recorder error %@", [error localizedDescription]);
}


#pragma mark - private
- (void)showAccessDenyOverlayView:(BOOL)visible
{
    if (visible && !self.accessDenyOverlayView) {
        self.accessDenyOverlayView = [[[NSBundle mainBundle] loadNibNamed:@"VPMediaAccessDenyView" owner:nil options:nil] objectAtIndex:0];
        self.accessDenyOverlayView.frame = self.view.bounds;
        [self.accessDenyOverlayView configureLabel:2];
        [self.view addSubview:self.accessDenyOverlayView];
        [self.view bringSubviewToFront:self.accessDenyOverlayView];
    } else if (!visible && self.accessDenyOverlayView) {
        self.accessDenyOverlayView.hidden = YES;
        [self.accessDenyOverlayView removeFromSuperview];
        self.accessDenyOverlayView = nil;
    }
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
