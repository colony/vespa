//
//  VPMediaCAHViewController.h
//  Vespa
//
//  Create media content for game "Cards Against Humanity"
//
//  Created by Jiayu Zhang on 9/5/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

@interface VPMediaCardViewController : VPBaseViewController

- (instancetype)initRequestCard;

- (instancetype)initResponseCardWithDeckId:(NSString *)deckId;

@end
