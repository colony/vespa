//
//  VPMediaCAHViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/5/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaCardViewController.h"

#import "OLGhostAlertView.h"
#import "UINavigationItem+Util.h"
#import "VPAPIClient.h"
#import "VPPosts.h"
#import "ZLSwipeableView.h"


@interface VPMediaCardViewController ()<ZLSwipeableViewDataSource, ZLSwipeableViewDelegate>

@property (strong, nonatomic) NSString *deckId;
@property (assign, nonatomic) BOOL isRequest;

@property (strong, nonatomic) NSMutableArray *items;
@property (assign, nonatomic) NSUInteger itemIndex;

@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) ZLSwipeableView *swipeableCardView;

@property (assign, nonatomic) BOOL isPosting;

// Context data
@property (strong, nonatomic) NSNumber *currentPickedGroupId;
@property (strong, nonatomic) NSNumber *currentThreadId;
@property (strong, nonatomic) NSString *createThreadType;

@end

@implementation VPMediaCardViewController

#pragma mark - lifecycle
- (instancetype)initRequestCard
{
    if (self = [super init]) {
        _isRequest = YES;
    }
    return self;
}

- (instancetype)initResponseCardWithDeckId:(NSString *)deckId
{
    if (self = [super init]) {
        _deckId = deckId;
        _isRequest = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.view.backgroundColor = [VPUI lightGrayColor];
    
    // Configure navbar
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0, 0, 30, 30);
    closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [closeBtn setImage:[UIImage imageNamed:@"close_button_black"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setCustomLeftWithView:closeBtn];
    [self.navigationItem tweakLeftWithSpace:-8];
    [self.navigationItem setCustomTitle:(self.isRequest ? @"Choose a Black Card" : @"Choose a White Card") textColor:[VPUI colonyColor] font:nil];
    
    // Create SwipeableView
    self.swipeableCardView = [[ZLSwipeableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 30, SCREEN_HEIGHT - 44.0f - 100.0f)];
    self.swipeableCardView.autoresizingMask = UIViewAutoresizingFill;
    self.swipeableCardView.centerX = self.view.centerX;
    self.swipeableCardView.centerY = self.view.centerY - 17.0f;
    self.swipeableCardView.rotationDegree = 2;
    [self.view addSubview:self.swipeableCardView];
    
    // Create label
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150.0f, 20.0f)];
    self.label.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
    self.label.font = [VPUI fontOpenSans:14.0f thickness:1];
    self.label.textColor = [VPUI darkGrayColor];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.centerX = self.view.centerX;
    self.label.y = self.swipeableCardView.y - 30.0f;
    self.label.text = @"Swipe";
    [self.view addSubview:self.label];
    
    // Create play button
    UIButton *postButton = [UIButton buttonWithType:UIButtonTypeCustom];
    postButton.backgroundColor = [VPUI colonyColor];
    postButton.frame = CGRectMake(0, self.view.height - 35.0f, SCREEN_WIDTH, 35.0f);
    postButton.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    postButton.titleLabel.textColor = [UIColor whiteColor];
    postButton.titleLabel.font = [VPUI fontOpenSans:15.0f thickness:2];
    [postButton setTitle:@"PLAY" forState:UIControlStateNormal];
    [postButton addTarget:self action:@selector(postButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:postButton];
    
    self.currentPickedGroupId = [[VPContext sharedInstance] getDomain:kVPCDMediaSubmission key:@"groupId"];
    self.currentThreadId = [[VPContext sharedInstance] getDomain:kVPCDMediaSubmission key:@"threadId"];
    self.createThreadType = [[VPContext sharedInstance] getDomain:kVPCDMediaSubmission key:@"createThreadType"];
    [[VPContext sharedInstance] clearDomain:kVPCDMediaSubmission];
    
    self.swipeableCardView.delegate = self;
    self.swipeableCardView.dataSource = self;
    self.itemIndex = 0;
    
    // Load cards
    [self loadData];
}


#pragma mark - ZLSwipeableViewDataSource
- (UIView *)nextViewForSwipeableView:(ZLSwipeableView *)swipeableView
{
    if ([self.items count] > 0) {
        UIView *cardView = [[UIView alloc] initWithFrame:swipeableView.bounds];
        cardView.layer.cornerRadius = 10.0f;
        cardView.layer.masksToBounds = NO;
        cardView.backgroundColor = self.isRequest ? [UIColor blackColor] : [UIColor whiteColor];
        cardView.layer.shouldRasterize = YES;
        cardView.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        [cardView.layer setShadowColor:[UIColor blackColor].CGColor];
        [cardView.layer setShadowOpacity:0.5];
        [cardView.layer setShadowRadius:5.0];
        [cardView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        [cardView.layer setShadowPath:[UIBezierPath bezierPathWithRect:swipeableView.bounds].CGPath];
        
        UILabel *cardLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, cardView.width - 20.0f, cardView.height - 10.0f)];
        cardLabel.center = cardView.center;
        cardLabel.text = self.items[self.itemIndex];
        cardLabel.textAlignment = NSTextAlignmentCenter;
        cardLabel.textColor = self.isRequest ? [UIColor whiteColor] : [UIColor blackColor];
        cardLabel.numberOfLines = 0;
        cardLabel.font = [VPUI fontOpenSans:20 thickness:3];
        [cardView addSubview:cardLabel];
        
        // Keep track where we at
        cardView.tag = self.itemIndex;
        
        // Loop the items
        self.itemIndex = (self.itemIndex + 1) % [self.items count];
        
        return cardView;
    }
    return nil;
}


#pragma mark - action handling
- (void)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)postButtonPressed:(id)sender
{
    if (self.isPosting)
        return;
    
    UIView *cardView = [self.swipeableCardView topSwipeableView];
    if (!cardView)
        return;
    NSInteger currentIndex = cardView.tag;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil userInfo:@{@"status":@"Uploading"}];
    
    self.isPosting = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [VPPosts createPostWithCard:self.items[currentIndex]
                             deckId:self.deckId
                          isRequest:self.isRequest
                            groupId:self.currentPickedGroupId
                   createThreadType:self.createThreadType
                           threadId:self.currentThreadId
                            success:^(VPPost *post) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [SVProgressHUD dismiss];
                                    [self dismissViewControllerAnimated:YES completion:^{
                                        OLGhostAlertView *alertView = [[OLGhostAlertView alloc] initWithTitle:@"Posted!"
                                                                                                      message:nil
                                                                                                      timeout:0.2
                                                                                                  dismissible:NO];
                                        alertView.position = OLGhostAlertViewPositionCenter;
                                        [alertView show];
                                    }];
                                });
                                self.isPosting = NO;
                            } failure:^(NSError *error) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [SVProgressHUD dismiss];
                                    [self dismissViewControllerAnimated:YES completion:^{
                                        OLGhostAlertView *alertView = [[OLGhostAlertView alloc] initWithTitle:@"Failed! Please retry"
                                                                                                      message:nil
                                                                                                      timeout:0.2
                                                                                                  dismissible:NO];
                                        alertView.position = OLGhostAlertViewPositionCenter;
                                        [alertView show];
                                    }];
                                });
                                self.isPosting = NO;
                            }];
    });
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - private
- (void)loadData
{
//    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
    
    VPAPIClient *client = [VPAPIClient sharedInstance];
    @weakify(self);
    [client drawCardWithId:self.deckId
                 isRequest:self.isRequest
                   success:^(NSString *deckId, NSArray *deck) {
                       @strongify(self);
                       self.deckId = deckId;
                       if (!self.items) {
                           self.items = [NSMutableArray array];
                       }
                       [self.items removeAllObjects];
                       [self.items addObjectsFromArray:deck];
                       self.itemIndex = 0;
                       [self.swipeableCardView discardAllSwipeableViews];
                       [self.swipeableCardView loadNextSwipeableViewsIfNeeded];
                   } failure:^(NSError *error) {
                       VPLog(@"Fail draw cards: %@", [error localizedDescription]);
                   }];
}

@end
