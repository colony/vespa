//
//  VPMediaLinkViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/12/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaLinkViewController.h"

#import "UINavigationItem+Util.h"
#import "VPMediaSubmitViewController.h"
#import "VPFileManager.h"

@interface VPMediaLinkViewController ()<UITextFieldDelegate, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) UITextField *inputField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *forwardButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end


@implementation VPMediaLinkViewController

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];

    self.webView.delegate = self;
    self.webView.hidden = YES;
    
    self.backButton.enabled = NO;
    self.forwardButton.enabled = NO;
    self.nextButton.enabled = NO;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)setupUI
{
    [super setupUI];
    
    // Configure navigationbar
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0, 0, 30, 30);
    closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [closeBtn setImage:[UIImage imageNamed:@"close_button_black"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setCustomLeftWithView:closeBtn];
    [self.navigationItem tweakLeftWithSpace:-8];
    
    self.inputField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 7.0f, SCREEN_WIDTH - 60.0f, 30.0f)];
    self.inputField.placeholder = @"Enter URL or search keyword";
    self.inputField.backgroundColor = [UIColor whiteColor];
    self.inputField.textColor = [UIColor blackColor];
    self.inputField.borderStyle = UITextBorderStyleRoundedRect;
    self.inputField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.inputField.returnKeyType = UIReturnKeyGo;
    self.inputField.keyboardType = UIKeyboardTypeWebSearch;
    self.inputField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.inputField.delegate = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.inputField];
}

- (void)dealloc
{
    [self.webView stopLoading];
    self.webView.delegate = nil;
    self.webView = nil;
}


#pragma mark - action handling
- (IBAction)backButtonPressed:(id)sender
{
    [self.webView goBack];
}

- (IBAction)forwardButtonPressed:(id)sender
{
    [self.webView goForward];
}

- (IBAction)nextButtonPressed:(id)sender
{
//    NSString *jsCode = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LinkParser.min" ofType:@"js"] encoding:NSUTF8StringEncoding error:nil];
    NSString *jsCode = [NSString stringWithContentsOfFile:[[VPFileManager sharedInstance] pathForFile:kVPFileLinkParserJavascript] encoding:NSUTF8StringEncoding error:nil];
    NSString *result = [self.webView stringByEvaluatingJavaScriptFromString:jsCode];
    VPLog(@"webview js result = %@", result);
    
    NSError *err = nil;
    NSData *jsonData = [result dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    if (err) {
        // TODO
    } else {
        NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:json];
        [data setValue:@"LINK" forKey:@"type"];
        VPMediaSubmitViewController *submitVC = [[VPMediaSubmitViewController alloc] initWithData:data];
        [self.navigationController pushViewController:submitVC animated:YES];
    }
}

- (void)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder]; // Dismiss the keyboard
    
    NSString *str = textField.text;
    if (str && [str length] > 0) {
        NSString *urlRegex = @"(http(s)?://)?([\\w-]+\\.)+[\\w-]+([\\w-\\./?%&amp;=]*)?";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegex];
        BOOL isValid = [predicate evaluateWithObject:str];

        NSString *url;
        if (isValid) {
            // Apparently, the URL is valid, prepend schema if necessary
            url = str;
            if (![str hasPrefix:@"http://"] && ![str hasPrefix:@"https://"]) {
                url = [NSString stringWithFormat:@"http://%@", str];
            }
            [VPReports reportEvent:kVPEventSearchLink withParam:@{@"search":@(0)} mode:kVPEventModeOnce];
        } else {
            [VPReports reportEvent:kVPEventSearchLink withParam:@{@"search":@(1)} mode:kVPEventModeOnce];
            // Apparently, the URL is invalid, use google keyword search instead
            url = [[NSString stringWithFormat:@"https://www.google.com/search?q=%@", str] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    } else {
        // Go to google.com directly
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.google.com"]]];
    }
    
    return YES;
}


#pragma mark - UIWebView delegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    self.webView.hidden = NO;
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.loadingIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.loadingIndicator stopAnimating];
    self.inputField.text = webView.request.URL.absoluteString;
    self.forwardButton.enabled = self.webView.canGoForward;
    self.backButton.enabled = self.webView.canGoBack;
    self.nextButton.enabled = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.loadingIndicator stopAnimating];
    self.inputField.text = webView.request.URL.absoluteString;
    self.forwardButton.enabled = self.webView.canGoForward;
    self.backButton.enabled = self.webView.canGoBack;
    self.nextButton.enabled = NO;
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


@end
