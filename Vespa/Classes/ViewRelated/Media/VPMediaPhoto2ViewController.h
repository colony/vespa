//
//  VPMediaImage2ViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

@interface VPMediaPhoto2ViewController : VPBaseViewController

- (id)initWithImage:(UIImage *)image;

@property (assign, nonatomic) BOOL isUpload;

@end
