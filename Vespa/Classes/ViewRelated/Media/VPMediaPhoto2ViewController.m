//
//  VPMediaImage2ViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaPhoto2ViewController.h"

#import "OLGhostAlertView.h"
#import "UIImageView+Addition.h"
#import "UINavigationItem+Util.h"
#import "UIView+Frame.h"
#import "VPDrawView.h"
#import "VPImageFilterUtil.h"
#import "VPMediaSubmitViewController.h"
#import "VPTutorialManager.h"
#import "VPUtil.h"


@interface VPMediaPhoto2ViewController ()<VPDrawViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (strong, nonatomic) UIView *wrapView;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) VPDrawView *drawView;
@property (weak, nonatomic) IBOutlet UIImageView *colorPickerImageView;
@property (weak, nonatomic) IBOutlet UIButton *redoButton;
@property (weak, nonatomic) IBOutlet UIButton *penButton;
@property (weak, nonatomic) IBOutlet UIImageView *penButtonBackgroundImageView;

/** The original image to keep intact */
@property (strong, nonatomic) UIImage *originalImage;

/** The first one "NORMAL" meaning the original image (no filter effect) */
@property (strong, nonatomic) NSArray *filterNames;
@property (assign, nonatomic) NSInteger filterIndex;
@property (assign, nonatomic) BOOL isFiltering;

@property (assign, nonatomic) BOOL isProcessing;

@end

@implementation VPMediaPhoto2ViewController

#pragma mark - lifecycle
- (id)initWithImage:(UIImage *)image
{
    if (self = [super init]) {
        _originalImage = image;
        _isProcessing = NO;
        _isFiltering = NO;
        _filterIndex = 0;
        _filterNames = [NSArray arrayWithObjects:@"Normal", @"Pro", @"Azure", @"Mature", @"B&W", @"Fade",nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];

    // Configure navbar
    [self.navigationItem setCustomRightWithTitle:@"NEXT" textColor:[UIColor blackColor] font:[VPUI fontOpenSans:15.0f thickness:3] target:self action:@selector(nextButtonPressed:)];
    
    // Make a copy of original image
    self.originalImage = [self.originalImage copy];
    
    [[VPTutorialManager sharedInstance] start:kVPTutorialPhotoEdit];
}

- (void)setupUI
{
    [super setupUI];
    // Create imageView and drawView on top of it
    CGSize imageFrameSize;
    if (self.isUpload) {
        // If we upload an image, compute proper image frame size within scrollview, with aspect ratio
        imageFrameSize = [VPUtil sizeForImageWithAspectRatio:self.originalImage.size parentViewSize:self.imageScrollView.frame.size];
    } else {
        // Else, we take the shot, fill up entire scrollview
        imageFrameSize = self.imageScrollView.bounds.size;
    }
    
    self.wrapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imageFrameSize.width, imageFrameSize.height)];
    self.wrapView.opaque = YES;
    self.wrapView.autoresizingMask = UIViewAutoresizingFill;
    self.wrapView.center = self.imageScrollView.center;
    
    self.imageView = [[UIImageView alloc] initWithFrame:self.wrapView.bounds];
    self.imageView.autoresizingMask = UIViewAutoresizingFill;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.image = self.originalImage;
    
    self.drawView = [[VPDrawView alloc] initWithFrame:self.wrapView.bounds];
    self.drawView.autoresizingMask = UIViewAutoresizingFill;
    self.drawView.backgroundColor = [UIColor clearColor];
    self.drawView.delegate = self;
    
    [self.wrapView addSubview:self.imageView];
    [self.wrapView addSubview:self.drawView];
    [self.imageScrollView addSubview:self.wrapView];
    self.imageScrollView.contentSize = self.wrapView.bounds.size;
    self.imageScrollView.delegate = self;
    
    // Configure all gestures
    self.colorPickerImageView.alpha = 0.0f;
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanOnColorPicker:)];
    [self.colorPickerImageView addGestureRecognizer:pan];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnColorPicker:)];
    [self.colorPickerImageView addGestureRecognizer:tap];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnImage:)];
    [self.imageView addGestureRecognizer:tap2];
    
    // Initially, draw is disabled, but filter is enabled
    self.imageView.userInteractionEnabled = YES;
    self.drawView.userInteractionEnabled = NO;
    
    // Also, hide the redo button initially
    self.redoButton.alpha = 0.0f;
    
    // Use tintcolor to change button image
    UIImage *btnImage = [[UIImage imageNamed:@"pen_button_nobox"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.penButton setImage:btnImage forState:UIControlStateNormal];
    self.penButton.tintColor = [UIColor blackColor];
}


#pragma mark - action handling
- (void)nextButtonPressed:(id)sender
{
    if (self.isProcessing)
        return;
    self.isProcessing = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil userInfo:@{@"status":@"Processing"}];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *resultImg = nil;
        
        VPLog(@"origin img size=%@, orientation=%td, scale=%f", NSStringFromCGSize(self.originalImage.size), self.originalImage.imageOrientation, self.originalImage.scale);
        
        UIGraphicsBeginImageContextWithOptions(self.imageScrollView.bounds.size, YES, 2.0f);
        CGPoint offset = self.imageScrollView.contentOffset;
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -offset.x, -offset.y);
        [self.imageScrollView.layer renderInContext:UIGraphicsGetCurrentContext()];

        resultImg = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        VPLog(@"result img size=%@, orientation=%td, scale=%f", NSStringFromCGSize(resultImg.size), resultImg.imageOrientation, resultImg.scale);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            self.isProcessing = NO;
            
            VPMediaSubmitViewController *submitVC = [[VPMediaSubmitViewController alloc] initWithData:@{
                                                                                                        @"type":@"PHOTO",
                                                                                                        @"image":resultImg,
                                                                                                        @"uploaded":@(self.isUpload),
                                                                                                        @"filtered":@(self.filterIndex != 0)
                                                                                                        }];
            [self.navigationController pushViewController:submitVC animated:YES];
        });
    });
}

- (IBAction)penButtonPressed:(id)sender;
{
    UIButton *pen = (UIButton *)sender;
    if ([pen isSelected]) {
        [pen setSelected:NO];
        self.colorPickerImageView.alpha = 0.0f;
        self.redoButton.alpha = 0.0f;
        self.drawView.userInteractionEnabled = NO;
        self.imageView.userInteractionEnabled = YES;
        self.penButton.tintColor = [UIColor blackColor];
        self.imageScrollView.scrollEnabled = YES;
    } else {
        [self.view endEditing:YES];
        [pen setSelected:YES];
        self.colorPickerImageView.alpha = 1.0f;
        self.redoButton.alpha = 1.0f;
        self.drawView.userInteractionEnabled = YES;
        self.imageView.userInteractionEnabled = NO;
        self.imageScrollView.scrollEnabled = NO;
        
        // The start (default) color
        self.penButton.tintColor = UIColorFromRGB(0x1702de);
        self.drawView.color = UIColorFromRGB(0x1702de);
    }
}

- (IBAction)redoButtonPressed:(id)sender
{
    [self.drawView reset];
}

- (void)handlePanOnColorPicker:(UIPanGestureRecognizer *)sender
{
    UIColor *color;
    CGPoint p = [sender locationInView:self.colorPickerImageView];
    
    // Note the color pixel is 40x100 (1x scale), on top and bottom, there is 3px for white border which need be excluded
    if (p.y < 3.0f)
        p.y = 3.0f;
    else if (p.y > 97.0f)
        p.y = 97.0f;
    p.x = 20.0f;
    color = [self.colorPickerImageView getPixelColorAtPoint:p];
    [self.penButton setTintColor:color];
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self.drawView setColor:color];
    }
}

- (void)handleTapOnColorPicker:(UIPanGestureRecognizer *)sender
{
    UIColor *color;
    CGPoint p = [sender locationInView:self.colorPickerImageView];
    
    // Note the color pixel is 40x100 (1x scale), on top and bottom, there is 3px for white border which need be excluded
    if (p.y < 3.0f)
        p.y = 3.0f;
    else if (p.y > 97.0f)
        p.y = 97.0f;
    p.x = 20.0f;
    color = [self.colorPickerImageView getPixelColorAtPoint:p];
    [self.penButton setTintColor:color];
    [self.drawView setColor:color];
}

- (void)handleTapOnImage:(UITapGestureRecognizer *)sender
{
    if (self.isFiltering)
        return;
    self.isFiltering = YES;
    
    self.filterIndex = (self.filterIndex + 1) % [self.filterNames count];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *name = [self.filterNames objectAtIndex:self.filterIndex];
        UIImage *filteredImage;
        if ([name isEqualToString:@"Normal"]) {
            filteredImage = self.originalImage;
        } else {
            filteredImage = [VPImageFilterUtil filterImage:self.imageView.image withName:name];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            OLGhostAlertView *alertView = [[OLGhostAlertView alloc] initWithTitle:self.filterNames[self.filterIndex]
                                                                          message:nil
                                                                          timeout:0.2
                                                                      dismissible:NO];
            alertView.position = OLGhostAlertViewPositionCenter;
            [alertView show];
            self.imageView.image = filteredImage;
            self.isFiltering = NO;
        });
    });
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - VPDrawViewDelegate
- (BOOL)drawViewShouldBeginDrawing:(VPDrawView *)drawView
{
    self.penButton.alpha = 0.0f;
    self.colorPickerImageView.alpha = 0.0f;
    self.penButtonBackgroundImageView.alpha = 0.0f;
    self.redoButton.alpha = 0.0f;
    return YES;
}

- (void)drawViewDidEndDrawing:(VPDrawView *)drawView
{
    self.penButton.alpha = 1.0f;
    self.colorPickerImageView.alpha = 1.0f;
    self.penButtonBackgroundImageView.alpha = 1.0f;
    self.redoButton.alpha = 1.0f;
}


#pragma mark - uiscrollview delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.wrapView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerScrollViewContents];
}


#pragma mark - private methods
- (UIImage *)cropImageScrollView
{
    UIImage *result;
    
    // We set scale to 2.0 instead of SCREEN_SCALE, so as to save some bandwidth as well as providing the acceptable quality of photo
    UIGraphicsBeginImageContextWithOptions(self.imageScrollView.bounds.size, YES, 2.0f);
    CGPoint offset = self.imageScrollView.contentOffset;
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -offset.x, -offset.y);
    [self.imageScrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
    result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

//http://www.raywenderlich.com/10518/how-to-use-uiscrollview-to-scroll-and-zoom-content
- (void)centerScrollViewContents
{
    // if the scroll view content size is smaller than its bounds, then it sits at the top-left rather than in the center
    // The method accomplishes that by positioning the image view such that is is always in the center of the scrollview bounds
    
    CGSize boundsSize = self.imageScrollView.bounds.size;
    UIView *wrapperView = self.imageView.superview;
    CGRect contentsFrame = wrapperView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    wrapperView.frame = contentsFrame;
}

@end
