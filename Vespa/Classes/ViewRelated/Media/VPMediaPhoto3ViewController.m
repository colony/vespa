//
//  VPMediaPhoto3ViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/28/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaPhoto3ViewController.h"

#import "QBImagePickerController.h"
#import "UINavigationItem+Util.h"
#import "VPCamView.h"
#import "VPMediaAccessDenyView.h"
#import "VPMediaPhoto2ViewController.h"
#import "VPUtil.h"


@interface VPMediaPhoto3ViewController ()<PBJVisionDelegate, QBImagePickerControllerDelegate>

@property (strong, nonatomic) UIButton *flashButton;
@property (assign, nonatomic) BOOL isCapturing;
@property (strong, nonatomic) VPMediaAccessDenyView *accessDenyOverlayView;
@property (strong, nonatomic) VPCamView *camView;

@property (strong, nonatomic) PBJVision *vision;

@end


@implementation VPMediaPhoto3ViewController

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.view.opaque = YES;
    self.view.backgroundColor = [UIColor blackColor];
    
    // Configure navbar
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0, 0, 30, 30);
    closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [closeBtn setImage:[UIImage imageNamed:@"close_button_black"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setCustomLeftWithView:closeBtn];
    [self.navigationItem tweakLeftWithSpace:-8];
    
    UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    uploadButton.frame = CGRectMake(0, 0, 100, 30);
    [uploadButton setImage:[UIImage imageNamed:@"cam_upload"] forState:UIControlStateNormal];
    [uploadButton addTarget:self action:@selector(uploadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = uploadButton;
    
    // Configure camview
    self.vision = [[PBJVision alloc] init];
    VPCamView *camView = [[VPCamView alloc] initWithPhotoFrame:self.view.bounds vision:self.vision];
    camView.delegate = self;
    [self.view addSubview:camView];
    
    // Configure flashbutton and switchbutton
    self.flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.flashButton.frame = CGRectMake(0, self.view.height-50, 50, 50);
    self.flashButton.contentMode = UIViewContentModeCenter;
    self.flashButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_off"] forState:UIControlStateNormal];
    [self.flashButton addTarget:self action:@selector(flashButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.flashButton];
    
    UIButton *switchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    switchButton.frame = CGRectMake(self.view.width-50, self.view.height-50, 50, 50);
    switchButton.contentMode = UIViewContentModeCenter;
    switchButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin;
    [switchButton setImage:[UIImage imageNamed:@"cam_switch"] forState:UIControlStateNormal];
    [switchButton addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:switchButton];
    
    // Configure shot button
    VPButton *shotButton = [[VPButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    shotButton.highlightAnimationEnabled = YES;
    shotButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    shotButton.centerX = self.view.centerX;
    shotButton.bottom = self.view.height - 40;
    [shotButton.layer setCornerRadius:35.0f];
    [shotButton.layer setBorderWidth:3.0f];
    [shotButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    [shotButton.layer setMasksToBounds:YES];
    [shotButton setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:0.3]];
    [shotButton addTarget:self action:@selector(shotButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shotButton];
}

- (void)setupUI
{
    [super setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Camera access handling
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        ASYNC_MAIN({
            if (granted) {
                [self showAccessDenyOverlayView:NO];
                [self.vision startPreview];
            } else {
                [self showAccessDenyOverlayView:YES];
            }
        });
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.vision stopPreview];
}


#pragma mark - action handling
- (void)uploadButtonPressed:(id)sender
{
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.filterType = QBImagePickerControllerFilterTypePhotos;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.showsNumberOfSelectedAssets = NO;
    imagePickerController.maximumNumberOfSelection = 1;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)flashButtonPressed:(id)sender
{
    PBJVision *vision = self.vision;
    PBJFlashMode mode = vision.flashMode;
    if (mode == PBJFlashModeOn) {
        vision.flashMode = PBJFlashModeOff;
        [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_off"] forState:UIControlStateNormal];
    } else {
        vision.flashMode = PBJFlashModeOn;
        [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_on"] forState:UIControlStateNormal];
    }
}

- (void)switchButtonPressed:(id)sender
{
    PBJVision *vision = self.vision;
    BOOL isBack = vision.cameraDevice == PBJCameraDeviceBack;
    vision.cameraDevice = isBack ? PBJCameraDeviceFront : PBJCameraDeviceBack;
    self.flashButton.hidden = isBack;
}

- (void)shotButtonPressed:(id)sender
{
    if (self.isCapturing)
        return;
    self.isCapturing = YES;
    
    [self.vision capturePhoto];
}


#pragma mark - PBJVision delegate
- (void)vision:(PBJVision * __nonnull)vision capturedPhoto:(nullable NSDictionary *)photoDict error:(nullable NSError *)error
{
    self.isCapturing = NO;
    if (error) {
        VPLog(@"Fail to capture photo %@", [error localizedDescription]);
        return;
    }
    
    UIImage *image = photoDict[PBJVisionPhotoImageKey];
    VPMediaPhoto2ViewController *nextStepVC = [[VPMediaPhoto2ViewController alloc] initWithImage:image];
    nextStepVC.isUpload = NO;
    [self.navigationController pushViewController:nextStepVC animated:YES];
}


#pragma mark - QBImagePickerControllerDelegate
- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAssets:(NSArray *)assets
{
    ALAsset *asset = [assets firstObject];
    UIImage *img = nil;

    id type = [asset valueForProperty:ALAssetPropertyType];
    if (type) {
        ALAssetRepresentation *assetRep = [asset defaultRepresentation];
        if (assetRep) {
            CGImageRef imgRef = [assetRep fullScreenImage];
            img = [UIImage imageWithCGImage:imgRef];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (img && type == ALAssetTypePhoto) {
            VPMediaPhoto2ViewController *nextStepVC = [[VPMediaPhoto2ViewController alloc] initWithImage:img];
            nextStepVC.isUpload = YES;
            [self.navigationController pushViewController:nextStepVC animated:YES];
        }
    }];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - private
- (void)showAccessDenyOverlayView:(BOOL)visible
{
    if (visible && !self.accessDenyOverlayView) {
        self.accessDenyOverlayView = [[[NSBundle mainBundle] loadNibNamed:@"VPMediaAccessDenyView" owner:nil options:nil] objectAtIndex:0];
        self.accessDenyOverlayView.frame = self.view.bounds;
        [self.accessDenyOverlayView configureLabel:1];
        [self.view addSubview:self.accessDenyOverlayView];
        [self.view bringSubviewToFront:self.accessDenyOverlayView];
    } else if (!visible && self.accessDenyOverlayView) {
        self.accessDenyOverlayView.hidden = YES;
        [self.accessDenyOverlayView removeFromSuperview];
        self.accessDenyOverlayView = nil;
    }
}

@end
