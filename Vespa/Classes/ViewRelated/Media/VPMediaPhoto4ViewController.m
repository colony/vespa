//
//  VPMediaPhoto4ViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/28/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaPhoto4ViewController.h"

#import "UINavigationItem+Util.h"
#import "VPCamView.h"
#import "VPConfigManager.h"
#import "VPFileManager.h"
#import "VPMediaAccessDenyView.h"
#import "VPMediaSubmitViewController.h"
#import "VPTutorialManager.h"



@interface VPMediaPhoto4ViewController ()<PBJVisionDelegate>

@property (strong, nonatomic) UIButton *flashButton;
@property (strong, nonatomic) VPButton *shotButton;
//@property (strong, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSTimer *progressTimer;
@property (strong, nonatomic) UIProgressView *progressView;

// If YES, it has started video record, could be paused or in the middle of capturing right now
@property (assign, nonatomic) BOOL hasStartedRecord;
@property (assign, nonatomic) BOOL isCapturing;
@property (assign, nonatomic) CGFloat maxCaptureDuration;

@property (strong, nonatomic) UILongPressGestureRecognizer *longpress;

@property (strong, nonatomic) VPMediaAccessDenyView *accessDenyOverlayView;

@property (strong, nonatomic) PBJVision *vision;

@end


@implementation VPMediaPhoto4ViewController

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.view.opaque = YES;
    self.view.backgroundColor = [UIColor blackColor];
    
    self.maxCaptureDuration = [[VPConfigManager sharedInstance] getFloat:kVPConfigMaxCaptureDuration];
    
    // Configure navbar
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0, 0, 30, 30);
    closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [closeBtn setImage:[UIImage imageNamed:@"close_button_black"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setCustomLeftWithView:closeBtn];
    [self.navigationItem tweakLeftWithSpace:-8];
    
//    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.nextButton.frame = CGRectMake(0, 0, 50, 30);
//    [self.nextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    self.nextButton.titleLabel.font = [VPUI fontOpenSans:15.0f thickness:3];
//    [self.nextButton setTitle:@"NEXT" forState:UIControlStateNormal];
//    [self.nextButton addTarget:self action:@selector(nextButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//    [self.navigationItem setCustomRightWithView:self.nextButton];
//    [self.navigationItem tweakRightWithSpace:-8];
    
    // Configure camview
    self.vision = [[PBJVision alloc] init];
    VPCamView *camView = [[VPCamView alloc] initWithVideoFrame:self.view.bounds vision:self.vision];
    camView.delegate = self;
    [self.view addSubview:camView];
    
    // Configure progressbar
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    self.progressView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth;
    self.progressView.progressTintColor = [VPUI colonyColor];
    self.progressView.width = self.view.width;
    self.progressView.y = 0.0f;
    [self.view addSubview:self.progressView];
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 10.0f);
    
    // Configure flashbutton and switchbutton
    self.flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.flashButton.frame = CGRectMake(0, self.view.height-50, 50, 50);
    self.flashButton.contentMode = UIViewContentModeCenter;
    self.flashButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_off"] forState:UIControlStateNormal];
    [self.flashButton addTarget:self action:@selector(flashButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.flashButton];
    
    UIButton *switchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    switchButton.frame = CGRectMake(self.view.width-50, self.view.height-50, 50, 50);
    switchButton.contentMode = UIViewContentModeCenter;
    switchButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin;
    [switchButton setImage:[UIImage imageNamed:@"cam_switch"] forState:UIControlStateNormal];
    [switchButton addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:switchButton];
    
    // Configure shot button
    self.shotButton = [[VPButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    self.shotButton.highlightAnimationEnabled = YES;
    self.shotButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    self.shotButton.centerX = self.view.centerX;
    self.shotButton.bottom = self.view.height - 40;
    [self.shotButton.layer setCornerRadius:35.0f];
    [self.shotButton.layer setBorderWidth:3.0f];
    [self.shotButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.shotButton.layer setMasksToBounds:YES];
    [self.shotButton setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:0.3]];
    self.longpress = [[UILongPressGestureRecognizer alloc] init];
    self.longpress.minimumPressDuration = 0.5f;
    [self.longpress addTarget:self action:@selector(handleShotButtonLongPress:)];
    [self.shotButton addGestureRecognizer:self.longpress];
    [self.view addSubview:self.shotButton];
}

- (void)setupUI
{
    [super setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.progressView setProgress:0.0f];
//    self.nextButton.hidden = YES;
    
    // Camera and audio access handling
    // NOTE, for VPMediaAccessDenyView, 1 - cam 2 - audio
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        if (granted) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
                ASYNC_MAIN({
                    if (granted) {
                        [[VPTutorialManager sharedInstance] start:kVPTutorialVideo];
                        [self showAccessDenyOverlayView:NO denyLabel:1];
                        [self.vision startPreview];
                    } else {
                        [self showAccessDenyOverlayView:YES denyLabel:2];
                    }
                });
            }];
        } else {
            ASYNC_MAIN({
                [self showAccessDenyOverlayView:YES denyLabel:1];
            });
        }
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.vision stopPreview];
    
    // HACK, after captured video, we will immediately transition to next VC, the shotButton
    // failed to shrink back (highlight will enlarge it), hence, we set it back here....
    self.shotButton.transform = CGAffineTransformIdentity;
}


#pragma mark - action handling
- (void)closeButtonPressed:(id)sender
{
//    [[PBJVision sharedInstance] resetAudio];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)flashButtonPressed:(id)sender
{
    PBJVision *vision = self.vision;
    PBJFlashMode mode = vision.flashMode;
    if (mode == PBJFlashModeOn) {
        vision.flashMode = PBJFlashModeOff;
        [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_off"] forState:UIControlStateNormal];
    } else {
        vision.flashMode = PBJFlashModeOn;
        [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_on"] forState:UIControlStateNormal];
    }
}

- (void)switchButtonPressed:(id)sender
{
    PBJVision *vision = self.vision;
    BOOL isBack = vision.cameraDevice == PBJCameraDeviceBack;
    vision.cameraDevice = isBack ? PBJCameraDeviceFront : PBJCameraDeviceBack;
    self.flashButton.hidden = isBack;
}

- (void)handleShotButtonLongPress:(UILongPressGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
//            self.nextButton.hidden = YES;
            
//            if (self.hasStartedRecord)
//                [self resumeCapture];
//            else
            if (self.isCapturing)
                return;
            [self startCapture];
            [self startProgress];
            break;
        }
    
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        {
            [self endCapture];
            [self stopProgress];
            
//            self.nextButton.hidden = NO;
            break;
        }

        default:
            break;
    }
}


#pragma mark - video recording
- (void)startCapture
{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    [self.vision startVideoCapture];
}

- (void)pauseCapture
{
    [self.vision pauseVideoCapture];
}

- (void)resumeCapture
{
    [self.vision resumeVideoCapture];
}

- (void)endCapture
{
    self.isCapturing = YES;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [self.vision endVideoCapture];
}


#pragma mark - PBJVision delegate
- (void)visionDidStartVideoCapture:(PBJVision * __nonnull)vision
{
    VPLog(@"visionDidStartVideoCapture");
    self.hasStartedRecord = YES;
}

- (void)visionDidPauseVideoCapture:(PBJVision * __nonnull)vision
{
    VPLog(@"visionDidPauseVideoCapture");
}

- (void)visionDidResumeVideoCapture:(PBJVision * __nonnull)vision
{
    VPLog(@"visionDidResumeVideoCapture");
}

- (void)visionDidEndVideoCapture:(PBJVision * __nonnull)vision
{
    VPLog(@"visionDidEndVideoCapture");
    self.hasStartedRecord = NO;
    
    // We reach here might because the maxCaptureDuration limit reached
    // We need reset longpress gesture here
    self.longpress.enabled = NO;
    self.longpress.enabled = YES;
}

- (void)vision:(PBJVision *)vision capturedVideo:(nullable NSDictionary *)videoDict error:(nullable NSError *)error
{
    self.isCapturing = NO;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    if (error) {
        VPLog(@"encount an error in video capture (%@)", error);
        [VPReports reportError:kVPErrorVideoCapture message:[error localizedDescription] error:error];
        return;
    }
    
#ifdef DEBUG
    VPLog(@"video file size = %llu", [[VPFileManager sharedInstance] sizeAtFilePath:kVPFileVideoRecordMP4]);
#endif
    
    // NOTE, we hardcode (VPCamView) the file to be kVPFileVideoRecordMP4, dont use [videoDict objectForKey:PBJVisionVideoPathKey]
    VPMediaSubmitViewController *submitVC = [[VPMediaSubmitViewController alloc] initWithData:@{
                                                                                                @"type":@"VIDEO",
                                                                                                @"videoFile":kVPFileVideoRecordMP4,
                                                                                                @"videoThumbnail":[videoDict objectForKey:PBJVisionVideoThumbnailKey]
                                                                                                }];
    [self.navigationController pushViewController:submitVC animated:YES];
    
    // Code for save video
    //    [_assetLibrary writeVideoAtPathToSavedPhotosAlbum:[NSURL URLWithString:videoPath] completionBlock:^(NSURL *assetURL, NSError *error1) {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Video Saved!" message: @"Saved to the camera roll."
    //                                                       delegate:self
    //                                              cancelButtonTitle:nil
    //                                              otherButtonTitles:@"OK", nil];
    //        [alert show];
    //    }];
    
    //    UIImage *videoThumbnail = [videoDict objectForKey:PBJVisionVideoThumbnailKey];
    //    UIImageWriteToSavedPhotosAlbum(videoThumbnail, nil, nil, nil);
}


#pragma mark - progress
- (void)progressChange
{
    if (![self.progressTimer isValid])
        return;
    
    if (self.progressView.progress >= 1.0f) {
        [self stopProgress];
        return;
    }
    
    CGFloat progress = self.progressView.progress + (0.01f / self.maxCaptureDuration);
    VPLog(@"progress %f", progress);
    [self.progressView setProgress:progress animated:YES];
}

- (void)startProgress
{
    self.progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                          target:self
                                                        selector:@selector(progressChange)
                                                        userInfo:nil
                                                         repeats:YES];
}

- (void)stopProgress
{
    [self.progressTimer invalidate];
    self.progressTimer = nil;
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - private
- (void)showAccessDenyOverlayView:(BOOL)visible denyLabel:(NSInteger)i
{
    if (visible && !self.accessDenyOverlayView) {
        self.accessDenyOverlayView = [[[NSBundle mainBundle] loadNibNamed:@"VPMediaAccessDenyView" owner:nil options:nil] objectAtIndex:0];
        self.accessDenyOverlayView.frame = self.view.bounds;
        [self.accessDenyOverlayView configureLabel:i];
        [self.view addSubview:self.accessDenyOverlayView];
        [self.view bringSubviewToFront:self.accessDenyOverlayView];
    } else if (!visible && self.accessDenyOverlayView) {
        self.accessDenyOverlayView.hidden = YES;
        [self.accessDenyOverlayView removeFromSuperview];
        self.accessDenyOverlayView = nil;
    }
}

@end
