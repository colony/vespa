//
//  VPMediaImageViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/20/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaPhotoViewController.h"

#import "QBImagePickerController.h"
//#import <MobileCoreServices/UTCoreTypes.h>
#import "UINavigationItem+Util.h"
#import "VPCamManager.h"
#import "VPCamView3.h"
#import "VPMediaAccessDenyView.h"
#import "VPMediaPhoto2ViewController.h"
#import "VPUtil.h"


@interface VPMediaPhotoViewController ()</*ELCImagePickerControllerDelegate*/QBImagePickerControllerDelegate>

@property (assign, nonatomic) BOOL isTakingPhoto;

@property (strong, nonatomic) UIButton *flashButton;
@property (strong, nonatomic) VPMediaAccessDenyView *accessDenyOverlayView;

@end

@implementation VPMediaPhotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Configure navbar
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0, 0, 30, 30);
    closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [closeBtn setImage:[UIImage imageNamed:@"close_button_black"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setCustomLeftWithView:closeBtn];
    [self.navigationItem tweakLeftWithSpace:-8];
    
    UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    uploadButton.frame = CGRectMake(0, 0, 100, 30);
    [uploadButton setImage:[UIImage imageNamed:@"cam_upload"] forState:UIControlStateNormal];
    [uploadButton addTarget:self action:@selector(uploadButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = uploadButton;

    self.flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.flashButton.frame = CGRectMake(0, 0, 30, 30);
    [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_off"] forState:UIControlStateNormal];
    [self.flashButton addTarget:self action:@selector(flashButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *flashItem = [[UIBarButtonItem alloc] initWithCustomView:self.flashButton];
    UIButton *switchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    switchButton.frame = CGRectMake(0, 0, 30, 30);
    [switchButton setImage:[UIImage imageNamed:@"cam_switch"] forState:UIControlStateNormal];
    [switchButton addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *switchItem = [[UIBarButtonItem alloc] initWithCustomView:switchButton];
    self.navigationItem.rightBarButtonItems = @[flashItem, switchItem];
    [self.navigationItem tweakRightWithSpace:-8];
}

- (void)setupUI
{
    [super setupUI];
    VPCamView3 *camView = [[VPCamView3 alloc] initWithFrame:self.view.bounds];
    [camView setAutoresizingMask:UIViewAutoresizingFill];
    [self.view insertSubview:camView atIndex:0];

    if ([VPCamManager sharedInstance].isFront) {
        self.flashButton.hidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Camera access handling
    VPAccess access = [VPUtil checkCameraAvailability];
    if (access == kVPAccessDenied) {
        [self showAccessDenyOverlayView:YES];
    } else if (access == kVPAccessUnknown) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                [self showAccessDenyOverlayView:NO];
                [[VPCamManager sharedInstance] startRunning];
            } else {
                [self showAccessDenyOverlayView:YES];
            }
        }];
    } else {
        [self showAccessDenyOverlayView:NO];
        [[VPCamManager sharedInstance] startRunning];
    }
}


#pragma mark - action handling
- (void)uploadButtonPressed:(id)sender
{
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.filterType = QBImagePickerControllerFilterTypePhotos;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.showsNumberOfSelectedAssets = NO;
    imagePickerController.maximumNumberOfSelection = 1;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)flashButtonPressed:(id)sender
{
    VPCamManager *camMgr = [VPCamManager sharedInstance];
    AVCaptureFlashMode mode = camMgr.flashMode;
    if (mode == AVCaptureFlashModeOn) {
        camMgr.flashMode = AVCaptureFlashModeOff;
        [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_off"] forState:UIControlStateNormal];
    } else { // Off
        camMgr.flashMode = AVCaptureFlashModeOn;
        [self.flashButton setImage:[UIImage imageNamed:@"cam_flash_on"] forState:UIControlStateNormal];
    }
}

- (void)switchButtonPressed:(id)sender
{
    VPCamManager *camMgr = [VPCamManager sharedInstance];
    
    if (camMgr.isFront) {
        self.flashButton.hidden = NO;
    } else {
        self.flashButton.hidden = YES;
    }
    [[VPCamManager sharedInstance] cameraToggle];
}

- (IBAction)shotButtonPressed:(id)sender
{
    if (self.isTakingPhoto)
        return;
    self.isTakingPhoto = YES;
    
    [[VPCamManager sharedInstance] captureStillImage:^(UIImage *image) {
        if ([VPCamManager sharedInstance].isFront) {
            image = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationLeftMirrored];
        }
        
        VPMediaPhoto2ViewController *nextStepVC = [[VPMediaPhoto2ViewController alloc] initWithImage:image];
        nextStepVC.isUpload = NO;
        [self.navigationController pushViewController:nextStepVC animated:YES];
        self.isTakingPhoto = NO;
    } failure:^(NSError *error) {
        VPLog(@"Fail to capture still image %@", [error localizedDescription]);
        self.isTakingPhoto = NO;
    }];
}


#pragma mark - QBImagePickerControllerDelegate
- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAssets:(NSArray *)assets
{
    ALAsset *asset = [assets firstObject];
    UIImage *img = nil;
    
    id type = [asset valueForProperty:ALAssetPropertyType];
    if (type) {
        ALAssetRepresentation *assetRep = [asset defaultRepresentation];
        if (assetRep) {
            CGImageRef imgRef = [assetRep fullScreenImage];
            img = [UIImage imageWithCGImage:imgRef];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (img && type == ALAssetTypePhoto) {
            VPMediaPhoto2ViewController *nextStepVC = [[VPMediaPhoto2ViewController alloc] initWithImage:img];
            nextStepVC.isUpload = YES;
            [self.navigationController pushViewController:nextStepVC animated:YES];
        }
    }];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - private
- (void)showAccessDenyOverlayView:(BOOL)visible
{
    if (visible && !self.accessDenyOverlayView) {
        self.accessDenyOverlayView = [[[NSBundle mainBundle] loadNibNamed:@"VPMediaAccessDenyView" owner:nil options:nil] objectAtIndex:0];
        self.accessDenyOverlayView.frame = self.view.bounds;
        [self.accessDenyOverlayView configureLabel:1];
        [self.view addSubview:self.accessDenyOverlayView];
        [self.view bringSubviewToFront:self.accessDenyOverlayView];
    } else if (!visible && self.accessDenyOverlayView) {
        self.accessDenyOverlayView.hidden = YES;
        [self.accessDenyOverlayView removeFromSuperview];
        self.accessDenyOverlayView = nil;
    }
}

@end
