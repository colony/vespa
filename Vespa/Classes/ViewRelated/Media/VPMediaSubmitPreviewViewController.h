//
//  VPMediaTestPreviewViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/22/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

@protocol VPMediaSubmitPreviewViewControllerDelegate;

@interface VPMediaSubmitPreviewViewController : VPBaseViewController

@property (weak, nonatomic) id<VPMediaSubmitPreviewViewControllerDelegate> delegate;

/**
 * referenceRect - 
 *      The previewVC will animate the image from its original position to center of screen (with aspect ratio)
 *      To do that, we need its original (aka reference) frame in WINDOW's coordinate system not superview's
 * image -
 *      The preview image
 * type -
 *      'AUDIO', 'YOUTUBE', 'IMAGE', 'GIF', 'VIDEO'
 * data -
 *      Additional data per specific type, if AUDIO, it contains playable audio path, if YOUTUBE, it contains
 *      youtube ID, if VIDEO, it contains
 * enablePicker, picked -
 *      When preview LINK post, we need user pick from a list of medias, the two parameter will display a 
 *      checkmark/cancel button at bottom
 */
- (id)initWithReferenceRect:(CGRect)referenceRect
                      image:(UIImage *)image
                previewType:(NSString *)type
                       data:(id)auxiliaryData
               enableScroll:(BOOL)enableScroll
               enablePicker:(BOOL)enablePicker
                     picked:(BOOL)picked;

- (void)showFromViewController:(UIViewController *)viewController;

- (void)dismiss;

@end

@protocol VPMediaSubmitPreviewViewControllerDelegate <NSObject>

/**
 * If enablePicker is YES when initialization and user pick (check or close) the preview, the delegate method will be called
 */
- (void)previewViewController:(VPMediaSubmitPreviewViewController *)viewController picked:(BOOL)picked;

@end

