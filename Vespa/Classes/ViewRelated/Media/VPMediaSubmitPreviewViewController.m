//
//  VPMediaTestPreviewViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/22/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaSubmitPreviewViewController.h"

#import "FLAnimatedImageView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIButton+Addition.h"
#import "VPAudioManager.h"
#import "VPPromptManager.h"
#import "VPUtil.h"
#import "VPYoutubeView.h"


@interface VPMediaSubmitPreviewViewController ()<VPAudioManagerDelegate, UIScrollViewDelegate, VPYoutubeViewDelegate>

@property (assign, nonatomic) CGRect referenceRect;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) FLAnimatedImageView *imageView;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIButton *audioButton;
@property (strong, nonatomic) VPYoutubeView *youtubeView;

@property (weak, nonatomic) IBOutlet UIButton *pickerButton;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) id data;
@property (assign, nonatomic) BOOL enableScroll;
@property (assign, nonatomic) BOOL enablePicker;
@property (assign, nonatomic) BOOL alreadyPicked;

@property (assign, nonatomic) BOOL isAudioPlaying;

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;

@end

@implementation VPMediaSubmitPreviewViewController

#pragma mark - lifecycle
- (id)initWithReferenceRect:(CGRect)referenceRect image:(UIImage *)image previewType:(NSString *)type data:(id)auxiliaryData enableScroll:(BOOL)enableScroll enablePicker:(BOOL)enablePicker picked:(BOOL)picked
{
    if (self = [super init]) {
        _referenceRect = referenceRect;
        _image = image;
        _type = type;
        _data = auxiliaryData;
        _enablePicker = enablePicker;
        _enableScroll = enableScroll;
        _alreadyPicked = picked;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    if (self.enablePicker) {
        [self.pickerButton setTitle:(self.alreadyPicked ? @"Deselect" : @"Select") forState:UIControlStateNormal];
        self.pickerButton.hidden = NO;
        self.pickerButton.layer.borderColor = [[UIColor whiteColor] CGColor];
        self.pickerButton.layer.borderWidth = 1.0f;
        self.pickerButton.layer.cornerRadius = 5.0f;
        [self.pickerButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    }
    
    self.backgroundView.alpha = 0.0f;

    self.imageView = [[FLAnimatedImageView alloc] initWithFrame:CGRectZero];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = YES;
    self.imageView.layer.allowsEdgeAntialiasing = YES;
    self.imageView.userInteractionEnabled = NO;
    self.imageView.shouldStartAnimatingAutomatically = NO;
    self.imageView.image = self.image;
    if ([self.type isEqualToString:@"GIF"]) {
        self.imageView.animatedImage = self.data;
        self.data = nil;
    }
    
    if (self.enableScroll) {
        self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        self.scrollView.autoresizingMask = UIViewAutoresizingFill;
        self.scrollView.delegate = self;
        self.scrollView.zoomScale = 1.0f;
        self.scrollView.maximumZoomScale = 8.0f;
        self.scrollView.scrollEnabled = YES;
        self.scrollView.showsHorizontalScrollIndicator = NO;
        self.scrollView.showsVerticalScrollIndicator = NO;
        [self.view insertSubview:self.scrollView aboveSubview:self.backgroundView];
    }
    
    if ([self.type isEqualToString:@"VIDEO"]) {
        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:self.data]];
        self.moviePlayer.shouldAutoplay = NO;
        self.moviePlayer.controlStyle = MPMovieControlStyleNone;
        self.moviePlayer.repeatMode = MPMovieRepeatModeOne;
        self.moviePlayer.view.autoresizingMask = UIViewAutoresizingFill;
        self.moviePlayer.view.frame = self.view.bounds;
        self.moviePlayer.view.userInteractionEnabled = NO;
        self.moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
        [self.moviePlayer prepareToPlay];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewSingleTapped:)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
}

- (void)showFromViewController:(UIViewController *)viewController
{
    CGRect referenceFrameInMyView = [self.view convertRect:self.referenceRect fromView:nil];
    self.imageView.frame = referenceFrameInMyView;
    [self.view insertSubview:self.imageView belowSubview:self.pickerButton];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0)) {
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    } else {
        viewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    }
    
    // Have to dispatch ahead two runloops,
    // or else the image view changes above won't be
    // committed prior to the animations below.
    //
    // Dispatching only one runloop ahead doesn't fix
    // the issue on certain devices.
    //
    // This issue also seems to be triggered by only
    // certain kinds of interactions with certain views,
    // especially when a UIButton is the reference
    // for the JTSImageInfo.
    //
    @weakify(self);
    [viewController presentViewController:self animated:NO completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                @strongify(self);
                [UIView animateWithDuration:0.3f
                                      delay:0
                                    options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                                     self.backgroundView.alpha = 1.0f;
                                     
                                     CGRect endFrameForImageView;
                                     if ([self.type isEqualToString:@"VIDEO"]) {
                                         // For video, we want scale up to fill entire view
                                         endFrameForImageView = self.view.bounds;
                                     } else {
                                         CGSize endSizeForImageView = [VPUtil sizeForImageWithAspectRatio:self.image.size parentViewSize:self.view.bounds.size];
                                         endFrameForImageView = CGRectMake(0, 0, endSizeForImageView.width, endSizeForImageView.height);
                                     }
                                     self.imageView.frame = endFrameForImageView;
                                     self.imageView.center = self.view.center;
                                 } completion:^(BOOL finished) {
                                     if ([self.type isEqualToString:@"AUDIO"]) {
                                         // If this is audio, we add a play button on top of preview image
                                         self.audioButton = [UIButton buttonWithType:UIButtonTypeCustom];
                                         [self.audioButton setImage:[UIImage imageNamed:@"audio_play"] forState:UIControlStateNormal];
                                         self.audioButton.frame = CGRectMake(0, 0, 120, 120);
                                         self.audioButton.center = self.imageView.center;
                                         [self.audioButton addTarget:self action:@selector(audioButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                                         [self.view addSubview:self.audioButton];
                                     } else if ([self.type isEqualToString:@"YOUTUBE"]) {
                                         self.youtubeView = [[VPYoutubeView alloc] init];
                                         self.youtubeView.videoId = self.data;
                                         self.youtubeView.delegate = self;
                                         self.youtubeView.frame = self.imageView.frame;
                                         [self.view addSubview:self.youtubeView];
                                     } else if ([self.type isEqualToString:@"VIDEO"]) {
                                     }
                                     
                                     if (self.enableScroll) {
                                         self.scrollView.frame = self.view.bounds;
                                         self.imageView.center = self.scrollView.center;
                                         [self.scrollView addSubview:self.imageView];
                                         self.scrollView.contentSize = self.imageView.frame.size;
                                     }
                                     
                                     if ([self.type isEqualToString:@"GIF"]) {
                                         [self.imageView startAnimating];
                                     }
                                     
                                     if (self.moviePlayer) {
                                         if ([self.moviePlayer readyForDisplay]) {
                                             [self showMoviePlayer];
                                         } else {
                                             [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMoviePlayer) name:MPMoviePlayerReadyForDisplayDidChangeNotification object:nil];
                                         }
                                     }
                                 }];
            });
        });
    }];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerReadyForDisplayDidChangeNotification object:nil];
}


#pragma mark - action handling
- (void)viewSingleTapped:(id)sender
{
    [self dismiss];
}

- (IBAction)pickerButtonPressed:(UIButton *)btn
{
    if ([self.delegate respondsToSelector:@selector(previewViewController:picked:)]) {
        [self.delegate previewViewController:self picked:!self.alreadyPicked];
    } else {
        [self dismiss];
    }
}

- (void)audioButtonPressed:(UIButton *)btn
{
    VPAudioManager *audioMgr = [VPAudioManager sharedInstance];
    if (self.isAudioPlaying) {
        self.isAudioPlaying = NO;
        [audioMgr stop];
    } else {
        [audioMgr playFile:self.data delegate:self];
    }
}


#pragma mark - VPAudioManagerDelegate
- (void)VPAudioManagerStart:(VPAudioManager *)manager
{
    [self.audioButton setImage:[UIImage imageNamed:@"audio_stop"] forState:UIControlStateNormal];
}

- (void)VPAudioManagerStop:(VPAudioManager *)manager
{
    [self.audioButton setImage:[UIImage imageNamed:@"audio_play"] forState:UIControlStateNormal];
    self.isAudioPlaying = NO;
}

- (void)VPAudioManagerEnd:(VPAudioManager *)manager
{
    [self.audioButton setImage:[UIImage imageNamed:@"audio_play"] forState:UIControlStateNormal];
    self.isAudioPlaying = NO;
}


#pragma mark - VPYoutubeViewDelegate
- (void)youtubeViewFailLoad:(VPYoutubeView *)youtubeView alternativeURL:(NSString *)alternativeYoutubeWebURL
{
    [[VPPromptManager sharedInstance] prompt:kVPPromptWeb data:alternativeYoutubeWebURL onlyOnce:NO overContext:NO animated:YES];
}


#pragma mark - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerScrollViewContents];
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - private
//http://www.raywenderlich.com/10518/how-to-use-uiscrollview-to-scroll-and-zoom-content
- (void)centerScrollViewContents
{
    // if the scroll view content size is smaller than its bounds, then it sits at the top-left rather than in the center
    // The method accomplishes that by positioning the image view such that is is always in the center of the scrollview bounds
    
    CGSize boundsSize = self.scrollView.bounds.size;
    CGRect contentsFrame = self.imageView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.imageView.frame = contentsFrame;
}

- (void)dismiss
{
    if (self.audioButton) {
        self.audioButton.hidden = YES;
        [self.audioButton removeFromSuperview];
        self.audioButton = nil;
    }
    
    if (self.youtubeView) {
        self.youtubeView.hidden = YES;
        [self.youtubeView removeFromSuperview];
        self.youtubeView = nil;
    }
    
    if (self.moviePlayer) {
        [self.moviePlayer stop];
        self.moviePlayer.view.hidden = YES;
        [self.moviePlayer.view removeFromSuperview];
    }
    
    if (self.scrollView) {
        CGRect imageFrame = [self.view convertRect:self.imageView.frame fromView:self.scrollView];
        self.imageView.transform = CGAffineTransformIdentity;
        self.imageView.layer.transform = CATransform3DIdentity;
        [self.imageView removeFromSuperview];
        self.imageView.frame = imageFrame;
        [self.view addSubview:self.imageView];
        self.scrollView.hidden = YES;
        [self.scrollView removeFromSuperview];
        self.scrollView = nil;
    }

    self.pickerButton.hidden = YES;
    
    [UIView animateWithDuration:0.3f
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.backgroundView.alpha = 0.0f;
                         CGRect referenceFrameInMyView = [self.view convertRect:self.referenceRect fromView:nil];
                         CGRect endFrameForImageView = referenceFrameInMyView;
                         self.imageView.frame = endFrameForImageView;
                     } completion:^(BOOL finished) {
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];

}

- (void)showMoviePlayer
{
    if (self.moviePlayer && self.moviePlayer.readyForDisplay) {
        self.moviePlayer.view.frame = self.view.bounds;
        [self.view insertSubview:self.moviePlayer.view aboveSubview:self.imageView];
        [self.moviePlayer play];
    }
}

@end
