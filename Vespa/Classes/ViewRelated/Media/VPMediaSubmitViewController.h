//
//  VPMediaSubmitViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/16/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

@interface VPMediaSubmitViewController : VPBaseViewController

/**
 * The param 'data' contains the following content:
 *
 * {
 *    type: enum string 'AUDIO','LINK','PHOTO',TEXT'
 *
 *    // AUDIO
 *    audioFile: '',
 *    audioFilterName: '',
 *
 *    // LINK
 *    title: '',
 *    url: '',
 *    medias:[
 *      {
 *        source
 *        youtubeTumbnail
 *        type
 *      }
 *    ]
 *
 *    // PHOTO
 *    image: object [UIImage]
 *    uploaded: boolean, true if the image is provided from camera roll, false if it is taken
 *    filtered: boolean, true if the image is filtered, false if it is original
 *
 *    // TEXT
 *    text: object [NSString]
 *    backgroundColor: object [UIColor]
 *
 *    // VIDEO
 *    videoFile: object [NSString], local file of recorded video
 *    videoThumbnail: object [UIImage], the image of first frame of the video
 * }
 */
- (id)initWithData:(NSDictionary *)data;

@end
