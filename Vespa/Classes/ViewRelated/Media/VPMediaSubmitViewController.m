
//
//  VPMediaSubmitViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/16/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaSubmitViewController.h"

#import "FLAnimatedImage.h"
#import "FLAnimatedImageView.h"
#import "OLGhostAlertView.h"
#import "UIAlertView+Blocks.h"
#import "UINavigationItem+Util.h"
#import "UIView+Frame.h"
#import "VPBaseCollectionViewCell.h"
#import "VPConfigManager.h"
#import "VPFileManager.h"
#import "VPGroupPickViewController.h"
#import "VPGroups.h"
#import "VPHonorCodeViewController.h"
#import "UIImage+Addition.h"
#import "VPMediaSubmitPreviewViewController.h"
#import "VPNavigationController.h"
#import "VPPosts.h"
#import "VPPromptManager.h"
#import "VPViewRedirectionUtil.h"


#define TITLE_TEXTVIEW_TAG 1
#define BODY_TEXTVIEW_TAG  2


@interface VPMediaSubmitCell : VPBaseCollectionViewCell

@property (weak, nonatomic) IBOutlet FLAnimatedImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *pickIndicator;
@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (weak, nonatomic) IBOutlet UIImageView *playableIndicator;

@end

@implementation VPMediaSubmitCell

- (void)setupUI
{
    self.imageView.shouldStartAnimatingAutomatically = NO;
}

- (void)setData:(id)data forType:(NSString *)type
{
    // Depends on different type, we need tweak our cell's appearance
    self.playableIndicator.hidden = YES;
    if ([type isEqualToString:@"LINK"]) {
        NSDictionary *dict = (NSDictionary *)data;
        NSString *linkItemType = [dict objectForKey:@"type"];
        if ([linkItemType isEqualToString:@"YOUTUBE"]) {
            self.playableIndicator.image = [UIImage imageNamed:@"youtube_play_button"];
            self.playableIndicator.hidden = NO;
            NSString *videoId = [dict objectForKey:@"youtubeVideoId"];
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://i1.ytimg.com/vi/%@/hqdefault.jpg", videoId]]];
        } else if ([linkItemType isEqualToString:@"GIF"]) {
            self.playableIndicator.image = [UIImage imageNamed:@"gif_indicator"];
            self.playableIndicator.hidden = NO;
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"source"]]];
        } else {
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"source"]]];
        }
        self.maskView.hidden = NO;
    } else if ([type isEqualToString:@"VIDEO"]) {
        self.playableIndicator.image = [UIImage imageNamed:@"video_play_button"];
        self.playableIndicator.hidden = NO;
        self.imageView.image = data;
        self.maskView.hidden = YES;
    } else {
        self.imageView.image = data;
        self.maskView.hidden = YES;
    }
}

- (void)setPicked:(BOOL)picked
{
    self.pickIndicator.alpha = picked ? 1.0 : 0.0;
    self.maskView.hidden = picked ? YES : NO;
}

- (void)resetCell
{
    [super resetCell];
}

@end


@interface VPMediaSubmitViewController ()<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, VPHonorCodeViewControllerDelegate, UITextViewDelegate, VPMediaSubmitPreviewViewControllerDelegate, VPGroupPickViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UILabel *bodyDescriptionLabel;
@property (weak, nonatomic) IBOutlet UITextView *bodyTextView;
@property (weak, nonatomic) IBOutlet UIView *bodyTextWrapperView;
@property (weak, nonatomic) IBOutlet UILabel *bodyTextViewPlaceholderLabel;
@property (weak, nonatomic) IBOutlet UIButton *suggestButton;

@property (weak, nonatomic) IBOutlet UIView *urlTextWrapperView;
@property (weak, nonatomic) IBOutlet UILabel *urlTextLabel;

@property (weak, nonatomic) IBOutlet UIView *groupPickWrapperView;
@property (weak, nonatomic) IBOutlet UIButton *groupPickButton;
@property (weak, nonatomic) IBOutlet UIImageView *groupPickRightArrowImageView;
@property (strong, nonatomic) NSNumber *currentViewGroupId;

// Context data
@property (strong, nonatomic) NSNumber *currentPickedGroupId;
@property (strong, nonatomic) NSString *currentPickedGroupName;
@property (strong, nonatomic) NSNumber *currentThreadId;
@property (strong, nonatomic) NSString *createThreadType;

@property (weak, nonatomic) IBOutlet UIView *noMediaIndicatorImageView;
@property (strong, nonatomic) UIView *blackMaskView;

@property (strong, nonatomic) NSMutableArray *items;

// 'AUDIO', 'LINK', 'PHOTO', 'TEXT'
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) id data;

@property (assign, nonatomic) BOOL isPosting;

// Picker
@property (assign, nonatomic) NSInteger pickedItemIndex;
@property (assign, nonatomic) NSInteger currentPreviewingItemIndex;

@end


@implementation VPMediaSubmitViewController

#pragma mark - lifecycle
- (id)initWithData:(NSDictionary *)data
{
    if (self = [super init]) {
        _type = data[@"type"];
        _data = data;
        _currentPreviewingItemIndex = -1;
        _pickedItemIndex = -1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.items = [NSMutableArray array];
    
    // Configure collection view and other views
    // We need convert text to an image for displaying effect
    if ([self.type isEqualToString:@"AUDIO"]) {
        self.bodyDescriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Add a Caption" attributes:@{
                                                                                                                            NSFontAttributeName:[VPUI fontOpenSans:13.0 thickness:3],
                                                                                                                            NSForegroundColorAttributeName:UIColorFromRGB(0x66696D)
                                                                                                                            }];
        
        self.noMediaIndicatorImageView.hidden = YES;
        [self.items addObject:[UIImage imageNamed:@"audio_submit_placeholder"]];
    }
    else if ([self.type isEqualToString:@"LINK"]) {
        NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithString:@"Add a Title (Req)" attributes:@{
                                                                                                                                 NSFontAttributeName:[VPUI fontOpenSans:13.0 thickness:3]
                                                                                                                                 }];
        [attrString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x66696D) range:NSMakeRange(0,12)];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(12,5)];
        self.bodyDescriptionLabel.attributedText = attrString;

        NSDictionary *dict = (NSDictionary *)self.data;
//        if ([dict objectForKey:@"title"]) {
//            self.bodyTextView.text = dict[@"title"];
//            self.bodyTextViewPlaceholderLabel.hidden = [self.bodyTextView hasText];
//            self.postButton.enabled = [self.bodyTextView hasText];
//        }
        
        self.urlTextLabel.text = dict[@"url"];
        self.items = [dict objectForKey:@"medias"];
        if ([self.items count] <= 0) {
            self.noMediaIndicatorImageView.hidden = NO;
            self.pickedItemIndex = -1;
        } else {
            self.noMediaIndicatorImageView.hidden = YES;
            self.pickedItemIndex = 0; // By default first item is picked
        }
        self.suggestButton.hidden = NO;
    }
    else if ([self.type isEqualToString:@"PHOTO"]) {
        self.bodyDescriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Add a Caption" attributes:@{
                                                                                                                            NSFontAttributeName:[VPUI fontOpenSans:13.0 thickness:3],
                                                                                                                            NSForegroundColorAttributeName:UIColorFromRGB(0x66696D)
                                                                                                                            }];
        self.noMediaIndicatorImageView.hidden = YES;
        [self.items addObject:self.data[@"image"]];
    }
    else if ([self.type isEqualToString:@"TEXT"]) {
        self.noMediaIndicatorImageView.hidden = YES;
        UIImage *image = [self convertTextToImage:self.data[@"text"] background:self.data[@"backgroundColor"]];
        [self.items addObject:image];
    }
    else if ([self.type isEqualToString:@"VIDEO"]) {
        self.bodyDescriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Add a Caption" attributes:@{
                                                                                                                            NSFontAttributeName:[VPUI fontOpenSans:13.0 thickness:3],
                                                                                                                            NSForegroundColorAttributeName:UIColorFromRGB(0x66696D)
                                                                                                                            }];
        self.noMediaIndicatorImageView.hidden = YES;
        [self.items addObject:self.data[@"videoThumbnail"]];
    }

    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.alwaysBounceHorizontal = YES;
    UINib *nib = [UINib nibWithNibName:@"VPMediaSubmitCell" bundle:nil];
    if (nib)
        [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"VPMediaSubmitCell"];
    
    // Configure navbar
    [self.navigationItem setCustomTitle:@"Your Post" textColor:[VPUI colonyColor] font:nil];
    
    // Configure text related
    if ([self.type isEqualToString:@"TEXT"]) {
        self.bodyTextWrapperView.hidden = YES;
        [self.bodyTextWrapperView removeFromSuperview];
        self.bodyTextWrapperView = nil;
        self.urlTextWrapperView.hidden = YES;
        [self.urlTextWrapperView removeFromSuperview];
        self.urlTextWrapperView = nil;
        self.topView.height -= (140 + 60);
        self.groupPickWrapperView.y -= (140 + 60);
    } else if ([self.type isEqualToString:@"AUDIO"] || [self.type isEqualToString:@"PHOTO"] || [self.type isEqualToString:@"VIDEO"]) {
        self.urlTextWrapperView.hidden = YES;
        [self.urlTextWrapperView removeFromSuperview];
        self.urlTextWrapperView = nil;
        self.topView.height -= 60;
        self.groupPickWrapperView.y -= 60;
    }

    if (self.bodyTextWrapperView) {
        self.bodyTextView.delegate = self;
    }
    
    // Setup current picked group
    self.currentPickedGroupId = [[VPContext sharedInstance] getDomain:kVPCDMediaSubmission key:@"groupId"];
    self.currentPickedGroupName = [[VPContext sharedInstance] getDomain:kVPCDMediaSubmission key:@"groupName"];
    self.currentThreadId = [[VPContext sharedInstance] getDomain:kVPCDMediaSubmission key:@"threadId"];
    self.createThreadType = [[VPContext sharedInstance] getDomain:kVPCDMediaSubmission key:@"createThreadType"];
    [[VPContext sharedInstance] clearDomain:kVPCDMediaSubmission];

    // If we do have stuff in VPContext, we reach to this VC from VPGroupVC, otherwise we need to pick the default configured one
    // Also, if we are within a group, we disable the pick list
    if (self.currentPickedGroupId) {
        self.currentViewGroupId = self.currentPickedGroupId;
        [self.groupPickButton setTitle:[self.currentPickedGroupName stringByAppendingString:@" (Current)"] forState:UIControlStateNormal];
        self.groupPickButton.enabled = NO;
        self.groupPickRightArrowImageView.hidden = YES;
    } else {
        self.currentViewGroupId = nil;
        // If server doesn't return general group in pickList, we will force user to choose one instead of using General as default
        if ([VPGroups containsGeneralInGroupPickList]) {
            self.currentPickedGroupId = @1;
            self.currentPickedGroupName = @"General";
            [self.groupPickButton setTitle:self.currentPickedGroupName forState:UIControlStateNormal];
        }
    }
    
    // Register notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark - action handling
- (IBAction)postButtonPressed:(id)sender
{
    if (self.isPosting)
        return;
    
    if (!self.currentPickedGroupId) {
        [UIAlertView showWithTitle:nil message:@"You must choose a tribe first." cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    if (![self.type isEqualToString:@"TEXT"] && ![self.bodyTextView hasText]) {
        if ([self.type isEqualToString:@"LINK"]) {
            [UIAlertView showWithTitle:nil message:@"Plz add a title" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        } else {
            [UIAlertView showWithTitle:nil message:@"Plz add a caption" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        }
        return;
    }
    
    BOOL prompted = [[VPPromptManager sharedInstance] prompt:kVPPromptHonorCode data:self onlyOnce:YES overContext:NO animated:NO];
    if (prompted) return;
    
    self.isPosting = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil userInfo:@{@"status":@"Uploading"}];
    
    @weakify(self);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @strongify(self);
        
        void (^createPostSuccess)(VPPost*) = ^(VPPost *post) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    OLGhostAlertView *alertView = [[OLGhostAlertView alloc] initWithTitle:@"Posted!"
                                                                                  message:nil
                                                                                  timeout:0.2
                                                                              dismissible:NO];
                    alertView.position = OLGhostAlertViewPositionCenter;
                    [alertView show];
                }];
            });
            self.isPosting = NO;
        };
        
        void (^createPostFailure)(NSError*) = ^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    OLGhostAlertView *alertView = [[OLGhostAlertView alloc] initWithTitle:@"Failed! Please retry"
                                                                                  message:nil
                                                                                  timeout:0.2
                                                                              dismissible:NO];
                    alertView.position = OLGhostAlertViewPositionCenter;
                    [alertView show];
                }];
//                [VPViewRedirectionUtil handleInternal:kVPFeedRedirectAction param:nil controller:self];
            });
            self.isPosting = NO;
        };
        
        if ([self.type isEqualToString:@"AUDIO"]) {
            [VPPosts createPostWithAudio:self.data[@"audioFile"]
                                duration:[self.data[@"audioDuration"] doubleValue] * 1000 // convert to ms, truncate the decimal points
                                 caption:self.bodyTextView.text
                      filterNameToReport:self.data[@"audioFilterName"]
                                 groupId:self.currentPickedGroupId
                        createThreadType:self.createThreadType
                                threadId:self.currentThreadId
                                 success:createPostSuccess
                                 failure:createPostFailure];
        } else if ([self.type isEqualToString:@"LINK"]) {
            if (self.pickedItemIndex < 0) {
                // No media is picked or found
                [VPPosts createPostWithLink:self.data[@"url"]
                                      title:self.bodyTextView.text
                              embedImageUrl:nil
                           embedImageHeight:0
                            embedImageWidth:0
                             embedYoutubeId:nil
                                  embedType:nil
                                    groupId:self.currentPickedGroupId
                           createThreadType:self.createThreadType
                                   threadId:self.currentThreadId
                                    success:createPostSuccess
                                    failure:createPostFailure];
            } else {
                NSDictionary *dict = (NSDictionary *)[self.items objectAtIndex:self.pickedItemIndex];
                NSString *linkItemType = [dict objectForKey:@"type"];
                VPMediaSubmitCell *cell = (VPMediaSubmitCell *)[self collectionView:self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:self.pickedItemIndex inSection:0]];
                CGSize imageSize = cell.imageView.image.size;
                if ([linkItemType isEqualToString:@"YOUTUBE"]) {
                    NSString *videoId = [dict objectForKey:@"youtubeVideoId"];
                    [VPPosts createPostWithLink:self.data[@"url"]
                                          title:self.bodyTextView.text
                                  embedImageUrl:nil
                               embedImageHeight:imageSize.height
                                embedImageWidth:imageSize.width
                                 embedYoutubeId:videoId
                                      embedType:@"YOUTUBE"
                                        groupId:self.currentPickedGroupId
                               createThreadType:self.createThreadType
                                       threadId:self.currentThreadId
                                        success:createPostSuccess
                                        failure:createPostFailure];
                } else {
                    [VPPosts createPostWithLink:self.data[@"url"]
                                          title:self.bodyTextView.text
                                  embedImageUrl:[dict objectForKey:@"source"]
                               embedImageHeight:imageSize.height
                                embedImageWidth:imageSize.width
                                 embedYoutubeId:nil
                                      embedType:linkItemType // GIF or IMAGE
                                        groupId:self.currentPickedGroupId
                               createThreadType:self.createThreadType
                                       threadId:self.currentThreadId
                                        success:createPostSuccess
                                        failure:createPostFailure];
                }
            }
        } else if ([self.type isEqualToString:@"PHOTO"]) {
            [VPPosts createPostWithPhoto:self.data[@"image"]
                                 caption:self.bodyTextView.text
                                uploaded:[self.data[@"uploaded"] boolValue]
                                filtered:[self.data[@"filtered"] boolValue]
                                 groupId:self.currentPickedGroupId
                        createThreadType:self.createThreadType
                                threadId:self.currentThreadId
                                 success:createPostSuccess
                                 failure:createPostFailure];
        } else if ([self.type isEqualToString:@"TEXT"]) {
            [VPPosts createPostWithText:self.data[@"text"]
                        backgroundColor:self.data[@"backgroundColor"]
                                groupId:self.currentPickedGroupId
                       createThreadType:self.createThreadType
                               threadId:self.currentThreadId
                                success:createPostSuccess
                                failure:createPostFailure];
        } else if ([self.type isEqualToString:@"VIDEO"]) {
            [VPPosts createPostWithVideo:self.data[@"videoFile"]
                               thumbnail:self.data[@"videoThumbnail"]
                                 caption:self.bodyTextView.text
                                 groupId:self.currentPickedGroupId
                        createThreadType:self.createThreadType
                                threadId:self.currentThreadId
                                 success:createPostSuccess
                                 failure:createPostFailure];
        }
    });
}

- (void)textEditingOkButtonPressed:(id)sender
{
    [self.view endEditing:YES];
}

- (void)blackMaskViewTapped:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)groupPickButtonPressed:(id)sender
{
    VPGroupPickViewController *pickVC = [[VPGroupPickViewController alloc] initWithMediaType:self.type
                                                                              currentGroupId:self.currentViewGroupId
                                                                               pickedGroupId:self.currentPickedGroupId];
    pickVC.delegate = self;
    VPNavigationController *nc = [[VPNavigationController alloc] initWithRootViewController:pickVC];
    nc.customNavigatonGestureEnabled = NO;
    [nc configureDefaultStyle];
    [self presentViewController:nc animated:YES completion:nil];
}

- (IBAction)suggestButtonPressed:(id)sender
{
    if ([self.data objectForKey:@"title"]) {
        self.bodyTextView.text = self.data[@"title"];
        self.bodyTextViewPlaceholderLabel.hidden = [self.bodyTextView hasText];
    }
}


#pragma mark - keyboard handling
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat kbAnimationTime = [info[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve kbAnimationCurve = [info[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];

    [UIView animateWithDuration:kbAnimationTime
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState|(kbAnimationCurve<<16)
                     animations:^{
                         self.topView.y = -self.bodyTextWrapperView.y;
                     } completion:^(BOOL finished){
                         if (!self.blackMaskView) {
                             CGFloat blackTop = self.bodyTextWrapperView.height;
                             self.blackMaskView = [[UIView alloc] initWithFrame:CGRectMake(0, blackTop, SCREEN_WIDTH, self.view.height - kbSize.height - blackTop)];
                             self.blackMaskView.opaque = YES;
                             self.blackMaskView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.65];
                             self.blackMaskView.userInteractionEnabled = YES;
                             [self.blackMaskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blackMaskViewTapped:)]];
                             [self.view addSubview:self.blackMaskView];
                         }
                     }];
    
    // Change navbar
    // HACK: disable the implicit animation when changing stuff on navbar (ios8)
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    [self.navigationItem setCustomTitle:@"TITLE" textColor:[VPUI colonyColor] font:nil];
    [self.navigationItem setCustomRightWithTitle:@"OK" textColor:[VPUI colonyColor] font:nil target:self action:@selector(textEditingOkButtonPressed:)];
    [CATransaction commit];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    CGFloat kbAnimationTime = [info[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve kbAnimationCurve = [info[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    
    if (self.blackMaskView) {
        self.blackMaskView.hidden = YES;
        [self.blackMaskView removeFromSuperview];
        self.blackMaskView = nil;
    }
    [UIView animateWithDuration:kbAnimationTime
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState|(kbAnimationCurve<<16)
                     animations:^{
                         self.topView.y = 0;
                     } completion:nil];
    
    // Change navbar back
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    [self.navigationItem setCustomTitle:@"Your Post" textColor:[UIColor blackColor] font:nil];
    self.navigationItem.rightBarButtonItem = nil;
    [CATransaction commit];
}


#pragma mark - collectionview delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.items count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Each cell is square appearance
    return CGSizeMake(100.0f, 100.0f);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    VPMediaSubmitCell *cell = (VPMediaSubmitCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"VPMediaSubmitCell" forIndexPath:indexPath];
    [cell setData:self.items[indexPath.row] forType:self.type];
    if ([self.type isEqualToString:@"LINK"]) {
        [cell setPicked:(self.pickedItemIndex == indexPath.row)];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    VPMediaSubmitCell *cell = (VPMediaSubmitCell *)[self collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellRect = attributes.frame;
    CGRect cellRectInWindow = [collectionView convertRect:cellRect toView:nil];
    
    self.currentPreviewingItemIndex = indexPath.row;
    
    VPMediaSubmitPreviewViewController *previewVC;
    if ([self.type isEqualToString:@"AUDIO"]) {
        previewVC = [[VPMediaSubmitPreviewViewController alloc] initWithReferenceRect:cellRectInWindow
                                                                              image:self.items[indexPath.row]
                                                                        previewType:@"AUDIO"
                                                                               data:[[VPFileManager sharedInstance] pathForFile:self.data[@"audioFile"]]
                                                                         enableScroll:NO
                                                                         enablePicker:NO
                                                                               picked:NO];
    } else if ([self.type isEqualToString:@"LINK"]) {
        NSDictionary *dict = (NSDictionary *)self.items[indexPath.row];
        NSString *linkItemType = [dict objectForKey:@"type"];
        NSString *previewType = linkItemType;
        id additionalData = nil;
        if ([linkItemType isEqualToString:@"YOUTUBE"]) {
            additionalData = [dict objectForKey:@"youtubeVideoId"];
        } else if ([linkItemType isEqualToString:@"GIF"]) {
            additionalData = cell.imageView.animatedImage;
        }
        previewVC = [[VPMediaSubmitPreviewViewController alloc] initWithReferenceRect:cellRectInWindow
                                                                                image:cell.imageView.image
                                                                          previewType:previewType
                                                                                 data:additionalData
                                                                         enableScroll:additionalData==nil
                                                                         enablePicker:YES
                                                                               picked:(indexPath.row == self.pickedItemIndex)];
    } else if ([self.type isEqualToString:@"PHOTO"]) {
        previewVC = [[VPMediaSubmitPreviewViewController alloc] initWithReferenceRect:cellRectInWindow
                                                                              image:self.items[indexPath.row]
                                                                        previewType:@"IMAGE"
                                                                               data:nil
                                                                         enableScroll:YES
                                                                         enablePicker:NO
                                                                               picked:NO];
    } else if ([self.type isEqualToString:@"TEXT"]) {
        previewVC = [[VPMediaSubmitPreviewViewController alloc] initWithReferenceRect:cellRectInWindow
                                                                              image:self.items[indexPath.row]
                                                                        previewType:@"IMAGE"
                                                                               data:nil
                                                                         enableScroll:NO
                                                                         enablePicker:NO
                                                                               picked:NO];
    } else if ([self.type isEqualToString:@"VIDEO"]) {
        previewVC = [[VPMediaSubmitPreviewViewController alloc] initWithReferenceRect:cellRectInWindow
                                                                                image:self.items[indexPath.row]
                                                                          previewType:@"VIDEO"
                                                                                 data:[[VPFileManager sharedInstance] pathForFile:self.data[@"videoFile"]]
                                                                         enableScroll:NO
                                                                         enablePicker:NO
                                                                               picked:NO];
    }
    if (previewVC) {
        previewVC.delegate = self;
        [previewVC showFromViewController:self.navigationController];
    }
}


#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.bodyTextViewPlaceholderLabel.hidden = [textView hasText];
}


#pragma mark - VPHonorcodeViewControllerDelegate
- (void)agreeButtonPressed:(VPHonorCodeViewController *)viewController
{
    [viewController dismissViewControllerAnimated:NO completion:nil];
    [self postButtonPressed:nil]; // Resume posting, simulate the press action
}


#pragma mark - VPMediaSubmitPreviewViewControllerDelegate
- (void)previewViewController:(VPMediaSubmitPreviewViewController *)viewController picked:(BOOL)picked
{
    self.pickedItemIndex = picked ? self.currentPreviewingItemIndex : -1;
    [self.collectionView reloadData];
    [viewController dismiss];
}


#pragma mark - VPGroupPickViewControllerDelegate
- (void)groupPickViewControllerDidPicked:(VPGroupPickViewController *)viewController groupId:(NSNumber *)groupId groupName:(NSString *)groupName
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
    self.currentPickedGroupId = groupId;
    self.currentPickedGroupName = groupName;
    [self.groupPickButton setTitle:groupName forState:UIControlStateNormal];
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - private
- (UIImage *)convertTextToImage:(NSString *)drawText background:(UIColor *)backgroundColor
{
    CGFloat width = SCREEN_WIDTH;
    CGFloat horizontalPadding = 15.0f;
    CGFloat verticalPadding = 80.0f;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSDictionary *textAttrs = @{
                                NSFontAttributeName:[VPUI fontOpenSans:17.0f thickness:2],
                                NSForegroundColorAttributeName:[UIColor blackColor],
                                NSParagraphStyleAttributeName:paragraphStyle
                                };
    
    CGRect drawRect = [drawText boundingRectWithSize:CGSizeMake(width - 2 * horizontalPadding, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                          attributes:textAttrs
                                             context:nil];

    CGRect rect = CGRectMake(0, 0, width, drawRect.size.height + 2 * verticalPadding);
    UIGraphicsBeginImageContextWithOptions(rect.size, YES, 0);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), [backgroundColor CGColor]);
    CGContextFillRect(UIGraphicsGetCurrentContext(), rect);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), [[UIColor whiteColor] CGColor]);
    [drawText drawInRect:CGRectMake((width - drawRect.size.width)/2, (rect.size.height - drawRect.size.height)/2, drawRect.size.width, drawRect.size.height) withAttributes:textAttrs];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

@end
