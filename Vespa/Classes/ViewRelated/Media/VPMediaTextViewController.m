//
//  VPMediaTextViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMediaTextViewController.h"

#import "UIButton+Addition.h"
#import "UINavigationItem+Util.h"
#import "VPMediaSubmitViewController.h"


@interface VPMediaTextViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (assign, nonatomic) BOOL navbarNextShown;

@property (strong, nonatomic) NSString *offscreenText;

@end

@implementation VPMediaTextViewController

#pragma mark - lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Configure navbar
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0, 0, 30, 30);
    closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [closeBtn setImage:[UIImage imageNamed:@"close_button_black"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setCustomLeftWithView:closeBtn];
    [self.navigationItem tweakLeftWithSpace:-8];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)setupUI
{
    [super setupUI];
}


#pragma mark - UITextView delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([self.textView isFirstResponder]) {
        [self.textView resignFirstResponder];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    self.placeholderLabel.hidden = YES;
    return YES;
}

//- (BOOL)textViewShouldEndEditing:(UITextView *)textView
//{
//    self.placeholderLabel.hidden = [textView.text length] > 0;
//    return YES;
//}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
//    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//        return NO;
//    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
//    static const NSUInteger MAX_NUMBER_OF_LINES_ALLOWED = 8;
//    
//    //https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/TextLayout/Tasks/CountLines.html
//    // Since, newline, will resignResponder and dismiss keyboard, we count lines of WRAPPED text here
//    NSLayoutManager *layoutManager = [textView layoutManager];
//    NSUInteger numberOfLines, index, numberOfGlyphs = [layoutManager numberOfGlyphs];
//    NSRange lineRange;
//    for (numberOfLines = 0, index = 0; index < numberOfGlyphs; numberOfLines++)
//    {
//        (void) [layoutManager lineFragmentRectForGlyphAtIndex:index
//                                               effectiveRange:&lineRange];
//        index = NSMaxRange(lineRange);
//    }
//    
//    if (numberOfLines > MAX_NUMBER_OF_LINES_ALLOWED)
//    {
//        // roll back
//        textView.text = _offscreenText;
//    }
//    else
//    {
//        // change accepted
//        _offscreenText = textView.text;
//    }
    
    if ([textView hasText]) {
        if (!self.navbarNextShown) {
            [self.navigationItem setCustomRightWithTitle:@"NEXT" textColor:[UIColor blackColor] font:[VPUI fontOpenSans:15.0f thickness:3] target:self action:@selector(nextButtonPressed:)];
            self.navbarNextShown = YES;
        }
    } else {
        self.navigationItem.rightBarButtonItem = nil;
        self.navbarNextShown = NO;
    }
}


#pragma mark - keyboard handling
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.textView.height = self.view.height - kbSize.height - 10.0f;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.textView.height = self.view.height;
}


#pragma mark - action handling
- (void)nextButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    VPMediaSubmitViewController *submitVC = [[VPMediaSubmitViewController alloc] initWithData:@{
                                                                                                @"type":@"TEXT",
                                                                                                @"text":self.textView.text,
                                                                                                @"backgroundColor":self.view.backgroundColor
                                                                                            }];
    [self.navigationController pushViewController:submitVC animated:YES];
}

- (void)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
