//
//  VPPhotoView.h
//  Vespa
//
//  Created by Jiayu Zhang on 10/6/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPBaseView.h"


@class VPPost;

/**
 * A multi-use view that supports multiple type of media content (i.e. video, photo, gif, etc)
 * based on provided post
 */
@interface VPPhotoView : VPBaseView

@property (nonatomic, strong) VPPost *post;
@property (nonatomic, assign) BOOL shouldStartAnimatingAutomatically;

- (void)resetUI;
- (UIImage *)image;

@end
