//
//  VPPhotoView.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/6/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPhotoView.h"

#import "FLAnimatedImageView.h"
#import "JTSImageViewController.h"
#import "UIWebView+Addition.h"
#import "VPMedia.h"
#import "VPPost.h"
#import "VPPromptManager.h"
#import "VPUtil.h"


static NSString *_kVPYoutubeImageUrlFmt = @"http://i1.ytimg.com/vi/%@/hqdefault.jpg";
static NSString *_kVPYoutubeUrlPrefix = @"https://www.youtube.com/watch?v=";

@interface VPPhotoView ()<JTSImageViewControllerOptionsDelegate, UIWebViewDelegate>

@property (strong, nonatomic) FLAnimatedImageView *photoImageView;
@property (strong, nonatomic) UIImageView *indicatorImageView;
@property (strong, nonatomic) UIButton *playButton; // Only display if photoview large enough

// youtube related stuff
@property (strong, nonatomic) NSString *youtubeVideoId;
@property (strong, nonatomic) NSString *youtubeVideoUrl;
@property (strong, nonatomic) UIWebView *webView;

@end

@implementation VPPhotoView

#pragma mark - lifecycle
- (void)setupUI
{
    if (!self.photoImageView) {
        self.photoImageView = [[FLAnimatedImageView alloc] initWithFrame:self.bounds];
        self.photoImageView.autoresizingMask = UIViewAutoresizingFill;
        self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.photoImageView.userInteractionEnabled = YES;
        self.photoImageView.shouldStartAnimatingAutomatically = self.shouldStartAnimatingAutomatically;
        [self addSubview:self.photoImageView];

        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapped:)];
        tap.numberOfTapsRequired = 1;
        [self.photoImageView addGestureRecognizer:tap];
    }
    if (!self.indicatorImageView) {
        self.indicatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.width - 20, self.height - 20, 20, 20)];
        self.indicatorImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        [self addSubview:self.indicatorImageView];
    }
    if (!self.playButton && self.width > 250) {
        self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.playButton.frame = CGRectMake((self.width-90)/2, (self.height-40)/2, 90, 40);
        [self.playButton setImage:[UIImage imageNamed:@"video_play_button"] forState:UIControlStateNormal];
        self.playButton.autoresizingMask = UIViewAutoresizingCentered;
        [self.playButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        self.playButton.hidden = YES;
        [self addSubview:self.playButton];
    }
}


#pragma mark - public
- (void)setShouldStartAnimatingAutomatically:(BOOL)shouldStartAnimatingAutomatically
{
    _shouldStartAnimatingAutomatically = shouldStartAnimatingAutomatically;
    if (self.photoImageView) {
        self.photoImageView.shouldStartAnimatingAutomatically = shouldStartAnimatingAutomatically;
    }
}

- (void)setPost:(VPPost *)post
{
    _post = post;
    SDWebImageCompletionBlock completedBlock = ^void(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!error) {
            self.photoImageView.alpha = 0.6f;
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{
                                 self.photoImageView.alpha = 1.0f;
                             }
                             completion:nil];
        }
    };
    
    if ([post isPhotoType]) {
        VPMedia *photoMedia = post.photoMedia;
        [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:photoMedia.photoUrl]
                               placeholderImage:[UIImage imageNamed:@"image_load_placeholder"]
                                        options:(SDWebImageRetryFailed|SDWebImageLowPriority)
                                      completed:completedBlock];
    } else if ([post isVideoType]) {
        [self.indicatorImageView setImage:[UIImage imageNamed:@"video_indicator"]];
        VPMedia *photoMedia = post.videoThumbnailMedia;
        [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:photoMedia.photoUrl]
                               placeholderImage:[UIImage imageNamed:@"image_load_placeholder"]
                                        options:(SDWebImageRetryFailed|SDWebImageLowPriority)
                                      completed:completedBlock];
        if (self.playButton)
            self.playButton.hidden = NO;
    } else if ([post isLinkType]) {
        [self.indicatorImageView setImage:[UIImage imageNamed:@"link_indicator"]];
        VPMedia *linkMedia = post.linkMedia;
        if ([linkMedia.linkEmbedItemFormat isEqualToString:@"YOUTUBE"]) {
            [self.indicatorImageView setImage:[UIImage imageNamed:@"video_indicator"]];
            self.youtubeVideoId = linkMedia.linkEmbedItemUrl;
            self.youtubeVideoUrl = [_kVPYoutubeUrlPrefix stringByAppendingString:self.youtubeVideoId];
            [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:_kVPYoutubeImageUrlFmt, self.youtubeVideoId]]];
            if (self.playButton)
                self.playButton.hidden = NO;
        } else if ([linkMedia.linkEmbedItemFormat isEqualToString:@"IMAGE"] ||
                   [linkMedia.linkEmbedItemFormat isEqualToString:@"GIF"]) {
            [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:linkMedia.linkEmbedItemUrl]
                                   placeholderImage:[UIImage imageNamed:@"image_load_placeholder"]
                                            options:(SDWebImageRetryFailed|SDWebImageLowPriority)
                                          completed:completedBlock];
        } else {
            // NO media
        }
    }
}

- (void)resetUI
{
    self.indicatorImageView.image = nil;
    [self.photoImageView sd_cancelCurrentImageLoad];
    self.photoImageView.image = nil;
    
    if ([self.post isLinkType]) {
        if ([self.post.linkMedia.linkEmbedItemFormat isEqualToString:@"GIF"]) {
            self.photoImageView.animatedImage = nil;
        } else if ([self.post.linkMedia.linkEmbedItemFormat isEqualToString:@"YOUTUBE"]) {
            [self stopYoutubeVideoAndCleanup];
        }
    }
}

- (UIImage *)image
{
    return self.photoImageView.image;
}

- (void)dealloc
{
    [self resetUI];
}


#pragma mark - action handling
- (void)photoImageViewTapped:(UITapGestureRecognizer *)sender
{
    if ([self.post isLinkType] && [self.post.linkMedia.linkEmbedItemFormat isEqualToString:@"YOUTUBE"]) {
        [self loadYoutubeVideo];
        return;
    }
    
    JTSImageInfo *info = [[JTSImageInfo alloc] init];
    info.image = self.photoImageView.image;
    info.referenceContentMode = self.photoImageView.contentMode;
    info.referenceCornerRadius = 0.0f;
    info.referenceRect = self.photoImageView.frame;
    info.referenceView = self;
    
    if ([self.post isPhotoType]) {
        // Do nothing
    } else if ([self.post isLinkType] && [self.post.linkMedia.linkEmbedItemFormat isEqualToString:@"GIF"]) {
        info.flAnimatedImage = self.photoImageView.animatedImage;
        info.flGif = YES;
    } else if ([self.post isVideoType]) {
        info.videoURL = [NSURL URLWithString:self.post.videoMedia.videoUrl];
    }
    
    JTSImageViewController *imageViewerVC = [[JTSImageViewController alloc] initWithImageInfo:info
                                                                                         mode:JTSImageViewControllerMode_Image
                                                                              backgroundStyle:JTSImageViewControllerBackgroundOption_None];
    imageViewerVC.optionsDelegate = self;
    [imageViewerVC showFromViewController:[VPUtil topmostPresentedViewController] transition:JTSImageViewControllerTransition_FromOriginalPosition];
}

- (void)playButtonPressed:(id)sender
{
    [self photoImageViewTapped:nil];
}


#pragma mark - JTSImageViewControllerOptionsDelegate
- (CGFloat)alphaForBackgroundDimmingOverlayInImageViewer:(JTSImageViewController *)imageViewer
{
    return 1.0f;
}


#pragma mark - private
- (void)loadYoutubeVideo
{
    NSString *youtubeHtmlFilePath = [[NSBundle mainBundle] pathForResource:@"youtube" ofType:@"html"];
    NSError *err = nil;
    NSString *htmlFormatString = [NSString stringWithContentsOfFile:youtubeHtmlFilePath encoding:NSUTF8StringEncoding error:&err];
    if (!err) {
        CGSize size = self.photoImageView.frame.size;
        if (size.width < 250) {
            size = CGSizeMake(200, 200);
            // Weird, on ios9 (or ios8?), if the dimension is too small, it will throw break constraints exception
        }
        NSString *html = [NSString stringWithFormat:htmlFormatString, size.height, size.width, self.youtubeVideoId];
        if (!self.webView) {
            self.webView = [[UIWebView alloc] initWithFrame:self.bounds];
            self.webView.mediaPlaybackRequiresUserAction = NO;
            self.webView.delegate = self;
            [self addSubview:self.webView];
        }
        [self.webView loadHTMLString:html baseURL:[[NSBundle mainBundle] resourceURL]];
    }
}

- (void)stopYoutubeVideoAndCleanup
{
    [self.webView cleanup];
    self.webView = nil;
}

- (void)createPlayButtonIfNeed
{
    if (!self.playButton && self.width > 250) {
        self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.playButton.frame = CGRectMake((self.width-90)/2, (self.height-40)/2, 90, 40);
        [self.playButton setImage:[UIImage imageNamed:@"video_play_button"] forState:UIControlStateNormal];
        self.playButton.autoresizingMask = UIViewAutoresizingCentered;
        [self.playButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.playButton];
    }
}


#pragma mark - webview delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    // youtube-callback://err
    // youtube-callback://end
    if ([[[request URL] scheme] isEqualToString:@"youtube-callback"]) {
        [self stopYoutubeVideoAndCleanup];
        
        NSString *host = [[[request URL] host] lowercaseString];
        if ([host isEqualToString:@"err"]) {
            //            if ([self.delegate respondsToSelector:@selector(youtubeViewFailLoad:alternativeURL:)]) {
            //                [self.delegate youtubeViewFailLoad:self alternativeURL:self.youtubeVideoUrl];
            //            }
            
            //TODO: memory leak?
            [[VPPromptManager sharedInstance] prompt:kVPPromptWeb data:self.youtubeVideoUrl onlyOnce:NO overContext:NO animated:YES];
        }
        return NO;
    }
    return YES;
}

@end
