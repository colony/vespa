//
//  VPAddPostMenuView.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/24/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseView.h"


@protocol VPAddPostMenuViewDelegate;

@interface VPAddPostMenuView : VPBaseView

@property (weak, nonatomic) id<VPAddPostMenuViewDelegate> delegate;
@property (strong, nonatomic) NSArray *allowedTypes;

- (void)allowAll;
@end


@protocol VPAddPostMenuViewDelegate <NSObject>

@optional
- (void)addPostCancelled:(VPAddPostMenuView *)menuView;

// type - AUDIO,LINK,PHOTO,TEXT
- (void)addPostSelected:(VPAddPostMenuView *)menuView mediaType:(NSString *)type;

@end
