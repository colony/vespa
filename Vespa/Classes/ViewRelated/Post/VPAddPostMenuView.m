//
//  VPAddPostMenuView.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/24/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPAddPostMenuView.h"

#import "FXBlurView.h"


#define AUDIO_BUTTON_TAG 1
#define LINK_BUTTON_TAG  2
#define PHOTO_BUTTON_TAG 3
#define TEXT_BUTTON_TAG  4

#define GROUP_ANIMATION_IDENTIFIER @"GroupAnimID"


@interface VPAddPostMenuView ()

@property (weak, nonatomic) IBOutlet UIButton *addAudioButton;
@property (weak, nonatomic) IBOutlet UIButton *addLinkButton;
@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *addTextButton;
@property (weak, nonatomic) IBOutlet UIButton *addVideoButton;

@property (weak, nonatomic) IBOutlet UILabel *addAudioLabel;
@property (weak, nonatomic) IBOutlet UILabel *addLinkLabel;
@property (weak, nonatomic) IBOutlet UILabel *addPhotoLabel;
@property (weak, nonatomic) IBOutlet UILabel *addTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *addVideoLabel;

@property (assign, nonatomic) NSInteger animationCount;

@end

@implementation VPAddPostMenuView

- (void)setupUI
{
    [super setupUI];

    UIView *backgroundView;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0)) {
        UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        backgroundView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    } else {
        FXBlurView *blurView = [[FXBlurView alloc] init];
        blurView.dynamic = NO;
        blurView.blurRadius = 10.0f;
        blurView.blurEnabled = YES;
        blurView.tintColor = nil;
        blurView.underlyingView = [UIApplication sharedApplication].keyWindow;
        
        // https://github.com/nicklockwood/FXBlurView/issues/41
        // FXBlurView is too bright, in order to blend some darkness, we need add a transparent darkview
        UIView *darkOverlay = [[UIView alloc] initWithFrame:blurView.bounds];
        darkOverlay.autoresizingMask = UIViewAutoresizingFill;
        darkOverlay.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.85];
        [blurView addSubview:darkOverlay];
        
        backgroundView = blurView;
    }
    backgroundView.autoresizingMask = UIViewAutoresizingFill;
    backgroundView.frame = self.bounds;
    [self insertSubview:backgroundView atIndex:0];

    [self showoffAnimation];
}

- (void)setAllowedTypes:(NSArray *)allowedTypes
{
    _allowedTypes = allowedTypes;
    if ([self.allowedTypes containsObject:@"ALL"]) {
        [self.addAudioButton setImage:[UIImage imageNamed:@"add_audio_button"] forState:UIControlStateNormal];
        [self.addLinkButton setImage:[UIImage imageNamed:@"add_link_button"] forState:UIControlStateNormal];
        [self.addPhotoButton setImage:[UIImage imageNamed:@"add_photo_button"] forState:UIControlStateNormal];
        [self.addTextButton setImage:[UIImage imageNamed:@"add_text_button"] forState:UIControlStateNormal];
        [self.addVideoButton setImage:[UIImage imageNamed:@"add_video_button"] forState:UIControlStateNormal];
    } else {
        for (NSString *type in self.allowedTypes) {
            if ([type isEqualToString:@"AUDIO"]) {
                [self.addAudioButton setImage:[UIImage imageNamed:@"add_audio_button"] forState:UIControlStateNormal];
            } else if ([type isEqualToString:@"LINK"]) {
                [self.addLinkButton setImage:[UIImage imageNamed:@"add_link_button"] forState:UIControlStateNormal];
            } else if ([type isEqualToString:@"PHOTO"]) {
                [self.addPhotoButton setImage:[UIImage imageNamed:@"add_photo_button"] forState:UIControlStateNormal];
            } else if ([type isEqualToString:@"TEXT"]) {
                [self.addTextButton setImage:[UIImage imageNamed:@"add_text_button"] forState:UIControlStateNormal];
            } else if ([type isEqualToString:@"VIDEO"]) {
                [self.addVideoButton setImage:[UIImage imageNamed:@"add_video_button"] forState:UIControlStateNormal];
            }
        }
    }
}

- (void)allowAll
{
    [self setAllowedTypes:@[@"ALL"]];
}


#pragma mark - core animation
- (void)showoffAnimation
{
    self.animationCount = 5;
    [self showoffAnimation:self.addPhotoButton identifier:@"PHOTO" delayInSec:0.0];
    [self showoffAnimation:self.addAudioButton identifier:@"AUDIO" delayInSec:0.1];
    [self showoffAnimation:self.addVideoButton identifier:@"VIDEO" delayInSec:0.2];
    [self showoffAnimation:self.addTextButton  identifier:@"TEXT" delayInSec:0.3];
    [self showoffAnimation:self.addLinkButton  identifier:@"LINK" delayInSec:0.4];
}

- (void)showoffAnimation:(UIButton *)button identifier:(NSString *)identifier delayInSec:(double)delay
{
    CALayer *layer = button.layer;
    CATransform3D transform = layer.transform;
    
    CAKeyframeAnimation *movingAnim = [CAKeyframeAnimation animation];
    movingAnim.keyPath = @"position.y";
    movingAnim.values = @[@0, @-20, @10, @-5, @0];
    movingAnim.keyTimes = @[@0, @(2/5.0), @(3/5.0), @(4/5.0), @1];
    movingAnim.duration = 0.6;
    movingAnim.additive = YES;
    
    CAKeyframeAnimation *scaleAnim = [CAKeyframeAnimation animation];
    scaleAnim.keyPath = @"transform";
    scaleAnim.values = @[
                         [NSValue valueWithCATransform3D:CATransform3DScale(transform, 0, 0, 0)],
                         [NSValue valueWithCATransform3D:CATransform3DScale(transform, 1.3, 1.3, 1.0)],
                         [NSValue valueWithCATransform3D:transform],
                         [NSValue valueWithCATransform3D:transform],
                         [NSValue valueWithCATransform3D:transform]
                         ];
    scaleAnim.keyTimes = @[@0, @(2/5.0), @(3/5.0), @(4/5.0), @1];
    scaleAnim.duration = 0.6;
    
//    CAKeyframeAnimation *opacityAnim = [CAKeyframeAnimation animation];
//    opacityAnim.keyPath = @"opacity";
//    opacityAnim.values = @[@1];
//    opacityAnim.calculationMode = kCAAnimationDiscrete;
//    opacityAnim.duration = 0.6;
//    opacityAnim.fillMode = kCAFillModeForwards;
//    opacityAnim.removedOnCompletion = NO;
    
    CABasicAnimation *opacityAnim = [CABasicAnimation animation];
    opacityAnim.keyPath = @"opacity";
    opacityAnim.fromValue = @1;
    opacityAnim.toValue = @1;
    opacityAnim.duration = 0.6;
    
    // We use kCAFillModeForwards, it will left presentationLayer active after animation done
    // which will block all touch events, (the uibutton won't be clickable). Instead, in
    // animation delegate method, we set the opacity back to 1 on real modal layer
    CAAnimationGroup *group = [[CAAnimationGroup alloc] init];
    group.animations = @[movingAnim, scaleAnim, opacityAnim];
    group.duration = 0.6;
    group.beginTime = [layer convertTime:CACurrentMediaTime() fromLayer:nil] + delay;
    [group setValue:identifier forKey:GROUP_ANIMATION_IDENTIFIER];
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    group.delegate = self;
    [layer addAnimation:group forKey:@"showoff"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    NSString *identifier = [anim valueForKey:GROUP_ANIMATION_IDENTIFIER];
    if (flag && [identifier isEqualToString:@"AUDIO"]) {
        self.addAudioButton.alpha = 1.0f;
    } else if (flag && [identifier isEqualToString:@"LINK"]) {
        self.addLinkButton.alpha = 1.0f;
    } else if (flag && [identifier isEqualToString:@"PHOTO"]) {
        self.addPhotoButton.alpha = 1.0f;
    } else if (flag && [identifier isEqualToString:@"TEXT"]) {
        self.addTextButton.alpha = 1.0f;
    } else if (flag && [identifier isEqualToString:@"VIDEO"]) {
        self.addVideoButton.alpha = 1.0f;
    }
    
    self.animationCount--;
    if (self.animationCount <= 0) {
        [UIView animateWithDuration:0.1 animations:^{
            self.addAudioLabel.alpha = 1.0f;
            self.addLinkLabel.alpha = 1.0f;
            self.addPhotoLabel.alpha = 1.0f;
            self.addTextLabel.alpha = 1.0f;
            self.addVideoLabel.alpha = 1.0f;
        }];
    }
}


#pragma mark - action handling
- (IBAction)buttonPressed:(UIButton *)btn
{
    if ([self.delegate respondsToSelector:@selector(addPostSelected:mediaType:)]) {
        // Each button has a tag corresponding to media type
        NSInteger i = btn.tag - 1;
        NSArray *allMediaTypes = @[@"AUDIO",@"LINK",@"PHOTO",@"TEXT",@"VIDEO"];
        if (i < [allMediaTypes count]) {
            NSString *mediaType = [allMediaTypes objectAtIndex:i];
            if ([self.allowedTypes containsObject:@"ALL"] || [self.allowedTypes containsObject:mediaType]) {
                [self.delegate addPostSelected:self mediaType:[allMediaTypes objectAtIndex:i]];
            }
        } else {
            // Unrecognized, do nothing
        }
    } else {
        self.hidden = YES;
        [self removeFromSuperview];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.animationCount > 0) {
        // The animation not done yet
        return;
    }

    if ([self.delegate respondsToSelector:@selector(addPostCancelled:)]) {
        [self.delegate addPostCancelled:self];
    } else {
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.alpha = 0.0f;
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                             [self removeFromSuperview];
                         }];
    }
}

@end
