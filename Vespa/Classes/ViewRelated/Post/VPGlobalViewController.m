//
//  VPTrendingListViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/23/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPGlobalViewController.h"

#import "HMSegmentedControl.h"
#import "UICountingLabel.h"
#import "UIViewController+ScrollingNavbar.h"
#import "UINavigationItem+Util.h"
#import "VPPaginator.h"
#import "VPPost.h"
#import "VPPosts.h"
#import "VPPromptManager.h"
#import "VPStatus.h"
#import "VPTutorialManager.h"
#import "VPUsers.h"
#import "VPUtil.h"

#import "VPPointViewController.h"


#define FilterNew 0
#define FilterBest 1

@interface VPGlobalViewController ()<AMScrollingNavbarDelegate>

@property (strong, nonatomic) VPPaginator *paginator;
@property (assign, nonatomic) BOOL loadWhenDidAppear;

// Points, NOTE, it is different than the one in VPNearbyVC since we don't need counting animation
@property (strong, nonatomic) UICountingLabel *pointLabel;
@property (copy, nonatomic) NSNumber *displayingPoint;

// 0 - New, 1 - Best
@property (assign, nonatomic) NSInteger currentFilterType;
@property (strong, nonatomic) UISegmentedControl *filterControl;

@end


@implementation VPGlobalViewController

#pragma mark - lifecycle
- (void)commonInit
{
    [super commonInit];
    self.showNavbarWhenScrollToTop = YES;
    self.enableScrollingNavbar = YES;
    self.displayingPoint = @0;
    
    // NOTE, the parent class dealloc will remove from notificationCenter
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTabbarCurrentTapNotification:) name:kTabbarCurrentTapNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pointLabel = [[UICountingLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    self.pointLabel.textColor = [UIColor whiteColor];
    self.pointLabel.userInteractionEnabled = YES;
    self.pointLabel.textAlignment = NSTextAlignmentLeft;
    self.pointLabel.font = [VPUI fontOpenSans:17.0f thickness:1];
    self.pointLabel.method = UILabelCountingMethodEaseInOut;
    self.pointLabel.format = @"%d";
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pointLabelTapped:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.pointLabel addGestureRecognizer:tapGestureRecognizer];
    
    UIView *leftNav = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
    leftNav.clipsToBounds = NO;
    [leftNav addSubview:self.pointLabel];
    [self.navigationItem setCustomLeftWithView:leftNav];
    [self.navigationItem tweakLeftWithSpace:-8.0f];
    
    HMSegmentedControl *segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"NEW", @"HOT"]];
    segmentedControl.frame = CGRectMake(0.0f, 0.0f, 130.0f, 44.0f);
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.backgroundColor = [UIColor clearColor];
    segmentedControl.titleTextAttributes = @{
                                             NSFontAttributeName: [VPUI fontOpenSans:16.0f thickness:3],
                                             NSForegroundColorAttributeName : [UIColor colorWithRed:255 green:255 blue:255 alpha:0.5]
                                             };
    segmentedControl.selectedTitleTextAttributes = @{
                                                     NSFontAttributeName: [VPUI fontOpenSans:16.0f thickness:3],
                                                     NSForegroundColorAttributeName : [UIColor whiteColor]
                                                     };
    segmentedControl.selectionIndicatorColor = [UIColor whiteColor];
    segmentedControl.selectedSegmentIndex = FilterNew;
    [segmentedControl addTarget:self action:@selector(filterSegmentControlChanged:) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = segmentedControl;
    
    UIButton *addPostButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addPostButton.frame = CGRectMake(0, 0, 40, 40);
    addPostButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [addPostButton addTarget:self action:@selector(addPostButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [addPostButton setImage:[UIImage imageNamed:@"compose_post_button"] forState:UIControlStateNormal];
    [self.navigationItem setCustomRightWithView:addPostButton];
    [self.navigationItem tweakRightWithSpace:-8];

    self.loadWhenDidAppear = YES;
}

- (void)setupUI
{
    [super setupUI];
    [self setFilter:FilterNew];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [VPReports reportEvent:(self.currentFilterType == FilterNew ? kVPEventViewPostGlobalNew : kVPEventViewPostGlobalBest) withParam:nil mode:kVPEventModeStart];
    
//    if (self.loadWhenDidAppear) {
//        self.loadWhenDidAppear = NO;
//        [self setFilter:FilterNew];
//    }
    
    [self updatePoints];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [VPReports reportEvent:(self.currentFilterType == FilterNew ? kVPEventViewPostGlobalNew : kVPEventViewPostGlobalBest) withParam:nil mode:kVPEventModeEnd];
}


#pragma mark - override function
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSMutableDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    @weakify(self);
    VPPostListFetchType fetchType = (self.currentFilterType == FilterNew ? kVPPostListFetchTypeGlobalRecent:kVPPostListFetchTypeGlobalPopular);
    self.ongoingRequestOperation = [VPPosts fetchPostsWithType:fetchType
                                                           url:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                         param:nil
                                                       success:^(VPPostList *postList) {
                                                           @strongify(self);
                                                           self.paginator = postList.paginator;
                                                           [self onObjectsDidLoaded:postList.posts pageNo:pageNo error:nil];
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                           
                                                           // Upon first page loading, we might start the tutorial
                                                           if (pageNo == 0) {
                                                               [self updatePoints];
                                                               //[[VPTutorialManager sharedInstance] start:kVPTutorialFeed];
                                                           }
                                                       } failure:^(NSError *error) {
                                                           @strongify(self);
                                                           VPLog(@"Fail fetchPostsByGlobal: %@", [error localizedDescription]);
                                                           [self onObjectsDidLoaded:nil pageNo:pageNo error:error];
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                       }];
}


#pragma mark - action handling
- (void)filterSegmentControlChanged:(id)sender
{
    // http://stackoverflow.com/questions/18894772/uisegmentedcontrol-in-ios-7-divider-image-is-wrong-during-animation
    // The problem is, when segmentControlValueChanged, the animation of background image (segment) and divider
    // is out of sync for a tiny second, which leads to an noticeable flashing
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    for (UIView *segment in [segmentedControl subviews]) {
        [segment.layer removeAllAnimations];
        for (UIView *view in [segment subviews]) {
            [view.layer removeAllAnimations];
        }
    }
    
    [self setFilter:segmentedControl.selectedSegmentIndex];
}

- (void)addPostButtonPressed:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPresentAddPostNotification object:nil];
}

- (void)inviteButtonPressed:(id)sender
{
    [VPUtil showInvitationView:self];
}

- (void)pointLabelTapped:(id)sender
{
    [VPReports reportEvent:kVPEventClickPoint withParam:nil mode:kVPEventModeOnce];
    [[VPPromptManager sharedInstance] prompt:kVPPromptPoint data:self.displayingPoint onlyOnce:NO overContext:NO animated:YES];
}


#pragma mark - notification
- (void)handleTabbarCurrentTapNotification:(NSNotification *)noti
{
    if (noti.userInfo && [noti.userInfo[@"tab"] isEqualToString:@"GLOBAL"]) {
        if (self.isTop) {
            [self.collectionView setContentOffset:CGPointMake(0.0f, 0.0f) animated:YES];
        }
    }
}


#pragma mark - private
- (void)setFilter:(NSInteger)type
{
    self.currentFilterType = type;
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
    [self pullDownRefresh];
}

- (void)updatePoints
{
    VPStatus *status = [VPUsers fetchUserStatusLocal];
    if (status) {
        long l1 = [status.points longValue];
        long l2 = [self.displayingPoint longValue];
        if (l1 != l2) {
            self.displayingPoint = status.points;
            NSTimeInterval interval = (labs(l2-l1)>10) ? 3.0f : 1.0f;
            [self.pointLabel countFrom:l2 to:l1 withDuration:interval];
        }
    }
}

@end
