//
//  VPPointViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/29/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"

@interface VPPointViewController : VPBaseViewController

- (instancetype)initWithPoint:(NSNumber *)point;

@end
