//
//  VPPointViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/29/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPointViewController.h"
#import "UICountingLabel.h"
#import "UIImageView+Addition.h"
#import "UIView+Frame.h"

@interface VPPointViewController ()

@property (weak, nonatomic) IBOutlet UICountingLabel *countingLabel;
@property (copy, nonatomic) NSNumber *point;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *unitLabel;


@end

@implementation VPPointViewController

- (instancetype)initWithPoint:(NSNumber *)point
{
    if (self = [super init]) {
        _point = point;
    }
    return self;
}

- (void)setupUI
{
    [super setupUI];
    [self setNeedsStatusBarAppearanceUpdate];
    
    UIImage *image = [UIImage imageNamed:@"point_background"];
    CGSize imageSize = image.size;
    
    CGFloat w = SCREEN_WIDTH;
    CGFloat h = SCREEN_WIDTH/imageSize.width * imageSize.height;
    
    self.backgroundImageView.image = image;
    self.backgroundImageView.frame = CGRectMake(0, 0, w, h);
    
    // NOTE: the UIImageView is AspectFillContentMode, resize height of image view to avoid clipping the image inside
//    CGSize imageSize = [self.backgroundImageView displayedImageSize];
//    CGRect frame = self.backgroundImageView.frame;
//    frame.size = imageSize;
//    self.backgroundImageView.frame = frame;

    self.scrollView.contentSize = self.backgroundImageView.bounds.size;
    
    // Don't blame me hardcode ;)
    // The point's counting and unit label should be centered within background's circle (the sun)
    // For now, there is no easy and unified way to correct position for a background image for all devices
    // Instead, we define that, position at y=256 when image height is 888.88 (after aspectfill), then do
    // the calculation proportionally
    CGFloat y = 9.0/30 * h;
    self.countingLabel.y = y;
    self.unitLabel.y = self.countingLabel.y + 70.0f;
    
    self.countingLabel.method = UILabelCountingMethodEaseInOut;
    self.countingLabel.format = @"%d";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.countingLabel countFromZeroTo:[self.point floatValue] withDuration:3.0f];
}


#pragma mark - status bar
- (BOOL)prefersStatusBarHidden{
    return YES;
}

@end
