//
//  VPPostActions.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


@class VPPost;
@protocol VPPostActionsDelegate;

@interface VPPostActions : NSObject

+ (void)doActionComment:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post;
+ (void)doActionShare:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post image:(UIImage *)image;

/** 
 * Open up an action sheet with various options, when the action behind option is finished, the corresponding delegate method
 * will be called (i.e. didActionOptionDelete)
 */
+ (void)doActionOption:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post;
+ (void)doActionVote:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post isDownvoteClicked:(BOOL)isDownvoteClicked;

@end


@protocol VPPostActionsDelegate <NSObject>
@optional
- (void)didActionComment:(VPPost *)post;
- (void)didActionOptionDelete:(VPPost *)post;
- (void)didActionOptionFlag:(VPPost *)post;

/** NOTE, didActionOptionShare means user select the 'share' option in action sheet, while didActionShare means user finsihes the real share */
- (void)didActionOptionShare:(VPPost *)post;
- (void)didActionShare:(VPPost *)post;

- (void)didActionVote:(VPPost *)post;

@end