//
//  VPPostActions.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPostActions.h"

#import "NSString+Util.h"
#import "VPAccount.h"
#import "VPConfigManager.h"
#import "VPMedia.h"
#import "VPPost.h"
#import "VPPosts.h"
#import "VPUser.h"
#import "VPUsers.h"
#import "VPUI.h"
#import "VPUtil.h"

static VPPostActions *_shared = nil;

@interface VPPostActions () <UIActionSheetDelegate,UIAlertViewDelegate>

/**
 For certain actions (i.e. option menu), we need store the reference to the ViewController and Post in action,
 which might be used in delegate (async). They must be reset everytime a new action occurs
 */
@property (weak, nonatomic) UIViewController *actingViewController;
@property (weak, nonatomic) VPPost *actingPost;
@property (weak, nonatomic) id<VPPostActionsDelegate> delegate;

@end


@implementation VPPostActions

+ (void)initialize
{
    // Threadsafe
    if (self == [VPPostActions class]) {
        _shared = [[self alloc] init];
    }
}

+ (void)doActionComment:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post
{
    // Nothing special, forward to delegate if any
    if ([delegate respondsToSelector:@selector(didActionComment:)]) {
        [delegate didActionComment:post];
    }
}

+ (void)doActionShare:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post image:(UIImage *)image
{
    [VPReports reportEvent:kVPEventSharePostAttempt withParam:@{@"postId":post.postId} mode:kVPEventModeOnce];
    
    // Build the shareable post URL, VERSION 1
    NSInteger shareSchemaVersion = 1;
    NSString *base64Encoded =[[[NSString stringWithFormat:@"sh|%@", post.postId] dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
    base64Encoded = [base64Encoded stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"-"]];
    if (shareSchemaVersion == 1) {
        NSMutableString *muString = [NSMutableString stringWithString:base64Encoded];
        [muString insertString:[NSString randomAlphanumericStringWithLength:1] atIndex:1];
        [muString insertString:[@(shareSchemaVersion) stringValue] atIndex:0];
        base64Encoded = muString;
    }
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@",[[VPConfigManager sharedInstance] getString:kVPConfigSharePostUrlPrefix], base64Encoded];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableArray *ary = [NSMutableArray new];
    if ([post isPhotoType]) {
        NSString *textToShare = [NSString isEmptyString:post.body] ? @"Snapped on the Ninjr app" : [NSString stringWithFormat:@"Snapped on Ninjr app: \"%@\"", post.body];
        [ary addObject:textToShare];
        [ary addObject:url];
        if (image) {[ary addObject:image];}
    } else if ([post isAudioType]) {
        NSString *textToShare = [NSString isEmptyString:post.body] ? @"I heard this on the Ninjr app" : [NSString stringWithFormat:@"I heard this on the Ninjr app: \"%@\"", post.body];
        [ary addObject:textToShare];
        [ary addObject:url];
    } else if ([post isTextType]) {
        [ary addObject:@"Someone said this on the Ninjr app"];
        [ary addObject:url];
        if (image) {[ary addObject:image];}
    } else if ([post isLinkType]) {
        [ary addObject:@"Look at what people are discussing on the Ninjr app"];
        [ary addObject:url];
    } else if ([post isVideoType]) {
        [ary addObject:@"Someone just recorded this for the Ninjr app"];
        [ary addObject:url];
        if (image) {[ary addObject:image];}
    } else if ([post isCardType]) {
        [ary addObject:@"Come play Ninjrs Against Humanity with me"];
        [ary addObject:url];
        if (image)
            [ary addObject:image];
    }
    
    NSArray *objectsToShare = [NSArray arrayWithArray:ary];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAirDrop];
    
    if ([activityVC respondsToSelector:@selector(setCompletionWithItemsHandler:)]) { // IOS 8
        [activityVC setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
            if (completed && !activityError) {
                [VPReports reportEvent:kVPEventSharePost withParam:@{@"type":activityType,@"postId":post.postId,@"postType":post.type} mode:kVPEventModeOnce];
            }
        }];
    } else {
        [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
            if (completed) {
                [VPReports reportEvent:kVPEventSharePost withParam:@{@"type":activityType,@"postId":post.postId,@"postType":post.type} mode:kVPEventModeOnce];
            }
        }];
    }
    
    [containerController presentViewController:activityVC animated:YES completion:nil];

    if ([delegate respondsToSelector:@selector(didActionShare:)]) {
        [delegate didActionShare:post];
    }
}

+ (void)doActionOption:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post
{
    BOOL showDelete = [post.owner.userId isEqualToNumber:[VPUsers fetchMe].userId];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:_shared
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    // NOTE: add button order corresponds to index
    actionSheet.destructiveButtonIndex = [actionSheet addButtonWithTitle:@"Flag"];
    [actionSheet addButtonWithTitle:@"Share"];
    if (showDelete) {
        [actionSheet addButtonWithTitle:@"Delete"];
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    actionSheet.tag = 1;
    
    // Save the interested object to be used by delegate later
    _shared.actingViewController = containerController;
    _shared.actingPost = post;
    _shared.delegate = delegate;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

+ (void)doActionOptionFlag:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:_shared
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    NSArray *flagReasons = [[VPConfigManager sharedInstance] get:kVPConfigFlagReason];
    for (NSString *reason in flagReasons) {
        [actionSheet addButtonWithTitle:reason];
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    actionSheet.tag = 2;
    
    // Save the interested object to be used by delegate later
    _shared.actingViewController = containerController;
    _shared.actingPost = post;
    _shared.delegate = delegate;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

+ (void)doActionVote:(UIViewController *)containerController delegate:(id<VPPostActionsDelegate>)delegate post:(VPPost *)post isDownvoteClicked:(BOOL)isDownvoteClicked
{
    VPVoteStatus status;
    if ([post.voteStatus isEqualToString:@"DOWNVOTE"]) {
        if (isDownvoteClicked) {
            post.voteStatus = @"NOVOTE";
            post.numOfDownvotes = [NSNumber numberWithLong:(post.numOfDownvotes.longValue - 1)];
            status = kVPVoteStatusUndown;
        } else {
            post.voteStatus = @"UPVOTE";
            post.numOfUpvotes = [NSNumber numberWithLong:(post.numOfUpvotes.longValue + 1)];
            post.numOfDownvotes = [NSNumber numberWithLong:(post.numOfDownvotes.longValue - 1)];
            status = kVPVoteStatusUpSwitch;
        }
    } else if ([post.voteStatus isEqualToString:@"UPVOTE"]) {
        if (isDownvoteClicked) {
            post.voteStatus = @"DOWNVOTE";
            post.numOfDownvotes = [NSNumber numberWithLong:(post.numOfDownvotes.longValue + 1)];
            post.numOfUpvotes = [NSNumber numberWithLong:(post.numOfUpvotes.longValue - 1)];
            status = kVPVoteStatusDownSwitch;
        } else {
            post.voteStatus = @"NOVOTE";
            post.numOfUpvotes = [NSNumber numberWithLong:(post.numOfUpvotes.longValue - 1)];
            status = kVPVoteStatusUnup;
        }
    } else { // NOVOTE
        if (isDownvoteClicked) {
            post.voteStatus = @"DOWNVOTE";
            post.numOfDownvotes = [NSNumber numberWithLong:(post.numOfDownvotes.longValue + 1)];
            status = kVPVoteStatusDown;
        } else {
            post.voteStatus = @"UPVOTE";
            post.numOfUpvotes = [NSNumber numberWithLong:(post.numOfUpvotes.longValue + 1)];
            status = kVPVoteStatusUp;
        }
    }
    
    post.numOfDownvotes = @(MAX(0, post.numOfDownvotes.longValue));
    post.numOfUpvotes = @(MAX(0, post.numOfUpvotes.longValue));
    
    if ([delegate respondsToSelector:@selector(didActionVote:)]) {
        [delegate didActionVote:post];
    }
    
    [VPPosts votePostWithId:post.postId status:status success:nil failure:nil];
}

+ (void)showFlagOptionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:_shared
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    NSArray *flagReasons = [[VPConfigManager sharedInstance] get:kVPConfigFlagReason];
    for (NSString *reason in flagReasons) {
        [actionSheet addButtonWithTitle:reason];
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    actionSheet.tag = 2;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}


#pragma mark - action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 1) { // This is main option menu
        NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
        if ([buttonTitle isEqualToString:@"Flag"]) {
            [VPPostActions showFlagOptionSheet];
        } else if ([buttonTitle isEqualToString:@"Share"]) {
            if ([_shared.delegate respondsToSelector:@selector(didActionOptionShare:)]) {
                [_shared.delegate didActionOptionShare:_shared.actingPost];
            }
        } else if ([buttonTitle isEqualToString:@"Delete"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Deletion"
                                                            message:@"Delete Post?"
                                                           delegate:self
                                                  cancelButtonTitle:@"No"
                                                  otherButtonTitles:@"Yes", nil];
            [alert show];
        }
    } else if (actionSheet.tag == 2){ // This is flag option menu
        VPLog(@"flag menu button index %tu", buttonIndex);
        
        NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
        if (![buttonTitle isEqualToString:@"Cancel"]) {
            [VPReports reportEvent:kVPEventFlagPost withParam:@{@"reason":buttonTitle, @"postId":_shared.actingPost.postId} mode:kVPEventModeOnce];
            [VPPosts createFlagWithPostId:_shared.actingPost.postId complaint:buttonTitle success:^{
                if ([_shared.delegate respondsToSelector:@selector(didActionOptionFlag:)]) {
                    [_shared.delegate didActionOptionFlag:_shared.actingPost];
                }
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Thanks for the input. The post has been flagged and sent to a moderator." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                [alert show];
                [self performSelector:@selector(dismissAlert:) withObject:alert afterDelay:2.0];
            } failure:nil];
        }
    }
}

- (void)dismissAlert:(UIAlertView *)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}


#pragma mark - alertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0://NO
            break;
        case 1://YES
            [VPPosts deletePostWithId:_shared.actingPost.postId success:^{
                if ([_shared.delegate respondsToSelector:@selector(didActionOptionDelete:)]) {
                    [_shared.delegate didActionOptionDelete:_shared.actingPost];
                }
            } failure:nil];
            break;
    }
}


@end
