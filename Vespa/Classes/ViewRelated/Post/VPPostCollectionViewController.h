//
//  VPPostCollectionViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 2/23/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseCollectionViewController.h"
#import "VPPostActions.h"

@interface VPPostCollectionViewController : VPBaseCollectionViewController<VPPostActionsDelegate>

- (void)handlePostModificationUpdate:(NSArray *)updatePosts delete:(NSArray *)deletePosts insert:(NSArray *)insertPosts;
- (void)handlePostModificationUpdate:(NSArray *)updatePosts;
- (void)handlePostModificationDelete:(NSArray *)deletePosts;
- (void)handlePostModificationInsert:(NSArray *)insertPosts;

- (NSInteger)getItemIndexForPostId:(NSNumber *)postId;

- (NSValue *)sizeOfPost:(VPPost *)post;

/**
 * If enableScrollingNavbar is NO, the property is ignored. When statusbar is clicked,
 * the collection view should scroll to top. The property will determine whether
 * to reveal the navbar (if hidden) or not
 */
@property (assign, nonatomic) BOOL showNavbarWhenScrollToTop;

/**
 * YES if auto expand/contract navigationbar when scroll down/up. NO otherwise.
 * For now, we implement by using AMScrollingNavbar
 */
@property (assign, nonatomic) BOOL enableScrollingNavbar;

/**
 * If set, a page session (duration) with specified reportEventName is recorded between didAppear and didDisappear
 */
@property (strong, nonatomic) NSString *reportEventName;
@property (strong, nonatomic) NSDictionary *reportParam;

@end
