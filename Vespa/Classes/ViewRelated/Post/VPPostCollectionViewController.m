//
//  VPPostCollectionViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 2/23/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPostCollectionViewController.h"

#import "UIViewController+ScrollingNavbar.h"
#import "VPPost.h"
#import "VPPostListSmallAudioCell.h"
#import "VPPostListSmallPhotoCell.h"
#import "VPPostListSmallTextCell.h"
#import "VPPostThreadViewController.h"
#import "VPPostViewController.h"
#import "VPPromptManager.h"


@interface VPPostCollectionViewController ()<UIScrollViewDelegate>

@end


@implementation VPPostCollectionViewController

#pragma mark - lifecycle
- (void)commonInit
{
    [super commonInit];
    self.cellClasses = @[[VPPostListSmallPhotoCell class], [VPPostListSmallAudioCell class], [VPPostListSmallTextCell class]];
    self.cellNibNames = @[@"VPPostListSmallPhotoCell", @"VPPostListSmallAudioCell", @"VPPostListSmallTextCell"];
    self.enablePullToRefresh = YES;
    self.enableLoadMore = YES;
    self.enableTabbarHandle = YES;
    self.enableScrollViewSmoothOptimization = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostModificationNotification:) name:kPostModificationNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.autoresizingMask = UIViewAutoresizingFill;
}

- (void)setupUI
{
    [super setupUI];
    
    if (self.enableScrollingNavbar) {
        [self followScrollView:self.collectionView];
        [self setUseSuperview:YES];
        [self setShouldScrollWhenContentFits:NO];
    }
}

- (void)setupCollectionView:(UICollectionView *)collectionView
{
    [super setupCollectionView:collectionView];
    collectionView.backgroundColor = [VPUI lightGrayColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.collectionView.scrollsToTop = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.reportEventName) {
        [VPReports reportEvent:self.reportEventName withParam:self.reportParam mode:kVPEventModeStart];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.collectionView.scrollsToTop = NO;
    if (self.enableScrollingNavbar) {
        [self showNavBarAnimated:NO];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (self.reportEventName) {
        [VPReports reportEvent:self.reportEventName withParam:self.reportParam mode:kVPEventModeEnd];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (self.enableScrollingNavbar) {
        [self stopFollowingScrollView];
    }
}


#pragma mark - public methods
- (void)handlePostModificationUpdate:(NSArray *)updatePosts delete:(NSArray *)deletePosts insert:(NSArray *)insertPosts
{
    [self handlePostModificationInsert:insertPosts];
    [self handlePostModificationDelete:deletePosts];
    [self handlePostModificationUpdate:updatePosts];
}

- (void)handlePostModificationDelete:(NSArray *)deletePosts
{
    NSMutableIndexSet *deleteItemIndexes;
    NSMutableArray *deleteIndexPaths;
    
    for (VPPost *post in deletePosts) {
        NSInteger itemIndex = [self getItemIndexForPostId:post.postId];
        if (itemIndex >= 0) {
            if (!deleteItemIndexes) {
                deleteItemIndexes = [NSMutableIndexSet new];
            }
            if (!deleteIndexPaths) {
                deleteIndexPaths = [NSMutableArray new];
            }
            [deleteItemIndexes addIndex:itemIndex];
            [deleteIndexPaths addObject:[self getIndexPathFromFromItemIndex:itemIndex]];
        }
    }
    
    if ([deleteIndexPaths count] > 0) {
        [UIView performWithoutAnimation:^{
            [self.collectionView performBatchUpdates:^{
                [self.items removeObjectsAtIndexes:deleteItemIndexes];
                [self.cellSizes removeObjectsAtIndexes:deleteItemIndexes];
                [self calculateSectionInfo];
                [self.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
            } completion:nil];
        }];
    }
}

- (void)handlePostModificationInsert:(NSArray *)insertPosts
{
    // Leave this EMPTY here, the class is very generic, but only specific children (i.e. PostListVC)
    // needs handle the inserts, hence, leave the implementation details to children
}

- (void)handlePostModificationUpdate:(NSArray *)updatePosts
{
    NSMutableArray *updateIndexPaths;
    NSArray *visibleIndexPaths = [self.collectionView indexPathsForVisibleItems];
    
    for (VPPost *post in updatePosts) {
        NSInteger itemIndex = [self getItemIndexForPostId:post.postId];
        if (itemIndex >= 0) {
            [self.items replaceObjectAtIndex:itemIndex withObject:post];
            NSIndexPath *updateIndexPath = [self getIndexPathFromFromItemIndex:itemIndex];
            if ([visibleIndexPaths containsObject:updateIndexPath]) { // Performance boost, only care visible cells
                if (!updateIndexPaths) {
                    updateIndexPaths = [NSMutableArray new];
                }
                [updateIndexPaths addObject:updateIndexPath];
            }
        }
    }
    
    if ([updateIndexPaths count] > 0) {
        VPLog(@"Post modification update %tu", [updateIndexPaths count]);
        [UIView performWithoutAnimation:^{
            [self.collectionView performBatchUpdates:^{
                [self.collectionView visibleCells];
                [self.collectionView reloadItemsAtIndexPaths:updateIndexPaths];
            } completion:nil];
        }];
    }
}

- (NSInteger)getItemIndexForPostId:(NSNumber *)postId
{
    NSInteger itemIndex = 0;
    for (; itemIndex < [self.items count]; ++itemIndex) {
        id obj = [self.items objectAtIndex:itemIndex];
        if (obj && [obj isKindOfClass:[VPPost class]]) {
            VPPost *post = (VPPost *)obj;
            if ([post.postId isEqualToNumber:postId]) {
                return itemIndex;
            }
        }
    }
    return -1;
}

- (NSValue *)sizeOfPost:(VPPost *)post
{
//    if ([post isPhotoType]) {
//        return [self.cellClasses[5] performSelector:@selector(sizeOfCell:) withObject:post];
//    } else if ([post isVideoType]) {
//        return [self.cellClasses[0] performSelector:@selector(sizeOfCell:) withObject:post];
//    } else if ([post isAudioType]) {
//        return [self.cellClasses[1] performSelector:@selector(sizeOfCell:) withObject:post];
//    } else if ([post isTextType] || [post isCardType]) {
//        return [self.cellClasses[2] performSelector:@selector(sizeOfCell:) withObject:post];
//    } else if ([post isLinkType]) {
//        if ([post hasLinkMedia]) {
//            return [self.cellClasses[3] performSelector:@selector(sizeOfCell:) withObject:post];
//        } else {
//            return [self.cellClasses[4] performSelector:@selector(sizeOfCell:) withObject:post];
//        }
//    }
    
    for (id cellClass in self.cellClasses) {
        if ([cellClass performSelector:@selector(isAcceptable:) withObject:post]) {
            return [cellClass performSelector:@selector(sizeOfCell:) withObject:post];
        }
    }
    return nil;
}


#pragma mark - override method
- (void)onObjectsDidLoaded:(NSArray *)objects pageNo:(NSInteger)pageNo error:(NSError *)error
{
    [super onObjectsDidLoaded:objects pageNo:pageNo error:error];
    if (!error) {
        if ([objects count] > 0) {
            NSMutableArray *updatedPosts = [NSMutableArray new];
            for (id obj in objects) {
                if ([obj isKindOfClass:[VPPost class]]) {
                    [updatedPosts addObject:obj];
                }
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                                object:self
                                                              userInfo:@{kPostModificationNotificationUpdateKey:updatedPosts}];
        }
    }
}


#pragma mark - collection view delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    VPPost *post = [self.items objectAtIndex:index];
    
    for (NSUInteger i = 0; i < [self.cellClasses count]; i++) {
        id cellClass = self.cellClasses[i];
        if ([cellClass performSelector:@selector(isAcceptable:) withObject:post]) {
            VPPostListSmallCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.cellNibNames[i] forIndexPath:indexPath];
            cell.containerController = self;
            cell.post = post;
            return cell;
        }
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    VPPost *post = [self.items objectAtIndex:index];
    
    UIViewController *toVC = nil;
    if ([post isThreadRoot]) {
        toVC = [[VPPostThreadViewController alloc] initWithThreadRoot:post];
    } else {
        toVC = [[VPPostViewController alloc] initWithPost:post];
    }

    toVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toVC animated:YES];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5.0f;
}


#pragma mark - VPPostActionsDelegate
- (void)didActionComment:(VPPost *)post
{
    UIViewController *vc = [[VPPostViewController alloc]initWithPost:post];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didActionOptionDelete:(VPPost *)post
{
    // The delete happens on cell within list, we need an animation
    NSInteger itemIndex = [self getItemIndexForPostId:post.postId];
    if (itemIndex >= 0) {
        NSIndexPath *indexPath = [self getIndexPathFromFromItemIndex:itemIndex];
        
        // Modify the backing data array
        [self.items removeObjectAtIndex:itemIndex];
        [self.cellSizes removeObjectAtIndex:itemIndex];
        [self calculateSectionInfo];
        
        [UIView animateWithDuration:0.3f animations:^{
            [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
        }];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                            object:self
                                                          userInfo:@{kPostModificationNotificationDeleteKey:@[post]}];
    }
}

- (void)didActionOptionFlag:(VPPost *)post
{
    [self didActionOptionDelete:post];
}

- (void)didActionVote:(VPPost *)post
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                        object:self
                                                      userInfo:@{kPostModificationNotificationUpdateKey:@[post]}];
}


#pragma mark - scrollview delegate
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    if (self.enableScrollingNavbar && self.showNavbarWhenScrollToTop)
        [self showNavbar];
    return YES;
}


#pragma mark - private methods
- (void)handlePostModificationNotification:(NSNotification *)noti
{
    id sender = [noti object];
    if (sender == self)
        return;
    
    NSDictionary *userinfo = [noti userInfo];
    if (userinfo) {
        [self handlePostModificationUpdate:userinfo[kPostModificationNotificationUpdateKey]
                                    delete:userinfo[kPostModificationNotificationDeleteKey]
                                    insert:userinfo[kPostModificationNotificationInsertKey]];
    }
}

@end
