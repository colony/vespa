//
//  VPPostDetailAudioView.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/6/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostDetailAudioView.h"

#import "VPAudioView.h"
#import "VPMedia.h"
#import "VPPost.h"


@interface VPPostDetailAudioView ()

@property (weak, nonatomic) IBOutlet VPAudioView *audioView;

@end


@implementation VPPostDetailAudioView

#pragma mark - override
- (void)setupUI
{
    [super setupUI];
    self.audioView.clipsToBounds = YES;
    self.audioView.layer.cornerRadius = 3;
}

+ (NSValue *)sizeOfPost:(VPPost *)post
{
    CGFloat audioViewHeight = (SCREEN_WIDTH - 20.0f)/3;
    
    // 25 - top location label
    // 30 - group label
    // caption height
    // 10 - vertical spacing between caption and photo
    // audio height
    // 10 - vertical spacing between photo and stats
    // 40 - stats
    // 10 - extra spacing at bottom
    
    CGFloat width = SCREEN_WIDTH;
    CGFloat height = 25.0f + 30.0f + [self sizeOfCaption:post].height + 10.0f + audioViewHeight + 10.0f + 40.0f + 10.0f;
    return [NSValue valueWithCGSize:CGSizeMake(width, height)];
}

- (void)setPost:(VPPost *)post
{
    [super setPost:post];
    VPMedia *audioMedia = post.audioMedia;
    self.audioView.audioUrl = audioMedia.audioUrl;
    self.audioView.audioDuration = audioMedia.audioDuration;
}

@end
