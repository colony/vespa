//
//  VPPostDetailTextView.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/5/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostDetailTextView.h"

@implementation VPPostDetailTextView

#pragma mark - override
+ (NSValue *)sizeOfPost:(VPPost *)post
{
    // 30 - group button
    // caption height
    // 30 - vertical spacing between caption and stats
    // 40 - stats
    // 10 - extra spacing at bottom
    
    CGFloat width = SCREEN_WIDTH;
    CGFloat height = 30.0f + [self sizeOfCaption:post].height + 30.0f + 40.0f + 10.0f;;
    return [NSValue valueWithCGSize:CGSizeMake(width, height)];
}

@end
