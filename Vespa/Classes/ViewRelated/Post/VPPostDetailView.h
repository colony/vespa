//
//  VPPostDetailView.h
//  Vespa
//
//  Created by Jiayu Zhang on 10/5/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostView.h"

@interface VPPostDetailView : VPPostView

+ (CGSize)sizeOfCaption:(VPPost *)post;

@end
