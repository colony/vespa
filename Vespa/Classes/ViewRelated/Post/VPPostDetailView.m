//
//  VPPostDetailView.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/5/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostDetailView.h"

#import "UIButton+Addition.h"


#define CAPTION_PADDING 20.0f


@interface VPPostDetailView ()

@property (weak, nonatomic) IBOutlet UIView *statsView;

@end


@implementation VPPostDetailView

#pragma mark - lifecycle
- (void)setupUI
{
    [super setupUI];
    self.captionLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - CAPTION_PADDING;
    
    self.actionButton.userInteractionEnabled = NO;
    
    self.statsView.layer.masksToBounds = NO;
    self.statsView.layer.shadowColor = [VPUI darkGrayColor].CGColor;
    self.statsView.layer.shadowOpacity = 0.5;
    self.statsView.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.statsView.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0.0f, 10.0f, SCREEN_WIDTH, 30.0f)].CGPath;
    
    self.groupButton.userInteractionEnabled = YES;
    [self.groupButton setHitTestEdgeInsets:UIEdgeInsetsMake(0, -10, 0, -30)];
}


#pragma mark - public
+ (CGSize)sizeOfCaption:(VPPost *)post
{
    return [self sizeOfCaption:post boundingWidth:(SCREEN_WIDTH - CAPTION_PADDING) font:nil maxLine:0];
}

@end
