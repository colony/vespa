//
//  VPPostListCardCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/8/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostListCardCell.h"

#import "UIAlertView+Blocks.h"
#import "UIView+EventIntercept.h"
#import "VPAccount.h"
#import "VPAPIClient.h"
#import "VPMedia.h"
#import "VPPost.h"
#import "VPPostThreadViewController.h"
#import "VPThread.h"
#import "VPUsers.h"
#import "VPUtil.h"


@interface VPPostListCardCell ()

@property (weak, nonatomic) IBOutlet UIView *textBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UIButton *winnerButton;

@end

@implementation VPPostListCardCell

#pragma mark - lifecycle
- (void)setupUI
{
    [super setupUI];
    self.textBackgroundView.stopEventPropagation = YES;
    self.textLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 30.0f;
}

- (void)resetCell
{
    [super resetCell];
    self.winnerButton.hidden = YES;
    self.winnerButton.selected = NO;
}

+ (BOOL)isAcceptable:(id)obj
{
    if ([super isAcceptable:obj]) {
        return [((VPPost *)obj) isCardType];
    }
    return NO;
}

+ (NSValue *)sizeOfPost:(VPPost *)post
{
    // 20 - top
    // text
    // 25 - group
    // 35 - stats
    
    // Compute size of text body
    // 30 - left plus right padding of label
    // 100 - top plus bottom padding of label
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    UIFont *font = [VPUI fontOpenSans:20.0f thickness:2];
    CGRect rect = [post.textMedia.text boundingRectWithSize:CGSizeMake(SCREEN_WIDTH - 30.0f, MAXFLOAT)
                                                    options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                 attributes:@{NSFontAttributeName:font,
                                                              NSParagraphStyleAttributeName:paragraphStyle}
                                                    context:nil];
    CGFloat textHeight = ceilf(rect.size.height) + 100.0f;

    CGFloat width = SCREEN_WIDTH;
    CGFloat height = 20.0f + textHeight + 25.0f + 35.0f;
    return [NSValue valueWithCGSize:CGSizeMake(width, height)];
}

- (void)setPost:(VPPost *)post
{
    [super setPost:post];
    
    self.textBackgroundView.backgroundColor = [VPUtil colorFromHexString:post.textMedia.textBackgroundColor];
    self.textLabel.textColor = [VPUtil colorFromHexString:post.textMedia.textColor];
    self.textLabel.text = post.textMedia.text;
    
    if ([self isInThread] && ![post isThreadRoot]) {
        VPPostThreadViewController *vc = (VPPostThreadViewController *)self.containerController;
        VPThread *thread = vc.thread;
        if (thread && [thread.type isEqualToString:@"CARD_GAME"]) {
            long long winPostId = [[thread getThreadData:@"WIN_POST_ID"] longLongValue];
            if (winPostId > 0) {
                if (winPostId == [post.postId longLongValue]) {
                    self.winnerButton.hidden = NO;
                    self.winnerButton.selected = YES;
                } else {
                    self.winnerButton.hidden = YES;
                }
            } else {
                // Only thread owner can declare winner
                NSNumber *ownerId = @([[thread getThreadData:@"OWNER_ID"] longLongValue]);
                if ([[VPUsers fetchMe].userId isEqualToNumber:ownerId]) {
                    self.winnerButton.hidden = NO;
                    self.winnerButton.selected = NO;
                }
            }
        }
    }
}


#pragma mark - action handling
- (IBAction)winnerButtonPressed:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if (btn.selected) {
        // Do nothing, a won post CAN NOT be undeclared
    } else {
        if ([self.post.owner.userId isEqualToNumber:[VPUsers fetchMe].userId]) {
            [UIAlertView showWithTitle:@""
                               message:@"You can't declare yourself the winner!"
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil
                              tapBlock:nil];
        } else {
            [UIAlertView showWithTitle:@""
                               message:@"Are you sure this one is the winner?"
                     cancelButtonTitle:@"NO"
                     otherButtonTitles:@[@"YES"]
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == 1) { // 0 - NO, 1 - YES
                                      VPAPIClient *client = [VPAPIClient sharedInstance];
                                      [client performThreadActionWithId:self.post.threadId actionKey:@"DECLARE_WINNER" actionVal:[self.post.postId stringValue] success:nil failure:nil];
                                      
                                      VPPostThreadViewController *vc = (VPPostThreadViewController *)self.containerController;
                                      VPThread *thread = vc.thread;
                                      [thread setThreadData:@"WIN_POST_ID" value:[self.post.postId stringValue]];
                                      
                                      [CATransaction begin];
                                      [CATransaction setDisableActions:YES];
                                      [vc.collectionView reloadData];
                                      [CATransaction commit];
                                  }
                              }];
        }
    }
}


#pragma mark - private
- (BOOL)isInThread
{
    return [self.containerController isKindOfClass:[VPPostThreadViewController class]];
}

@end
