//
//  VPPostListPhotoCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/15/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPostListPhotoCell.h"

#import "VPMedia.h"
#import "VPPhotoView.h"
#import "VPPost.h"
#import "VPPostActions.h"
#import "VPUtil.h"


@interface VPPostListPhotoCell ()

@property (weak, nonatomic) IBOutlet VPPhotoView *photoView;

@end

@implementation VPPostListPhotoCell

#pragma mark - lifecycle
- (void)setupUI
{
    [super setupUI];
    
//    NSArray *photoImageViewConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[photoImageView(<=maxHeight@750)]"
//                                                                                 options:0
//                                                                                 metrics:@{@"maxHeight":@(SCREEN_WIDTH)}
//                                                                                   views:@{@"photoImageView":self.photoImageView}];
//    [self.photoImageView addConstraints:photoImageViewConstraints];
//    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapped:)];
//    tap.numberOfTapsRequired = 1;
//    [self.photoImageView addGestureRecognizer:tap];
}

- (void)resetCell
{
    [super resetCell];
    [self.photoView resetUI];
}


#pragma mark - override
+ (BOOL)isAcceptable:(id)obj
{
    if ([super isAcceptable:obj]) {
        VPPost *post = obj;
        return [post isPhotoType] || [post isVideoType] || [post hasLinkMedia];
    }
    return NO;
}

+ (NSValue *)sizeOfPost:(VPPost *)post
{
    CGFloat screenWidth = SCREEN_WIDTH - 20.0f;
    
    // 20 - top
    // 5  - vertical spacing between top and photo
    // photo height
    // 10 - vertical spacing between caption and photo
    // caption height
    // 10 - vertical spacing between caption and group
    // 25 - group
    // 35 - stats
    
    CGFloat photoWidth = 0.0f;
    CGFloat photoHeight = 0.0f;
    if ([post isPhotoType]) {
        photoWidth = post.photoMedia.photoWidth * 1.0f;
        photoHeight = post.photoMedia.photoHeight * 1.0f;
    } else if ([post isVideoType]) {
        photoWidth = post.videoThumbnailMedia.photoWidth * 1.0f;
        photoHeight = post.videoThumbnailMedia.photoHeight * 1.0f;
    } else if ([post isLinkType]) {
        photoWidth = post.linkMedia.linkEmbedItemWidth * 1.0f;
        photoHeight = post.linkMedia.linkEmbedItemHeight * 1.0f;
    }
    
    CGFloat targetHeight = screenWidth * (photoHeight / photoWidth);
    // For YOUTUBE and GIF, we don't shrink the photo
    if (![post hasLinkMedia] || [post.linkMedia.linkEmbedItemFormat isEqualToString:@"IMAGE"]) {
        targetHeight = MIN(targetHeight, screenWidth);
    }
    
    CGFloat captionHeight = [self sizeOfCaption:post boundingWidth:screenWidth font:nil maxLine:3].height;
    
    CGFloat width = SCREEN_WIDTH;
    CGFloat height = 20.0f + 5.0f + targetHeight + 10.0f + captionHeight + 10.0f + 25.0f + 35.0f;
    return [NSValue valueWithCGSize:CGSizeMake(width, height)];
}

- (void)setPost:(VPPost *)post
{
    [super setPost:post];
    [self.photoView setPost:post];
}

- (void)sharePost
{
    if ([self.post isPhotoType] || [self.post isVideoType]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *sharedImage = nil;
            UIImage *originalImage = self.photoView.image;
            UIImage *watermarkImage = [UIImage imageNamed:@"ninja_watermark"]; //(136,40)
            CGSize originalSize = originalImage.size;
            UIGraphicsBeginImageContextWithOptions(originalSize, YES, 0.0f);
            [originalImage drawInRect:CGRectMake(0, 0, originalSize.width, originalSize.height)];
            [watermarkImage drawInRect:CGRectMake(originalSize.width - 136 - 10, originalSize.height - 40 - 10, 136, 40)];
            sharedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [VPPostActions doActionShare:self.containerController delegate:self.containerController post:self.post image:sharedImage];
            });
        });
    } else {
        [super sharePost];
    }
}

@end
