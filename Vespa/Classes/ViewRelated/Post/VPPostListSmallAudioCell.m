//
//  VPPostListSmallAudioCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/1/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostListSmallAudioCell.h"

#import "VPAudioView.h"
#import "VPMedia.h"
#import "VPPost.h"


@interface VPPostListSmallAudioCell ()

@property (weak, nonatomic) IBOutlet VPAudioView *audioView;

@end


@implementation VPPostListSmallAudioCell

- (void)setupUI
{
    [super setupUI];
    self.captionLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 120.0f;
}

+ (BOOL)isAcceptable:(id)obj
{
    if ([super isAcceptable:obj]) {
        VPPost *post = obj;
        return [post isAudioType];
    }
    return NO;
}

- (void)resetCell
{
    [super resetCell];
    [self.audioView resetUI];
}

- (void)setPost:(VPPost *)post
{
    [super setPost:post];
    VPMedia *audioMedia = post.audioMedia;
    self.audioView.audioUrl = audioMedia.audioUrl;
    self.audioView.audioDuration = audioMedia.audioDuration;
}

@end
