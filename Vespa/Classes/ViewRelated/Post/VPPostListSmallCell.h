//
//  VPPostListSmallCell.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/29/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostView.h"


@interface VPPostListSmallCell : VPPostView

/** If YES, the thread related UI should be hidden */
@property (assign, nonatomic) BOOL hideThreadInfo;

@end