//
//  VPPostListSmallCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/29/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostListSmallCell.h"

#import "VPGeoUtil.h"
#import "VPPost.h"
#import "VPPostThreadViewController.h"


static CGFloat const _kVPSmallCellHeight = 140.0f;


@interface VPPostListSmallCell ()

// thread related
@property (weak, nonatomic) IBOutlet UIView *threadAnnotationView;
@property (weak, nonatomic) IBOutlet UIButton *threadAnnotationButton;

@end


@implementation VPPostListSmallCell

#pragma mark - lifecycle
- (void)setupUI
{
    [super setupUI];
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = SCREEN_SCALE;
    
    self.actionButton.userInteractionEnabled = YES;
    self.groupButton.userInteractionEnabled = NO;
    
//    self.layer.masksToBounds = NO;
//    self.layer.shadowColor = [VPUI darkGrayColor].CGColor;
//    self.layer.shadowOpacity = 0.5;
//    self.layer.shadowOffset = CGSizeMake(0.0f,2.0f);
//    self.layer.shadowRadius = 0.0;
//    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, SCREEN_WIDTH, _kVPSmallCellHeight)].CGPath;
}

- (void)resetCell
{
    [super resetCell];
    self.threadAnnotationView.hidden = YES;
}


#pragma mark - override
+ (NSValue *)sizeOfPost:(VPPost *)post
{
    return [NSValue valueWithCGSize:CGSizeMake(SCREEN_WIDTH, _kVPSmallCellHeight)];
}


#pragma mark - public
- (void)setPost:(VPPost *)post
{
    [super setPost:post];
    
    // thread related
    if ([self shouldDisplayThreadInfo]) {
        [self.threadAnnotationButton setTitle:self.post.threadAnnotation forState:UIControlStateNormal];
        self.threadAnnotationView.hidden = NO;
    } else {
        self.threadAnnotationView.hidden = YES;
    }
}


#pragma mark - action handling
- (IBAction)threadAnnotationButtonPressed:(id)sender
{
    UIViewController *vc = [[VPPostThreadViewController alloc] initWithThreadRoot:self.post];
    vc.hidesBottomBarWhenPushed = YES;
    [self.containerController.navigationController pushViewController:vc animated:YES];
}


#pragma mark - private
- (BOOL)isInThread
{
    return [self.containerController isKindOfClass:[VPPostThreadViewController class]];
}

- (BOOL)shouldDisplayThreadInfo
{
    if (self.hideThreadInfo)
        return NO;
    return ![self isInThread] && [self.post isThreadRoot];
}

@end
