//
//  VPPostListSmallMediaCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/1/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostListSmallPhotoCell.h"

#import "VPPhotoView.h"
#import "VPPost.h"
#import "VPPostActions.h"


@interface VPPostListSmallPhotoCell ()

@property (weak, nonatomic) IBOutlet VPPhotoView *photoView;

@end


@implementation VPPostListSmallPhotoCell

- (void)setupUI
{
    [super setupUI];
    self.captionLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 120.0f;
    self.photoView.clipsToBounds = YES;
    self.photoView.shouldStartAnimatingAutomatically = YES;
}

+ (BOOL)isAcceptable:(id)obj
{
    if ([super isAcceptable:obj]) {
        VPPost *post = obj;
        return [post isPhotoType] || [post isVideoType] || [post hasLinkMedia];
    }
    return NO;
}

- (void)resetCell
{
    [super resetCell];
    [self.photoView resetUI];
}

- (void)setPost:(VPPost *)post
{
    [super setPost:post];
    [self.photoView setPost:post];
}

- (void)sharePost
{
    if ([self.post isPhotoType] || [self.post isVideoType]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *sharedImage = nil;
            UIImage *originalImage = self.photoView.image;
            UIImage *watermarkImage = [UIImage imageNamed:@"ninja_watermark"]; //(136,40)
            CGSize originalSize = originalImage.size;
            UIGraphicsBeginImageContextWithOptions(originalSize, YES, 0.0f);
            [originalImage drawInRect:CGRectMake(0, 0, originalSize.width, originalSize.height)];
            [watermarkImage drawInRect:CGRectMake(originalSize.width - 136 - 10, originalSize.height - 40 - 10, 136, 40)];
            sharedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [VPPostActions doActionShare:self.containerController delegate:self.containerController post:self.post image:sharedImage];
            });
        });
    } else {
        [super sharePost];
    }
}

@end
