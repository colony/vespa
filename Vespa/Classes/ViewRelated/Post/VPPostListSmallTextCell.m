//
//  VPPostListSmallTextCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/29/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostListSmallTextCell.h"
#import "VPPost.h"


@implementation VPPostListSmallTextCell

- (void)setupUI
{
    [super setupUI];
    self.captionLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 20.0f;
}

+ (BOOL)isAcceptable:(id)obj
{
    if ([super isAcceptable:obj]) {
        VPPost *post = obj;
        return [post isTextType] || [post isCardType] || ([post isLinkType] && ![post hasLinkMedia]);
    }
    return NO;
}

@end
