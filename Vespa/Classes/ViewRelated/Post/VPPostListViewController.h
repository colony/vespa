//
//  VPPostListViewController.h
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPPostCollectionViewController.h"

/**
 * A common postlist contains specific impl for various purpose
 */
@interface VPPostListViewController : VPPostCollectionViewController

- (id)initWithHashtag:(NSString *)hashtag;

- (id)initWithAnnouncement;

- (id)initWithMyPost;

- (id)initWithNearby;

@end
