//
//  VPPostListViewController.m
//  Vespa
//
//  Created by Jay on 12/7/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import "VPPostListViewController.h"


#import "UINavigationItem+Util.h"
#import "VPPaginator.h"
#import "VPPost.h"
#import "VPPosts.h"
#import "VPUsers.h"
#import "VPUtil.h"


typedef NS_ENUM(NSInteger, VPPostListPurpose)
{
    kVPPurposeHashtag = 0,
    kVPPurposeAnnouncement,
    kVPPurposeMyPost,
    kVPPurposeNearby
};


@interface VPPostListViewController ()

@property (strong, nonatomic) UILabel *noDataIndicatorLabel;
@property (strong, nonatomic) VPPaginator *paginator;
@property (assign, nonatomic) VPPostListPurpose purpose;
@property (strong, nonatomic) NSString *hashtag;
@property (assign, nonatomic) BOOL loadWhenDidAppear;

@end

@implementation VPPostListViewController

#pragma mark - lifecycle
- (id)initWithHashtag:(NSString *)hashtag
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        [self commonInit];
        self.hashtag = hashtag;
        self.purpose = kVPPurposeHashtag;
        self.reportEventName = kVPEventViewPostHashtag;
        self.reportParam = @{@"hashtag":self.hashtag};
    }
    return self;
}

- (id)initWithAnnouncement
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        [self commonInit];
        self.purpose = kVPPurposeAnnouncement;
        self.reportEventName = kVPEventViewAnnouncement;
        self.enableLoadMore = NO; // We load all announcements for now, in future, we might need support pagination
    }
    return self;
}

- (id)initWithMyPost
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        [self commonInit];
        self.purpose = kVPPurposeMyPost;
        self.reportEventName = kVPEventViewMyPost;
    }
    return self;
}

- (id)initWithNearby
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        [self commonInit];
        self.purpose = kVPPurposeNearby;
        self.reportEventName = kVPEventViewNearby;
    }
    return self;
}

- (void)commonInit
{
    [super commonInit];
    self.showNavbarWhenScrollToTop = YES;
    self.enableScrollingNavbar = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    switch (self.purpose) {
        case kVPPurposeAnnouncement:
            [self.navigationItem setCustomTitle:@"Announcements"];
            break;
            
        case kVPPurposeHashtag:
            [self.navigationItem setCustomTitle:[NSString stringWithFormat:@"#%@", self.hashtag]];
            break;
            
        case kVPPurposeMyPost:
            [self.navigationItem setCustomTitle:@"My Post"];
            break;
            
        case kVPPurposeNearby:
            [self.navigationItem setCustomTitle:@"Nearby"];
            break;
    }
    self.loadWhenDidAppear = YES;
}

- (void)setupUI
{
    [super setupUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.loadWhenDidAppear) {
        self.loadWhenDidAppear = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
        [self pullDownRefresh];
    }
}


#pragma mark - override function
-(void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSMutableDictionary *) param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    
    @weakify(self);
    void (^fetchSuccess)(VPPostList*) = ^(VPPostList *postList) {
        @strongify(self);
        self.paginator = postList.paginator;
        [self onObjectsDidLoaded:postList.posts pageNo:pageNo error:nil];
        [self attemptShowNoDataIndicator];
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
    };
    
    void (^fetchFailure)(NSError*) = ^(NSError *error) {
        @strongify(self);
        VPLog(@"Error: %@", [error localizedDescription]);
        [self onObjectsDidLoaded:nil pageNo:pageNo error:error];
        [self attemptShowNoDataIndicator];
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
    };
    
    switch (self.purpose) {
        case kVPPurposeAnnouncement:{
            self.ongoingRequestOperation = [VPPosts fetchPostsWithType:kVPPostListFetchTypeAnnouncement
                                                                   url:nil
                                                                 param:nil
                                                               success:^(VPPostList *postList){
                                                                   @strongify(self);
                                                                   self.paginator = postList.paginator;
                                                                   [self onObjectsDidLoaded:postList.posts pageNo:pageNo error:nil];
                                                                   [self attemptShowNoDataIndicator];
                                                                   [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                                   
                                                                   if (pageNo == 0) {
                                                                       // Pull to refresh the latest announcement, will basically clear out unread counters
                                                                       [VPUsers updateUserStatusPropertyKey:@"numUnreadAnnouncements" value:0 incr:NO zero:YES syncTime:[VPUtil currentSystemTimeInMs]];
                                                                   }
                                                               }
                                                               failure:fetchFailure];
        }
            break;
            
        case kVPPurposeHashtag:
            self.ongoingRequestOperation = [VPPosts fetchPostsWithType:kVPPostListFetchTypeHashtag
                                                                   url:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                                 param:@{@"hashtag":self.hashtag}
                                                               success:fetchSuccess
                                                               failure:fetchFailure];
            break;
            
        case kVPPurposeMyPost:
            self.ongoingRequestOperation = [VPPosts fetchMyPostsWithUrl:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                                  param:nil
                                                                success:fetchSuccess
                                                                failure:fetchFailure];
            break;
            
        case kVPPurposeNearby:
            self.ongoingRequestOperation = [VPPosts fetchPostsWithType:kVPPostListFetchTypeNearbyRecent
                                                                   url:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                                 param:nil
                                                               success:fetchSuccess
                                                               failure:fetchFailure];
            break;
            
        default:
            break;
    }
}


#pragma mark - private
- (void)attemptShowNoDataIndicator
{
    if ([self.items count] > 0) {
        self.noDataIndicatorLabel.hidden = YES;
        [self.noDataIndicatorLabel removeFromSuperview];
        self.noDataIndicatorLabel = nil;
    } else {
        if (!self.noDataIndicatorLabel) {
            self.noDataIndicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.collectionView.width, 20.0f)];
            self.noDataIndicatorLabel.textAlignment = NSTextAlignmentCenter;
            self.noDataIndicatorLabel.textColor = [VPUI darkGrayColor];
            self.noDataIndicatorLabel.text = @"No posts yet.";
            self.noDataIndicatorLabel.center = self.collectionView.center;
            
            [self.collectionView addSubview:self.noDataIndicatorLabel];
            [self.collectionView bringSubviewToFront:self.noDataIndicatorLabel];
        }
        self.noDataIndicatorLabel.hidden = NO;
    }
}

@end