//
//  VPThreadViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 9/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPostCollectionViewController.h"

@class VPThread;

@interface VPPostThreadViewController : VPPostCollectionViewController

@property (strong, nonatomic) NSNumber *threadId;
@property (strong, nonatomic) VPThread *thread;

- (id)initWithThreadRoot:(VPPost *)rootPost;

/** NOTE, threadId is same as ID of root post of the thread */
- (id)initWithThreadId:(NSNumber *)threadId;

@end