//
//  VPThreadViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 9/6/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPostThreadViewController.h"

#import "UIAlertView+Blocks.h"
#import "UINavigationItem+Util.h"
#import "VPBaseCollectionReusableView.h"
#import "VPMediaCardViewController.h"
#import "VPNavigationController.h"
#import "VPPaginator.h"
#import "VPPost.h"
#import "VPPostListCardCell.h"
#import "VPPostListPhotoCell.h"
#import "VPPostListSmallAudioCell.h"
#import "VPPostListSmallTextCell.h"
#import "VPPosts.h"
#import "VPPostViewController.h"
#import "VPThread.h"
#import "VPThreads.h"
#import "VPTutorialManager.h"
#import "VPUsers.h"
#import "VPUtil.h"


static CGFloat const _kVPThreadResponseHeaderViewHeight = 40.0f;

@interface _VPThreadResponseHeaderView : VPBaseCollectionReusableView
@end


@implementation _VPThreadResponseHeaderView

- (void)setupUI
{
    [super setupUI];
    self.userInteractionEnabled = NO;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, 0.0f, 200.0f, _kVPThreadResponseHeaderViewHeight)];
    imageView.image = [UIImage imageNamed:@"response_header"];
    imageView.contentMode = UIViewContentModeLeft;
    [self addSubview:imageView];
}

+ (NSValue *)sizeOfView
{
    return [NSValue valueWithCGSize:CGSizeMake(SCREEN_WIDTH, _kVPThreadResponseHeaderViewHeight)];
}

@end



@interface VPPostThreadViewController ()

@property (strong, nonatomic) VPPaginator *paginator;
@property (assign, nonatomic) BOOL loadWhenDidAppear;

@property (strong, nonatomic) VPPost *rootPost;

@property (strong, nonatomic) UIButton *actionButton;

@end


@implementation VPPostThreadViewController

#pragma mark - lifecycle
- (id)initWithThreadId:(NSNumber *)threadId
{
    if (self = [super init]) {
        _threadId = threadId;
    }
    return self;
}

- (id)initWithThreadRoot:(VPPost *)rootPost
{
    if (self = [super init]) {
        _rootPost = rootPost;
        _threadId = _rootPost.threadId;
    }
    return self;
}

- (void)commonInit
{
    [super commonInit];
    self.showNavbarWhenScrollToTop = YES;
    self.enableScrollingNavbar = YES;
    self.enableTabbarHandle = NO;
    self.sectionHeaderClass = [_VPThreadResponseHeaderView class];
    self.cellClasses = @[[VPPostListPhotoCell class], [VPPostListSmallAudioCell class], [VPPostListCardCell class], [VPPostListSmallTextCell class]];
    self.cellNibNames = @[@"VPPostListPhotoCell", @"VPPostListSmallAudioCell", @"VPPostListCardCell", @"VPPostListSmallTextCell"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setCustomTitle:@"The Thread"];
}

- (void)setupUI
{
    [super setupUI];
    
    // Create action button, NOTE [super setupUI] should setup collectionView already
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [VPUI colonyColor];
    self.actionButton.frame = CGRectMake(0, self.view.height - 35.0f, SCREEN_WIDTH, 35.0f);
    self.actionButton.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    self.actionButton.titleLabel.textColor = [UIColor whiteColor];
    self.actionButton.titleLabel.font = [VPUI fontOpenSans:15.0f thickness:2];
    [self.actionButton addTarget:self action:@selector(actionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.actionButton];
    
    [self loadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [VPReports reportEvent:kVPEventViewPostThread withParam:@{@"threadId":self.threadId} mode:kVPEventModeStart];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [VPReports reportEvent:kVPEventViewPostThread withParam:@{@"threadId":self.threadId} mode:kVPEventModeEnd];
}


#pragma mark - override function
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    @weakify(self);
    self.ongoingRequestOperation = [VPPosts fetchPostsWithType:kVPPostListFetchTypeThread
                                                           url:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                         param:@{@"threadId":self.threadId}
                                                       success:^(VPPostList *postList) {
                                                           @strongify(self);
                                                           self.paginator = postList.paginator;
                                                           // If first page, we need insert thread root post into items array for collectionview data
                                                           if (pageNo == 0) {
                                                               [self updateThreadData:postList.thread];
                                                               NSMutableArray *ary = [NSMutableArray arrayWithArray:postList.posts];
                                                               [ary insertObject:self.rootPost atIndex:0];
                                                               [self onObjectsDidLoaded:ary pageNo:pageNo error:nil];
                                                               
                                                               NSNumber *ownerId = @([[self.thread getThreadData:@"OWNER_ID"] longLongValue]);
                                                               if ([[VPUsers fetchUserId] isEqualToNumber:ownerId] && [ary count] > 1) {
                                                                   [[VPTutorialManager sharedInstance] start:kVPTutorialThreadWinner];
                                                               }
                                                           } else {
                                                               [self onObjectsDidLoaded:postList.posts pageNo:pageNo error:nil];
                                                           }
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                       } failure:^(NSError *error) {
                                                           @strongify(self);
                                                           VPLog(@"Fail to fetchPostsByThread %@: %@", self.threadId, [error localizedDescription]);
                                                           [self onObjectsDidLoaded:nil pageNo:pageNo error:error];
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                       }];
}

- (void)calculateSectionInfo
{
    [self.sectionInfo removeAllObjects];
    [self.sectionInfo addObject:@(1)];
    [self.sectionInfo addObject:@(self.items.count - 1)];
}


#pragma mark - collection view delegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return [super collectionView:collectionView layout:collectionViewLayout referenceSizeForHeaderInSection:section];
    }
    return CGSizeZero;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSInteger index = [self getItemIndexFromIndexPath:indexPath];
    VPPost *post = [self.items objectAtIndex:index];
    
    UIViewController *toVC = [[VPPostViewController alloc] initWithPost:post];
    toVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toVC animated:YES];
}


#pragma mark - action handling
- (void)actionButtonPressed:(id)sender
{
    if (!self.rootPost || !self.thread)
        return;
    
    [[VPContext sharedInstance] putDomain:kVPCDMediaSubmission key:@"groupId" val:self.rootPost.groupId];
    [[VPContext sharedInstance] putDomain:kVPCDMediaSubmission key:@"groupName" val:self.rootPost.groupName];
    [[VPContext sharedInstance] putDomain:kVPCDMediaSubmission key:@"threadId" val:self.thread.threadId];
    
    NSString *type = [self.rootPost mediaType];
    // If the thread has allowed post types, use them, otherwise, use the same as root post's postType
    if ([self.thread.allowedPostTypes count] > 0) {
        // For now (kVPAppVersion=2.3.0), we will only fetch the first allowed post type from the array
        type = [self.thread.allowedPostTypes firstObject];
    }
    
    // For now, the thread action is identical to root post's postType, in future, we might add more fun
    if ([type isEqualToString:@"CARD"]) {
        VPMediaCardViewController *vc = [[VPMediaCardViewController alloc] initResponseCardWithDeckId:[self.thread getThreadData:@"DECK_ID"]];
        VPNavigationController *nc = [[VPNavigationController alloc] initWithRootViewController:vc];
        nc.customNavigatonGestureEnabled = NO;
        [nc configureDefaultStyle];
        nc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        nc.modalPresentationCapturesStatusBarAppearance = YES;
        [self presentViewController:nc animated:YES completion:nil];
        return;
    }
    [VPUtil presentMediaViewController:self mediaType:type];
}


#pragma mark - private
- (void)loadData
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:self userInfo:nil];
    if (self.rootPost) {
        [self pullDownRefresh];
    } else {
        @weakify(self);
        [VPThreads fetchRootPostWithId:self.threadId
                               success:^(VPPost *post) {
                                   @strongify(self);
                                   self.rootPost = post;
                                   [self pullDownRefresh];
                               } failure:^(NSError *error) {
                                   @strongify(self);
                                   VPLog(@"Fail to fetch thread with ID %@: %@", self.threadId, [error localizedDescription]);
                                   [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:self userInfo:nil];
                                   
                                   id response = [[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey];
                                   if (response) {
                                       NSInteger statusCode = [((NSHTTPURLResponse *)response) statusCode];
                                       if (statusCode == 404) {
                                           [self show404Alert];
                                       }
                                   }
                               }];
    }
}

- (void)show404Alert
{
    [UIAlertView showWithTitle:@""
                       message:@"This thread is no longer available"
             cancelButtonTitle:@"Back"
             otherButtonTitles:nil
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          [self.navigationController popViewControllerAnimated:YES];
                      }];
}

- (void)updateThreadData:(VPThread *)thread
{
    self.thread = thread;
    [self.actionButton setTitle:thread.actionTerm forState:UIControlStateNormal];
    [self.navigationItem setCustomTitle:thread.title];
}

@end
