//
//  VPPostView.h
//  Vespa
//
//  Created by Jiayu Zhang on 10/7/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPBaseCollectionViewCell.h"

@class VPPost;
@protocol VPPostActionsDelegate;

@interface VPPostView : VPBaseCollectionViewCell

/**
 * UI components that are accessible to subclass
 */
@property (weak, nonatomic) IBOutlet UILabel  *captionLabel;
@property (weak, nonatomic) IBOutlet UIButton *groupButton;
// Usually, it is comments button, under some special cases, it might be different button
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (strong, nonatomic) VPPost *post;
@property (weak, nonatomic) UIViewController<VPPostActionsDelegate> *containerController;

/**
 * Helper to calculate boundingSize of a post's caption
 * font - if nil, openSans-14.0-regular will be assumed
 * maxLine - maximum number of lines allowed, if 0, no maximum
 */
+ (CGSize)sizeOfCaption:(VPPost *)post boundingWidth:(CGFloat)width font:(UIFont *)font maxLine:(NSInteger)maxLine;
+ (NSString *)captionForPost:(VPPost *)post;

+ (NSValue *)sizeOfPost:(VPPost *)post;

/**
 * Triggered if user select "share" option in action sheet, each subclass should override this
 */
- (void)sharePost;

@end
