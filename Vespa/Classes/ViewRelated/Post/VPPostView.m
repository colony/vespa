//
//  VPPostView.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/7/15.
//  Copyright © 2015 Colony. All rights reserved.
//

#import "VPPostView.h"

#import "NSString+Util.h"
#import "VPConfigManager.h"
#import "VPGeoUtil.h"
#import "VPMedia.h"
#import "VPGroupViewController.h"
#import "VPPost.h"
#import "VPPostActions.h"


#define VOTEDOWN_BUTTON_TAG 1
#define VOTEUP_BUTTON_TAG 2

@interface VPPostView ()<VPPostActionsDelegate>

@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

// Cover mask
@property (weak, nonatomic) IBOutlet UIView *blockMaskView;
@property (weak, nonatomic) IBOutlet UILabel *blockMaskLabel;
@property (weak, nonatomic) IBOutlet UIImageView *blockMaskImageView; // tomato cover

// Stats
@property (weak, nonatomic) IBOutlet UIButton *downvoteButton;
@property (weak, nonatomic) IBOutlet UIButton *upvoteButton;

// Vote animation
@property (assign, nonatomic) BOOL voteAnimationGate;
@property (weak, nonatomic) IBOutlet UIImageView *animatedVoteImageView;

@end


@implementation VPPostView

#pragma mark - lifecycle
- (void)setupUI
{
    [super setupUI];
    if (self.blockMaskView) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blockMaskViewTapped:)];
        [self.blockMaskView addGestureRecognizer:tap];
    }
}

- (void)resetCell
{
    [super resetCell];
    
    if (self.animatedVoteImageView && self.voteAnimationGate) {
        [self.animatedVoteImageView.layer removeAllAnimations];
        self.animatedVoteImageView.hidden = YES;
        self.voteAnimationGate = NO;
    }
}


#pragma mark - override
+ (BOOL)isAcceptable:(id)obj
{
    return [obj isKindOfClass:[VPPost class]];
}

+ (NSValue *)sizeOfCell:(id)obj
{
    return [self sizeOfPost:(VPPost *)obj];
}


#pragma mark - public
+ (CGSize)sizeOfCaption:(VPPost *)post boundingWidth:(CGFloat)width font:(UIFont *)font maxLine:(NSInteger)maxLine
{
    CGFloat captionWidth = width;
    CGFloat captionHeight = 0.0f;
    
    NSString *caption = [self captionForPost:post];
    UIFont *captionFont = font ?: [VPUI fontOpenSans:14.0f thickness:1];
    CGRect rect = [caption boundingRectWithSize:CGSizeMake(captionWidth, MAXFLOAT)
                                        options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                     attributes:@{NSFontAttributeName:captionFont}
                                        context:nil];
    captionHeight = ceilf(rect.size.height);
    if (maxLine > 0) {
        captionHeight = MIN(ceilf(captionFont.lineHeight * maxLine), captionHeight);
    }
    return CGSizeMake(captionWidth, captionHeight);
}

+ (NSString *)captionForPost:(VPPost *)post
{
    NSString *caption = nil;
    if ([post isLinkType]) {
        caption = [@"[LINK] " stringByAppendingString:post.linkMedia.linkTitle];
    } else if ([post isTextType] || [post isCardType]) {
        caption = post.textMedia.text;
    } else {
        caption = post.body;
    }
    if ([NSString isEmptyString:caption]) {
        caption = @"[NO CAPTION]";
    }
    return caption;
}

+ (NSValue *)sizeOfPost:(VPPost *)post
{
    return [NSValue valueWithCGSize:CGSizeZero];
}

- (void)setPost:(VPPost *)post
{
    _post = post;
    
    // Location
    if (self.locationLabel) {
        self.locationLabel.text = [VPGeoUtil formatDistanceFromHere:post.geo withPost:post];
    }
    
    // Vote
    self.downvoteButton.selected = [post isDownvote];
    [self.downvoteButton setTitle:[post.numOfDownvotes stringValue] forState:UIControlStateNormal];
    [self.downvoteButton setTitle:[post.numOfDownvotes stringValue] forState:UIControlStateSelected];
    self.upvoteButton.selected = [post isUpvote];
    [self.upvoteButton setTitle:[post.numOfUpvotes stringValue] forState:UIControlStateNormal];
    [self.upvoteButton setTitle:[post.numOfUpvotes stringValue] forState:UIControlStateSelected];
    
    // Comment (action)
    if ([post.numOfComments longLongValue] <= 0) {
        [self.actionButton setTitle:@"Chat" forState:UIControlStateNormal];
    } else {
        [self.actionButton setTitle:[NSString stringWithFormat:@"Chat (%@)", post.numOfComments] forState:UIControlStateNormal];
    }
    
    // Cover
    if (self.blockMaskView) {
        if (post.isBlocked) {
            self.blockMaskView.hidden = NO;
            self.blockMaskImageView.hidden = YES;
            self.blockMaskLabel.text = @"Awaiting moderation";
        } else if ([[VPConfigManager sharedInstance] getBool:kVPConfigBadReviewCoverEnabled] && post.isBadReviewed) {
            self.blockMaskView.hidden = NO;
            self.blockMaskImageView.hidden = NO;
            self.blockMaskLabel.text = @"Too many tomatoes";
        } else {
            self.blockMaskView.hidden = YES;
        }
    }
    
    // Group
    if (self.groupButton) {
        [self.groupButton setTitle:(post.isAnnouncement ? @"Announcement" : post.groupName) forState:UIControlStateNormal];
    }
    
    // Caption
    if (self.captionLabel) {
        self.captionLabel.text = [VPPostView captionForPost:post];
    }
}

- (void)sharePost
{
    if ([self.post isTextType] || [self.post isCardType]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(320.0f, 320.0f), YES, 2.0f);
            UIImage *image = [UIImage imageNamed:@"ShareTextBackground"]; //[[UIImage alloc] initWithContentsOfFile:imagePath];
            [image drawInRect:CGRectMake(0.0f, 0.0f, 320.0f, 320.0f)];
            
            NSString *drawText = self.post.textMedia.text;
            NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            paragraphStyle.alignment = NSTextAlignmentCenter;
            NSDictionary *textAttrs = @{
                                        NSFontAttributeName:[VPUI fontOpenSans:19.0f thickness:2],
                                        NSForegroundColorAttributeName:[UIColor whiteColor],
                                        NSParagraphStyleAttributeName:paragraphStyle
                                        };
            
            CGRect drawRect = [drawText boundingRectWithSize:CGSizeMake(300, 300)
                                                     options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                                  attributes:textAttrs
                                                     context:nil];
            [drawText drawInRect:CGRectMake((320 - drawRect.size.width)/2, (320 - drawRect.size.height)/2, drawRect.size.width, drawRect.size.height) withAttributes:textAttrs];
            
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [VPPostActions doActionShare:self.containerController delegate:self.containerController post:self.post image:newImage];
            });
        });
    }
    
    else if ([self.post isPhotoType] || [self.post isVideoType]) {
        // Leave photo subclass to override this method
    }
    
    else { // LINK, AUDIO
        [VPPostActions doActionShare:self.containerController delegate:self.containerController post:self.post image:nil];
    }
}


#pragma mark - action handling
- (IBAction)actionButtonPressed:(id)sender
{
    [VPPostActions doActionComment:self.containerController delegate:self.containerController post:self.post];
}

- (void)blockMaskViewTapped:(id)sender
{
    self.blockMaskView.hidden = YES;
}

- (IBAction)groupButtonPressed:(id)sender
{
    // If the post doesn't have group or General group (id=1) or special group (id=0,
    // this is a special group containing all special posts such as announcement posts)
    if (self.post.groupId == nil || [self.post.groupId intValue] <= 1) {
        return;
    }
    
    VPGroupViewController *groupVC = [[VPGroupViewController alloc] initWithGroupId:self.post.groupId groupName:self.post.groupName];
    groupVC.hidesBottomBarWhenPushed = YES;
    [self.containerController.navigationController pushViewController:groupVC animated:YES];
}

- (IBAction)optionButtonPressed:(id)sender
{
    // NOTE, we set the cell as delegate of option action, by knowing which option has been selected
    // the cell could impl certain option better than containerController, for example, SHARE function
    [VPPostActions doActionOption:self.containerController delegate:self post:self.post];
}

- (IBAction)voteButtonPressed:(id)sender
{
    if (self.voteAnimationGate)
        return;
    self.voteAnimationGate = YES;
    
    // NOTE, we use cell as delegate of vote action, for detail, check didActionVote
    [VPPostActions doActionVote:self.containerController delegate:self.containerController post:self.post isDownvoteClicked:(((UIButton *)sender).tag == VOTEDOWN_BUTTON_TAG)];
    
    self.downvoteButton.selected = [self.post isDownvote];
    [self.downvoteButton setTitle:[self.post.numOfDownvotes stringValue] forState:UIControlStateNormal];
    [self.downvoteButton setTitle:[self.post.numOfDownvotes stringValue] forState:UIControlStateSelected];
    self.upvoteButton.selected = [self.post isUpvote];
    [self.upvoteButton setTitle:[self.post.numOfUpvotes stringValue] forState:UIControlStateNormal];
    [self.upvoteButton setTitle:[self.post.numOfUpvotes stringValue] forState:UIControlStateSelected];
    
    if (![self.post isNovote]) {
        self.animatedVoteImageView.image = [UIImage imageNamed:([self.post isDownvote]?@"tomatosplat":@"starsplat")];
        self.animatedVoteImageView.hidden = NO;
        
        CALayer *layer = self.animatedVoteImageView.layer;
        CAKeyframeAnimation *voteAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
        
        CATransform3D startingScale = CATransform3DScale (layer.transform, 0, 0, 0);
        CATransform3D middleScale = layer.transform;
        CATransform3D endingScale = layer.transform;
        NSArray *boundsValues = @[[NSValue valueWithCATransform3D:startingScale],
                                  [NSValue valueWithCATransform3D:middleScale],
                                  [NSValue valueWithCATransform3D:endingScale]];
        [voteAnimation setValues:boundsValues];
        
        NSArray *times = @[[NSNumber numberWithFloat:0.0f],
                           [NSNumber numberWithFloat:0.2f],
                           [NSNumber numberWithFloat:1.0f]];
        [voteAnimation setKeyTimes:times];
        voteAnimation.delegate = self;
        voteAnimation.repeatCount = 1;
        voteAnimation.duration = 0.8;
        [layer addAnimation:voteAnimation forKey:@"transform"];
    } else {
        self.voteAnimationGate = NO;
    }
    
    if ([self.post isDownvote]) {
        [VPReports reportEvent:kVPEventDownvotePost withParam:@{@"postId":self.post.postId} mode:kVPEventModeOnce];
    } else if ([self.post isUpvote]) {
        [VPReports reportEvent:kVPEventUpvotePost withParam:@{@"postId":self.post.postId} mode:kVPEventModeOnce];
    }
}


#pragma mark - UIView animation delegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (self.animatedVoteImageView) {
        self.animatedVoteImageView.hidden = YES;
        self.voteAnimationGate = NO;
    }
}


#pragma mark - VPPostActionsDelegate
- (void)didActionOptionDelete:(VPPost *)post
{
    [self.containerController didActionOptionDelete:post];
}

- (void)didActionOptionFlag:(VPPost *)post
{
    [self.containerController didActionOptionFlag:post];
}

- (void)didActionOptionShare:(VPPost *)post
{
    [self sharePost];
}

@end
