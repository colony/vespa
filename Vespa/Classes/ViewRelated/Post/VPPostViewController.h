//
//  VPPostViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "SLKTextViewController.h"

@class VPPost;

@interface VPPostViewController : SLKTextViewController

- (id)initWithPost:(VPPost *)post;

- (id)initWithPostId:(NSNumber *)postId;

@end
