//
//  VPPostViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/7/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPostViewController.h"

#import "NSString+Util.h"
#import "UIActionSheet+Blocks.h"
#import "UIButton+Addition.h"
#import "UINavigationItem+Util.h"
#import "UIViewController+ScrollingNavbar.h"
#import "VPComment.h"
#import "VPCommentCell.h"
#import "VPCommentCellInfo.h"
#import "VPCommentCoverCell.h"
#import "VPCommentPhotoCell.h"
#import "VPCommentPhotoPickerViewController.h"
#import "VPComments.h"
#import "VPGeoUtil.h"
#import "VPMedia.h"
#import "VPPost.h"
#import "VPPostActions.h"
#import "VPPostDetailView.h"
#import "VPPostInputTextView.h"
#import "VPPosts.h"
#import "VPTutorialManager.h"
#import "VPUser.h"
#import "VPUtil.h"
#import "VPWebViewController.h"


static NSString * const _kVPCommentCellIdentifier = @"VPCommentCell";
static NSString * const _kVPCommentPhotoCellIdentifier = @"VPCommentPhotoCell";
static NSString * const _kVPCommentCoverCellIdentifier = @"VPCommentCoverCell";
static CGFloat const _kVPCommentCoverCellHeight = 40.0f;


@interface VPPostViewController ()<VPPostActionsDelegate,UIAlertViewDelegate,VPCommentCellDelegate>
{
    BOOL _didSetupUI;
}

@property (strong, nonatomic) VPPostDetailView *detailView;
//@property (nonatomic, strong) SSPullToRefreshView *refreshView;

@property (strong, nonatomic) VPPost *post;
@property (strong, nonatomic) NSNumber *postId;

@property (strong, nonatomic) NSNumber *replyingCommentId; // The comment ID, currently being replied to
@property (strong, nonatomic) NSIndexPath *replyingIndexPath;
@property (strong, nonatomic) NSArray *expandedIndexPaths; // A list of indexPaths

@property (strong, nonatomic) NSMutableArray *items; // Datasource backed the table
@property (strong, nonatomic) NSMutableArray *cellInfos; // Internal cache for cell info such as height, etc

@property (assign, nonatomic) BOOL isLoading;
@property (assign, nonatomic) BOOL isSending;

// Comment photo picker option
@property (strong, nonatomic) NSNumber *pickedOptionItemId;

@end


@implementation VPPostViewController


- (instancetype)initWithPost:(VPPost *)post
{
    if (self = [super initWithTableViewStyle:UITableViewStylePlain]) {
        _post = post;
        _postId = post.postId;
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithPostId:(NSNumber *)postId
{
    if (self = [super initWithTableViewStyle:UITableViewStylePlain]) {
        _post = nil;
        _postId = postId;
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    _didSetupUI = NO;
    _isLoading = NO;
    _isSending = NO;
    _items = [NSMutableArray new];
    _cellInfos = [NSMutableArray new];
    [self registerClassForTextView:[VPPostInputTextView class]];
}

+ (UITableViewStyle)tableViewStyleForCoder:(NSCoder *)decoder
{
    return UITableViewStylePlain;
}


#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // This is a weird bug from SlackTextViewController, when inverted it will give a content inset 64 on top
    // unless we DON'T set UIRectEdgeTop
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.extendedLayoutIncludesOpaqueBars = NO;
    
    // Setup navigationbar
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationItem setCustomBackTitle:@""];
    
    // !important, seems like SLK will initialize the tableview with ZERO frame, this will break the autolayout
    // constraints when we create the detail cell on ios7
    self.tableView.frame = self.view.bounds;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, self.textInputbar.appropriateHeight, 0);
    
    // SLK customization
    self.inverted = NO; // chronological order
    self.shakeToClearEnabled = YES;
    self.keyboardPanningEnabled = YES;
    self.shouldScrollToBottomAfterKeyboardShows = NO;
    [self.leftButton setImage:[[UIImage imageNamed:@"photo_picker_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [self.leftButton setHitTestEdgeInsets:UIEdgeInsetsMake(-15.0f, -8.0f, -15.0f, -8.0f)];
    [self.rightButton setTitle:@"Send" forState:UIControlStateNormal];
    self.textInputbar.autoHideRightButton = YES;
    self.textInputbar.backgroundColor = [VPUI lightGrayColor];
    
    // Table view
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerNib:[UINib nibWithNibName:@"VPCommentCell" bundle:nil] forCellReuseIdentifier:_kVPCommentCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"VPCommentPhotoCell" bundle:nil] forCellReuseIdentifier:_kVPCommentPhotoCellIdentifier];
    [self.tableView registerClass:[VPCommentCoverCell class] forCellReuseIdentifier:_kVPCommentCoverCellIdentifier];

//    _refreshView = [[SSPullToRefreshView alloc] initWithScrollView:self.tableView delegate:self];
//    _refreshView.y = self.tableView.bounds.size.height;
//    _refreshView.expandedHeight = 60.f;
//    _refreshView.contentView = [[VPPullToRefreshContentView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, 60.f)];
//    _refreshView.backgroundColor = UIColorFromRGB(0x0c68c3);
//    _refreshView.transform = self.tableView.transform;

    if (self.post && [self.post isLinkType]) {
        // If we init with post obj, we put the 'LINK' button on navbar, otherwise, we wait till we load the post
        UIButton *readBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        readBtn.frame = CGRectMake(0, 0, 50, 50);
        readBtn.contentMode = UIViewContentModeScaleToFill;
        readBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [readBtn addTarget:self action:@selector(readButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [readBtn setImage:[UIImage imageNamed:@"read_button"] forState:UIControlStateNormal];
        [self.navigationItem setCustomRightWithView:readBtn];
        [self.navigationItem tweakRightWithSpace:-8];
        
        [[VPTutorialManager sharedInstance] start:kVPTutorialLinkSource];
    }
    
    [self.navigationItem setCustomTitle:[VPGeoUtil formatDistanceFromHere:self.post.geo withPost:self.post]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_didSetupUI) {
        [self setupUI];
        _didSetupUI = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [VPReports reportEvent:kVPEventViewPost withParam:@{@"postId":self.postId} mode:kVPEventModeStart];
    
    NSNumber *selectedItemId = [[VPContext sharedInstance] getDomain:kVPCDPostViewController key:@"optionItemId"];
    UIImage *selectedItemMedia = [[VPContext sharedInstance] getDomain:kVPCDPostViewController key:@"optionItemMedia"];
    if (selectedItemId) {
        self.pickedOptionItemId = selectedItemId;
        // SLKTextInput's leftbutton is SystemType, we have to hack with renderingMode to avoid tint color
        [self.leftButton setImage:[selectedItemMedia imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [VPReports reportEvent:kVPEventViewPost withParam:@{@"postId":self.postId} mode:kVPEventModeEnd];
    
    [[VPContext sharedInstance] clearDomain:kVPCDPostViewController];
}

- (void)setupUI
{
//    self.tableView.frame = self.view.bounds;
    
    // Try to load local post (IF POSSIBLE),
    // note that, if we initWithPostId, the method will issue a remote request and show spinner
    // otherwise, it will reuse the post object passed in
    [self loadData:YES spinner:YES offsetToZero:YES expandRefreshView:NO];
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[VPCommentCoverCell class]])
        return;
    
    if (!self.replyingIndexPath) {
        [((VPCommentCell *)cell) setSelectedForReply:NO];
        return;
    }
    
    if (indexPath.row == self.replyingIndexPath.row && indexPath.section == self.replyingIndexPath.section) {
        [((VPCommentCell *)cell) setSelectedForReply:YES];
    } else {
        [((VPCommentCell *)cell) setSelectedForReply:NO];
    }
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    VPComment *comment = [self.items objectAtIndex:index];
    VPCommentCellInfo *info = [self.cellInfos objectAtIndex:index];
    
    UITableViewCell *returnCell;
    if (info.isCovered) {
        returnCell = [self.tableView dequeueReusableCellWithIdentifier:_kVPCommentCoverCellIdentifier];
    } else if ([comment isPhotoType]) {
        VPCommentPhotoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:_kVPCommentPhotoCellIdentifier];
        cell.delegate = self;
        [cell setComment:comment postOwnerId:self.post.owner.userId info:[self.cellInfos objectAtIndex:indexPath.row]];
        returnCell = cell;
    } else {
        VPCommentCell *cell = [self.tableView dequeueReusableCellWithIdentifier:_kVPCommentCellIdentifier];
        cell.delegate = self;
        [cell setComment:comment postOwnerId:self.post.owner.userId info:[self.cellInfos objectAtIndex:indexPath.row]];
        returnCell = cell;
    }
    
    // Cells must inherit the table view's transform, since it may be inverted
//    returnCell.transform = tableView.transform;
    return returnCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.items.count < indexPath.row)
        return 0.f;
    
    NSInteger index = indexPath.row;
    VPCommentCellInfo *info = [self.cellInfos objectAtIndex:index];
    if ([info isEqual:[NSNull null]]) {
        info = [[VPCommentCellInfo alloc] init];
        VPComment *comment = [self.items objectAtIndex:index];
        if ([comment isPhotoType]) {
            [[VPCommentPhotoCell class] heightOfComment:comment withInfo:info];
        } else {
            [[VPCommentCell class] heightOfComment:comment withInfo:info];
        }
        info.isCovered = comment.isBadReviewed;
        self.cellInfos[index] = info;
    }
    
    if (info.isCovered)
        return _kVPCommentCoverCellHeight;
    return info.height;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    VPCommentCellInfo *info = [self.cellInfos objectAtIndex:index];
    if (info && info.isCovered)
        return YES;
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    VPCommentCellInfo *info = [self.cellInfos objectAtIndex:index];
    if (info.isCovered) {
        info.isCovered = NO;
        [UIView performWithoutAnimation:^{
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
    }
}


#pragma mark - override
- (void)didPressLeftButton:(id)sender
{
    if (self.isSending)
        return;
    
    if (self.pickedOptionItemId) { // We already picked an option item
        [UIActionSheet showInView:self.view
                        withTitle:nil
                cancelButtonTitle:@"Cancel"
           destructiveButtonTitle:nil
                otherButtonTitles:@[@"Retake", @"Remove"]
                         tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
                             NSString *bt = [actionSheet buttonTitleAtIndex:buttonIndex];
                             if ([bt isEqualToString:@"Retake"]) {
                                 [self.view endEditing:YES];
                                 VPCommentPhotoPickerViewController *pickerVC = [[VPCommentPhotoPickerViewController alloc] init];
                                 [self.navigationController pushViewController:pickerVC animated:YES];
                             } else if ([bt isEqualToString:@"Remove"]) {
                                 self.pickedOptionItemId = nil;
                                 [self.leftButton setImage:[[UIImage imageNamed:@"photo_picker_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
                             }
                         }];
    } else {
        VPCommentPhotoPickerViewController *pickerVC = [[VPCommentPhotoPickerViewController alloc] init];
        [self.navigationController pushViewController:pickerVC animated:YES];
    }
}

- (void)didPressRightButton:(id)sender
{
    if (self.isSending)
        return;
    
    if ([NSString isEmptyString:self.textView.text])
        return;
    
    self.isSending = YES;
    
    // This little trick validates any pending auto-correction or auto-spelling just after hitting the 'Send' button
    [self.textView refreshFirstResponder];
    
    [VPReports reportEvent:kVPEventCommentPost withParam:@{@"postId":self.post.postId} mode:kVPEventModeOnce];
    if (self.replyingCommentId) {
        [VPReports reportEvent:kVPEventReplyComment withParam:@{@"cmtId":self.replyingCommentId} mode:kVPEventModeOnce];
    }

    @try {
        [VPComments createCommentWithPostId:self.post.postId
                         parentCommentId:self.replyingCommentId
                                    body:[self.textView.text copy]
                 photoPickerOptionItemId:self.pickedOptionItemId
                                   image:nil
                                 success:^(VPComment *comment) {
                                     [self.view endEditing:YES];
                                     
                                     // Clear photo picker selection
                                     self.pickedOptionItemId = nil;
                                     [self.leftButton setImage:[[UIImage imageNamed:@"photo_picker_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
                                     
                                     // Update tableview
                                     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.items.count inSection:0];
                                     UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
                                     UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
                                     
                                     [self.tableView beginUpdates];
                                     [self.items addObject:comment];
                                     [self.cellInfos addObject:[NSNull null]];
                                     
                                     [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
                                     [self.tableView endUpdates];
                                     
                                     // Fixes the cell from blinking (because of the transform, when using translucent cells)
                                     // See https://github.com/slackhq/SlackTextViewController/issues/94#issuecomment-69929927
                                     // [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                     
                                     // Update numOfComments
                                     self.post.numOfComments = [NSNumber numberWithLong:(self.post.numOfComments.longValue + 1)];
//                                     [self.detailView setNumCommentsUI]; // Refresh UI
                                     [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                                                         object:self
                                                                                       userInfo:@{kPostModificationNotificationUpdateKey:@[self.post]}];
                                     
                                     [super didPressRightButton:sender];
                                     [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
                                     
                                     self.isSending = NO;
                                 } failure:^(NSError *error) {
                                     VPLog(@"Fail to create cmt: %@", [error localizedDescription]);
                                     [self.view endEditing:YES];
                                     // Clear photo picker selection
                                     self.pickedOptionItemId = nil;
                                     [self.leftButton setImage:[[UIImage imageNamed:@"photo_picker_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
                                     [super didPressRightButton:sender];
                                     self.isSending = NO;
                                 }];
    }
    @catch (NSException *ex) {
        // Peace the warning that, we MUST call parent's didPressRightButton
        [super didPressRightButton:sender];
    }
}

- (void)didChangeKeyboardStatus:(SLKKeyboardStatus)status
{
    if (status == SLKKeyboardStatusDidShow) {
        if (self.replyingIndexPath) {
            [self.tableView reloadData];
            [self.tableView scrollToRowAtIndexPath:self.replyingIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    } else if (status == SLKKeyboardStatusDidHide) {
        if (self.replyingCommentId) {
            // Cancel the selected replyingCommentCell
            self.replyingCommentId = nil;
            self.replyingIndexPath = nil;
            [self.tableView reloadData];
        }
    }
}


#pragma mark - pull to refresh delegate
//- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view
//{
//    [self loadData:NO spinner:NO offsetToZero:NO expandRefreshView:YES];
//}


#pragma mark - action handling
- (void)readButtonPressed:(id)sender
{
    VPMedia *linkMedia = self.post.linkMedia;
    VPWebViewController *webVC = [[VPWebViewController alloc] initWithURL:[NSURL URLWithString:linkMedia.linkUrl] customTitle:linkMedia.linkHost];
    [self.navigationController pushViewController:webVC animated:YES];
}


#pragma mark - VPCommentCellDelegate
- (void)commentReplyButtonPressed:(VPCommentCell *)cell
{
    self.replyingCommentId = cell.comment.commentId;
    self.replyingIndexPath = [self.tableView indexPathForCell:cell];
    
    // Skips if already first responder
    if ([self.textView isFirstResponder]) {
        [self.tableView reloadData];
        [self.tableView scrollToRowAtIndexPath:self.replyingIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        return;
    }
    
    [self presentKeyboard:YES];
}

- (void)commentParentCommentViewTapped:(VPCommentCell *)cell
{
    NSIndexPath *ip = [self.tableView indexPathForCell:cell];
    VPCommentCellInfo *info = self.cellInfos[ip.row];
    [info toggle];
    
    // Turn off UIView animations so that it changes instantly without jerkyness
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.tableView reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationNone];
    [UIView setAnimationsEnabled:animationsEnabled];
}


#pragma mark - VPPostActionsDelegate
- (void)didActionComment:(VPPost *)post
{
    [self.textView becomeFirstResponder];
}

- (void)didActionOptionDelete:(VPPost *)post
{
    [[NSNotificationCenter defaultCenter] postNotificationName: kPostModificationNotification
                                                        object: self
                                                      userInfo:@{kPostModificationNotificationDeleteKey:@[post]}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didActionOptionFlag:(VPPost *)post
{
    [self didActionOptionDelete:post]; // Treat flag as same as Delete!!
}

- (void)didActionLike:(VPPost *)post
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                        object:self
                                                      userInfo:@{kPostModificationNotificationUpdateKey:@[post]}];
}

- (void)didActionStar:(VPPost *)post
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification
                                                        object:self
                                                      userInfo:@{kPostModificationNotificationUpdateKey:@[post]}];
}


#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 404) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        // The alertView might be the shakeUndoAlert, let SLK handle it
//        [super alertView:alertView didDismissWithButtonIndex:buttonIndex];
    }
}


#pragma mark - private method
/**
 useLocalPostIfPossible -
 set if use local VPPost object to populate content, otherwise, send request to fetch a new one.
 Note that, if there's no local VPPost, the method will still send a remote request
 spinner -
 show spinner while request remotely
 */
- (void)loadData:(BOOL)useLocalPostIfPossible spinner:(BOOL)spinner offsetToZero:(BOOL)offsetToZero expandRefreshView:(BOOL)expandRefreshView
{
    if (spinner)
        [[NSNotificationCenter defaultCenter] postNotificationName: kLoadingSpinnerShowNotification
                                                            object: self
                                                          userInfo: nil];
    if (useLocalPostIfPossible && self.post) {
        [self createDetailViewIfNeed];
        
        // Load comments
        [self internalPullDownRefresh:offsetToZero expandRefreshView:expandRefreshView];
    } else {
        @weakify(self);
        [VPPosts fetchPostWithId:self.postId
                           param:nil
                         success:^(VPPost *post) {
                             @strongify(self);
                             self.post = post;
                             
                             if ([self.post isLinkType]) {
                                 // If we init with post obj, we put the 'read' button on navbar, otherwise, we wait till we load the post
                                 UIButton *readBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                                 readBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                                 [readBtn addTarget:self action:@selector(readButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                                 [readBtn setTitle:@"READ" forState:UIControlStateNormal];
                                 [self.navigationItem setCustomRightWithView:readBtn];
                             }
                             
                             if (self.detailView) {
                                 self.detailView.post = self.post;
                             } else {
                                 [self createDetailViewIfNeed];
                             }
                             
                             // Notify interested parties about latest synced VPPost
                             [[NSNotificationCenter defaultCenter] postNotificationName:kPostModificationNotification object:self
                                                                               userInfo:@{kPostModificationNotificationUpdateKey:@[self.post]}];
                             
                             // Then, also, refresh all comments
                             [self internalPullDownRefresh:offsetToZero expandRefreshView:expandRefreshView];
                         } failure:^(NSError *error) {
                             @strongify(self);
                              VPLog(@"Fail to fetch post with ID %@: %@", self.postId, [error localizedDescription]);
                           //  [self.refreshView finishLoading];
                             [[NSNotificationCenter defaultCenter] postNotificationName: kLoadingSpinnerHideNotification
                                                                                 object: self
                                                                               userInfo: nil];
                             
                             id response = [[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey];
                             if (response) {
                                 NSInteger statusCode = [((NSHTTPURLResponse *)response) statusCode];
                                 if (statusCode == 404) {
                                     [self show404Alert];
                                 }
                             }
                         }];
    }
}

- (void)internalPullDownRefresh:(BOOL)offsetToZero expandRefreshView:(BOOL)expandRefreshView
{
    if (offsetToZero)
        [self.tableView setContentOffset:CGPointZero animated:NO];
    [self loadObjectsOfPageNo:0 parameters:@{@"expandRefreshView":@(expandRefreshView)}];
}

- (void)show404Alert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"This post is no longer available" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil];
    alert.tag = 404;
    [alert show];
}

/** Precondition, self.post not nil */
- (void)createDetailViewIfNeed
{
    if (self.detailView)
        return;
    
    if (!self.post)
        return;
    
    NSString *detailViewName;
    if ([self.post isTextType] || [self.post isCardType] || ([self.post isLinkType] && ![self.post hasLinkMedia])) {
        detailViewName = @"VPPostDetailTextView";
    } else if ([self.post isPhotoType] || [self.post isVideoType] || [self.post hasLinkMedia]) {
        detailViewName = @"VPPostDetailPhotoView";
    } else if ([self.post isAudioType]) {
        detailViewName = @"VPPostDetailAudioView";
    }

    id detailViewClass = NSClassFromString(detailViewName);
    NSValue *sizeValue = [detailViewClass performSelector:@selector(sizeOfPost:) withObject:self.post];
    CGSize size = [sizeValue CGSizeValue];
    self.detailView = [[[NSBundle mainBundle] loadNibNamed:detailViewName owner:nil options:nil] objectAtIndex:0];
    self.detailView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
    self.detailView.containerController = self;
    [self.detailView setPost:self.post];
    NSLog(self.detailView.userInteractionEnabled ? @"JIAYU ENABLED" : @"JIAYU DISABLED");
    
    // Draw shadow bneath the detailView
//    self.detailView.clipsToBounds = NO;
//    self.detailView.layer.shadowColor = [VPUI darkGrayColor].CGColor;
//    self.detailView.layer.shadowOpacity = 0.8;
//    self.detailView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
//    self.detailView.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0.0f, 0.0f, size.width, size.height)].CGPath;
    
    if (self.inverted) {
        self.tableView.tableFooterView = self.detailView;
        self.tableView.tableFooterView.backgroundColor = [UIColor whiteColor];
        // https://github.com/slackhq/SlackTextViewController/issues/157
        self.tableView.tableHeaderView = [UIView new]; // Create empty view to remove unwanted seperator
    } else {
        self.tableView.tableHeaderView = self.detailView;
        self.tableView.tableHeaderView.backgroundColor = [UIColor whiteColor];
        // https://github.com/slackhq/SlackTextViewController/issues/157
        self.tableView.tableFooterView = [UIView new]; // Create empty view to remove unwanted seperator
    }
}

- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSDictionary *)param
{
    // NOTE, we load all comments, pageNo always 0
    
    if (self.isLoading)
        return;
    self.isLoading = YES;
    
    if (pageNo == 0) {
        if (param) {
            NSNumber *v = [param objectForKey:@"expandRefreshView"];
            if (v && [v boolValue]) {
             //   [self.refreshView startLoadingAndExpand:YES animated:YES];
            }
        }
    }
    
    NSDictionary *fetchParam = [[NSDictionary alloc]initWithObjectsAndKeys:
                                @"0",@"_e", // Inclusive
                                @"1",@"_o", // Ascending
                                [NSNull null], @"_b", // Before the NULL boundary, that means the end of list
                                @1, @"all",  // Fetch all comments
                                nil];
    @weakify(self);
    [VPComments fetchCommentsWithPostId:self.postId
                                 url:nil
                               param:fetchParam
                             success:^(VPCommentList *commentList) {
                                 @strongify(self);
                                 
                                 [self.items removeAllObjects];
                                 [self.cellInfos removeAllObjects];
                                 [self.items addObjectsFromArray:commentList.comments];
                                 for (int i=0; i<self.items.count; i++)
                                     [self.cellInfos addObject:[NSNull null]];
                                 
                                 [self.tableView reloadData];
                                 
                                 self.isLoading = NO;
                             //    [self.refreshView finishLoading];
                                 [[NSNotificationCenter defaultCenter] postNotificationName: kLoadingSpinnerHideNotification
                                                                                     object: self
                                                                                   userInfo: nil];
                             } failure:^(NSError *error) {
                                 @strongify(self);
                                 VPLog(@"%@", [error localizedDescription]);

                                 self.isLoading = NO;
                            //     [self.refreshView finishLoading];
                                 [[NSNotificationCenter defaultCenter] postNotificationName: kLoadingSpinnerHideNotification
                                                                                     object: self
                                                                                   userInfo: nil];

                                 id response = [[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey];
                                 if (response) {
                                     NSInteger statusCode = [((NSHTTPURLResponse *)response) statusCode];
                                     if (statusCode == 404) {
                                         [self show404Alert];
                                     }
                                 }
                             }];
}

@end

