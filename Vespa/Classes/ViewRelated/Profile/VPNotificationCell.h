//
//  VPNotificationCell.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VPBaseTableViewCell.h"

@class VPActivity;

@interface VPNotificationCell : VPBaseTableViewCell

@property (weak, nonatomic) VPActivity *activity;

/* A method to inform the cell that it has been selected and dim the content */
- (void)selected;

@end