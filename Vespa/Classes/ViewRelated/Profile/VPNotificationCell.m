//
//  VPNotificationCell.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/27/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPNotificationCell.h"
#import "VPActivity.h"
#import "VPUtil.h"


static CGFloat const _kCellSpace = 5.0f;

@interface VPNotificationCell ()

@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *statsView;

@end


@implementation VPNotificationCell

- (void)setupUI
{
    [super setupUI];
    self.bodyLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 40.0f;
}

- (void)resetCell
{
    [super resetCell];
}

- (void)setActivity:(VPActivity *)activity
{
    _activity = activity;
    
    if (activity.read) {
        _bodyLabel.textColor = [VPUI mediumGrayColor];
    } else {
        _bodyLabel.textColor = [UIColor blackColor];
    }
    _bodyLabel.text = activity.body;
    _timeLabel.text = [VPUtil formatDateSinceNow:activity.createTime];
}

+ (NSNumber *)heightOfCell:(id)obj
{
    CGFloat width = SCREEN_WIDTH;
    
    VPActivity *act = obj;
    // Calculate the height for caption label
    UIFont *font = [VPUI fontOpenSans:14.0f thickness:1];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    CGRect rect = [act.body boundingRectWithSize:CGSizeMake(width - 20 - 20, MAXFLOAT)
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                      attributes:@{NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle}
                                          context:nil];
    CGFloat bodyHeight = ceilf(rect.size.height);

    // 10 - top vertical space
    // 5  - margin between body and stats
    // 13 - height of stats
    // _kCellSpace - extra space
    return [NSNumber numberWithFloat:(10.0f + bodyHeight + 5.0f + 13.0f + _kCellSpace)];
}

- (void)selected
{
    self.bodyLabel.textColor = [VPUI mediumGrayColor];
}

@end
