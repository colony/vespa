//
//  VPNotificationViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 1/26/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"
#import "VPBaseTableViewController.h"

@interface VPNotificationViewController : VPBaseTableViewController

@end
