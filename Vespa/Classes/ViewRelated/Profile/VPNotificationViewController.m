//
//  VPNotificationViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/26/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPNotificationViewController.h"

#import "UINavigationItem+Util.h"
#import "VPActivity.h"
#import "VPActivities.h"
#import "VPNotificationCell.h"
#import "VPPaginator.h"
#import "VPPostThreadViewController.h"
#import "VPPostViewController.h"
#import "VPUsers.h"
#import "VPUtil.h"

@interface VPNotificationViewController ()

@property (strong, nonatomic) VPPaginator *paginator;
@property (strong, nonatomic) UILabel *noDataIndicatorLabel;
@property (assign, nonatomic) BOOL loadWhenDidAppear;

@end

@implementation VPNotificationViewController

#pragma mark - lifecycle
- (void)commonInit
{
    [super commonInit];
    self.cellInfos = [NSMutableArray arrayWithObject:[[VPTableCellInfo alloc] initWithClass:[VPNotificationCell class] nib:@"VPNotificationCell" identifier:@"VPNotificationCell"]];
    self.enablePullToRefresh = YES;
    self.enableLoadMore = YES;
    self.enableTabbarHandle = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [VPUI lightGrayColor];
    [self.navigationItem setCustomTitle:@"Notifications"];
    self.loadWhenDidAppear = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTabbarCurrentTapNotification:) name:kTabbarCurrentTapNotification object:nil];
}

- (void)setupUI
{
    [super setupUI];
}

- (void)setupTableView:(UITableView *)tableView
{
    [super setupTableView:tableView];
    [tableView setSeparatorColor:[UIColor clearColor]];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [VPReports reportEvent:kVPEventViewNotification withParam:nil mode:kVPEventModeStart];
    
    if (self.loadWhenDidAppear) {
        self.loadWhenDidAppear = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerShowNotification object:nil];
        [self pullDownRefresh];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [VPReports reportEvent:kVPEventViewNotification withParam:nil mode:kVPEventModeEnd];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTabbarCurrentTapNotification object:nil];
}


#pragma mark - override function
- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSMutableDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    
    @weakify(self);
    self.ongoingRequestOperation = [VPActivities fetchActivitiesWithUrl:(pageNo == 0 ? nil : self.paginator.nextPageUrl)
                                                                  param:nil
                                                                success:^(VPActivityList *actList) {
                                                                    @strongify(self);
                                                                    [self onObjectsDidLoaded:actList.activities pageNo:pageNo error:nil block:nil];
                                                                    [self attemptShowNoDataIndicator];
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
//                                                                    if (pageNo == 0) {
                                                                        // Pull to refresh the latest notification, will basically clear out unread counters
//                                                                        [VPUsers updateUserStatusPropertyKey:@"numUnreadActivities" value:0 incr:NO zero:YES syncTime:[VPUtil currentSystemTimeInMs]];
//                                                                    }
                                                                } failure:^(NSError *error) {
                                                                    @strongify(self);
                                                                    VPLog(@"Fail fetch activities: %@", [error localizedDescription]);
                                                                    [self attemptShowNoDataIndicator];
                                                                    [self onObjectsDidLoaded:nil pageNo:pageNo error:error block:nil];
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:kLoadingSpinnerHideNotification object:nil];
                                                                }];
}


#pragma mark - table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    VPActivity *activity = [self getItemFromIndexPath:indexPath];
    ((VPNotificationCell *)cell).activity = activity;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VPActivity *act = [self getItemFromIndexPath:indexPath];
    if (!act.read) {
        act.read = YES;
        VPNotificationCell *selectedCell = (VPNotificationCell *)[tableView cellForRowAtIndexPath:indexPath];
        [selectedCell selected];
        [VPActivities ackActivityWithId:act.activityId];
        [VPUsers updateUserStatusPropertyKey:@"numUnreadActivities" value:-1 incr:YES zero:YES syncTime:[VPUtil currentSystemTimeInMs]];
    }
    
    // The navigation logic, handle recognized activity type, for the rest, we will
    // check threadId, postId, etc in order
//    if ([act.type isEqualToString:@""]) {
//        
//    }
    
    NSNumber *threadId = act.threadId;
    if (threadId) {
        VPPostThreadViewController *threadVC = [[VPPostThreadViewController alloc] initWithThreadId:threadId];
        threadVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:threadVC animated:YES];
        return;
    }
    
    NSNumber *postId = act.postId;
    if (postId) {
        VPPostViewController *postVC = [[VPPostViewController alloc] initWithPostId:postId];
        postVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:postVC animated:YES];
        return;
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


#pragma mark - notification
- (void)handleTabbarCurrentTapNotification:(NSNotification *)noti
{
    if (noti.userInfo && [noti.userInfo[@"tab"] isEqualToString:@"NOTIFICATION"]) {
        if (self.isTop) {
            [self.tableView setContentOffset:CGPointMake(0.0f, 0.0f) animated:YES];
        }
    }
}


#pragma mark - private method
- (void)attemptShowNoDataIndicator
{
    if ([self.items count] > 0) {
        self.noDataIndicatorLabel.hidden = YES;
        [self.noDataIndicatorLabel removeFromSuperview];
        self.noDataIndicatorLabel = nil;
    } else {
        if (!self.noDataIndicatorLabel) {
            self.noDataIndicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, 20.0f)];
            self.noDataIndicatorLabel.textAlignment = NSTextAlignmentCenter;
            self.noDataIndicatorLabel.textColor = [VPUI darkGrayColor];
            self.noDataIndicatorLabel.text = @"No notifications yet.";
            self.noDataIndicatorLabel.center = self.tableView.center;

            [self.tableView addSubview:self.noDataIndicatorLabel];
            [self.tableView bringSubviewToFront:self.noDataIndicatorLabel];
        }
        self.noDataIndicatorLabel.hidden = NO;
    }
}

@end
