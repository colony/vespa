//
//  VPProfile2ViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 7/1/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPProfile2ViewController.h"

#import "CustomBadge.h"
#import "NSString+Util.h"
#import "UINavigationItem+Util.h"
#import "VPBaseTableViewCell.h"
#import "VPConfigManager.h"
#import "VPNotificationViewController.h"
#import "VPPostListViewController.h"
#import "VPStatus.h"
#import "VPUsers.h"
#import "VPUtil.h"
#import "VPWebViewController.h"


static CGFloat const _kVPProfile2CellHeight = 40.0f;

@interface _VPProfile2Cell : VPBaseTableViewCell
@property (strong, nonatomic) CustomBadge *badgeView;
@end

@implementation _VPProfile2Cell

- (void)setupUI
{
    [super setupUI];
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    self.textLabel.font = [UIFont systemFontOfSize:14];
    self.textLabel.textAlignment = NSTextAlignmentLeft;
    [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    self.badgeView = [CustomBadge customBadgeWithString:@""];
    self.badgeView.hidden = YES;
    [self addSubview:self.badgeView];
}

- (void)setTitle:(NSString *)title color:(UIColor *)color badge:(NSString *)badge
{
    self.textLabel.text = title;
    self.textLabel.textColor = color ?: [UIColor darkGrayColor];
    if (badge) {
        [self.badgeView autoBadgeSizeWithString:badge];
        CGFloat textWidth = [title boundingRectWithSize:CGSizeMake(MAXFLOAT, self.textLabel.height)
                                                options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                             attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                                context:nil].size.width;
        self.badgeView.frame = CGRectMake(15 + textWidth + 8, (40 - self.badgeView.height)/2, self.badgeView.width, self.badgeView.height);
        self.badgeView.hidden = NO;
    } else {
        self.badgeView.hidden = YES;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.textLabel.x = 15;
}

+ (NSNumber *)heightOfCell:(id)obj
{
    return [NSNumber numberWithFloat:_kVPProfile2CellHeight];
}

@end


@interface VPProfile2ViewController ()

@property (copy, nonatomic) NSNumber *announcementIndiCount;

@end

@implementation VPProfile2ViewController

#pragma mark - lifecycle
- (void)commonInit
{
    [super commonInit];
    self.cellInfos = [NSMutableArray arrayWithObject:[[VPTableCellInfo alloc] initWithClass:[_VPProfile2Cell class] nib:nil identifier:@"_VPProfile2Cell"]];
    self.enableLoadMore = NO;
    self.enablePullToRefresh = NO;
    self.isGroupStyle = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [VPUI lightGrayColor];
    [self.navigationItem setCustomTitle:@"Me"];

    VPStatus *status = [VPUsers fetchUserStatusLocal];
    if (status) {
        self.announcementIndiCount = status.numUnreadAnnouncements;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleUserStatusChangeNotification:) name:kUserStatusChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTabbarCurrentTapNotification:) name:kTabbarCurrentTapNotification object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self pullDownRefresh];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupUI
{
    [super setupUI];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, _kVPProfile2CellHeight)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 10, _kVPProfile2CellHeight)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:13];
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = NSTextAlignmentRight;
    label.text = [NSString stringWithFormat:@"version: %@", kVPAppVersion];
    [view addSubview:label];
    self.tableView.tableFooterView = view;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [VPReports reportEvent:kVPEventViewProfile withParam:nil mode:kVPEventModeStart];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [VPReports reportEvent:kVPEventViewProfile withParam:nil mode:kVPEventModeEnd];
}


#pragma mark - override function
- (void)calculateSectionInfo
{
    [self.sectionInfos removeAllObjects];
    [self.sectionInfos addObject:[NSNumber numberWithInteger:1]];
    if (![NSString isEmptyString:[[VPConfigManager sharedInstance] getString:KVPConfigNinjrCookieUrl]]) {
        [self.sectionInfos addObject:[NSNumber numberWithInteger:1]];
    }
    [self.sectionInfos addObject:[NSNumber numberWithInteger:2]];
    [self.sectionInfos addObject:[NSNumber numberWithInteger:6]];
    [self.sectionInfos addObject:[NSNumber numberWithInteger:3]];
}

- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSMutableDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    
    // To avoid section header floating (sticky) problem, we create a title cell (with gray background)
    NSMutableArray *menu = [[NSMutableArray alloc] init];
    [menu addObject:@"Announcements"];
    if (![NSString isEmptyString:[[VPConfigManager sharedInstance] getString:KVPConfigNinjrCookieUrl]]) {
        [menu addObject:@"Ninjr Cookie"];
    }
    [menu addObjectsFromArray:@[@"Nearby", @"My Posts"]];
    [menu addObjectsFromArray:@[@"Invite Ninjas", @"Rate Ninjr", @"Like Us on Facebook", @"Follow Us on Twitter", @"Follow Us on Tumblr", @"Give Us Feedback"]];
    [menu addObjectsFromArray:@[@"Terms of Service", @"Privacy Policy", @"Honor Code"]];
    [menu addObject:[NSString stringWithFormat:@"version: %@", kVPAppVersion]];
    [self onObjectsDidLoaded:menu pageNo:pageNo error:nil block:nil];
}


#pragma mark - table view delegate
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (VPTableCellInfo *)cellInfoForIndexPath:(NSIndexPath *)indexPath
{
    return self.cellInfos[0];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _VPProfile2Cell *cell = (_VPProfile2Cell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    NSString *title = [self getItemFromIndexPath:indexPath];
    UIColor *titleColor = nil;
    NSString *badge = nil;
    if ([title isEqualToString:@"Ninjr Cookie"] || [title isEqualToString:@"Give Us Feedback"]) {
        titleColor = [UIColor redColor];
    } else if ([title isEqualToString:@"Announcements"]) {
        long l = [self.announcementIndiCount longValue];
        if (l > 0) {
            badge = l > 99 ? @"99+" : [@(l) stringValue];
        }
    }

    [cell setTitle:title color:titleColor badge:badge];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = [self getItemFromIndexPath:indexPath];
    if ([title isEqualToString:@"Announcements"]) {
        VPPostListViewController *postListVC = [[VPPostListViewController alloc] initWithAnnouncement];
        postListVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:postListVC animated:YES];
    }
    else if ([title isEqualToString:@"Ninjr Cookie"]) {
        [self openWebView:[[VPConfigManager sharedInstance] getString:KVPConfigNinjrCookieUrl] title:@"Ninjr Cookie"];
    }
    else if ([title isEqualToString:@"Nearby"]) {
        VPPostListViewController *postListVC = [[VPPostListViewController alloc] initWithNearby];
        postListVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:postListVC animated:YES];
    }
    else if ([title isEqualToString:@"My Posts"]) {
        VPPostListViewController *postListVC = [[VPPostListViewController alloc] initWithMyPost];
        postListVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:postListVC animated:YES];
    }
    else if ([title isEqualToString:@"Invite Ninjas"]) {
        [VPUtil showInvitationView:self];
    }
    else if ([title isEqualToString:@"Rate Ninjr"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id968448971"]];
    }
    else if ([title isEqualToString:@"Like Us on Facebook"]) {
        NSURL *fbAppURL = [[NSURL alloc] initWithString:@"fb://profile/430271017149314"];
        NSURL *fbWebURL = [[NSURL alloc] initWithString:@"https://www.facebook.com/ninjrapp"];
        [self openAppURL:fbAppURL web:fbWebURL];
    }
    else if ([title isEqualToString:@"Follow Us on Twitter"]) {
        NSURL *twAppURL = [[NSURL alloc] initWithString:@"twitter://user?screen_name=ninjrapp"];
        NSURL *twWebURL = [[NSURL alloc] initWithString:@"https://twitter.com/ninjrapp"];
        [self openAppURL:twAppURL web:twWebURL];
    }
    else if ([title isEqualToString:@"Follow Us on Tumblr"]) {
        NSURL *tmAppURL = [[NSURL alloc] initWithString:@"tumblr://x-callback-url/blog?blogName=ninjrapp"];
        NSURL *tmWebURL = [[NSURL alloc] initWithString:@"http://ninjrapp.tumblr.com"];
        [self openAppURL:tmAppURL web:tmWebURL];
    }
    else if ([title isEqualToString:@"Give Us Feedback"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tell us what you think about the app?"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Send", nil];
        
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert show];
    }
    else if ([title isEqualToString:@"Terms of Service"]) {
        [self openWebView:@"http://ninjrapp.com/terms" title:@"Terms of Service"];
    }
    else if ([title isEqualToString:@"Privacy Policy"]) {
        [self openWebView:@"http://ninjrapp.com/privacy" title:@"Privacy Policy"];
    }
    else if ([title isEqualToString:@"Honor Code"]) {
        [self openWebView:@"http://ninjrapp.com/code" title:@"Honor Code"];
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - notification
- (void)handleUserStatusChangeNotification:(NSNotification *)noti
{
    BOOL needReload = NO;
    VPStatus *status = [VPUsers fetchUserStatusLocal];
    if (status) {
        needReload = ![self.announcementIndiCount isEqualToNumber:status.numUnreadAnnouncements];
        self.announcementIndiCount = status.numUnreadAnnouncements;
    }
    if (needReload) {
        [self.tableView reloadData];
    }
}

- (void)handleTabbarCurrentTapNotification:(NSNotification *)noti
{
    if (noti.userInfo && [noti.userInfo[@"tab"] isEqualToString:@"PROFILE"]) {
        if (self.isTop) {
            [self.tableView setContentOffset:CGPointMake(0.0f, 0.0f) animated:YES];
        }
    }
}


#pragma mark - private method
- (void)openAppURL:(NSURL *)appUrl web:(NSURL *)webUrl
{
    NSURL *url = appUrl;
    if (![[UIApplication sharedApplication] canOpenURL:appUrl])
        url = webUrl;
    [[UIApplication sharedApplication] openURL:url];
}

- (void)openWebView:(NSString *)urlStr title:(NSString *)title
{
    VPWebViewController *web = [[VPWebViewController alloc] initWithURL:[NSURL URLWithString:urlStr] customTitle:title];
    web.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:web animated:YES];
}

@end
