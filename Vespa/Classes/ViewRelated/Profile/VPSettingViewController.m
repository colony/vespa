//
//  VPSettingViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 1/8/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPSettingViewController.h"

#import <MessageUI/MessageUI.h>
#import "NSString+Util.h"
#import "UINavigationItem+Util.h"
#import "VPApiClient.h"
#import "VPBaseTableViewCell.h"
#import "VPUtil.h"
#import "VPWebViewController.h"


static CGFloat const _kVPCellHeight = 40.0f;

/*
 Setting header
 */
@interface _SettingHeaderCell : VPBaseTableViewCell

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) UILabel *label;

@end

@implementation _SettingHeaderCell

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    self.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1.0];

    UILabel *label = [[UILabel alloc] init];
    [label setAutoresizingMask:(UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth)];
    label.frame = CGRectMake(12, 0, self.width - 24, _kVPCellHeight);
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:13];
    label.textColor = [UIColor darkGrayColor];
    [self addSubview:label];
    [label setNeedsLayout];
     
    _label = label;
}

- (void)setTitle:(NSString *)title
{
    [self setTitle:title isVersion:NO];
}

- (void)setTitle:(NSString *)title isVersion:(BOOL)version
{
    _label.text = title;
    _label.textAlignment = version ? NSTextAlignmentRight : NSTextAlignmentLeft;
}

+ (NSNumber *)heightOfCell:(id)obj
{
    return [NSNumber numberWithFloat:_kVPCellHeight];
}

@end


/*
 Setting cell
 */
@interface _SettingCell : VPBaseTableViewCell

@property (nonatomic, strong)NSString *title;

@end

@implementation _SettingCell

- (void)setupUI
{
    self.selectionStyle = UITableViewCellSelectionStyleGray;
}

- (void)setTitle:(NSString *)title color:(UIColor *)color
{
    _title = title;
    self.textLabel.text = title;
    self.textLabel.textColor = color ?: [UIColor darkGrayColor];
    self.textLabel.font = [UIFont systemFontOfSize:14];
    self.textLabel.textAlignment = NSTextAlignmentLeft;
    [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
}

+ (NSNumber *)heightOfCell:(id)obj
{
    return [NSNumber numberWithFloat:_kVPCellHeight];
}

@end


@interface VPSettingViewController ()<MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@end

@implementation VPSettingViewController

#pragma mark - lifecycle
- (void)commonInit
{
    [super commonInit];
    self.cellInfos = [NSMutableArray arrayWithObjects:
                      [[VPTableCellInfo alloc] initWithClass:[_SettingHeaderCell class] nib:nil identifier:@"_SettingHeaderCell"],
                      [[VPTableCellInfo alloc] initWithClass:[_SettingCell class] nib:nil identifier:@"_SettingCell"],
                      nil];
    self.enableLoadMore = NO;
    self.enablePullToRefresh = NO;
}

- (void)setupUI
{
    [super setupUI];
    [self.navigationItem setCustomTitle:@"More"];
    self.view.backgroundColor = [VPUI lightGrayColor];
    [self pullDownRefresh];
}

- (void)setupTableView:(UITableView *)tableView
{
    [super setupTableView:tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero]; // hack, To remove extra table spaces
}


#pragma mark - override function
- (void)calculateSectionInfo
{
    [self.sectionInfos removeAllObjects];
    // Each setting group is basically a section with single cell (this is mainly for UI reason)
    // NOTE: change the number whenever setting options changed
    [self.sectionInfos addObject:[NSNumber numberWithInteger:1]];
    [self.sectionInfos addObject:[NSNumber numberWithInteger:6]];
    [self.sectionInfos addObject:[NSNumber numberWithInteger:1]];
    [self.sectionInfos addObject:[NSNumber numberWithInteger:3]];
    [self.sectionInfos addObject:[NSNumber numberWithInteger:1]];
}

- (VPTableCellInfo *)cellInfoForIndexPath:(NSIndexPath *)indexPath
{
    return self.cellInfos[indexPath.section % 2];
}

- (void)loadObjectsOfPageNo:(NSInteger)pageNo parameters:(NSMutableDictionary *)param
{
    [super loadObjectsOfPageNo:pageNo parameters:param];
    
    // To avoid section header floating (sticky) problem, we create a title cell (with gray background)
    NSMutableArray *menu = [[NSMutableArray alloc] init];
    [menu addObject:@"LOVE US"];
    [menu addObjectsFromArray:@[@"Share Ninjr", @"Rate Ninjr", @"Like Us on Facebook", @"Follow Us on Twitter", @"Follow Us on Tumblr", @"Give Us Feedback"]];
    [menu addObject:@"RATES AND STUFF"];
    [menu addObjectsFromArray:@[@"Terms of Service", @"Privacy Policy", @"Honor Code"]];
    [menu addObject:[NSString stringWithFormat:@"version: %@", kVPAppVersion]];
    [self onObjectsDidLoaded:menu pageNo:pageNo error:nil block:nil];
}


#pragma mark - table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    NSString *title = [self getItemFromIndexPath:indexPath];
    if (indexPath.section % 2 == 0) {
        // Last section show app version
        [((_SettingHeaderCell *)cell) setTitle:title isVersion:(indexPath.section == 4)];
    } else {
        [((_SettingCell *)cell) setTitle:title color:[title isEqualToString:@"Give Us Feedback"]?[UIColor redColor]:nil];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section % 2 != 0) {
        NSString *title = [self getItemFromIndexPath:indexPath];
        
        if ([title isEqualToString:@"Share Ninjr"]) {
            [VPUtil showInvitationView:self];
        }
        else if ([title isEqualToString:@"Rate Ninjr"]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id968448971"]];
        }
        else if ([title isEqualToString:@"Like Us on Facebook"]) {
            NSURL *fbAppURL = [[NSURL alloc] initWithString:@"fb://profile/430271017149314"];
            NSURL *fbWebURL = [[NSURL alloc] initWithString:@"https://www.facebook.com/ninjrapp"];
            [self openAppURL:fbAppURL web:fbWebURL];
        }
        else if ([title isEqualToString:@"Follow Us on Twitter"]) {
            NSURL *twAppURL = [[NSURL alloc] initWithString:@"twitter://user?screen_name=ninjrapp"];
            NSURL *twWebURL = [[NSURL alloc] initWithString:@"https://twitter.com/ninjrapp"];
            [self openAppURL:twAppURL web:twWebURL];
        }
        else if ([title isEqualToString:@"Follow Us on Tumblr"]) {
            NSURL *tmAppURL = [[NSURL alloc] initWithString:@"tumblr://x-callback-url/blog?blogName=ninjrapp"];
            NSURL *tmWebURL = [[NSURL alloc] initWithString:@"http://ninjrapp.tumblr.com"];
            [self openAppURL:tmAppURL web:tmWebURL];
        }
        else if ([title isEqualToString:@"Give Us Feedback"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tell us what you think about the app?"
                                                              message:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                                    otherButtonTitles:@"Send", nil];
            
            [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [alert show];
        }
//        else if ([title isEqualToString:@"Contact Us"]) {
//            if ([MFMailComposeViewController canSendMail]) {
//                MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
//                mailVC.mailComposeDelegate = self;
//                [mailVC setToRecipients:@[@"support@ninjrapp.com"]];
//                [self presentViewController:mailVC animated:YES completion:^{
//                    [VPUtil resetNavigationBar];
//                }];
//            } else {
//                [self openWebView:@"http://ninjrapp.com/aboutus" title:@"About Us"];
//            }
//        }
        else if ([title isEqualToString:@"Terms of Service"]) {
            [self openWebView:@"http://ninjrapp.com/terms" title:@"Terms of Service"];
        }
        else if ([title isEqualToString:@"Privacy Policy"]) {
            [self openWebView:@"http://ninjrapp.com/privacy" title:@"Privacy Policy"];
        }
        else if ([title isEqualToString:@"Honor Code"]) {
            [self openWebView:@"http://ninjrapp.com/code" title:@"Honor Code"];
        }
//        else if ([title isEqualToString:@"How To"]) {
//            UIViewController *vc = [[UIViewController alloc] init];
//            vc.view.frame = self.view.bounds;
//            vc.view.backgroundColor = [VPUI lightGrayColor];
//            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"howto"]];
//            imageView.frame = self.view.bounds;
//            [vc.view addSubview:imageView];
//            [self.navigationController pushViewController:vc animated:YES];
//        }
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section % 2 != 0;
}


#pragma mark - mail compose delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Send"])
    {
        NSString *s = [alertView textFieldAtIndex:0].text;
        if (![NSString isEmptyString:s]) {
            [[VPAPIClient sharedInstance] reportFeedback:s success:nil failure:nil];
        }
    }
}


#pragma mark - private method
- (void)openAppURL:(NSURL *)appUrl web:(NSURL *)webUrl
{
    NSURL *url = appUrl;
    if (![[UIApplication sharedApplication] canOpenURL:appUrl])
        url = webUrl;
    [[UIApplication sharedApplication] openURL:url];
}

- (void)openWebView:(NSString *)urlStr title:(NSString *)title
{
    VPWebViewController *web = [[VPWebViewController alloc] initWithURL:[NSURL URLWithString:urlStr] customTitle:title];
    [self.navigationController pushViewController:web animated:YES];
}

@end
