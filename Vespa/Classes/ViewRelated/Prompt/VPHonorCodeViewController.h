//
//  VPHonorCodeViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 5/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseViewController.h"


@protocol VPHonorCodeViewControllerDelegate;

@interface VPHonorCodeViewController : VPBaseViewController

@property (weak, nonatomic) id<VPHonorCodeViewControllerDelegate> delegate;

@end

@protocol VPHonorCodeViewControllerDelegate <NSObject>

- (void)agreeButtonPressed:(VPHonorCodeViewController *)viewController;

@end
