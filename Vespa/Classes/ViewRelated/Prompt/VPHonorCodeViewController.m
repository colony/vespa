//
//  VPHonorCodeViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/21/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPHonorCodeViewController.h"

#import "VPPromptManager.h"
#import "VPWebViewController.h"


static NSString *_kVPAgreementText = @"You also agree to Ninjr's Terms of Service and Privacy Policy.";

@interface VPHonorCodeViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *agreementTextView;

@end

@implementation VPHonorCodeViewController

- (void)setupUI
{
    [super setupUI];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:_kVPAgreementText];
    NSRange range1 = [attrStr.string rangeOfString:@"Terms of Service"];
    NSRange range2 = [attrStr.string rangeOfString:@"Privacy Policy"];
    
    [attrStr beginEditing];
    [attrStr addAttribute:NSFontAttributeName
                    value:[VPUI fontOpenSans:11 thickness:3]
                    range:range1];
    [attrStr addAttribute:NSFontAttributeName
                    value:[VPUI fontOpenSans:11 thickness:3]
                    range:range2];
    [attrStr addAttribute:NSLinkAttributeName
                    value:[NSURL URLWithString:@"http://ninjrapp.com/terms"]
                    range:range1];
    [attrStr addAttribute:NSLinkAttributeName
                    value:[NSURL URLWithString:@"http://ninjrapp.com/privacy"]
                    range:range2];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [attrStr.string length])];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment                = NSTextAlignmentCenter;
    [attrStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attrStr.string length])];
    
    [attrStr endEditing];
    
    self.agreementTextView.attributedText = attrStr;
    self.agreementTextView.linkTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.agreementTextView.delegate = self;
}

- (IBAction)agreeButtonPressed:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(agreeButtonPressed:)]) {
        [self.delegate agreeButtonPressed:self];
    }
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    [[VPPromptManager sharedInstance] prompt:kVPPromptWeb data:[URL absoluteString] onlyOnce:NO overContext:NO animated:YES];
    
    // Avoid opening the URL in browser
    return NO;
}

@end
