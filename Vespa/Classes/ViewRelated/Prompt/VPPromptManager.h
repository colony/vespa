//
//  VPPromptManager.h
//  Vespa
//
//  Created by Jiayu Zhang on 4/24/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//



@interface VPPromptManager : NSObject

+ (instancetype)sharedInstance;

/**
 * fromRoot - if YES, try to present the vc from rootVC, if there's already one presented
 *            view controller, bail out and return NO
 * onlyOnce - present the view controller only once, it will set value in UserDefaults based
 *            the specified key, and will asure the value will only be set once
 *
 * return YES if prompted, NO otherwise
 * (NOTE, the method will change modalPresentationStyle to UIModalPresentationCurrentContext)
 */
//- (BOOL)prompt:(NSString *)key data:(id)auxiliaryData delegate:(id)delegate fromRoot:(BOOL)fromRoot onlyOnce:(BOOL)onlyOnce animated:(BOOL)animated;

- (BOOL)prompt:(NSString *)key data:(id)data onlyOnce:(BOOL)onlyOnce overContext:(BOOL)overContext animated:(BOOL)animated;

@end
