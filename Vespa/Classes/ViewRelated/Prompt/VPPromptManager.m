//
//  VPPromptManager.m
//  Vespa
//
//  Created by Jiayu Zhang on 4/24/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPPromptManager.h"

#import "VPGroupIdeaViewController.h"
#import "VPHonorCodeViewController.h"
#import "VPNavigationController.h"
#import "VPPointViewController.h"
#import "VPUtil.h"
#import "VPWebViewController.h"


@implementation VPPromptManager

+ (instancetype)sharedInstance
{
    static VPPromptManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (BOOL)prompt:(NSString *)key data:(id)data onlyOnce:(BOOL)onlyOnce overContext:(BOOL)overContext animated:(BOOL)animated
{
    if (onlyOnce) {
        BOOL shouldShow = ![[NSUserDefaults standardUserDefaults] boolForKey:key];
        if (!shouldShow)
            return NO; // We've showed once before
    }
    
    UIViewController *presentingVC;
    if (overContext) {
        // Imagine our top vc is something within a navigationController that is being presented
        // To achieve the overContext effect, we need over the context of navigationController
        // instead of its visible child
        UIViewController *presentedVC = [VPUtil rootViewController];
        while (presentedVC) {
            presentingVC = presentedVC;
            presentedVC = presentedVC.presentedViewController;
        }
    } else {
        presentingVC = [VPUtil getTopMostController];
    }
    
    UIViewController *promptVC;
    if ([key isEqualToString:kVPPromptGroupIdea]) {
        promptVC = [VPGroupIdeaViewController new];
    } else if ([key isEqualToString:kVPPromptHonorCode]) {
        VPHonorCodeViewController *vc = [VPHonorCodeViewController new];
        vc.delegate = data;
        promptVC = vc;
    } else if ([key isEqualToString:kVPPromptPoint]) {
        VPPointViewController *vc = [[VPPointViewController alloc] initWithPoint:data];
        promptVC = vc;
    } else if ([key isEqualToString:kVPPromptWeb]) {
        VPWebViewController *vc = [[VPWebViewController alloc] initWithURL:[NSURL URLWithString:data] customTitle:nil];
        vc.isPresenting = YES;
        VPNavigationController *nc = [[VPNavigationController alloc] initWithRootViewController:vc];
        nc.customNavigatonGestureEnabled = NO;
        promptVC = nc;
    }
    // Other prompt type goes here
    
    if (promptVC) {
        if (overContext) {
            // Tricky for presenting with transparency
            // http://stackoverflow.com/questions/26032472/presenting-semi-transparent-viewcontroller-that-works-both-in-ios7-and-ios8
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0)) {
                promptVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            } else {
                presentingVC.modalPresentationStyle = UIModalPresentationCurrentContext;
            }
        }
        
        [presentingVC presentViewController:promptVC animated:animated completion:^{
            if (onlyOnce)
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:key];
        }];
        return YES;
    }
    return NO; // Unrecognized specified prompt key
}

@end
