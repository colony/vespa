//
//  VPMain2ViewController.h
//  Vespa
//
//  Created by Jiayu Zhang on 5/5/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


typedef NS_ENUM(NSInteger, VPTabIndex)
{
    kVPTabIndexGlobal = 0,
    kVPTabIndexGroup,
    kVPTabIndexNotification,
    kVPTabIndexProfile
};

// Should be one-to-one mapping to VPMediaIndex
typedef NS_ENUM(NSInteger, VPAddPostIndex)
{
    kVPAddPostIndexText = 0,
    kVPAddPostIndexCamera,
    kVPAddPostIndexMicrophone
};



@interface VPMain2ViewController : UITabBarController

@property (nonatomic, readonly) NSInteger currentTab;

/**
 * A quick shortcut to access the instance from app's view controllers hierarchy.
 * NOTE, the method might return nil if the VC has not been initialized yet
 */
+ (instancetype)accessInstance;

- (void)gotoTab:(VPTabIndex)index;
- (UIViewController *)viewControllerAtTab:(VPTabIndex)index;

- (void)hideTabbarAnimated:(BOOL)animated;
- (void)showTabbarAnimated:(BOOL)animated;

- (void)presentAddPostViewController;

@end
