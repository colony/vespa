//
//  VPMainViewController.m
//  Vespa
//
//  Created by Jiayu Zhang on 5/5/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPMain2ViewController.h"

#import "CustomBadge.h"
#import "VPAddPostMenuView.h"
#import "VPAppDelegate.h"
#import "VPConfigManager.h"
#import "VPNavigationController.h"
#import "VPPromptManager.h"
#import "VPStatus.h"
#import "VPUsers.h"
#import "VPUtil.h"


static NSInteger const _kTabContainerViewTag = 99;


//@interface _VPMain2AnimTransition : NSObject<UIViewControllerAnimatedTransitioning>
//
//@property (assign, nonatomic) BOOL presenting;
//
//@end


@interface VPMain2ViewController ()<VPAddPostMenuViewDelegate>

//@property (strong, nonatomic) _VPMain2AnimTransition *animTransition;

@property (assign, nonatomic) BOOL isTabbarHidden;
@property (assign, nonatomic) BOOL isProfileTabIndicated;

@end


@implementation VPMain2ViewController

+ (instancetype)accessInstance
{
    VPAppDelegate *d = (VPAppDelegate *)[UIApplication sharedApplication].delegate;
    return (VPMain2ViewController *)d.window.rootViewController;
}

#pragma mark - lifecycle
- (id)init
{
    if (self = [super init]) {
        _currentTab = -1;
        _isTabbarHidden = NO;
        _isProfileTabIndicated = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleUserStatusChangeNotification:) name:kUserStatusChangeNotification object:nil];
    
    [self setup];
    [self gotoTab:kVPTabIndexGlobal];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    // The UITabbar will add a UITabbarButton per view controller, in order to hookup
    // our custom event handling, we need bring the custom view to front
    UIView *tabContainer = [self.tabBar viewWithTag:_kTabContainerViewTag];
    [self.tabBar bringSubviewToFront:tabContainer];
}

- (void)setup
{
    self.view.autoresizingMask = UIViewAutoresizingFill;
    
    UIView *tabContainer = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, kVPTabbarHeight)];
    tabContainer.backgroundColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.5];
    tabContainer.userInteractionEnabled = YES;
    tabContainer.tag = 99;
    
    NSArray *tabIcons = @[@"global_tab", @"group_tab", @"alert_tab", @"profile_tab"];
    CGFloat tabButtonWidth = SCREEN_WIDTH / [tabIcons count];
    for (NSUInteger i = 0; i < [tabIcons count]; ++i) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(i * tabButtonWidth, 0.0f, tabButtonWidth, kVPTabbarHeight);
        [btn setImage:[UIImage imageNamed:tabIcons[i]] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:[tabIcons[i] stringByAppendingString:@"_selected"]] forState:UIControlStateSelected];
        btn.contentMode = UIViewContentModeCenter;
        btn.tag = i + 10; // By default tag is 0, to avoid duplicate, we plus 10
        [btn addTarget:self action:@selector(tabPressed:) forControlEvents:UIControlEventTouchUpInside];
        [tabContainer addSubview:btn];
    }
    [self.tabBar addSubview:tabContainer];
    self.tabBar.translucent = YES;
    
    // Setup controller per tab
    NSArray *vcClazzs = @[@"VPGlobalViewController", @"VPGroupListViewController", @"VPNotificationViewController", @"VPProfile2ViewController"];
    NSMutableArray *vcs = [NSMutableArray array];
    for (NSString *vcClazz in vcClazzs) {
        UIViewController *rootVC = [[NSClassFromString(vcClazz) alloc] init];
        VPNavigationController *nc = [[VPNavigationController alloc] initWithRootViewController:rootVC];
        nc.customNavigatonGestureEnabled = YES;
        
        [vcs addObject:nc];
    }
    [self setViewControllers:vcs];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - public
- (void)gotoTab:(VPTabIndex)index
{
    UIButton *tabButton = (UIButton *)[self.tabBar viewWithTag:(index + 10)];
    if (tabButton)
        [self tabPressed:tabButton];
}

- (UIViewController *)viewControllerAtTab:(VPTabIndex)index
{
    return [self.viewControllers objectAtIndex:index];
}

- (void)hideTabbarAnimated:(BOOL)animated
{
    if (self.isTabbarHidden)
        return;
    
    [UIView animateWithDuration:(animated ? 0.2 : 0.0)
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.tabBar.y = SCREEN_HEIGHT;
                     } completion:^(BOOL finished) {
                         self.isTabbarHidden = YES;
                     }];
}

- (void)showTabbarAnimated:(BOOL)animated
{
    if (!self.isTabbarHidden)
        return;
    
    [UIView animateWithDuration:(animated ? 0.2 : 0.0)
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.tabBar.y = SCREEN_HEIGHT - kVPTabbarHeight;// - 64;
                     } completion:^(BOOL finished) {
                         self.isTabbarHidden = NO;
                     }];
}

- (void)presentAddPostViewController
{
    VPAddPostMenuView *menuView = (VPAddPostMenuView *)[[[NSBundle mainBundle] loadNibNamed:@"VPAddPostMenuView" owner:nil options:nil] objectAtIndex:0];
    [menuView allowAll];
    menuView.frame = [UIScreen mainScreen].bounds;
    menuView.opaque = YES;
    menuView.delegate = self;
    [[UIApplication sharedApplication].keyWindow addSubview:menuView];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:menuView];
}


#pragma mark - override
- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    [super setSelectedIndex:selectedIndex];
    UIView *tabContainer = [self.tabBar viewWithTag:99];
    for (id subview in tabContainer.subviews) {
        if ([subview isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)subview;
            btn.selected = (btn.tag == selectedIndex + 10);
        }
    }
}


#pragma mark - action handling
- (void)tabPressed:(UIButton *)sender
{
    NSInteger tag = sender.tag;
    NSInteger tabIdx = tag - 10;
    if (self.currentTab == tabIdx) {
        if (tabIdx == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kTabbarCurrentTapNotification object:nil userInfo:@{@"tab":@"GLOBAL"}];
        } else if (tabIdx == 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kTabbarCurrentTapNotification object:nil userInfo:@{@"tab":@"GROUP"}];
        } else if (tabIdx == 2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kTabbarCurrentTapNotification object:nil userInfo:@{@"tab":@"NOTIFICATION"}];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kTabbarCurrentTapNotification object:nil userInfo:@{@"tab":@"PROFILE"}];
        }
    } else {
        // Press on unselected tab, switch
        self.selectedIndex = tabIdx;
        _currentTab = tabIdx;
    }
}


#pragma mark - notification
- (void)handleUserStatusChangeNotification:(NSNotification *)noti
{
    VPStatus *status = [VPUsers fetchUserStatusLocal];
    if (status) {
        [self showTabIndicator:[status.numUnreadActivities longValue] index:kVPTabIndexNotification];
        [self showTabIndicator:[status.numUnreadAnnouncements longValue] index:kVPTabIndexProfile];
        return;
    }
    [self showTabIndicator:0 index:kVPTabIndexNotification];
    [self showTabIndicator:0 index:kVPTabIndexProfile];
}


#pragma mark - private
//- (void)resizeSystemTabbar:(NSInteger)tabbarHeight
//{
//    float screenWidth = SCREEN_WIDTH;
//    float screenHeight = SCREEN_HEIGHT;
//    
//    for(UIView *view in self.view.subviews)
//    {
//        if([view isKindOfClass:[UITabBar class]])
//        {
//            view.backgroundColor = [UIColor clearColor];
//            view.frame = CGRectMake(0, screenHeight - tabbarHeight, screenWidth, tabbarHeight);
//            view.hidden = YES;
//            break;
//        } else if (view == self.tabContainer) {
//            view.frame = CGRectMake(0, SCREEN_HEIGHT - tabbarHeight, SCREEN_WIDTH, kVPTabbarHeight);
//            break;
//        }
//    }
//}

- (void)showTabIndicator:(NSInteger)num index:(VPTabIndex)index
{
    NSInteger tag = index + 10;
    UIButton *tabButton = (UIButton *)[self.tabBar viewWithTag:tag];
    
    if (!tabButton)return;
    
    NSInteger indiTag = tag * 10;
    UIView *indiView = [self.tabBar viewWithTag:indiTag];
    if (indiView) {
        indiView.hidden = YES;
        [indiView removeFromSuperview];
        indiView = nil;
    }
    
    if (num > 0) {
        NSString *badgeStr = (num > 99) ? @"99+" : [@(num) stringValue];
        CustomBadge *badgeView = [CustomBadge customBadgeWithString:badgeStr withScale:.8f];
        badgeView.tag = indiTag;
        badgeView.center = CGPointMake(tabButton.width/2 + badgeView.width/2 + 10, badgeView.height/2);
        [tabButton addSubview:badgeView];
    }
}


//#pragma mark - UIViewControllerTransitioningDelegate 
//- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
//                                                                  presentingController:(UIViewController *)presenting
//                                                                      sourceController:(UIViewController *)source
//{
//    self.animTransition.presenting = YES;
//    return self.animTransition;
//}
//
//- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
//{
//    self.animTransition.presenting = NO;
//    return self.animTransition;
//}


#pragma mark - VPAddPostMenuViewDelegate
- (void)addPostSelected:(VPAddPostMenuView *)view mediaType:(NSString *)type
{
    view.hidden = YES;
    [view removeFromSuperview];

    // VPMediaSubmitVC needs present the selected group by investigating the context data
    // If we click addPost from mainVC, we should cleanup all context data, let VPMediaSubmitVC
    // choose the DEFAULT group "General"
    [[VPContext sharedInstance] clearDomain:kVPCDMediaSubmission];
    
    [VPUtil presentMediaViewController:self mediaType:type];
}

@end


//#pragma mark -
//@implementation _VPMain2AnimTransition
//
//- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
//{
//    return 0.25;
//}
//
//- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
//{
//    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
//    UIView *container = [transitionContext containerView];
//    NSTimeInterval duration = [self transitionDuration:transitionContext];
//    
//    // Presenting
//    if (self.presenting) {
//        toVC.view.frame = container.bounds;
//        [container insertSubview:toVC.view belowSubview:fromVC.view];
//        
//        toVC.view.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(0.95, 0.95), CGAffineTransformMakeTranslation(0, 5));
//        [UIView animateWithDuration:duration
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseIn
//                         animations:^{
//                             toVC.view.transform = CGAffineTransformIdentity;
//                             fromVC.view.y = SCREEN_HEIGHT;
//                         } completion:^(BOOL finished) {
//                             // NO need worry about fromVC.view, it will be removed from view hierarchy by context
//                             [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
//                         }];
//    }
//
//    // Dismiss
//    else {
//        toVC.view.y = SCREEN_HEIGHT;
//        [container addSubview:toVC.view];
//        
//        [UIView animateWithDuration:duration
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseIn
//                         animations:^{
//                             fromVC.view.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(0.95, 0.95), CGAffineTransformMakeTranslation(0, 5));
//                             toVC.view.y = 0.0f;
//                         } completion:^(BOOL finished) {
//                             // NO need worry about fromVC.view, it will be removed from view hierarchy by context
//                             [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
//                         }];
//    }
//}
//
//@end
