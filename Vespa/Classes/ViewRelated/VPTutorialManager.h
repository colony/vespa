//
//  VPTutorialManager.h
//  Vespa
//
//  Created by Jiayu Zhang on 4/25/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//


// ID: Feed
//     Step 1 - Tap to view full post image (tapimage)
//     Step 2 - Introduce tomato and star (tomatostar)

// ID: LinkSource
//     Step 1 - Tap the 'link' button to read source (linksource)

// ID: PhotoEdit
//     Step 1 - Introduce tap for image filters and pinch for zoom (photoedit)

// ID: Tribe
//

// NOTE: per tutorial ID string corresponds to a UserDefaults key

extern NSString * const kVPTutorialFeed;
extern NSString * const kVPTutorialLinkSource;
extern NSString * const kVPTutorialPhotoEdit;
extern NSString * const kVPTutorialThreadWinner;
//extern NSString * const kVPTutorialTribe;
extern NSString * const kVPTutorialVideo;


@protocol VPTutorialManagerDelegate;

@interface VPTutorialManager : NSObject

+ (instancetype)sharedInstance;

- (void)start:(NSString *)tutorialId;
- (void)finish:(NSString *)tutorialId;

- (void)registerTutorialId:(NSString *)tutorialId step:(NSInteger)step delegate:(id<VPTutorialManagerDelegate>)delegate;
- (void)doneTutorialId:(NSString *)tutorialId step:(NSInteger)step;

@end

@protocol VPTutorialManagerDelegate <NSObject>

@optional
- (void)handleStep:(NSInteger)step manager:(VPTutorialManager *)manager sender:(id)sender param:(NSDictionary *)param;

@end
