//
//  VPTutorialManager.m
//  Vespa
//
//  Created by Jiayu Zhang on 4/25/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPTutorialManager.h"

#import "UIView+Frame.h"
#import "VPAppDelegate.h"
#import "VPConfigManager.h"
#import "VPUI.h"
#import "VPUtil.h"


NSString * const kVPTutorialFeed = @"TutorialFeed";
NSString * const kVPTutorialLinkSource = @"TutorialLinkSource";
NSString * const kVPTutorialPhotoEdit = @"TutorialPhotoEdit";
NSString * const kVPTutorialThreadWinner = @"ThreadWinner";
//NSString * const kVPTutorialTribe = @"TutorialTribe";
NSString * const kVPTutorialVideo = @"TutorialVideo";


@interface VPTutorialManager ()

@property (strong, nonatomic) NSString *currentTutorialId;

@property (strong, nonatomic) UIView *tutorialView;

// Mapping from TutorialID to currentStepNum
@property (strong, nonatomic) NSMutableDictionary *currentStepMap;

// Mapping from "TutorialID-StepNumber" to delegate obj
@property (strong, nonatomic) NSMutableDictionary *delegateMap;

@end


@implementation VPTutorialManager

#pragma mark - lifecycle
+ (instancetype)sharedInstance
{
    static VPTutorialManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.currentStepMap = [NSMutableDictionary new];
        self.delegateMap = [NSMutableDictionary new];
    }
    return self;
}

- (void)start:(NSString *)tutorialId
{
    if (![self checkIfTutorialDone:tutorialId]) {
        if (!self.tutorialView) {
            self.tutorialView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
            self.tutorialView.opaque = YES;
            self.tutorialView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
            
            [[UIApplication sharedApplication].keyWindow addSubview:self.tutorialView];
            [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self.tutorialView];
        }
        
        self.currentTutorialId = tutorialId;
        [self setupView:tutorialId step:1];
    }
}

- (void)finish:(NSString *)tutorialId
{
    if ([self checkIfTutorialDone:tutorialId])
        return;
    
    // Cleanup everything
    self.tutorialView.hidden = YES;
    [self.tutorialView removeFromSuperview];
    self.tutorialView = nil;
    
    NSMutableArray *keysToDelete = [NSMutableArray array];
    for (NSString *key in [self.delegateMap keyEnumerator]) {
        if ([key hasPrefix:tutorialId]) {
            [keysToDelete addObject:key];
        }
    }
    [self.delegateMap removeObjectsForKeys:keysToDelete];
    [self.currentStepMap removeObjectForKey:tutorialId];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:tutorialId];
    
    self.currentTutorialId = nil;
}

- (void)registerTutorialId:(NSString *)tutorialId step:(NSInteger)step delegate:(id<VPTutorialManagerDelegate>)delegate
{
    if (![self checkIfTutorialDone:tutorialId]) {
        if (delegate) {
            [self.delegateMap setObject:delegate forKey:[NSString stringWithFormat:@"%@-%td", tutorialId, step]];
        }
    }
}

- (void)doneTutorialId:(NSString *)tutorialId step:(NSInteger)step
{
    if (![self checkIfTutorialDone:tutorialId]) {
        if ([tutorialId isEqualToString:kVPTutorialFeed]) {
            if (step == 2) {
                [self finish:tutorialId];
            } else {
                [self setupViewTutorialFeed:(step+1)];
            }
        } else {
            [self finish:tutorialId];
        }
    }
}


#pragma mark - Tutorial "Feed"
- (void)setupViewTutorialFeed:(NSInteger)toStep
{
    NSInteger fromStep = [self currentStepFor:kVPTutorialFeed];
    if (fromStep != toStep - 1)
        return;
    
    // Cleanup everything from previous step
    [self cleanup];
    if (toStep == 1) {
        // 360x334
        CGFloat height = 334 * (SCREEN_WIDTH / 360);
        UIImageView *holdImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tutorial_tapimage"]];
        holdImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height); // This is the dimension of a real photo post
        holdImageView.top = 64; //below navbar
        [self.tutorialView addSubview:holdImageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToNextStep:)];
        [self.tutorialView addGestureRecognizer:tap];
        
        // Always animation (fade in) step 1
        [self fadein];
    }
    
    if (toStep == 2) {
        // 360x334
        CGFloat height = 334 * (SCREEN_WIDTH / 360);
        UIImageView *tomatostarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tutorial_tomatostar"]];
        tomatostarImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
        tomatostarImageView.top = 64; //below navbar
        [self.tutorialView addSubview:tomatostarImageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToNextStep:)];
        [self.tutorialView addGestureRecognizer:tap];
    }
    [self.currentStepMap setObject:@(toStep) forKey:kVPTutorialFeed];
}


#pragma mark - Tutorial "LinkSource"
- (void)setupViewTutorialLinkSource:(NSInteger)toStep
{
    NSInteger fromStep = [self currentStepFor:kVPTutorialFeed];
    if (fromStep != toStep - 1)
        return;
    
    UIImageView *readButtonImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"read_button"]];
    readButtonImageView.frame = CGRectMake(SCREEN_WIDTH - 50, 28, 50, 27);
    [self.tutorialView addSubview:readButtonImageView];
    
    UIImageView *tutorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tutorial_linksource"]];
    // 310x175
    tutorImageView.frame = CGRectMake(0, 64, SCREEN_WIDTH, 175 * (SCREEN_WIDTH/310));
    [self.tutorialView addSubview:tutorImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToNextStep:)];
    [self.tutorialView addGestureRecognizer:tap];
    
    [self fadein];
    
    [self.currentStepMap setObject:@(toStep) forKey:kVPTutorialLinkSource];
}


#pragma mark - Tutorial "PhotoEdit"
- (void)setupViewTutorialPhotoEdit:(NSInteger)toStep
{
    NSInteger fromStep = [self currentStepFor:kVPTutorialPhotoEdit];
    if (fromStep != toStep - 1)
        return;

    UIImageView *tutorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tutorial_photoedit"]];
    tutorImageView.frame = CGRectMake(0, 0, 250, 193);
    tutorImageView.center = self.tutorialView.center;
    [self.tutorialView addSubview:tutorImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToNextStep:)];
    [self.tutorialView addGestureRecognizer:tap];
    
    [self fadein];
    
    [self.currentStepMap setObject:@(toStep) forKey:kVPTutorialPhotoEdit];
}

#pragma mark - Tutorial "ThreadWinner"
- (void)setupViewTutorialThreadWinner:(NSInteger)toStep
{
    NSInteger fromStep = [self currentStepFor:kVPTutorialPhotoEdit];
    if (fromStep != toStep - 1)
        return;
    
    // 334x244
    CGFloat height = 244 * (SCREEN_WIDTH / 334);
    UIImageView *holdImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tutorial_threadwinner"]];
    holdImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height); // This is the dimension of a real photo post
    holdImageView.top = 64; //below navbar
    [self.tutorialView addSubview:holdImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToNextStep:)];
    [self.tutorialView addGestureRecognizer:tap];

    [self fadein];
    
    [self.currentStepMap setObject:@(toStep) forKey:kVPTutorialThreadWinner];
}


#pragma mark - Tutorial "Video"
- (void)setupViewTutorialVideo:(NSInteger)toStep
{
    NSInteger fromStep = [self currentStepFor:kVPTutorialVideo];
    if (fromStep != toStep - 1)
        return;
    
    UIImageView *tutorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tutorial_video"]];
    tutorImageView.frame = CGRectMake(0, 0, 300, 70);
    tutorImageView.center = self.tutorialView.center;
    [self.tutorialView addSubview:tutorImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToNextStep:)];
    [self.tutorialView addGestureRecognizer:tap];
    
    [self fadein];
    
    [self.currentStepMap setObject:@(toStep) forKey:kVPTutorialVideo];
}


#pragma mark - private
- (NSInteger)currentStepFor:(NSString  *)tutorialId
{
    NSInteger curStep = 0;
    NSNumber *num = [self.currentStepMap objectForKey:tutorialId];
    if (num)
        curStep = [num integerValue];
    return curStep;
}

- (BOOL)checkIfTutorialDone:(NSString *)tutorialId
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:tutorialId];
}

- (void)setupView:(NSString *)tutorialId step:(NSInteger)step
{
    if ([tutorialId isEqualToString:kVPTutorialFeed]) {
        [self setupViewTutorialFeed:step];
    } else if ([tutorialId isEqualToString:kVPTutorialLinkSource]) {
        [self setupViewTutorialLinkSource:step];
    } else if ([tutorialId isEqualToString:kVPTutorialPhotoEdit]) {
        [self setupViewTutorialPhotoEdit:step];
    } else if ([tutorialId isEqualToString:kVPTutorialThreadWinner]) {
        [self setupViewTutorialThreadWinner:step];
    } else if ([tutorialId isEqualToString:kVPTutorialVideo]) {
        [self setupViewTutorialVideo:step];
    }
}

- (void)cleanup
{
    for (UIView *v in [self.tutorialView subviews]) {
        v.hidden = YES;
        [v removeFromSuperview];
    }
    for (UIGestureRecognizer *recognizer in self.tutorialView.gestureRecognizers) {
        [self.tutorialView removeGestureRecognizer:recognizer];
    }
}

- (void)fadein
{
    self.tutorialView.alpha = 0.0;
    [UIView animateWithDuration:0.5 animations:^{
        self.tutorialView.alpha = 1.0;
    }];
}

- (void)tapToNextStep:(id)sender
{
    NSString *tutorialId = self.currentTutorialId;
    [self handleTutorialId:tutorialId step:[self currentStepFor:tutorialId] sender:sender param:nil];
}

- (void)handleTutorialId:(NSString *)tutorialId step:(NSInteger)step sender:(id)sender param:(NSDictionary *)param
{
    NSInteger curStep = [self currentStepFor:tutorialId];
    if (curStep != step)
        return;
    
    id delegate = [self.delegateMap objectForKey:[NSString stringWithFormat:@"%@-%td", tutorialId, step]];
    if (delegate) {
        if ([delegate respondsToSelector:@selector(handleStep:manager:sender:param:)]) {
            [delegate handleStep:step manager:self sender:sender param:param];
        }
    } else {
        // If there is no delegate, just simply finish it, as easy as, Tap-To-Dismiss
        [self doneTutorialId:tutorialId step:step];
    }
}

@end
