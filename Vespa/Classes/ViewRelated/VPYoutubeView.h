//
//  VPYouTubeWebView.h
//  Vespa
//
//  Created by Jiayu Zhang on 6/18/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPBaseView.h"

@protocol VPYoutubeViewDelegate;


@interface VPYoutubeView : VPBaseView

@property (weak, nonatomic) id<VPYoutubeViewDelegate> delegate;
@property (strong, nonatomic, readwrite) NSString *videoId;

- (void)loadVideo;
- (void)stopVideoAndCleanup;

@end


@protocol VPYoutubeViewDelegate <NSObject>
@optional

/**
 * By default VPYoutubeView will mimic simliar behavior of Youtube site in safari. It will launch a video player
 * However, it might fail for example the video is privacy-protected and only viewable on youtube website
 * In this case, the delegate should be responsible for opening up a webview or external launch safari
 */
- (void)youtubeViewFailLoad:(VPYoutubeView *)youtubeView alternativeURL:(NSString *)alternativeYoutubeWebURL;

@end
