//
//  VPYouTubeWebView.m
//  Vespa
//
//  Created by Jiayu Zhang on 6/18/15.
//  Copyright (c) 2015 Colony. All rights reserved.
//

#import "VPYoutubeView.h"

#import "UIWebView+Addition.h"
#import "VPPromptManager.h"


static NSString *_kVPYoutubeImageUrlFmt = @"http://i1.ytimg.com/vi/%@/hqdefault.jpg";
static NSString *_kVPYoutubeUrlPrefix = @"https://www.youtube.com/watch?v=";

@interface VPYoutubeView ()<UIWebViewDelegate>
{
    // Since we custom getter and setter, we need declare the ivar here
    NSString *_videoId;
}

@property (strong, nonatomic) NSString *youtubeVideoUrl;

@property (strong, nonatomic) UIImageView *coverImageView;
@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UIButton *playButton;

@end

@implementation VPYoutubeView

- (void)setupUI
{
    [super setupUI];
    
    if (!self.coverImageView) {
        self.coverImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.coverImageView.clipsToBounds = YES;
        self.coverImageView.autoresizingMask = UIViewAutoresizingFill;
        [self addSubview:self.coverImageView];
    }
    
    if (!self.playButton) {
        self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.playButton setImage:[UIImage imageNamed:@"youtube_play_button"] forState:UIControlStateNormal];
        self.playButton.frame = CGRectMake(0, 0, 90, 60);
        self.playButton.center = self.center;
        self.playButton.autoresizingMask = UIViewAutoresizingCentered;
        [self.playButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.playButton];
    }
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:_kVPYoutubeImageUrlFmt, self.videoId]]];
}

- (NSString *)videoId
{
    return _videoId;
}

- (void)setVideoId:(NSString *)videoId
{
    _videoId = videoId;
    _youtubeVideoUrl = [_kVPYoutubeUrlPrefix stringByAppendingString:videoId];
    [self setupUI];
}

- (void)loadVideo
{
    NSString *youtubeHtmlFilePath = [[NSBundle mainBundle] pathForResource:@"youtube" ofType:@"html"];
    NSError *err = nil;
    NSString *htmlFormatString = [NSString stringWithContentsOfFile:youtubeHtmlFilePath encoding:NSUTF8StringEncoding error:&err];
    if (!err) {
        NSString *html = [NSString stringWithFormat:htmlFormatString, self.height, self.width, self.videoId];
        if (!self.webView) {
            self.webView = [[UIWebView alloc] initWithFrame:self.bounds];
            self.webView.mediaPlaybackRequiresUserAction = NO;
            self.webView.delegate = self;
            [self addSubview:self.webView];
        }
        [self.webView loadHTMLString:html baseURL:[[NSBundle mainBundle] resourceURL]];
    }
}

- (void)stopVideoAndCleanup
{
    [self.webView cleanup];
    self.webView = nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    // youtube-callback://err
    // youtube-callback://end
    if ([[[request URL] scheme] isEqualToString:@"youtube-callback"]) {
        // Cleanup webview
        self.webView.hidden = YES;
        self.webView.delegate = nil;
        [self.webView stopLoading];
        [self.webView removeFromSuperview];
        self.webView = nil;
        
        NSString *host = [[[request URL] host] lowercaseString];
        if ([host isEqualToString:@"err"]) {
//            if ([self.delegate respondsToSelector:@selector(youtubeViewFailLoad:alternativeURL:)]) {
//                [self.delegate youtubeViewFailLoad:self alternativeURL:self.youtubeVideoUrl];
//            }
            
            //TODO: memory leak?
            [[VPPromptManager sharedInstance] prompt:kVPPromptWeb data:self.youtubeVideoUrl onlyOnce:NO overContext:NO animated:YES];
        }
        return NO;
    }
    return YES;
}

- (void)playButtonPressed:(id)sender
{
    [self loadVideo];
}

@end
