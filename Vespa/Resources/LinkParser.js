;(function() {

  function getElementTopLeftOffset(el) {
    var rect = el.getBoundingClientRect();
    var docEl = document.documentElement;
    return {
      left: rect.left + (window.pageXOffset || docEl.scrollLeft || 0),
      top:  rect.top + (window.pageYOffset || docEl.scrollTop || 0)
    };
  }
  
  function getQueryParamFromUrl(url) {
    var query = window.location.href, result = {};  

    // cleanup hash
    query = query.split('#')[0];  

    // extract the query string
    var pos = query.indexOf("?");
    if (pos == -1)
      return result;
    query = query.substr(pos+1);  

    query.split("&").forEach(function(part) {
      if(!part) return;
      var item = part.split("=");
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
  }
  
  // source - src URL
  // youtubeVideoId - ID of youtube video used when it is 'YOUTUBE'
  // type - 'GIF', 'YOUTUBE', 'IMAGE'
  function Media() {
    this.resolveYoutubeData = function() {
        if (this.type == 'YOUTUBE') {
            // We need fetch the VIDEO_ID from http(s)://www.youtube.com/embed/VIDEO_ID?query_string
            this.source = this.source.split('?')[0];
            this.youtubeVideoId = this.source.split('embed/')[1];
        }
    };
  }
  
  var url,title;
    
  var d = document,
      h = d.getElementsByTagName('head')[0],
      metas = h.getElementsByTagName('meta'),
      p,
      c,
      parsed = false;
    
  // Open graph
  if (!parsed) {
      for (var i=0; i<metas.length; i++) {
          p = metas[i].getAttribute('property');
          c = metas[i].getAttribute('content');
          if (p && c && p.indexOf('og:') == 0) {
              parsed = true; // Doesn't matter if we fetch the data or not, we've parsed in og-way
              
              p = p.substring(3);
              if (p == 'url')
                  url = c;
              else if (p == 'title')
                  title = c;
          }
      }
  }
  
  // Twitter cards
  if (!parsed) {
      for (var i=0; i<metas.length; i++) {
          p = metas[i].getAttribute('name');
          c = metas[i].getAttribute('content');
          if (p && c && p.indexOf('twitter:') == 0) {
              parsed = true;
              
              p = p.substring(8);
              if (p == 'title')
                  title = c;
          }
      }
  }
  
  // Other, i.e. Google+
  if (!parsed) {
  }
  
  // Fallback
  if (!url) {url = document.URL;}
  if (!title) {title = document.title;}
//  if (!description) {
//      for (var i=0; i<metas.length; i++) {
//          p = metas[i].getAttribute('name');
//          c = metas[i].getAttribute('content');
//          if (p && c && p == 'description') {
//              description = c;
//              break;
//          }
//      }
//  }
  
  function fetchMedias() {
      var medias=[], host=window.location.host;  

      // Special handling for certain host and URL
      if (host.indexOf("youtube.com") > -1) {
          // Fetch youtube ID from url in format of youtube.com/watch?v=C9qIPnnOUHY
          var params = getQueryParamFromUrl();
          if (params['v']) {
              var media = new Media();
              media.source = document.URL;
              media.type = 'YOUTUBE';
              media.youtubeVideoId = params['v'];
              medias.push(media);
              return medias;
          }
      }
      // Other host goes here

      var imgs=document.images,src,wid,hgh,nwid,nhgh,img;

      for (var j=0; j<imgs.length; j++) {
        img = imgs[j];    

        src = img.src;
        wid = img.clientWidth;
        hgh = img.clientHeight;
        nwid = img.naturalWidth;
        nhgh = img.naturalHeight;

        if (img.style.visibility == 'hidden' || img.style.display == 'none')
            continue;
  
        if (wid <= 80 && hgh <= 80)
            continue;    

        if (nwid <= 150 || nhgh <= 150)
            continue;
  
        // Sometimes the image src is base64 encoded raw data instead of a link (i.e. google image)
        if (src.indexOf('data:', 0) == 0) //startsWith
            continue;
  
//        if (src.indexOf('.gif', src.length - 4) != -1)
//            continue;
        if (src.indexOf('.bmp', src.length - 4) != -1)
            continue;
        if (src.indexOf('.tif', src.length - 4) != -1)
            continue;
        // Other unsupported file goes here...
      
        var media = new Media();
        media.source = src;
        media.type = 'IMAGE';
        if (src.indexOf('.gif', src.length - 4)!=-1) {
            media.type = 'GIF';
        }
        medias.push(media);
      }  

      var frames = d.getElementsByTagName('iframe'),frm;
      for (var j=0; j<frames.length; j++) {
        frm = frames[j];    

        if (frm.style.visibility == 'hidden' || frm.style.display == 'none')
            continue;    

        if (frm.src.indexOf('www.youtube.com/embed/') > -1) {
            var media = new Media();
            media.source = frm.src;
            media.type = 'YOUTUBE';
            media.resolveYoutubeData();
            medias.push(media);
        }
      }
      return medias;
  }
  
  var medias = fetchMedias();
  medias.sort(function(a,b){
      if (a.type == 'YOUTUBE') return -1;
      if (b.type == 'YOUTUBE') return 1;
      return 0;
  });
//  medias.sort(function(a,b){
//    if (a.ele && b.ele) {
//      return getElementTopLeftOffset(a.ele).top - getElementTopLeftOffset(b.ele).top;
//    }
//    // If we can't find the backed element, the Media could be a special one we created, give it lower index
//    if (!a.ele)
//      return -1;
//    if (!b.ele)
//      return 1;
//    return 0;
//  });
  
  // Result
  if (title) title = title.trim();
  var result = {
      title: title,
      url: url,
      medias: medias
  };
  return JSON.stringify(result);
})();