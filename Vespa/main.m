//
//  main.m
//  Vespa
//
//  Created by Jiayu Zhang on 10/25/14.
//  Copyright (c) 2014 Colony. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VPAppDelegate class]));
    }
}
